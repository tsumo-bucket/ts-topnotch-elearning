<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Page extends BaseModel
{
    
    protected $fillable = [
    	'name',
        'slug',
        'published',
        'page_category_id',
        'content',
        /* 'allow_add_contents',
        'order', */
    	];
    
    public function pageCategory()
    {
        return $this->belongsTo('App\PageCategory');
    }

    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function pageContent() {
        return $this->hasMany('App\PageContent', 'page_id')->where('slug', '!=', 'template-default')->orderBy('order');
    }
    public function content() {
        return $this->hasMany('App\PageContent', 'page_id')->orderBy('order');
	}

	public function publishedContent() {
        return $this->hasMany('App\PageContent', 'page_id')->with('bgImage')->where('published', 'published');
    }

    public function clientContents(){
        return $this->hasMany('App\PageContent', 'page_id')->where('slug', '!=', 'template-default')->where('enabled','1')->orderBy('order');
    }

    public function batches()
    {
        return $this->hasMany('App\BatchPage', 'page_id');
    }
}

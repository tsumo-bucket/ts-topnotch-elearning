<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Watermark extends Model
{
    
    protected $fillable = [
        'user_id', 'asset_id', 'url'
    ];

    public function asset()
    {  
        return $this->belongsTo('App\Asset', 'asset_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

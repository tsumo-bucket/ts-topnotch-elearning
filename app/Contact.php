<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Contact extends BaseModel
{
    
    protected $fillable = [
    	'name',
        'email',
        'contact_number',
        'address',
        'message',
        'subject',
        'birthdate',
        'order',
        'newsletter',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }
}

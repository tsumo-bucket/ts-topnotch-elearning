<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Announcement extends BaseModel
{
    
    protected $fillable = [
    	'title',
        'description',
        'start_date',
        'end_date',
        'category'
    ];
    
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function batches()
    {
        return $this->hasMany('App\BatchAnnouncement', 'announcement_id');
    }
}

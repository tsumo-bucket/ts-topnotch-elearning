<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Video extends BaseModel
{
    
    protected $fillable = [
    	'name', 'description', 'start', 'end', 'path', 'url'
    ];
    
    public $appends = [
        'vimeo'
    ];

    public function getVimeoAttribute(){
        $vimeo_url = 'https://api.vimeo.com'.$this->url.'?quality=540p';

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $c = curl_init($vimeo_url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($c);

        $video = json_decode($response, true);

        return $video;
    }
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function lectures()
    {
        return $this->hasMany('App\LectureVideo', 'video_id');
    }

}

<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Session extends BaseModel
{
    
    protected $fillable = [
    	'user_id',
        'ip_address',
        'user_agent',
        'payload',
        'last_activity',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }
}

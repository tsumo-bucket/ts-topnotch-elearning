<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchPage extends Model
{
    //

    protected $fillable = [
        'batch_id',
        'page_id'
    ];

    public function batch()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }
}

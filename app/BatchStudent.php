<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class BatchStudent extends BaseModel
{
    
    protected $fillable = [
    	'user_id',

        'batch_id',

        'grades',

        'status',
    	];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function batch(){
        return $this->belongsTo('App\Batch', 'batch_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}

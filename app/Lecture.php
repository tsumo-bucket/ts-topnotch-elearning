<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Lecture extends BaseModel
{
    
    protected $fillable = [
    	'name',

        'description',

        'video_id',

        'status',
        ];
    
    protected $with = ['tags'];
    
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function videos(){
        return $this->hasMany('App\LectureVideo', 'lecture_id');
    }

    public function video(){
        return $this->belongsTo('App\Video', 'video_id');
    }

    public function tags()
    {
        return $this->morphMany('App\Tag', 'taggable');
    }
}

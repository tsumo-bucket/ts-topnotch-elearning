<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchAnnouncement extends Model
{
    //

    protected $fillable = [
        'batch_id',
        'announcement_id'
    ];

    public function batch()
    {
        return $this->belongsTo('App\Batch', 'batch_id');
    }
}

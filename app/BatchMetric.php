<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchMetric extends Model
{
    //
    protected $fillable = [
        'name',
        'slug',
        'batch_id',
        'hide_color_grade',
        'hide_percentile',
    ];

    
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

use App\User;
use App\Batch;

class GradesSample implements FromCollection, WithHeadings
{
    
    public $batch;

    public function __construct($id){
        $this->batch = Batch::find($id);
    }

    public function headings(): array
    {
        $headings = [
            'Student Number', 'Family Name', 'Given Name', 'Raw Score', 'Percentile Rank', 'Color Grade'
        ];

        return $headings;
    }

    public function collection()
    {

        $students = $this->batch->students;

        $data = [];

        foreach ($students as $d) {
            $new_data = [
                $d->user->student_number, $d->user->last_name, $d->user->first_name, '0', '0', 'GREEN'
            ];
            $data[] = $new_data;
        }

        return collect($data);
    }
}

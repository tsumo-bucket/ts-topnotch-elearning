<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use DB;

class StudentsExport implements FromCollection
{    
    use Exportable;

    protected $batch_id;

    public function __construct($batch_id)
    {
        $this->batch_id = $batch_id;
    }

    public function collection()
    {
        $students = DB::table('batch_students')
                        ->select([
                            'users.student_number',
                            'users.last_name',
                            'users.first_name',
                            'users.email',
                            'users.contact_no',
                            'users.address',
                            'users.school',
                            'users.birthdate'
                        ])
                        ->where('batch_students.batch_id', '=', $this->batch_id)
                        ->leftJoin('users', 'users.id', '=', 'batch_students.user_id')
                        ->orderBy('users.student_number', 'asc')
                        ->get()
                        ->toArray();
        
        $headers = ['Student Number', 'Family Name', 'Given Name', 'Email', 'Contact Number', 'Address', 'School', 'Birthdate'];

        array_unshift($students, $headers);

        return collect($students);
    }
}

<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class Batch extends BaseModel
{
    
    protected $fillable = [
    	'name',
        'boards_date',
        'status',
    	];
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function assetGroups()
    {
        return $this->morphMany('App\AssetGroup', 'reference');
    }

    public function students(){
        return $this->hasMany('App\BatchStudent');
    }

    public function lectures(){
        return $this->hasMany('App\BatchLecture');
    }

    public function metrics()
    {
        return $this->hasMany('App\BatchMetric', 'batch_id');
    }
}

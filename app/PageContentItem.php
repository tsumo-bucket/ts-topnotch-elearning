<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class PageContentItem extends BaseModel
{
    
    protected $fillable = [
    	'content_id',

        'title',

        'description',

        'image',

        'bg_image',

        'type',

        'published',

        'order',

        'name',

        'slug',

        'enabled',

        'editable',
    	];
    

 	public function asset()
	{
		return $this->hasOne('App\Asset', 'id', 'image');
	}
    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function controls()
    {
        return $this->morphMany('App\PageControl', 'reference')->orderBy('order');
    }

    public function contentGroup() {
        return $this->belongsTo('App\PageContent', 'content_id');
    }
    public function bgImage() {
        return $this->hasOne('App\Asset', 'id', 'bg_image');
    }
}

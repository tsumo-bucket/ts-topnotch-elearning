<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class BatchLecture extends BaseModel
{
    
    protected $fillable = [
    	'batch_id',

        'lecture_id',

        'start_date',

        'end_date',

        'status',
    	];
    
    protected $with = ['lecture'];

    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function lecture(){
        return $this->belongsTo('App\Lecture', 'lecture_id');
    }
}

<?php

namespace App;

use App\Acme\Model\BaseModel;
use Illuminate\Database\Eloquent\Model;

class LectureVideo extends BaseModel
{
    
    protected $fillable = [

        'lecture_id','video_id'
        
    ];

    
    public function seo()
    {
        return $this->morphMany('App\Seo', 'seoable');
    }
    
    public function activities()
    {
        return $this->morphMany('App\Activity', 'loggable');
    }

    public function lecture(){
        return $this->belongsTo('App\Lecture', 'lecture_id');
    }

    public function video(){
        return $this->belongsTo('App\Video', 'video_id');
    }
}

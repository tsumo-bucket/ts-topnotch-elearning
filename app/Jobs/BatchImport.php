<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use App\Batch;
use App\BatchStudent;
use App\User;

class BatchImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $csv;
    public $batch_id;
    

    public function __construct($csv, $batch_id)
    {
        $this->csv = $csv;
        $this->batch_id = $batch_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        DB::connection()->disableQueryLog();

        $csv = $this->csv;
        $batch_id = $this->batch_id;


        $student_numbers = [];

        $all_student_numbers = User::pluck('student_number')->toArray();
        $ongoing_batches = Batch::where('status', '!=', 'completed')->get();

        $ongoing_batch_emails = User::where('status', '!=', 'completed')->pluck('email')->toArray();

        if($all_student_numbers != null){
            foreach ($all_student_numbers as $stud_no) {
                $student_numbers[] = $stud_no;
            }
        }

        $batch_students = User::whereHas('batch', function($q) use ($batch_id){
            $q->where('batch_id', $batch_id);
        })->get();

        if($batch_students != null){
            foreach ($batch_students as $key => $value) {
                $emails[] = $value->email;
            }
        }

        $students = [];
        $errors = false;
        $emails = [];
        $response = [];
        $response['notifMessage'] = '';

        foreach ($csv as $count => $row) {
            if($count > 0 ){

                $student_number = '';

                if(strlen($row[0]) < 5){
                    $zeros = '';
                    for ($i=5; $i > strlen($row[0]); $i--){
                        $zeros .= '0';
                    }
                    $student_number = date('Y').$zeros.$row[0];
                }
                else{
                    $student_number = date('Y').$row[0];
                }

                if(!isset($row[0]) && !isset($row[1]) && !isset($row[2]) && !isset($row[3])){
                    continue;
                }

                if(!isset($row[0])){
                    $errors = true;
                }
                if(!isset($row[1])){
                    $errors = true;
                }
                if(!isset($row[2])){
                    $errors = true;
                }
                if(!isset($row[3])){
                    $errors = true;
                }
                if(in_array($row[3], $emails)){
                    $errors = true;
                }
                if(in_array($row[3], $ongoing_batch_emails)){
                    $errors = true;
                }
                if(in_array($row[0], $student_numbers)){
                    $errors = true;
                }

                if($errors == true){
                    /* $response = [
                        'notifMessage'=>$response['notifMessage'],
                        'notifStatus'=>'error'
                    ];
                    return response()->json($response); */
                    continue;
                }


                $emails[] = $row[3];
                array_push($student_numbers, $student_number);

                $new_data = array(
                    'student_number'=> $student_number,
                    'last_name'=> $row[1],
                    'first_name'=> $row[2],
                    'name' => $row[2].' '.$row[1],
                    'email' => $row[3],
                    'password' => bcrypt(
                        strtolower(str_replace(' ','',$row[1].$student_number))
                    ),
                    'contact_no' => $row[4],
                    'address' => $row[5],
                    'school' => $row[6],
                    'birthdate' => date('Y-m-d', strtotime($row[7])),
                    'type' => 'normal',
                    'cms' => 0,
                    'verified' => 0,
                    'status' => 'blocked'
                );
                $students[] = $new_data;
            }
        }

        if($students != null){
            $users = DB::table('users')->insert($students);

            $batch_students = [];

            foreach ($students as $student) {
                $user = User::where('student_number', $student['student_number'])->first();

                $batch_students[] = array(
                    'batch_id' => $batch_id,
                    'user_id' => $user->id
                );
            }
            DB::table('batch_students')->insert($batch_students);

            // $response = [
            //     'notifStatus'=>'success',
            //     'notifTitle'=>'Importing may take a while.',
            //     'notifMessage'=>'Please come back later.',
            //     'resetForm'=>true,
            //     'redirect'=>route('adminBatchStudents', $input['batch_id'])
            // ];
        }
        return $response;
    }
}

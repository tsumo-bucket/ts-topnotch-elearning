<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Video;

class SetVideoSettings implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;
    public $videos;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($videos)
    {
        $this->videos = $videos;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $domains = ['demofort.com', 'homestudy.topnotchboardprep.com.ph', 'homestudybeta.topnotchboardprep.com.ph'];

        $videos = $this->videos;

        foreach ($videos as $video) {

            $headers = array(
                'Content-Type: application/json',
                'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
            );
    
            $body = array(
                'embed' => array(
                    'buttons' => array(
                        'embed' => false,
                        'hd' => true,
                        'like' => false,
                        'share' => false,
                        'watchlater' => false,
                    ),
                    'title' => array(
                        'name' => 'hide',
                        'owner' => 'hide',
                        'portrait' => 'hide'
                    ),
                    'logos' => array(
                        'vimeo' => false
                    )
                ),
                'privacy' => array(
                    'comments' => 'nobody',
                    'download' => false,
                    'embed' => 'whitelist',
                    'view' => 'disable'
                )
            );
    
            $c = curl_init('https://api.vimeo.com'.$video->url);
            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PATCH');
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
            curl_exec($c);


            foreach ($domains as $domain) {
                $headers = array(
                    'Content-Type: application/json',
                    'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
                );
        
                $c = curl_init('https://api.vimeo.com'.$video->url.'/privacy/domains/'.$domain);
                curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                curl_exec($c);
            }
        }
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use App\Batch;
use App\BatchStudent;
use App\User;

class BatchTransfer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $csv;
    public $batch_id;
    

    public function __construct($csv, $batch_id)
    {
        $this->csv = $csv;
        $this->batch_id = $batch_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        DB::connection()->disableQueryLog();

        $csv = $this->csv;
        $batch_id = $this->batch_id;

        $student_numbers = [];
        $emails = [];
        $errors = false;
        $emails = [];
        $response = [];
        $response['notifMessage'] = '';

        foreach ($csv as $count => $row) {
            if($count > 0 ){
                if(!isset($row[0])){
                    continue;
                }
                $student_number = '';
                if (is_numeric($row[0])) {
                    if(strlen($row[0]) < 5){
                        $zeros = '';
                        for ($i=5; $i > strlen($row[0]); $i--){
                            $zeros .= '0';
                        }
                        $student_number = date('Y').$zeros.$row[0];
                    }
                    else if (strlen($row[0]) < 9) {
                        $student_number = date('Y').$row[0];
                    } else {
                        $student_number = $row[0];
                    }
                    array_push($student_numbers, $student_number);
                } else {
                    array_push($emails, $row[0]);
                }
            }
        }

        if ($student_numbers || $emails) {
            $records = DB::table('batch_students')
            ->join('users', 'users.id', '=', 'batch_students.user_id')
            ->whereIn('users.email', $emails)
            ->orWhereIn('users.student_number', $student_numbers)
            ->update(['batch_id' => $batch_id]);
        }
        return $response;
    }
}

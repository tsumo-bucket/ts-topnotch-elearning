<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Acme\Facades\Option;

use App\Batch;
use App\BatchStudent;
use App\User;

use SumoMail;
use Carbon;

class SendEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 300;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $users = User::whereHas('batch', function($q){
                    $q->whereHas('batch', function($que){
                        $que->where('status', 'ongoing');
                    });
                })->where('email_verified_at', null)->take(20)->get();

        if(count($users) > 0){
            foreach ($users as $user) {
                $this->send_welcome_email($user);
            }
        }
    }

    public function send_welcome_email($user) {
        $options = Option::email();
        $params=[];
        $params['from_email'] = $options['sender-email'];
        $params['from_name'] =$options['sender-name'];
        $params['subject'] = $options['name'] . ': User account created';
        $params['to'] = $user->email;
        $params['replyTo'] =  $options['receiver-email'];
        $params['debug'] = false; 

        $data = [];
        $data['user'] = $user;
        $data['password'] = strtolower(str_replace(' ','',$user->last_name.$user->student_number));
        $data['options'] = $options;

        $sent = SumoMail::send('emails.student-create', $data, $params);

        if($sent === null){
            $user->status = 'active'; // default to active if email is being sent
            $user->is_active =  true;
            $user->email_verified_at = Carbon::now();
            $user->save();
        }
    }
}

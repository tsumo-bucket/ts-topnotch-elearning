<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

// use Jobs\SyncVideos;

class TriggerVideoSync implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $page;
    public $page_size;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($page = 1, $page_size = 100)
    {
        $this->page = $page;
        $this->page_size = $page_size;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $c = curl_init('https://api.vimeo.com/users/'.env('VIMEO_USER_ID').'/videos?page='.$this->page.'&per_page='.$this->page_size);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        
        $vimeo = curl_exec($c);

        $data = json_decode($vimeo, true);

        SyncVideos::dispatch($data['data']);

        if (!empty($data['paging']['next'])) {
            TriggerVideoSync::dispatch($this->page + 1, $this->page_size);
        }
        
        // CHECK IF EXISTING VIDEOS STILL EXIST IN VIMEO
        // This should be a scheduled task instead
        // $all_videos = Video::all();
        // if(count($all_videos) > 0){
        //     foreach ($all_videos as $v) {
        //         $c = curl_init('https://api.vimeo.com'.$v->url);
        //         curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        //         curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        //         curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
                
        //         $video = curl_exec($c);

        //         $video_arr = json_decode($video, true);

        //         if(isset($video_arr['error'])){
        //             $v->delete();
        //         }
        //     }
        // }
        
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Illuminate\Support\Facades\DB;

use App\User;
use App\BatchStudent;

class GradesImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public $batch_id;
    public $csv;
    public $period;
    public $all_student_numbers;
    
    public $timeout = 1800;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($csv, $batch_id, $period, $all_student_numbers)
    {
        $this->csv = $csv;
        $this->batch_id = $batch_id;
        $this->period = $period;
        $this->all_student_numbers = $all_student_numbers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        DB::connection()->disableQueryLog();

        $csv = $this->csv;
        $batch_id = $this->batch_id;
        $period = $this->period;
        $all_student_numbers = $this->all_student_numbers;

        $headings = [
            'Student Number', 'Family Name', 'Given Name', 'raw_score', 'percentile_rank', 'color_grade'
        ];

        $grades = [];
        $errors = false;
        $student_numbers = [];
        $headers = [];

        foreach ($csv as $count => $row) {

            if($count > 0){

                if(!isset($row[0])){
                    $errors = true;
                }

                if(!isset($row[1])){
                    $errors = true;
                }

                if(!isset($row[2])){
                    $errors = true;
                }

                /* if(!isset($row[3])){
                    if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
                    $response['notifMessage'] .= 'Row #'.($count+1).' - Raw Score is required.';
                    $errors = true;
                }

                if(!isset($row[4])){
                    if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
                    $response['notifMessage'] .= 'Row #'.($count+1).' - Percentile Rank is required.';
                    $errors = true;
                } */

                if(!isset($row[5])){
                    $errors = true;
                }

                if(!in_array($row[0], $all_student_numbers)){
                    $errors = true;
                }

                if(in_array($row[0], $student_numbers)){
                    $errors = true;
                }

                if($errors == true){
                    continue;
                }
                else{
                    $student_numbers[] = $row[0];

                    $values = [];
                    $values_arr = [];
                    
                    foreach ($row as $key => $value) {
                        if($key > 2){
                            $values[$headings[$key]] = $value;
                        }
                    }

                    $new_data = array(
                        'student_number' => $row[0],
                        'grades' => $values,
                        'period' => $period
                    );

                    $grades[] = $new_data;

                }

            }

        }

        if($grades != null){
            foreach ($grades as $g) {
                $user = DB::table('users')->where('student_number',$g['student_number'])->first();
                $student = DB::table('batch_students')->where('user_id', $user->id)->first();
                
                if($student->grades != null){
                    
                    $student_grades = json_decode($student->grades, true);
                    if(array_key_exists($period, $student_grades)){
                        $student_grades[$period] = $g['grades'];
                    }

                    $student_grades[$g['period']] = $g['grades'];

                    DB::table('batch_students')->where('id', $student->id)->update(['grades'=>json_encode($student_grades)]);
                }
                else{
                    $new_grade = array(
                        $g['period'] => $g['grades']
                    );

                    DB::table('batch_students')->where('id', $student->id)->update(['grades'=>json_encode($new_grade)]);
                }
            }
        }
    }
}

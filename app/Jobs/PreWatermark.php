<?php

namespace App\Jobs;

use App\Acme\PDFwatermark\PDFWatermark;
use App\Acme\PDFwatermarker\PDFWatermarker;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

use App\Asset;
use App\User;
use App\Watermark;

use Image;

class PreWatermark implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $asset_id;
    
    public $user_id;
    
    private $s3_active = NULL;

    public $timeout = 600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $asset_id)
    {
        $this->asset_id = $asset_id;
        $this->user_id = $user_id;
        $s3_active = strtolower(getenv('S3_ACTIVE'));
        $this->s3_active = ($s3_active == "true");
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $s3 = Storage::disk('watermark_s3');

        $user = User::findOrFail($this->user_id);
        $asset = Asset::find($this->asset_id);

        $hash = md5($user->id.$asset->id.time());

        $existing_watermarked_file = Watermark::where(function($q) use($user, $asset){
            $q->where('user_id', $user->id);
            $q->where('asset_id', $asset->id);
        })->first();

        if($existing_watermarked_file != null){
            return;
        }

        // STUDENT WATERMARK
        $path = $this->create_users_name_image($user);
        
        $watermark = new PDFWatermark($path);
        unlink($path);
        
        $watermark->setPosition('center');
        
        $path = public_path('/upload/assets/temp'.$hash.'.pdf');
        
        if($this->s3_active){
            file_put_contents($path, fopen($asset->path, 'r'));
            $watermarker = new PDFWatermarker($path,public_path('/upload/assets/temporary_pdf'.$hash.'.pdf'),$watermark); 
        }
        else{
            $watermarker = new PDFWatermarker(public_path($asset->path),public_path('/upload/assets/temporary_pdf'.$hash.'.pdf'),$watermark); 
        }

        $watermarker->setPageRange(1);
        $watermarker->savePdf();
        
        if(file_exists($path)){
            unlink($path);
        }

        $filename = $hash.'.pdf';

        $file_path = $this->asset_id.'/'.$this->user_id.'/'.$filename;

        $s3->put($file_path, file_get_contents(public_path('/upload/assets/temporary_pdf'.$hash.'.pdf')), 'public');
        
        if(file_exists(public_path('/upload/assets/temporary_pdf'.$hash.'.pdf'))){
            unlink(public_path('/upload/assets/temporary_pdf'.$hash.'.pdf'));
        }

        $new_data = [
            'user_id' => $user->id,
            'asset_id' => $asset->id,
            'url' => $file_path
        ];

        Watermark::create($new_data);
    }
    
    public function create_users_name_image($user)
    {
        $img = Image::make(public_path('upload/users_name/template.png'));

        $string = implode(' ', str_split(strtoupper($this->stripAccents($user->name).' '.$user->student_number)));
        if(strlen($user->name) > 21){
          $string = implode(' ', str_split(strtoupper(substr($this->stripAccents($user->first_name), 0, 1).'. '.$this->stripAccents($user->last_name).' '.$user->student_number)));
        }

        $opacity = floatval(getenv('WATERMARK_OPACITY'));
        if ($opacity == 0.0) {
            $opacity = 0.2;
        }
        $img->text($string, 620, 850, function($font) use ($opacity) {
            $font->file(public_path('fonts/ClearSans-Bold.ttf'));
            $font->size(45);
            $font->color(array(0, 0, 0, $opacity));
            $font->align('center');
            $font->valign('top');
            $font->angle(55);
        });

        $img->save(public_path('upload/users_name/'.$user->student_number.'.png'));
        return public_path('upload/users_name/'.$user->student_number.'.png');
    }

    public function stripAccents($str) {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use App\Video;

class SyncVideos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        
        $domains = ['demofort.com', 'homestudy.topnotchboardprep.com.ph', 'homestudybeta.topnotchboardprep.com.ph'];
        
        $videos = [];

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        foreach ($data as $value) {
            
            $exists = Video::where('url', $value['uri'])->first();

            if($exists == null){

                $videos[] = array(
                    'name' => $value['name'],
                    'description' => $value['description'],
                    'url' => $value['uri'],
                    'path' => $value['pictures']['sizes'][4]['link_with_play_button']
                );
            }  else {
                // if we need to update the name and description, do update
                if ($exists->name !== $value['name'] || $exists->description !== $value['description']){
                    $exists->name = $value['name'];
                    $exists->description = $value['description'];
                    $exists->path = $value['pictures']['sizes'][4]['link_with_play_button'];
                    $exists->save();
                }

            } 
        };

        if(count($videos) > 0){
            DB::table('videos')->insert($videos);

            foreach ($videos as $video) {
                
                // SET VIDEO DOMAINS
                foreach ($domains as $domain) {
            
                    $c = curl_init('https://api.vimeo.com'.$video['url'].'/privacy/domains/'.$domain);
                    curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                    curl_exec($c);
                }

                // SET VIDEO SETTINGS
        
                $body = array(
                    'embed' => array(
                        'buttons' => array(
                            'embed' => false,
                            'hd' => true,
                            'like' => false,
                            'share' => false,
                            'watchlater' => false,
                        ),
                        'title' => array(
                            'name' => 'hide',
                            'owner' => 'hide',
                            'portrait' => 'hide'
                        ),
                        'logos' => array(
                            'vimeo' => false
                        )
                    ),
                    'privacy' => array(
                        'comments' => 'nobody',
                        'download' => false,
                        'embed' => 'whitelist',
                        'view' => 'disable'
                    )
                );
        
                $c = curl_init('https://api.vimeo.com'.$video['url']);
                curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
                $status = curl_exec($c);
            }
        }

        // CHECK IF EXISTING VIDEOS STILL EXIST IN VIMEO
        // This should be a scheduled task instead
        // $all_videos = Video::all();
        // if(count($all_videos) > 0){
        //     foreach ($all_videos as $v) {
        //         $c = curl_init('https://api.vimeo.com'.$v->url);
        //         curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        //         curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        //         curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
                
        //         $video = curl_exec($c);

        //         $video_arr = json_decode($video, true);

        //         if(isset($video_arr['error'])){
        //             $v->delete();
        //         }
        //     }
        // }
    }
}

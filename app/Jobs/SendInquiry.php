<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

use App\Acme\Facades\Option;

use App\User;

use SumoMail;

class SendInquiry implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $chunk;

    protected $input;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($chunk, $input)
    {
        $this->chunk = $chunk;
        $this->input = $input;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach($this->chunk as $c){

            $user = DB::table('users')->find($c['id']);

            $options = Option::email();
    
            $params=[];
            $params['from_email'] = env('MAIL_FROM_EMAIL');
            $params['from_name'] = $this->input['from_name'] != '' ? $this->input['from_name'] : env('MAIL_FROM_NAME');
            $params['subject'] = '['.$user->name.'] '.$this->input['subject'];
            $params['to'] = $user->email;
            $params['replyTo'] =  env('MAIL_REPLY_TO');
            $params['debug'] = false; 
    
            $data = [];
            $data['content'] = $this->input['message'];
    
            $sent = SumoMail::send('emails.inquiry', $data, $params);
        }
        
    }
}

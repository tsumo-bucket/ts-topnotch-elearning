<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PageTemplateItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reference_id' => 'required|integer',
            'reference_type' => 'required|max:255',
            'name' => 'required|max:255',
            'label' => 'required|max:255',
            'type' => 'required|in:text,number,checkbox,textarea,asset,select,color,date,time,date_time,products,tags',
            'options_json' => 'required',
            'required' => 'required',
            'order' => 'required|integer',
            'value' => 'required',
        ];
    }
}

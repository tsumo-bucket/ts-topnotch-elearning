<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use App\User;

use Closure;

class PortalMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::check()) {
            return redirect()->guest(route('login'))
                ->with('notifStatus','error')
                ->with('notifMessage', 'Unauthorized Access');
        }

        if(Auth::check() && Auth::user()->type != 'normal'){
            return redirect()->route('adminDashboard')
                ->with('errorToken','Unauthorized Access');
        }

        if(Auth::check()){
            $user = User::find(Auth::user()->id);

            if($user->status != 'active'){
                return redirect()->route('logout');
            }
        }

        return $next($request);
    }
}

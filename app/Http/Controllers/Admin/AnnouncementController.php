<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AnnouncementRequest;

use Acme\Facades\Activity;

use App\Announcement;
use App\Batch;
use App\BatchAnnouncement;
use App\Seo;

use Carbon;
use View;

class AnnouncementController extends Controller
{
    public $category, $title_text, $breadcrumb, $menu;

    public function __construct(Request $request)
    {
        $this->middleware('admin');
    }

    public function get_category_values($category = null)
    {
        if ($category != null) {
            session()->put('category', $category);
        }

        $this->category = session()->get('category') == 'announcements' ? 'announcement' : 'banner';
        $this->title_text = $this->category == 'announcement' ? 'Announcement' : 'Banner Text';
        $this->breadcrumb = $this->title_text . 's';
        $this->menu = $this->category == 'announcement' ? 'announcements' : 'banner-texts';

        $category_data = [
            'category' => $this->category,
            'title_text' => $this->title_text,
            'breadcrumb' => $this->breadcrumb,
            'menu' => $this->menu
        ];

        return $category_data;
    }
    
    public function index(Request $request)
    {
        $input = $request->all();

        $category_data = $this->get_category_values(@$request->category);

        $data = Announcement::where('category', $this->category);

        if (isset($input['title']) && $input['title']) {
            $data = $data->where('title', 'LIKE', '%' . $input['title'] . '%');
        }

        if (isset($input['batch']) && $input['batch']) {
            $data = $data->whereHas('batches', function($q) use($input) {
                        $q->whereHas('batch', function($q2) use($input) {
                            $q2->where('name', 'LIKE', '%'.$input['batch'].'%');
                        });
                    });
        }

        $data = $data->orderBy('created_at', 'DESC')
                    ->paginate(25);

        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/announcements/index')
            ->with($category_data)
            ->with('filters', $input)
            ->with('title', $this->title_text)
            ->with('keyword', $request->title)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create(Request $request)
    {
        $all_batches = Batch::where('status', 'ongoing')->pluck('name', 'id');
        $category_data = $this->get_category_values();

        return view('admin/announcements/create')
            ->with($category_data)
            ->with('title', 'Create ' . $this->title_text)
            ->with('all_batches', $all_batches);
    }

    public function get_batches(Request $request)
    {
        $data = Batch::pluck('name', 'id')->get();

        if (! is_null($data)) {
            $response['data'] = $data;
            $response['message'] = 'Successfully retrieved data.';
            $status = 200;
        } else {
            $response['message'] = 'Failed to retrieve data.';
            $status = 404;
        }

        return response($response, $status);
    }
    
    public function store(AnnouncementRequest $request)
    {
        $input = $request->all();

        $category_data = $this->get_category_values();

        $input['category'] = $this->category;

        if (@$input['start_date']) {
            $input['start_date'] = Carbon::parse(@$input['start_date']);
        }

        if (@$input['end_date']) {
            $input['end_date'] = Carbon::parse(@$input['end_date']);

            if (@$input['start_date']) {

                if (@$input['start_date']->gte(@$input['end_date'])) {
    
                    return response()->json([
                        'notifStatus' => 'error',
                        'notifTitle' => 'Error saving',
                        'notifMessage' => 'Start date must be set before the end date'
                    ]);
    
                }
            }
        }

        $announcement = Announcement::create($input);
        
        // $log = 'creates a new announcement "' . $announcement->name . '"';
        // Activity::create($log);

        if (!empty($input['batches'])) {

            $batches = [];
            foreach ($input['batches'] as $batch) {

                $temp = new BatchAnnouncement;
                $temp->announcement_id = $announcement->id;
                $temp->batch_id = $batch;

                $batches[] = $temp;
            }
            
            $announcement->batches()->saveMany($batches);
        }


        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminAnnouncementsEdit', [$announcement->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/announcements/show')
            ->with('title', 'Show ' . $this->title_text)
            ->with('data', Announcement::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/announcements/view')
            ->with('title', 'View ' . $this->title_text)
            ->with('data', Announcement::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Announcement::findOrFail($id);

        $category_data = $this->get_category_values();
        $all_batches = Batch::where('status', 'ongoing')->pluck('name', 'id');
        $batch_announcements = BatchAnnouncement::where('announcement_id', $id)->pluck('batch_id');
        $data->branches = Batch::whereIn('id', $batch_announcements)->pluck('id');

        return view('admin/announcements/edit')
            ->with($category_data)
            ->with('title', 'Edit ' . $this->title_text)
            ->with('all_batches', $all_batches)
            ->with('data', $data);
    }
    
    public function update(AnnouncementRequest $request, $id)
    {
        $input = $request->all();
        $announcement = Announcement::findOrFail($id);

        if (@$input['start_date']) {
            $input['start_date'] = Carbon::parse(@$input['start_date']);
        }

        if (@$input['end_date']) {
            $input['end_date'] = Carbon::parse(@$input['end_date']);

            if (@$input['start_date']) {

                if (@$input['start_date']->gte(@$input['end_date'])) {
    
                    return response()->json([
                        'notifStatus' => 'error',
                        'notifTitle' => 'Error saving',
                        'notifMessage' => 'Start date must be set before the end date'
                    ]);
    
                }
            }
        }
        
        $announcement->update($input);

        $announcement->batches()->delete();
        if (!empty($input['batches'])) {

            $batches = [];
            foreach ($input['batches'] as $batch) {

                $temp = new BatchAnnouncement;
                $temp->announcement_id = $announcement->id;
                $temp->batch_id = $batch;

                $batches[] = $temp;
            }
            
            $announcement->batches()->saveMany($batches);
        }

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Announcement::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->title;
        }
        // $log = 'deletes a new announcement "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Announcement::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminAnnouncements')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/announcements', array('as'=>'adminAnnouncements','uses'=>'Admin\AnnouncementController@index'));
Route::get('admin/announcements/create', array('as'=>'adminAnnouncementsCreate','uses'=>'Admin\AnnouncementController@create'));
Route::post('admin/announcements/', array('as'=>'adminAnnouncementsStore','uses'=>'Admin\AnnouncementController@store'));
Route::get('admin/announcements/{id}/show', array('as'=>'adminAnnouncementsShow','uses'=>'Admin\AnnouncementController@show'));
Route::get('admin/announcements/{id}/view', array('as'=>'adminAnnouncementsView','uses'=>'Admin\AnnouncementController@view'));
Route::get('admin/announcements/{id}/edit', array('as'=>'adminAnnouncementsEdit','uses'=>'Admin\AnnouncementController@edit'));
Route::patch('admin/announcements/{id}', array('as'=>'adminAnnouncementsUpdate','uses'=>'Admin\AnnouncementController@update'));
Route::post('admin/announcements/seo', array('as'=>'adminAnnouncementsSeo','uses'=>'Admin\AnnouncementController@seo'));
Route::delete('admin/announcements/destroy', array('as'=>'adminAnnouncementsDestroy','uses'=>'Admin\AnnouncementController@destroy'));
*/
}

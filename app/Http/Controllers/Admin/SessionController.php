<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SessionRequest;

use Acme\Facades\Activity;

use App\Session;
use App\Seo;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($user_id = $request->user_id) {
            $data = Session::where('user_id', 'LIKE', '%' . $user_id . '%')->paginate(25);
        } else {
            $data = Session::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/sessions/index')
            ->with('title', 'Sessions')
            ->with('menu', 'sessions')
            ->with('keyword', $request->user_id)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/sessions/create')
            ->with('title', 'Create session')
            ->with('menu', 'sessions');
    }
    
    public function store(SessionRequest $request)
    {
        $input = $request->all();
        $session = Session::create($input);
        
        // $log = 'creates a new session "' . $session->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminSessionsEdit', [$session->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/sessions/show')
            ->with('title', 'Show session')
            ->with('data', Session::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/sessions/view')
            ->with('title', 'View session')
            ->with('menu', 'sessions')
            ->with('data', Session::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Session::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/sessions/edit')
            ->with('title', 'Edit session')
            ->with('menu', 'sessions')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(SessionRequest $request, $id)
    {
        $input = $request->all();
        $session = Session::findOrFail($id);
        $session->update($input);

        // $log = 'edits a session "' . $session->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Session::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Session::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->user_id;
        }
        // $log = 'deletes a new session "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Session::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminSessions')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/sessions', array('as'=>'adminSessions','uses'=>'Admin\SessionController@index'));
Route::get('admin/sessions/create', array('as'=>'adminSessionsCreate','uses'=>'Admin\SessionController@create'));
Route::post('admin/sessions/', array('as'=>'adminSessionsStore','uses'=>'Admin\SessionController@store'));
Route::get('admin/sessions/{id}/show', array('as'=>'adminSessionsShow','uses'=>'Admin\SessionController@show'));
Route::get('admin/sessions/{id}/view', array('as'=>'adminSessionsView','uses'=>'Admin\SessionController@view'));
Route::get('admin/sessions/{id}/edit', array('as'=>'adminSessionsEdit','uses'=>'Admin\SessionController@edit'));
Route::patch('admin/sessions/{id}', array('as'=>'adminSessionsUpdate','uses'=>'Admin\SessionController@update'));
Route::post('admin/sessions/seo', array('as'=>'adminSessionsSeo','uses'=>'Admin\SessionController@seo'));
Route::delete('admin/sessions/destroy', array('as'=>'adminSessionsDestroy','uses'=>'Admin\SessionController@destroy'));
*/
}

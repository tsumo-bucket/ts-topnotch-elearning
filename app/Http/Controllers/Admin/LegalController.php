<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;
use App\PageContent;
use App\PageContentItem;
use App\PageControl;

use View;

class LegalController extends Controller
{
    
    public $category;

    public function __construct()
    {
        $this->middleware('admin');
        $this->category = PageCategory::where('slug', 'terms-and-conditions')->first();
        View::share('category', $this->category);
    }

    public function index()
    {
        if($this->category == null){
            $this->seed();
        }

        $data = Page::where('page_category_id', $this->category->id)->get();

        return view('admin/terms/index')
            ->with('title', 'Legal Information')
            ->with('menu', 'terms')
            ->with('data', $data);
    }

    public function edit($id)
    {
        $data = Page::findOrFail($id);

        return view('admin/terms/edit')
            ->with('title', ucwords($data->name))
            ->with('menu', 'terms')
            ->with('data', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $page = Page::findOrFail($id);
        $page->update($input);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifStatus'=>'success',
        ];

        return response()->json($response);
    }

    public function seed()
    {
        $this->category = PageCategory::create([
            'name' => 'Terms and Conditions',
            'slug' => 'terms-and-conditions'
        ]);

        $waver_and_agreement = Page::create([
            'name' => 'Waver and Agreement',
            'slug' => 'waver-and-agreement',
            'page_category_id' => $this->category->id,
            'published' => 'published'
        ]);

        $rules_and_regulations = Page::create([
            'name' => 'Rules and Regulations',
            'slug' => 'rules-and-regulations',
            'page_category_id' => $this->category->id,
            'published' => 'published'
        ]);

        /* $waver_and_agreement_content = PageContent::create([
            'page_id' => $waver_and_agreement->id,
            'reference_id' => 0,
            'reference_type' => '',
            'name' => 'Content',
            'slug' => 'waver-and-agreement-content'
        ]);

        PageControl::create([
            'reference_id' => $waver_and_agreement_content->id,
            'reference_type' => 'App\PageContent',
            'name' => 'content',
            'label' => 'Content',
            'type' => 'textarea',
            'order' => 0
        ]);

        $template_default = PageContent::create([
            'page_id' => $rules_and_regulations->id,
            'reference_id' => 0,
            'reference_type' => '',
            'name' => '',
            'slug' => 'template-default'
        ]);

        $rules_and_regulations_sections = PageContent::create([
            'page_id' => $rules_and_regulations->id,
            'reference_id' => 0,
            'reference_type' => '',
            'name' => 'Sections',
            'slug' => 'rules-and-regulations-sections',
            'allow_add_items' => true
        ]);

        $rules_and_regulations_items_default = PageContentItem::create([
            'content_id' => $rules_and_regulations_sections->id,
            'slug' => 'template-default'
        ]);

        PageControl::create([
            'reference_id' => $rules_and_regulations_items_default->id,
            'reference_type' => 'App\PageContentItem',
            'name' => 'title',
            'label' => 'Title',
            'type' => 'text',
            'order' => 0,
        ]);

        PageControl::create([
            'reference_id' => $rules_and_regulations_items_default->id,
            'reference_type' => 'App\PageContentItem',
            'name' => 'content',
            'label' => 'Content',
            'type' => 'textarea',
            'order' => 1,
        ]); */
    }

    public function view(Request $request, $slug)
    {
        $page = Page::with(['seo', 'clientContents.controls'])
                    ->where('slug', $slug);
        
        $page = $page->first();
        // return get_class($page->clientContents[0]);
        if ($page->allow_add_contents !== 1) {
            
            if (count($page->clientContents) == 1) {
                // return $page;
				return view('admin/legal/edit')
					->with('menu', 'legal')
                    ->with('title', $page->name)
                    ->with('cancelRoute', route('adminDashboard'))
                    ->with('page', $page)
                    ->with('seo', '')
                    ->with('withSEO', true)
                    ->with('content', $page->clientContents[0])
                    ->with('data', $page->clientContents[0]);
                    // ->with('data', $page->clientContents[0]);
            } else {
                // return $page;
				return view('admin/legal/edit')
					->with('menu', 'legal')
                    ->with('title', $page->name)
                    ->with('seo', '')
                    ->with('data', $page)
                    ->with('page', $page);
            }
        } else {
            // return $page;
			return view('admin/legal/edit')
					->with('menu', 'legal')
                    ->with('title', $page->name)
                    ->with('seo', '')
                    ->with('data', $page)
                    ->with('page', $page);	
        }
    }

    public function itemAdd($slug, $content)
    {
        $page = Page::where('slug', $slug)->first();
		$content = PageContent::findOrFail($content);
		// return $content;
		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();
			if ($contentTemplate) {
				$data = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
			}
		} else {
			if ($content->allow_add_items == 1) {
				
				$data = PageContentItem::where('content_id', $content->id)->first();
			} else {
				$data = $content;
			}
		}
		
		return view('admin/legal/items/create')
            ->with('title', 'Create Item')
            ->with('menu', 'legal')
            ->with('page', $page)
            ->with('content', $content)
            ->with('data', $data);
    }

    public function itemEdit(Request $request, $slug, $content, $id)
	{
        // dd($slug);
		$page = Page::where('slug', $slug)->first();
		$content = PageContent::findOrFail($content);
		$item = PageContentItem::findOrFail($id);

		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();
			if ($contentTemplate) {
				$data = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
				foreach ($data->controls as $key => $control) {
					$control->value = @$item->controls()->where('name', $control->name)->first()->value;
				}
			} 
		} else {
			if($content->allow_add_items == 1) {
				$data = PageContentItem::where('content_id', $content['id'])->where('slug', 'template-default')->first();
				foreach ($data->controls as $key => $control) {
					$control_value = @$item->controls()->where('name', $control->name)->first();
					$control->value = @$control_value->value;
					$control->products = @$control_value->products;
				}
			}  else {
				$data = $item;
			}
		}
		
		$title = '';

		if($data->editable == 1){
			$title = 'Edit Item';
		}else{
			$title = 'View Item';
		}

		return view('admin/legal/items/edit')
            ->with('title', $title )
            ->with('menu', 'page-'.$slug)
            ->with('page', $page)
            ->with('content', $content)
            ->with('item', $item)
            ->with('data', $data);
	}

    public function itemStore(Request $request, $slug, $content)
    {
        $input = $request->all();
		$page = Page::where('slug', $slug)->first();
		$pageContent = PageContent::findOrFail($content);
		if ($page->allow_add_contents == 1) {
			$contentTemplate = PageContent::where('page_id', $page->id)->where('slug', 'template-default')->first();

			if ($contentTemplate) {
				$template = PageContentItem::where('content_id', $contentTemplate->id)->where('slug', 'template-default')->first();
			} else {
				$response = [
					'notifTitle'=>'Error Saving.',
					'notifMessage'=>'Template for content not found.',
					'notifStatus'=>'error'
				];

				return response()->json($response);
			}
		} else {
			$template = PageContentItem::where('content_id', $content)->where('slug', 'template-default')->first();
		}

		$new = $template->replicate();
		$new->content_id = $content;
		$new->order = count($pageContent->items);
		$new->save();
		$new->slug = uniqid() . $new->id;
		$new->save();
		
		foreach ($template->controls as $key => $control) {
			$newControl = $control->replicate();
			$newControl->reference_id = $new->id;
			$newControl->value = @$input[$control->name];

			if ($newControl->type == "products") {
				$newControl->value = "products";
			}
			
			$newControl->save();

			if ($newControl->type == "products") {

				if (isset($input["products"])) {
					$default_control = $template->controls()->where('name', $control->name)->first();
	
					if (isset($input["products"][$default_control->id])) {
						$newControl->products()->sync($input["products"][$default_control->id]);
					}
				}
			}
		}

		$response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to list.',
            'notifStatus'=>'success',
            'redirect'=>route('adminLegalPage', [$slug])
        ];

        return response()->json($response);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Carbon\Carbon;

use App\Activity;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request)
    {
        if ($log = $request->log) {
            $activities = Activity::where('identifier_value', 'LIKE',  $log . '%')
                            ->with('user')
                            ->latest()
                            ->paginate(25);
        } else {
            $activities = Activity::with('user')
                            ->latest()
                            ->paginate(25);
        }
        $pagination = $activities->appends($request->except('page'))->links();

        return view('admin/activities/index')
            ->with('title', 'Activities')
            ->with('menu', 'activities')
            ->with('keyword', $request->product_id)
            ->with('data', $activities)
            ->with('pagination', $pagination);
    }

    public function view($id)
    {
        $data = Activity::findOrFail($id);

        if (!$this->isJson($data->value_from)) {
            // then force it to a json structure
            $data->value_from = json_encode((object)array('data' => $data->value_from));
            $data->value_to = json_encode((object)array('data' => $data->value_to));
        }

        return view('admin/activities/view')
            ->with('title', 'View activity')
            ->with('menu', 'activities')
            ->with('data', $data );
    }

    private function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BatchRequest;

use App\Acme\Facades\Activity;
use App\Acme\Facades\Asset;
use App\Acme\Facades\Option;

use App\Jobs\PreWatermark;
use App\Jobs\SendEmails;
use App\Jobs\SendInquiry;

use App\Mail\Inquiry;

use App\Batch;
use App\BatchMetric;
use App\BatchLecture;
use App\BatchStudent;
use App\Lecture;

use App\AssetGroup;
use App\Option as EmailOption;
use App\User;
use App\Seo;

use SumoMail;
use Carbon;

class BatchController extends Controller
{
    public $defaults = [
        [
            'name' => 'Diagnostic',
            'slug' => 'diagnostic'
        ],
        [
            'name' => 'Pre-Midterm',
            'slug' => 'premidterm'
        ],
        [
            'name' => 'Midterm',
            'slug' => 'midterm'
        ],
        [
            'name' => 'Pre-Finals',
            'slug' => 'prefinals'
        ],
        [
            'name' => 'Finals',
            'slug' => 'finals'
        ]
    ];

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($name = $request->name) {
            $data = Batch::where('name', 'LIKE', '%' . $name . '%')->paginate(25);
        } else {
            $data = Batch::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/batches/index')
            ->with('title', 'Batches')
            ->with('menu', 'batches')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/batches/create')
            ->with('title', 'Create batch')
            ->with('menu', 'batches');
    }
    
    public function store(BatchRequest $request)
    {
        $input = $request->all();
        $input['status'] = 'draft';
        $input['boards_date'] = @$input['boards_date'] ? Carbon::parse(@$input['boards_date']) : null;
        $batch = Batch::create($input);
        
        // $log = 'creates a new batch "' . $batch->name . '"';
        // Activity::create($log);       

        $defaults = $this->defaults;

        foreach ($defaults as $index => $default) {
            $defaults[$index]['batch_id'] = $batch->id;
            $defaults[$index]['type'] = 'defaults';
        }
        BatchMetric::insert($defaults);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminBatchesEdit', [$batch->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/batches/show')
            ->with('title', 'Show batch')
            ->with('data', Batch::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/batches/view')
            ->with('title', 'View batch')
            ->with('menu', 'batches')
            ->with('data', Batch::findOrFail($id));
    }
    
    public function edit($id)
    {
        /* app('App\Console\Kernel')->command('schedule:run', function(){
            return null;
        }); */
        $data = Batch::findOrFail($id);

        $mainImages = '';
        if ($data->assetGroups()->getResults()->where('name','BatchMainImages')->count() > 0) {

            $mainImages = $data->assetGroups()->getResults()->where('name', 'BatchMainImages')->first();

            $mainImages->assets = $mainImages->assets()
                                            ->select('asset_group_items.*', 'assets.name as name')
                                            ->leftJoin('assets', 'asset_group_items.asset_id', '=', 'assets.id')
                                            ->orderBy('name', 'asc')
                                            ->with('asset')
                                            ->get();
        }

        $batch_lectures = $data->lectures;
        $lecture_ids = [];

        foreach ($batch_lectures as $lecture) {
            $lecture_ids[] = $lecture->lecture_id;
        }
        $lectures = Lecture::whereNotIn('id', $lecture_ids)->get();

        if ($data->metrics->count() < 1) {

            $defaults = $this->defaults;

            foreach ($defaults as $index => $default) {
                $defaults[$index]['batch_id'] = $data->id;
                $defaults[$index]['type'] = 'defaults';
            }

            BatchMetric::insert($defaults);
        }

        return view('admin/batches/edit')
            ->with('title', 'Edit batch')
            ->with('menu', 'batches')
            ->with('mainImages', $mainImages)
            ->with('lectures', $lectures)
            ->with('data', $data);
    }

    public function save_pdf(Request $request){
        $input = $request->all();

        $batch = Batch::findOrFail($input['batch_id']);
        
        $mainImages = '';
        if ($batch->assetGroups()->getResults()->where('name','BatchMainImages')->count() > 0) {

            $mainImages = $batch->assetGroups()->getResults()->where('name', 'BatchMainImages')->first();

            // $mainImages->assets = $mainImages->assets()->with('asset')->get()->toArray();
            $mainImages->assets = $mainImages->assets()->get()->toArray();
        }

        if($input['operation'] == 'add'){
            $new_handouts = [];
            
            foreach(json_decode($input['main_images'], true)['assets'] as $new_handout){
                if(!isset($new_handout['id'])){
                    $new_handouts[] = $new_handout;
                }
            }

            foreach($batch->students as $skey=>$student){
                foreach ($new_handouts as $handout) {
                    PreWatermark::dispatch($student->user->id, $handout['asset_id'])
                    ->delay(now()->addSeconds($skey * 5));
                }
            }
        }

        Asset::sync($batch, $input['main_images']);

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Attachments added.',
        ];

        return response()->json($response);
    }
    
    public function update(BatchRequest $request, $id)
    {

        $input = $request->all();
        $batch = Batch::findOrFail($id);
        $batch_students = BatchStudent::where('batch_id', $batch->id)->get();

        $options = Option::email();

        if($input['status'] == 'draft' && $batch->emails_sent == true){
            $response = [
                'notifStatus' => 'error',
                'notifTitle' => 'Batch is already ongoing'
            ];
            return response()->json($response);
        }

        if($input['status'] == 'ongoing'){
            User::whereHas('batch', function($q) use($batch){
                $q->where('batch_id', $batch->id);
            })->update(['status' => 'active']);
        }

        if($input['status'] == 'completed'){
            User::whereHas('batch', function($q) use($batch){
                $q->where('batch_id', $batch->id);
            })->update(['status' => 'completed']);
        }

        $input['boards_date'] = @$input['boards_date'] ? Carbon::parse(@$input['boards_date']) : null;

        $batch->update($input);

        /* app('App\Console\Kernel')->command('schedule:run', function(){
            return null;
        }); */

        Asset::sync($batch, $input['main_images']);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage' => 'Refreshing page...',
            'redirect' => route('adminBatchesEdit', $batch->id)
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();
 
        foreach ($input['ids'] as $id) {
            
            $batch = Batch::findOrFail($id);
            $batch_students = DB::table('batch_students')->where('batch_id', $id)->pluck('user_id')->toArray();

            if($batch->status == 'ongoing'){
                $response = [
                    'notifStatus' => 'error',
                    'notifTitle' => 'Cannot delete ongoing batches.'
                ];
                return response()->json($response);
            }

            DB::table('users')->whereIn('id', $batch_students)->delete();
            $batch->delete();

        }

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminBatches')
        ];

        return response()->json($response);
    }

    public function batch_email($id)
    {
        $batch = Batch::findOrFail($id);

        /* $student_list = User::whereHas('batch', function($q) use($batch){
            $q->where('batch_id', $batch->id);
        })->get(); */

        return view('admin/batches/email')
            ->with('menu', 'batches')
            ->with('title', 'Batch Email')
            // ->with('student_list', $student_list)
            ->with('batch', $batch);
    }

    public function batch_email_filter(Request $request)
    {
        $input = $request->all();

        $student_list = User::whereHas('batch', function($q) use($input){
            $q->where('batch_id', $input['batch_id']);
        })->where($input['query_type'], 'LIKE', '%'.$input['keyword'].'%')->paginate(500);

        $response = [
            'items' => $student_list 
        ];

        return response()->json($response);
    }

    public function batch_email_send(Request $request)
    {
        $input = $request->all();

        $emails = json_decode($input['emails'], true);

        if(count($emails) < 1){
            $response = [
                'notifStatus' => 'error',
                'notifTitle' => 'Please select recipients'
            ];

            return response()->json($response);
        }

        foreach(array_chunk($emails, 50) as $chunk){
            SendInquiry::dispatch($chunk, $input);
        }
        /* foreach($input['recipients'] as $user){
            SendInquiry::dispatch($user, $input);
        } */

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Email/s sent successfully',
            'notifMessage' => 'Redirecting to batch page',
            'redirect'=>route('adminBatchesEdit', $input['batch_id'])
        ];
        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/batches', array('as'=>'adminBatches','uses'=>'Admin\BatchController@index'));
Route::get('admin/batches/create', array('as'=>'adminBatchesCreate','uses'=>'Admin\BatchController@create'));
Route::post('admin/batches/', array('as'=>'adminBatchesStore','uses'=>'Admin\BatchController@store'));
Route::get('admin/batches/{id}/show', array('as'=>'adminBatchesShow','uses'=>'Admin\BatchController@show'));
Route::get('admin/batches/{id}/view', array('as'=>'adminBatchesView','uses'=>'Admin\BatchController@view'));
Route::get('admin/batches/{id}/edit', array('as'=>'adminBatchesEdit','uses'=>'Admin\BatchController@edit'));
Route::patch('admin/batches/{id}', array('as'=>'adminBatchesUpdate','uses'=>'Admin\BatchController@update'));
Route::post('admin/batches/seo', array('as'=>'adminBatchesSeo','uses'=>'Admin\BatchController@seo'));
Route::delete('admin/batches/destroy', array('as'=>'adminBatchesDestroy','uses'=>'Admin\BatchController@destroy'));
*/
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response as Download;

use App\Acme\PDFwatermark\PDFWatermark;
use App\Acme\PDFwatermarker\PDFWatermarker;
use App\Acme\Facades\SumoImage;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Asset;
use App\AssetGroupItem;
use App\Tag;
use App\User;
use App\Watermark;
use App\Activity;

use Image;
class AssetController extends Controller
{
  private $upload_path = './upload/assets';
  private $redactor_path = './upload/redactor';
  private $upload_path_s3 = 'upload/assets/';
  private $redactor_path_s3 = 'upload/redactor/';
  private $s3_active = NULL;

  public function __construct()
  {
    //Convert env S3_ACTIVE to boolean
    $s3_active = strtolower(getenv('S3_ACTIVE'));
    $this->s3_active = ($s3_active == "true");
  }

  public function upload(Request $request)
  {
    $s3 = Storage::disk('s3');
    $input = $request->all();
    $response = [];
    $filepath = '';
    if ($request->hasFile('photo')) {
      $photo = $request->file('photo');

      $asset = new Asset;

      /*
       * GIFs shouldn't be resized.
       */
      // if ($input['type'] == 'image') {
      if ($input['type'] == 'image' && @getimagesize($input["photo"])["mime"] !== "image/gif"){
        $data = SumoImage::upload($photo, 'upload/assets/',$this->s3_active);
        if($this->s3_active){
          $asset->path = $s3->url($data['paths']['main']);
          $asset->medium_thumbnail = $s3->url($data['paths']['medium']);
          $asset->small_thumbnail = $s3->url($data['paths']['small']);
        }
        else{
          $asset->path = $data['paths']['main'];
          $asset->medium_thumbnail = $data['paths']['medium'];
          $asset->small_thumbnail = $data['paths']['small'];
        }
        $response['filepath'] = '';
        if($this->s3_active){
          $response['filepath'] = $s3->url($data['paths']['small']);
        } else {
          $response['filepath'] = url($data['paths']['small']);
        }
      } else {
        $filename = str_random(50) . '.' . $photo->getClientOriginalExtension();

        if($this->s3_active) {
          $s3->put($this->upload_path_s3.$filename,file_get_contents($request->file('photo')),'public');
          $filepath = $s3->url($this->upload_path_s3 . $filename);
          $asset->path = $filepath;

        } else {
          $photo->move($this->upload_path, $filename);
          $filepath = 'upload/assets/' . $filename;
          $asset->path = $filepath;
        }
        if($input['type'] == 'image'){
          $response['filepath'] = url('upload/assets/' . $filename);
        }else {
          $response['filepath'] = SumoImage::defaultImageProcessor($asset->path);
        }
      }

      $asset->name = pathinfo($photo->getClientOriginalName(), PATHINFO_FILENAME);
      $asset->type = SumoImage::fileTypeIdentifier($photo->getClientOriginalName());
      $asset->save();

      $this->processTags($asset, $input);

      $response['id'] = $asset->id;
      $response['name'] = $asset->name;
      $response['type'] = $input['type'];
      return response()->json($response);
    }
  }

  function pdf_version($filename)
  { 

      $fp = @fopen($filename, 'rb');
  
      if (!$fp) {
          return 0;
      }
  
      /* Reset file pointer to the start */
      fseek($fp, 0);
  
      /* Read 20 bytes from the start of the PDF */
      preg_match('/\d\.\d/',fread($fp,20),$match);
  
      fclose($fp);
  
      if (isset($match[0])) {
          return $match[0];
      } else {
          return 0;
      }
  } 

  public function pdf_version_api(Request $request){
    $input = $request->all();
    $response = [
      'file' => $request->file('file'),
      'type' => $input['type'],
      'version' => $this->pdf_version($request->file('file'))
    ];
    return response()->json($response);
  }

  public function redactor(Request $request)
  {
    $s3 = Storage::disk('s3');
    if ($request->hasFile('file')) {
      $photos = $request->file('file');
      $response = array();
      foreach ($photos as $key => $photo) {
        $filename = str_random(50) . '.' . $photo->getClientOriginalExtension();
        $response = [];
        if($this->s3_active) {
          $fullPath = $this->redactor_path_s3 . $filename;
          $file = $request->file;
          $f = $request->file[0]->storeAs('', $fullPath, 's3');
          $filepath = $s3->url($fullPath);
          $response['file-'. $key] =[
            'url'=>$filepath,
            'filelink' => $filepath,
            'path' => $filepath,
            'id' => md5(date('YmdHis'))
          ];
        } else {
          $photo->move($this->redactor_path, $filename);
          $filepath = $this->redactor_path . '/' . $filename;
          $response['file-'. $key] =[
            'url'=>url($filepath),
            'filelink' => url($filepath),
            'path' => $this->redactor_path,
            'id' => md5(date('YmdHis'))
          ];
        }
      }
      return response()->json($response);
    }
  }

  public function all(Request $request)
  {
    $s3 = Storage::disk('s3');
    $batch = 20;

    $sortOrder = $request['sortBy']['order'];
    $sortBy = $request['sortBy']['by'];

    $filterByDate = 'created_at';
    $filterByDateFrom = '1900-1-1'; // default for $filterByDateFrom if not set
    $filterByDateTo = '2100-12-31'; // default for $filterByDateTo if not set

    if(isset($request['filterBy']['date'])){
      $filterByDateFrom = $request['filterBy']['from'];
      $filterByDateTo = $request['filterBy']['to'];
      $filterByDateTo = str_replace('-', '/', $filterByDateTo);
      $filterByDateTo = date('Y-m-d',strtotime($filterByDateTo . "+1 days"));
    }

    $filterByColumn = "type"; // default for $filterByWhere if not set *this is for what column for where
    $filterByOperator = "<>"; // default for $filterByWhere if not set and filterBy filetype is all *this is for what operator to use
    $filterByValue = "''"; // default for $filterByWhere if not set and filterBy filetype is all
    if(isset($request['filterBy']['fileType']) AND $request['filterBy']['fileType'] != 'all'){
      $filterByOperator = "=";
      $filterByValue = $request['filterBy']['fileType'];
    }

    $assets = New Asset;
    // DB::enableQueryLog();

    $assets = $assets->skip($request['count'])->take($batch)
                      ->where('name', 'like', $request['searchInput'].'%')
                      ->where('source', 'admin')
                      ->where($filterByColumn, $filterByOperator, $filterByValue)
                      ->whereBetween($filterByDate,[$filterByDateFrom,$filterByDateTo])
                      ->orderBy($sortOrder, $sortBy);

    if (!empty($request['tags'])) {
      $assets = $assets->where($filterByColumn, $filterByOperator, $filterByValue)
                        ->where('name', 'like', $request['searchInput'].'%')
                        ->where('source', 'admin')
                        ->whereBetween($filterByDate,[$filterByDateFrom,$filterByDateTo])
                        ->whereHas('tags', function($query) use ($request) {
                          $query->whereIn('name', explode(',', $request['tags']))
                                ->where('taggable_type','App\Asset');
      });
    }

    $assets = $assets->get();
    // dd(DB::getQueryLog());
    // dd($assets);
    foreach ($assets as $a) {
      if ($a->type == 'image') {
        if($this->s3_active) {
          $a->mime_path = $a->path;
        } else {
          $a->mime_path = url($a->path);
        }
      } else {
        $a->mime_path = SumoImage::defaultImageProcessor($a->path);
      }
    }

    $view = view('admin/modals/assets-list')
      ->with('assets', $assets)
      ->render();

    $next = Asset::skip($request['count'] + $batch)->take($batch)
                                                    ->where('name', 'like', $request['searchInput'].'%')
                                                    ->where('source', 'admin')
                                                    ->where($filterByColumn, $filterByOperator, $filterByValue)
                                                    ->whereBetween($filterByDate,[$filterByDateFrom,$filterByDateTo])
                                                    ->orderBy($sortOrder, $sortBy);

    if (!empty($request['tags'])) {
      $next = Asset::skip($request['count'] + $batch)->take($batch)
                                                      ->where('name', 'like', $request['searchInput'].'%')
                                                      ->where('source', 'admin')
                                                      ->where($filterByColumn, $filterByOperator, $filterByValue)
                                                      ->whereBetween($filterByDate,[$filterByDateFrom,$filterByDateTo])
                                                      ->whereHas('tags', function($query) use ($request) {
                                                        $query->whereIn('name', explode(',', $request['tags']))
                                                              ->where('taggable_type','App\Asset');
      });
    }

    $next = $next->get();

    $response = [
    'view'=>$view,
    'next'=>count($next)
    ];
    return response($response);
  }

  public function get(Request $request)
  {
    $s3 = Storage::disk('s3');
    $input = $request->all();
    $asset = Asset::find($input['id']);

    if ($asset->type == 'image') {
        if($this->s3_active) {
          $asset->absolute_path = $asset->path;
        } else {
          $asset->absolute_path = url($asset->path);
        }
    } else {
      $asset->absolute_path = SumoImage::defaultImageProcessor($asset->path);
    }
    return response()->json($asset);
  }

  public function getAssetTags(Request $request)
  {
    $response = Tag::select(array('id','name'))
                    ->groupBy('name')
                    ->where('taggable_type', '=', 'App\Asset')
                    ->orderBy('name', 'ASC')
                    ->get();
    return response()->json($response);
  }

  public function download(Request $request)
  {
    $s3 = Storage::disk('s3');
    $input = $request->all();
    $asset = Asset::find($input['id']);
    if($this->s3_active) {
      $file_url = $s3->url($asset->path);
      header('Content-Type: application/octet-stream');
      header("Content-Transfer-Encoding: Binary"); 
      header("Content-disposition: attachment; filename=\"" . $asset->name . '.' . pathinfo($asset->path)['extension'] . "\""); 
      readfile($asset->path);
    } else {
      return response()->download($asset->path);
    }
  }

  // added karlob - save tags
  public function processTags($asset, $input) {
    $asset->tags()->delete();
    if (isset($input['tags'])) {
      $tags = explode(",", $input['tags']);
      foreach ($tags as $key => $value) {
        $tag = New Tag;
        if (!empty($value)) {
          $tag->name = str_slug($value);
          $asset->tags()->save($tag);
        }
      }
    }
  }
  // end add karlob

  public function update(Request $request)
  {
    $input = $request->all();
    $asset = Asset::with('tags')->findOrFail($input['id']);
    $asset->name = $input['name'];
    $asset->caption = $input['caption'];
    $asset->save();

    $this->processTags($asset, $input);

    $response = [
      'notifTitle'=>'Save successful.',
    ];
    return response()->json($response);
  }

  public function destroy(Request $request)
  {
    $input = $request->all();
    $filepath = Asset::select('path')->where('id',$input['id'])->first();
    if($this->s3_active) {
      $asset = Asset::findOrFail($input['id']);
      Storage::disk('s3')->delete($asset->first()->path);
      $asset->delete();
      Tag::where('taggable_type','App\Asset')->where('taggable_id',$input['id'])->delete();
      AssetGroupItem::where('asset_id',$input['id'])->delete();
      $response = [
        'notifTitle'=>'Delete successful.',
        'id' => $input['id']
      ];
    } else {
      if (file_exists($filepath->path)) {
        unlink($filepath->path);
      }
      Asset::findOrFail($input['id'])->delete();
      Tag::where('taggable_type','App\Asset')->where('taggable_id',$input['id'])->delete();
      AssetGroupItem::where('asset_id',$input['id'])->delete();
      $response = [
        'notifTitle'=>'Delete successful.',
        'id' => $input['id']
      ];
    }
    return response()->json($response);
  }


  public function downloadPDF($assetId, $hash, $testing = false, Request $request)
  {

    $s3 = Storage::disk('watermark_s3');

    if (!$testing) {
      $user = User::findOrFail(Auth::user()->id);
    } else {
      // Dummy user data
      $user = new User;
      $user->student_number = $this->generate_random_string(5);
      $user->first_name = $this->generate_random_string(3);
      $user->last_name = $this->generate_random_string(10);
      $user->name = $this->generate_random_string(13);
      $user->email = $this->generate_random_string(24);
    }

    $asset = Asset::findOrFail($assetId);
    if ($asset->type != "pdf") {
      return response()->download(public_path($asset->path),$asset->name.".".$asset->type);
    }

    // save an activity based on user ID + asset ID
    $activity = new Activity;
    $activity->user_id = $user->id;
    $activity->value_from = $request->ip();
    $activity->value_to = json_encode($request->header());
    $activity->loggable_type = 'App\Asset';
    $activity->loggable_id = $assetId;
    $activity->identifier_value = $assetId;
    $activity->log = 'Downloaded: '.$asset->name.' ID: '. $activity->identifier_value. ' By: '.$user->name;
    $activity->save();

    $destinationFile = public_path('/upload/assets/temporary_pdf'.$hash.'.pdf');

    $watermark_file = Watermark::where(function($q) use($user, $asset){
      $q->where('user_id', $user->id);
      $q->where('asset_id', $asset->id);
    })->first();

    if($watermark_file != null && $s3->exists($watermark_file->url) ){

      $size = $s3->size($watermark_file->url);
      $filename = $asset->name.' '.$user->name.'.pdf';

      header('Content-Type: application/pdf');
      header('Content-Disposition: attachment; filename="'. $filename .'"');
      header('Content-Length:'. $size .'');

      return readfile($s3->url($watermark_file->url));
    }
    else{
      // STUDENT WATERMARK
      $path = $this->create_users_name_image($user);
      
      $watermark = new PDFWatermark($path); //Specify path to image. The image must have a 96 DPI resolution.
      unlink($path);
      
      $watermark->setPosition('center'); //Set the position
      
      $path = public_path('/upload/assets/temp'.$hash.'.pdf');
      
      //Specify the path to the existing pdf, the path to the new pdf file, and the watermark object
      if($this->s3_active){
        file_put_contents($path, fopen($asset->path, 'r'));

        $watermarker = new PDFWatermarker($path,$destinationFile,$watermark); 
      }
      else{
        $watermarker = new PDFWatermarker(public_path($asset->path),$destinationFile,$watermark); 
      }

      $watermarker->setPageRange(1); //Set page range. Use 1-based index.
      $watermarker->savePdf(); //Save the new PDF to its specified location
      
      if(file_exists($path)){
        unlink($path);
      }

      // NOT FOR SALE WATERMARK
      /* $no_sale = $this->not_for_sale_watermark();

      $no_sale_watermark = new PDFWatermark($no_sale);
      unlink($no_sale);

      $no_sale_watermark->setPosition('center');

      // Specify the path to the existing pdf, the path to the new pdf file, and the watermark object
      $no_sale_watermarker = new PDFWatermarker($destinationFile,public_path('/upload/assets/temporary_pdf'.$hash.'_nosale.pdf'),$no_sale_watermark); 

      $no_sale_watermarker->setPageRange(1,1);
      $no_sale_watermarker->savePdf();

      unlink($destinationFile); */

      // Download File
      $size = filesize($destinationFile);
      $headers = array(
        'Content-Type: application/pdf',
        'Content-Length:'. $size .''
      );
      return response()->download($destinationFile,$asset->name.' '.$user->name.".pdf", $headers)->deleteFileAfterSend(); 
    }
  }

  public function not_for_sale_watermark()
  {
      $img = Image::make(public_path('upload/users_name/template.png'));

      $opacity = floatval(getenv('WATERMARK_OPACITY'));
      if ($opacity == 0.0) {
        $opacity = 0.2;
      }

      $img->text('N O T  F O R  S A L E', 680, 880, function($font) use ($opacity) {
        $font->file(public_path('fonts/ClearSans-Bold.ttf'));
        $font->size(45);
        $font->color(array(0, 0, 0, $opacity));
        $font->align('center');
        $font->valign('top');
        $font->angle(55);
      });
      
      $img->save(public_path('upload/users_name/not-for-sale.png'));
      return public_path('upload/users_name/not-for-sale.png');
  }

  public function create_users_name_image($user)
  {
      // $user = User::findOrFail(Auth::user()->id);
        
      $img = Image::make(public_path('upload/users_name/template.png'));

      $string = implode(' ', str_split(strtoupper($this->stripAccents($user->name).' '.$user->student_number)));
      if(strlen($user->name) > 21){
        $string = implode(' ', str_split(strtoupper(substr($this->stripAccents($user->first_name), 0, 1).'. '.$this->stripAccents($user->last_name).' '.$user->student_number)));
      }
      
      $opacity = floatval(getenv('WATERMARK_OPACITY'));
      if ($opacity == 0.0) {
        $opacity = 0.2;
      }
      $img->text($string, 620, 850, function($font) use ($opacity) {
          $font->file(public_path('fonts/ClearSans-Bold.ttf'));
          $font->size(45);
          $font->color(array(0, 0, 0, $opacity));
          $font->align('center');
          $font->valign('top');
          $font->angle(55);
      });

      $img->save(public_path('upload/users_name/'.$user->student_number.'.png'));
      return public_path('upload/users_name/'.$user->student_number.'.png');
  }

  public function test_downloadPDF()
  {
    if (getenv('TEST_ASSET_ID')) {
      return $this->downloadPDF(getenv('TEST_ASSET_ID'), md5($this->generate_random_string(10)), true);
    }
    echo "No configured TEST_ASSET_ID";
  }

  public function generate_random_string($length)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  function stripAccents($str) {
    return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
  }
}

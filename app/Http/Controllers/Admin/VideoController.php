<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;

use App\Jobs\SyncVideos;
use App\Jobs\TriggerVideoSync;

use Acme\Facades\Activity;
use App\Acme\Facades\SumoImage;

use App\Video;
use App\Lecture;
use App\Seo;

class VideoController extends Controller
{   

    private $upload_path = './upload/assets';
    public $domains = ['demofort.com', 'homestudy.topnotchboardprep.com.ph', 'homestudybeta.topnotchboardprep.com.ph'];

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($caption = $request->caption) {
            $data = Video::where('caption', 'LIKE', '%' . $caption . '%')->paginate(25);
        } else {
            $data = Video::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();
        return view('admin/videos/index')
            ->with('title', 'Videos')
            ->with('menu', 'lectures')
            ->with('keyword', $request->caption)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function upload(Request $request)
    {
        $input = $request->all();
        $response = [];
        $asset = new Video;
        $asset->url = $input['url'];

        $asset->name = $input['name'];
        $asset->save();

        $response = [
            'id' => $asset->id,
            'name' => $asset->name,
            'upload_link' => $input['upload_link']
        ];
        return response()->json($response);
    }

    public function store(Request $request){
        $input = $request->all();

        $data = json_decode($input['data'], true);
        
        // SyncVideos::dispatch($data);
        SyncVideos::dispatchNow($data); // force dispatch for syncing
        
        $response = [
            'status' => 'Videos synched'
        ];
        
        return response()->json($response);
    }

    public function set_video_settings($video){

        $this->set_video_domains($video);

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $body = array(
            'embed' => array(
                'buttons' => array(
                    'embed' => false,
                    'hd' => true,
                    'like' => false,
                    'share' => false,
                    'watchlater' => false,
                ),
                'title' => array(
                    'name' => 'hide',
                    'owner' => 'hide',
                    'portrait' => 'hide'
                ),
                'logos' => array(
                    'vimeo' => false
                )
            ),
            'privacy' => array(
                'comments' => 'nobody',
                'download' => false,
                'embed' => 'whitelist',
                'view' => 'disable'
            )
        );

        $c = curl_init('https://api.vimeo.com'.$video->url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
        curl_exec($c);
    }

    public function set_video_domains($video){

        foreach ($this->domains as $domain) {
            $headers = array(
                'Content-Type: application/json',
                'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
            );
    
            $c = curl_init('https://api.vimeo.com'.$video->url.'/privacy/domains/'.$domain);
            curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
            curl_exec($c);
        }
    }

    public function all(Request $request)
    {
        $batch = 10;

        $sortOrder = $request['sortBy']['order'];
        $sortBy = $request['sortBy']['by'];


        $assets = New Video;
        // DB::enableQueryLog();

        $assets = $assets->skip($request['count'])->take($batch)
                        ->where('name', 'like', '%'.$request['searchInput'].'%')
                        ->orderBy($sortOrder, $sortBy);

        $assets = $assets->get();

        foreach ($assets as $a) {
            if (empty($a->path)) {
                $headers = array(
                    'Content-Type: application/json',
                    'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
                );
        
                $c = curl_init('https://api.vimeo.com'.$a->url);
                curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
                
                $response = curl_exec($c);
        
                $data = json_decode($response, true);
    
                if (!isset($data['error'])) {
                    $a->path = $data['pictures']['sizes'][4]['link_with_play_button'];
                    $a->save();
                } else {
                    // delete this asset - might be deleted in vimeo
                    Video::destroy($a->id);
                }
            }
        }

        foreach ($assets as $a) {
            $a->mime_path = $a->path;
        }

        $view = view('admin/modals/assets-list')
                ->with('assets', $assets)
                ->render();

        $next = Video::skip($request['count'] + $batch)
                    ->take($batch)
                    ->where('name', 'like', $request['searchInput'].'%')
                    ->orderBy($sortOrder, $sortBy);

        $next = $next->get();

        $response = [
            'view'=>$view,
            'next'=>count($next)
        ];

        return response($response);
    }

    public function get(Request $request)
    {
        $input = $request->all();
        $asset = Video::find($input['id']);

        $html = $this->get_html($request);
        // $asset->absolute_path = SumoImage::defaultImageProcessor($asset->path);

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $c = curl_init('https://api.vimeo.com'.$asset->url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($c);

        $data = json_decode($response, true);

        $response = [];

        switch ($http_code = curl_getinfo($c, CURLINFO_HTTP_CODE)) {
            
            case 200:

                $asset->absolute_path = $data['pictures']['sizes'][4]['link_with_play_button'];
                $asset->src_path = $html->getData()->src;
                $response = $asset;
                return response()->json($response);

                break;

            default:

                $response = [
                    'notifTitle' => 'An error occured',
                    'notifStatus' => 'success'
                ];
              
        }
    }

    public function get_html(Request $request){
        $input = $request->all();

        $video = Video::findOrFail($input['id']);

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $c = curl_init('https://api.vimeo.com'.$video->url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($c);

        $data = json_decode($response, true);

        $video_id = str_replace('videos', 'video', $data['uri']);
        $src = 'https://player.vimeo.com'.$video_id.'?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=168233';

        $response = [
            'src' => $src
        ];

        return response()->json($response);
    }

    public function create()
    {
        return view('admin/videos/create')
            ->with('title', 'Create video')
            ->with('menu', 'lectures');
    }
    
    public function update(Request $request)
    {
        $input = $request->all();
        $asset = Video::findOrFail($input['id']);
        $asset->name = $input['name'];
        $asset->description = $input['description'];
        $asset->save();

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $body = array(
            'name' => $asset->name,
            'description' => $asset->description
        );

        $c = curl_init('https://api.vimeo.com'.$asset->url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'PATCH');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
        curl_exec($c);

        $response = [
        'notifTitle'=>'Save successful.'
        ];
        
        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();
        $video = Video::where('id',$input['id'])->first();

        $lecture = Lecture::where('video_id', $video->id)->first();

        if($lecture != null){

            $response = [
                'notifStatus' => 'error',
                'notifTitle' => 'Video is currently assigned in a lecture.'
            ];

            return response()->json($response);
        }

        $headers = array(
            'Content-Type: application/json',
            'Authorization: bearer '.env('VIMEO_ACCESS_TOKEN')
        );

        $c = curl_init('https://api.vimeo.com'.$video->url);
        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        
        $vimeo = curl_exec($c);

        $vimeo_result_arr = json_decode($vimeo, true);

        if (!isset($vimeo_result_arr['error'])) {
            $d = curl_init('https://api.vimeo.com'.$video->url);
            curl_setopt($d, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($d, CURLOPT_CUSTOMREQUEST, 'DELETE');
            curl_exec($d);
        }
        dd('not found');

        $video->delete();

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Removed successfully.',
        ];

        return response()->json($response);
    }

    public function trigger_video_sync(Request $request) {
        TriggerVideoSync::dispatch(1, 100);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/videos', array('as'=>'adminVideos','uses'=>'Admin\VideoController@index'));
Route::get('admin/videos/create', array('as'=>'adminVideosCreate','uses'=>'Admin\VideoController@create'));
Route::post('admin/videos/', array('as'=>'adminVideosStore','uses'=>'Admin\VideoController@store'));
Route::get('admin/videos/{id}/show', array('as'=>'adminVideosShow','uses'=>'Admin\VideoController@show'));
Route::get('admin/videos/{id}/view', array('as'=>'adminVideosView','uses'=>'Admin\VideoController@view'));
Route::get('admin/videos/{id}/edit', array('as'=>'adminVideosEdit','uses'=>'Admin\VideoController@edit'));
Route::patch('admin/videos/{id}', array('as'=>'adminVideosUpdate','uses'=>'Admin\VideoController@update'));
Route::post('admin/videos/seo', array('as'=>'adminVideosSeo','uses'=>'Admin\VideoController@seo'));
Route::delete('admin/videos/destroy', array('as'=>'adminVideosDestroy','uses'=>'Admin\VideoController@destroy'));
*/
}

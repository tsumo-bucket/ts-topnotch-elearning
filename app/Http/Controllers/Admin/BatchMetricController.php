<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Batch;
use App\BatchMetric;

class BatchMetricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $batch = Batch::findOrFail($id);

        return view('admin/batch_metrics/index')
            ->with('title', 'Metrics')
            ->with('menu', 'batches')
            ->with('data', $batch->metrics)
            ->with('batch', $batch);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $batch = Batch::findOrFail($input['batch_id']);

        $is_existing = BatchMetric::where('batch_id', $batch->id)->where('name', $input['name'])->first();
        if ($is_existing != null) {
            $response = [
                'notifStatus' => 'error',
                'notifMessage' => 'Metric is already existing'
            ];

            return response()->json($response);
        }

        $input['slug'] = $this->generate_slug($input['name']);

        $data = BatchMetric::create($input);

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Metric successfully created',
            'notifMessage' => 'Refreshing page...',
            'redirect' => route('adminBatchMetrics', $batch->id)
        ];

        return response()->json($response);
    }

    /**
     * Generate a slug based on the given string.
     *
     * @param  String $str
     * @return String
     */
    public function generate_slug($str)
    {
        $str = strtolower(str_replace(' ', '', $str)); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $input = $request->all();
        
        $batch = Batch::findOrFail($input['batch_id']);
        $data = BatchMetric::findOrFail($input['metric_id']);

        $is_existing = BatchMetric::where(function($query) use($input) {
                            $query->where('batch_id', $input['batch_id']);
                            $query->where('id', '!=', $input['metric_id']);
                            $query->where('name', $input['name']);
                        })->first();
        if ($is_existing != null) {

            $response = [
                'notifStatus' => 'error',
                'notifTitle' => 'Metric is already existing'
            ];

            return response()->json($response);
        }

        $input['slug'] = $this->generate_slug($input['name']);
        $data->update($input);

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Metric successfully updated',
            'notifMessage' => 'Refreshing page...',
            'redirect' => route('adminBatchMetrics', $batch->id)
        ];

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $input = $request->all();
        $data = BatchMetric::findOrFail($input['id']);
        
        $data->delete();

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Metric successfully deleted',
            'notifMessage' => 'Refreshing page...',
            'redirect' => route('adminBatchMetrics', $input['batch_id'])
        ];

        return response()->json($response);
    }
}

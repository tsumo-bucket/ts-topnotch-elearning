<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\PreviewRequest;

use Acme\Facades\Activity;
use Acme\Facades\General;
use App\Preview;
use App\Seo;

class PreviewController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($slug = $request->slug) {
            $data = Preview::where('slug', 'LIKE', '%' . $slug . '%')->paginate(25);
        } else {
            $data = Preview::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/previews/index')
            ->with('title', 'Previews')
            ->with('menu', 'previews')
            ->with('keyword', $request->slug)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/previews/create')
            ->with('title', 'Create preview')
            ->with('menu', 'previews');
    }
    
    public function store(PreviewRequest $request)
    {
        $input = $request->all();
        $preview = Preview::create($input);
        
		$input['slug'] = General::slug($preview->slug,$preview->id);
		$preview->update($input);

        // $log = 'creates a new preview "' . $preview->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminPreviewsEdit', [$preview->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/previews/show')
            ->with('title', 'Show preview')
            ->with('data', Preview::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/previews/view')
            ->with('title', 'View preview')
            ->with('menu', 'previews')
            ->with('data', Preview::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Preview::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/previews/edit')
            ->with('title', 'Edit preview')
            ->with('menu', 'previews')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(PreviewRequest $request, $id)
    {
        $input = $request->all();
        $preview = Preview::findOrFail($id);
        $preview->update($input);

        // $log = 'edits a preview "' . $preview->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Preview::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Preview::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->slug;
        }
        // $log = 'deletes a new preview "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Preview::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminPreviews')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/previews', array('as'=>'adminPreviews','uses'=>'Admin\PreviewController@index'));
Route::get('admin/previews/create', array('as'=>'adminPreviewsCreate','uses'=>'Admin\PreviewController@create'));
Route::post('admin/previews/', array('as'=>'adminPreviewsStore','uses'=>'Admin\PreviewController@store'));
Route::get('admin/previews/{id}/show', array('as'=>'adminPreviewsShow','uses'=>'Admin\PreviewController@show'));
Route::get('admin/previews/{id}/view', array('as'=>'adminPreviewsView','uses'=>'Admin\PreviewController@view'));
Route::get('admin/previews/{id}/edit', array('as'=>'adminPreviewsEdit','uses'=>'Admin\PreviewController@edit'));
Route::patch('admin/previews/{id}', array('as'=>'adminPreviewsUpdate','uses'=>'Admin\PreviewController@update'));
Route::post('admin/previews/seo', array('as'=>'adminPreviewsSeo','uses'=>'Admin\PreviewController@seo'));
Route::delete('admin/previews/destroy', array('as'=>'adminPreviewsDestroy','uses'=>'Admin\PreviewController@destroy'));
*/
}

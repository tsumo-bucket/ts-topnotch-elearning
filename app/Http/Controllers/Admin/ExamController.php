<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Page;
use App\PageCategory;
use App\BatchPage;
use App\Batch;

class ExamController extends Controller
{

    public $category;

    public function __construct()
    {
        $this->middleware('admin');

        $this->category = PageCategory::where('slug', 'exams')->first();
    }

    public function index(Request $request)
    {
        
        $input = $request->all();

        if($this->category == null){
            $this->category = PageCategory::create([
                'name' => 'Exams',
                'slug' => 'exams'
            ]);
        }

        $data = Page::where('page_category_id', $this->category->id);

        if (isset($input['title']) && $input['title']) {
            $data = $data->where('name', 'LIKE', '%' . $input['title'] . '%');
        } 
        
        if (isset($input['batch']) && $input['batch']) {
            $data = $data->whereHas('batches', function($q) use($input) {
                $q->whereHas('batch', function($q2) use($input) {
                    $q2->where('name', 'LIKE', '%'.$input['batch'].'%');
                });
            });
        }

        $data = $data->orderBy('created_at', 'desc')->paginate(25);
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/exams/index')
            ->with('filters', $input)
            ->with('title', 'Exams')
            ->with('menu', 'exams')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    
    public function create()
    {
        $category = PageCategory::where('name','exams')->first();
        $all_batches = Batch::where('status', 'ongoing')->pluck('name', 'id');

        return view('admin/exams/create')
            ->with('title', 'Create Exam')
            ->with('all_batches', $all_batches)
            ->with('menu', 'exams')
            ->with('category', $category);
    }

    
    public function store(Request $request)
    {
        $input = $request->all();

        $page = Page::create($input);
        $page->slug = strtolower(str_replace(' ', '-', $page->name));
        $page->save();


        if (!empty($input['batches'])) {

            $batches = [];
            foreach ($input['batches'] as $batch) {

                $temp = new BatchPage;
                $temp->page_id = $page->id;
                $temp->batch_id = $batch;

                $batches[] = $temp;
            }
            
            $page->batches()->saveMany($batches);
        }


        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'notifStatus'=>'success',
            'redirect'=>route('adminExamsEdit', [$page->id])
        ];

        return response()->json($response);
    }

    
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $all_batches = Batch::where('status', 'ongoing')->pluck('name', 'id');
        $batch_pages = BatchPage::where('page_id', $id)->pluck('batch_id');
        $page->batches = Batch::whereIn('id', $batch_pages)->pluck('id');

        return view('admin/exams/edit')
            ->with('title', 'Edit Exam')
            ->with('menu', 'exams')
            ->with('all_batches', $all_batches)
            ->with('data', $page);
    }

    
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $page = Page::findOrFail($id);
        $page->name = $input['name'];
        $page->published = $input['published'];
        $page->content = $input['content'];
        $page->save();


        $page->batches()->delete();
        if (!empty($input['batches'])) {

            $batches = [];
            foreach ($input['batches'] as $batch) {

                $temp = new BatchPage;
                $temp->page_id = $page->id;
                $temp->batch_id = $batch;

                $batches[] = $temp;
            }
            
            $page->batches()->saveMany($batches);
        }


        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Refreshing page.',
            'notifStatus'=>'success',
            'redirect'=>route('adminExamsEdit', [$page->id])
        ];

        return response()->json($response);
    }

    
    public function destroy(Request $request)
    {
        $input = $request->all();

        Page::whereIn('id', $input['ids'])->delete();

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminExams')
        ];

        return response()->json($response);
    }
}

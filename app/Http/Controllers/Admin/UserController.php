<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserPasswordUpdateRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\UserRole;
use App\Batch;
use App\BatchStudent;
use Hash;
use SumoMail;

use App\Acme\Facades\Activity;
use App\Acme\Facades\General;
use App\Acme\Facades\Option;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->middleware('super', ['only' => ['edit', 'update']]);
    }

    public function index(Request $request)
    {
        if ($name = $request->name) {
            $users = User::where('name', 'LIKE', '%' . $name . '%')->paginate(25);
        } else {
            $users = User::paginate(25);
        }
        $pagination = $users->appends($request->except('page'))->links();

        return view('admin/users/index')
            ->with('title', 'Users')
            ->with('menu', 'users')
            ->with('data', $users)
            ->with('pagination', $pagination);
    }

    public function datatable()
    {
        return view('admin/users/datatable')->with('data', User::all());
    }

    public function create()
    {
        Option::email();
        $user_roles = UserRole::pluck('name','id');
        $batches = Batch::where('status','!=','completed')->get();


        if(count($batches) > 0){
            $types = [
                'normal' => 'Normal',
                'admin' => 'Admin',
                'super' => 'Super'
            ];
        }
        else{
            $types = [
                'admin' => 'Admin',
                'super' => 'Super'
            ];
        }

        return view('admin/users/create')
            ->with('user_roles',$user_roles)
            ->with('batches', $batches)
            ->with('types',$types)
            ->with('title', 'Create User');
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        $options = Option::email();

        $errors = false;
        $response = [];
        $response['notifMessage'] = '';

        $first_name = '';
        $last_name = '';

        $name_arr = explode(' ', $input['name']);
        if(count($name_arr) > 1){
            $last_name = ucwords(end($name_arr));
            array_pop($name_arr);

            $first_name = ucwords(implode($name_arr));
        }
        else{
            $first_name = ucwords($input['name']);
            $last_name = ucwords($input['name']);
        }

        $password = '';
        $student_number = '';
        $status = '';
        $verified = '';


        $other_emails = User::where('status', '!=', 'completed')->pluck('email');
        $check_other_emails = in_array($input['email'], $other_emails->toArray());

        if($check_other_emails == true){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Email already in use.';
            $errors = true;
        }

        if($input['type'] == 'normal'){

            if(strlen($input['student_number']) < 5){
                $zeros = '';
                for ($i=5; $i > strlen($input['student_number']); $i--){
                    $zeros .= '0';
                }
                $student_number = date('Y').$zeros.$input['student_number'];
            }
            else{
                $student_number = date('Y').$input['student_number'];
            }

            // VALIDATIONS

            $check_student_number = User::where('student_number', $student_number)->first();

            if(strlen($input['student_number']) != 5){
                if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
                $response['notifMessage'] .= 'Student number should have 5 characters.';
                $errors = true;
            }

            if($check_student_number != null){
                if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
                $response['notifMessage'] .= 'Student number must be unique.';
                $errors = true;
            }   

            $password = str_replace(' ', '', strtolower($last_name).$student_number);
            $student_number = $student_number;
            $status = 'blocked';
            $verified = 0;

        }
        else{
            $password = str_random(8);
            $student_number = uniqid();
            $status = 'active';
            $verified = 1;
        }

        if($errors == true){
            $response['notifStatus'] = 'error';
            return response()->json($response);
        }

        $user = new User;
        $user->student_number = $student_number;
        $user->name = $first_name.' '.$last_name;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $input['email'];
        $user->cms = 0;
        $user->verified = $verified;
        $user->status = $status;
        $user->password = bcrypt($password);
        $user->type = $input['type'];

        $user->save();

        if($input['type'] == 'normal'){
            $student = new BatchStudent;
            $student->user_id = $user->id;
            $student->batch_id = $input['batch_id'];
            $student->save();

            $batch = Batch::findOrFail($input['batch_id']);

            if($batch->status == 'ongoing'){
                $user->status = 'active';
                $user->save();
            }
        }

        $data = [];
        $data['user'] = $input;
        $data['password'] = $password;
        $data['options'] = $options;
      
        $params=[];
        $params['from_email'] = $options['sender-email'];
        $params['from_name'] =$options['sender-name'];
        $params['subject'] = $options['name'] . ': User account created';
        $params['to'] = $input['email'];

        //not required fields
        $params['replyTo'] =  $options['email'];

        //DEBUG Options are as follows:
        // FALSE - Default
        // TRUE - Shows HTML output upon sending
        // SIMULATE = Does not send email but saves information to the database
        $params['debug'] = false; 

        if($input['type'] != 'normal'){
            $return= SumoMail::send('emails.user-create', $data, $params);
        }

        $log = 'creates a new user "' . $user->name . '"';
        Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to users list.',
            'resetForm'=>true,
            'redirect'=>route('adminUsers')
        ];

        return response()->json($response);
    }

    public function show($id)
    {
        return view('admin/users/show')
            ->with('title', 'Show User')
            ->with('user', User::findOrFail($id));
    }

    public function edit($id)
    {
        $user_roles = UserRole::pluck('name','id');
        $types = [
            'normal' => 'Normal',
            'admin' => 'Admin',
            'super' => 'Super'
        ];
        return view('admin/users/edit')
            ->with('title', 'Edit User')
            ->with('menu', 'users')
            ->with('types', $types)
            ->with('user_roles',$user_roles)
            ->with('user', User::findOrFail($id));
    }

    public function change_password($id){
        $user_roles = UserRole::pluck('name','id');
        
        if(Auth::user()->type != 'super'){
            abort(403, "Unauthorized action.");
        }
        return view('admin/users/password_change')
            ->with('title', 'Change Password')
            ->with('menu', 'users')
            ->with('user_roles',$user_roles)
            ->with('user', User::findOrFail($id));
    }

    public function user_password_update(UserPasswordUpdateRequest $request, $id){
        $input = $request->all();
        $isChecked = $request->has('send_email');

        $logged_user = User::findOrFail(Auth::user()->id);

        if ($input['new'] == $input['new_confirmation']){
            $user = User::findOrFail($id);
            $user->password = Hash::make($input['new']);
            $user->save();

            $log = $logged_user['name'] . ' updates password of ' . $user['name'];
            Activity::create($log);

            if ($isChecked){
                $options = Option::email();
                $data = [];
                $data['user'] = $user;
                $data['password'] = $input['new'];
                $data['options'] = $options;
                $data['logged'] = $logged_user;
            
                $params=[];
                $params['from_email'] = $options['sender-email'];
                $params['from_name'] =$options['sender-name'];
                $params['subject'] = $options['name'] . ': Password change';
                $params['to'] = $user['email'];

                //not required fields
                $params['replyTo'] =  $options['email'];

                //DEBUG Options are as follows:
                // FALSE - Default
                // TRUE - Shows HTML output upon sending
                // SIMULATE = Does not send email but saves information to the database
                $params['debug'] = false;
                    
                $return = SumoMail::send('emails.password-change', $data, $params);
            }

            $response = [
                'notifTitle'=>'Save successful.',
                'notifMessage'=>'Redirecting to users list.',
                'resetForm'=>true,
                'redirect'=>route('adminUsers')
            ];

        } else{
            $response = [
                'notifTitle'=>'Password does not match.',
            ];
        }

        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $user = User::findOrFail($id);

        $other_emails = User::where(function($q) use($user){
            $q->where('status', '!=', 'completed');
            $q->where('id', '!=', $user->id);
        })->pluck('email');
        $check_other_emails = in_array($input['email'], $other_emails->toArray());

        if($check_other_emails == true){
            $response = [
                'notifStatus' => 'error',
                'notifMessage' => 'Email already in use.'
            ];
            return response()->json($response);
        }

        $user->name = $input['name'];
        $user->email = $input['email'];
        $user->save();

        $log = 'edits a user "' . $user->name . '"';
        Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function update_status(Request $request){
        $input = $request->all();

        $user = User::findOrFail($input['id']);
        $user->update($input);
        $user->save();

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Update successful.',
        ];

        return response()->json($response);
    }

    public function destroy(Request $request)
    {
        $input = $request->all();

        $samples = User::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($samples as $s) {
            $names[] = $s->name;
        }
        $log = 'deletes a new sample "' . implode(', ', $names) . '"';
        Activity::create($log);

        User::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminUsers')
        ];

        return response()->json($response);
    }
}

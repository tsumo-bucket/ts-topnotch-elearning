<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CacheRequest;

use Acme\Facades\Activity;

use App\Cache;
use App\Seo;

class CacheController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($key = $request->key) {
            $data = Cache::where('key', 'LIKE', '%' . $key . '%')->paginate(25);
        } else {
            $data = Cache::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/cache/index')
            ->with('title', 'Cache')
            ->with('menu', 'cache')
            ->with('keyword', $request->key)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/cache/create')
            ->with('title', 'Create cache')
            ->with('menu', 'cache');
    }
    
    public function store(CacheRequest $request)
    {
        $input = $request->all();
        $cache = Cache::create($input);
        
        // $log = 'creates a new cache "' . $cache->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminCacheEdit', [$cache->id])
        ];

        return response()->json($response);
    }
    
    public function show($id)
    {
        return view('admin/cache/show')
            ->with('title', 'Show cache')
            ->with('data', Cache::findOrFail($id));
    }

    public function view($id)
    {
        return view('admin/cache/view')
            ->with('title', 'View cache')
            ->with('menu', 'cache')
            ->with('data', Cache::findOrFail($id));
    }
    
    public function edit($id)
    {
        $data = Cache::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/cache/edit')
            ->with('title', 'Edit cache')
            ->with('menu', 'cache')
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(CacheRequest $request, $id)
    {
        $input = $request->all();
        $cache = Cache::findOrFail($id);
        $cache->update($input);

        // $log = 'edits a cache "' . $cache->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function seo(Request $request)
    {
        $input = $request->all();

        $data = Cache::findOrFail($input['seoable_id']);
        $seo = Seo::whereSeoable_id($input['seoable_id'])->whereSeoable_type($input['seoable_type'])->first();
        if (is_null($seo)) {
            $seo = new Seo;
        }
        $seo->title = $input['title'];
        $seo->description = $input['description'];
        $seo->image = $input['image'];
        $data->seo()->save($seo);

        $response = [
            'notifTitle'=>'SEO Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        $data = Cache::whereIn('id', $input['ids'])->get();
        $names = [];
        foreach ($data as $d) {
            $names[] = $d->key;
        }
        // $log = 'deletes a new cache "' . implode(', ', $names) . '"';
        // Activity::create($log);

        Cache::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminCache')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/cache', array('as'=>'adminCache','uses'=>'Admin\CacheController@index'));
Route::get('admin/cache/create', array('as'=>'adminCacheCreate','uses'=>'Admin\CacheController@create'));
Route::post('admin/cache/', array('as'=>'adminCacheStore','uses'=>'Admin\CacheController@store'));
Route::get('admin/cache/{id}/show', array('as'=>'adminCacheShow','uses'=>'Admin\CacheController@show'));
Route::get('admin/cache/{id}/view', array('as'=>'adminCacheView','uses'=>'Admin\CacheController@view'));
Route::get('admin/cache/{id}/edit', array('as'=>'adminCacheEdit','uses'=>'Admin\CacheController@edit'));
Route::patch('admin/cache/{id}', array('as'=>'adminCacheUpdate','uses'=>'Admin\CacheController@update'));
Route::post('admin/cache/seo', array('as'=>'adminCacheSeo','uses'=>'Admin\CacheController@seo'));
Route::delete('admin/cache/destroy', array('as'=>'adminCacheDestroy','uses'=>'Admin\CacheController@destroy'));
*/
}

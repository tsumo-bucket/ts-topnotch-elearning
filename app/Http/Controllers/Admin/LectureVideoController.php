<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LectureVideoRequest;

use Acme\Facades\Activity;

use App\Lecture;
use App\LectureVideo;
use App\Seo;

use DateTime;

class LectureVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function store(Request $request)
    {
        $input = $request->all();

        $errors = false;

        /* $input['start_date'] = date('Y-m-d H:i:s', strtotime($input['start_date']));
        $input['end_date'] = date('Y-m-d H:i:s', strtotime($input['end_date'])); */
        
        $response = [];
        $response['notifMessage'] = '';

        /* if($input['start_date'] > $input['end_date']){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Start date should be set before the end date.';
            $errors = true;
        } */

        if($input['video_id'] == null){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Please select a video';
            $erros = true;
        }

        if($errors == true){
            $response['notifStatus'] = 'error';
            return response()->json($response);
        }

        $lecture_video = LectureVideo::create($input);

        // $log = 'creates a new lecture_video "' . $lecture_video->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Video successfully added.'
        ];

        return response()->json($response);
    }

    public function destroy(Request $request)
    {
        $input = $request->all();

        $video = LectureVideo::findOrFail($input['id']);
        $lecture_id = $video->lecture_id;
        $video->delete();

        $response = [
            'notifTitle'=>'Delete successful.'
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/lecture_videos', array('as'=>'adminLectureVideos','uses'=>'Admin\LectureVideoController@index'));
Route::get('admin/lecture_videos/create', array('as'=>'adminLectureVideosCreate','uses'=>'Admin\LectureVideoController@create'));
Route::post('admin/lecture_videos/', array('as'=>'adminLectureVideosStore','uses'=>'Admin\LectureVideoController@store'));
Route::get('admin/lecture_videos/{id}/show', array('as'=>'adminLectureVideosShow','uses'=>'Admin\LectureVideoController@show'));
Route::get('admin/lecture_videos/{id}/view', array('as'=>'adminLectureVideosView','uses'=>'Admin\LectureVideoController@view'));
Route::get('admin/lecture_videos/{id}/edit', array('as'=>'adminLectureVideosEdit','uses'=>'Admin\LectureVideoController@edit'));
Route::patch('admin/lecture_videos/{id}', array('as'=>'adminLectureVideosUpdate','uses'=>'Admin\LectureVideoController@update'));
Route::post('admin/lecture_videos/seo', array('as'=>'adminLectureVideosSeo','uses'=>'Admin\LectureVideoController@seo'));
Route::delete('admin/lecture_videos/destroy', array('as'=>'adminLectureVideosDestroy','uses'=>'Admin\LectureVideoController@destroy'));
*/
}

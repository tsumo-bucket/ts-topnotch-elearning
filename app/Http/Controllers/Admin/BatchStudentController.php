<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BatchStudentRequest;

use Acme\Facades\Activity;

use App\Jobs\BatchImport;
use App\Jobs\BatchTransfer;
use App\Jobs\GradesImport;
use App\Jobs\SendEmails;
use App\Jobs\PreWatermark;

use App\Batch;
use App\BatchMetric;
use App\BatchStudent;
use App\Seo;
use App\User;
use App\Watermark;

use App\Exports\GradesSample;
use App\Exports\StudentsExport;
use Maatwebsite\Excel\Facades\Excel;

use Carbon;

class BatchStudentController extends Controller
{

    private $upload_path = './upload/imports/';

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request,$id)
    {
        $batch = Batch::find($id);
        if($keyword = $request->keyword){
            $data = User::where('name', 'LIKE', '%'.$keyword.'%')->whereHas('batch', function($q) use ($id){
                $q->where('batch_id', $id);
            })->paginate(25);
        }
        else{
            $data = User::whereHas('batch', function($q) use($id){
                $q->where('batch_id', $id);
            })->paginate(25);
        }
        
        $pagination = $data->appends($request->except('page'))->links();
        

        return view('admin/batch_students/index')
            ->with('title', 'Students')
            ->with('menu', 'batches')
            ->with('keyword', $keyword)
            ->with('pagination', $pagination)
            ->with('batch', $batch)
            ->with('data', $data);
    }

    public function create($id)
    {
        $batch = Batch::findOrFail($id);

        return view('admin/batch_students/create')
            ->with('title', 'Add Student')
            ->with('menu', 'batches')
            ->with('batch', $batch);
    }

    public function add_student(Request $request)
    {
        $input = $request->all();
        $batch = Batch::findOrFail($input['batch_id']);

        $response = [];
        $response['notifMessage'] = '';
        $errors = false;

        $student_number = '';

        if(strlen($input['student_number']) < 5){
            $zeros = '';
            for ($i=5; $i > strlen($input['student_number']); $i--){
                $zeros .= '0';
            }
            $student_number = date('Y').$zeros.$input['student_number'];
        }
        else{
            $student_number = date('Y').$input['student_number'];
        }

        $other_emails = User::where('status', '!=', 'completed')->pluck('email');

        $check_other_emails = in_array($input['email'], $other_emails->toArray());

        $check_student_number = User::where('student_number', $student_number)->first();

        if(strlen($input['student_number']) != 5){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Student number should have 5 characters';
            $errors = true;
        }

        if($check_student_number != null){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Student number must be unique.';
            $errors = true;
        }                            
        
        if($check_other_emails == true){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Email already in use.';
            $errors = true;
        }

        if($errors == true){
            $response['notifStatus'] = 'error';
            return response()->json($response);
        }

        $new_data = [
            'student_number' => $student_number,
            'name' => ucwords($input['first_name']).' '.ucwords($input['last_name']),
            'first_name' => ucwords($input['first_name']),
            'last_name' => ucwords($input['last_name']),
            'email' => $input['email'],
            'password' => bcrypt(strtolower(str_replace(' ', '', $input['last_name'])).$student_number),
            'contact_no' => $input['contact_no'],
            'address' => $input['address'],
            'birthdate' => $input['birthdate'] != '' ? date('Y-m-d', strtotime($input['birthdate'])) : null,
            'school' => $input['school'],
            'cms' => 0,
            'status' => 'blocked',
            'verified' => 0,
            'type' => 'normal',
        ];

        $user = User::create($new_data);

        $student = new BatchStudent;
        $student->user_id = $user->id;
        $student->batch_id = $batch->id;
        $student->save();

        $response = [
            'notifStatus' => 'success',
            'notifMessage' => 'Student successfully created.',
            'redirect' => route('adminBatchStudents', $batch->id)
        ];

        return response()->json($response);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrFail($id);
        $batch = $user->batch->batch;

        $errors = false;
        $response = [];
        $response['notifMessage'] = '';

        /* $other_emails = User::whereHas('batch', function($q) use($input, $batch){
                            $q->whereHas('batch', function($que) use($batch){
                                $que->where('status', '!=', 'completed');
                            });
                        })->where(function($q) use($input, $user){
                            $q->where('id', '!=', $user->id);
                            $q->where('status', '!=', 'completed');
                        })->pluck('email'); */
        
        $other_emails = User::where(function($q) use($user){
                            $q->where('status', '!=', 'completed');
                            $q->where('id', '!=', $user->id);
                        })->pluck('email');

        $check_other_emails = in_array($input['email'], $other_emails->toArray());

        $check_student_number = User::where('student_number', $input['student_number'])->where('id', '!=', $id)->first();
        
        if(strlen($input['student_number']) != 9){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Student number should have 9 characters.';
            $errors = true;
        }

        if($check_student_number != null){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Student number must be unique.';
            $errors = true;
        }                            
        
        if($check_other_emails == true){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Email already in use.';
            $errors = true;
        }

        if($errors == true){
            $response['notifStatus'] = 'error';
            return response()->json($response);
        }

        $input['name'] = $input['first_name'].' '.$input['last_name'];
        // $input['password'] = bcrypt(strtolower(str_replace(' ', '', $input['last_name'].$input['student_number'])));
        $user->update($input);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }

    public function grades(Request $request,$id){
        $batch = Batch::findOrFail($id);

        if($request->input('keyword') != null){
            $data = User::whereHas('batch', function($q) use($id, $request){
                $q->where('batch_id', $id);
                $q->where('name', 'LIKE', '%'.$request->input('keyword').'%');
            })->paginate(25);
        }
        else{
            $data = User::whereHas('batch', function($q) use($id){
                $q->where('batch_id', $id);
            })->paginate(25);
        }

        $pagination = $data->appends($request->except('page'))->links();
        $batch_metrics = BatchMetric::where('batch_id', $batch->id)->pluck('name', 'slug');

        return view('admin/batch_students/grades')
            ->with('title', 'Grades')
            ->with('menu', 'batches')
            ->with('pagination', $pagination)
            ->with('batch', $batch)
            ->with('batch_metrics', $batch_metrics)
            ->with('data', $data);
    }

    public function create_grade($id, $student_number)
    {
        $user = User::where('student_number', $student_number)->first();
        $batch = $user->batch->batch;
        $batch_metrics = BatchMetric::where('batch_id', $batch->id)->pluck('name', 'slug');

        return view('admin/batch_students/grades-create')
            ->with('title', 'Input Grade')
            ->with('menu', 'batches')
            ->with('batch', $batch)
            ->with('batch_metrics', $batch_metrics)
            ->with('user', $user);
    }

    public function store_grade(Request $request)
    {
        $input = $request->all();

        $user = User::findOrFail($input['user_id']);
        $student = $user->batch;
        $grades = $student->grades ? json_decode($student->grades, true) : null;

        $batch = $student->batch;

        $new_grade = array(
            'raw_score' => $input['raw_score'],
            'percentile_rank' => $input['percentile_rank'],
            'color_grade' => $input['color_grade']
        );

        if($grades != null){
            if(array_key_exists($input['period'], $grades)){

                $existing = BatchMetric::where('slug', $input['period'])->where('batch_id', $batch->id)->first();

                $response = [
                    'notifStatus' => 'error',
                    'notifTitle' => 'Grades for '.$existing->name.' was already submitted'
                ];
                return response()->json($response);
            }

            $grades[$input['period']] = $new_grade;
            $student->grades = json_encode($grades);
        }
        else{
            $student->grades = json_encode(array($input['period'] => $new_grade));
        }
        
        $student->save();

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful',
            'notifMessage'=>'Redirecting to edit.',
            'redirect' => route('adminBatchGradesEdit', [$user->batch->batch->id, $user->student_number])
        ];

        return response()->json($response);
    }

    public function grades_sample($id){
        $batch = Batch::findOrFail($id);
        return Excel::download(new GradesSample($batch->id), 'grades-sample.csv');
    }

    public function edit_grades($id, $student_number)
    {
        $user = User::with('batch')->where('student_number', $student_number)->first();
        $grades = $user->batch->grades ? json_decode($user->batch->grades, true) : null;

        if ($grades != null) {
            foreach ($grades as $period => $values) {
                $grades[$period]['metric'] = BatchMetric::where('slug', $period)->where('batch_id', $user->batch->batch_id)->first()->name;
            }
        }

        return view('admin/batch_students/grades-edit')
            ->with('title', 'Edit Grades')
            ->with('menu', 'batches')
            ->with('grades', $grades)
            ->with('batch', $user->batch->batch)
            ->with('user', $user);
    }

    public function update_grades(Request $request){
        
        $input = $request->all();

        $user = User::findOrFail($input['user_id']);

        $student = BatchStudent::where('user_id', $user->id)->where('batch_id', $input['batch_id'])->first();
        $student->grades = json_encode($input['grades']);
        $student->save();

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Update successful.'
        ];

        return response()->json($response);

    }

    public function remove_grade(Request $request)
    {

        $input = $request->all();

        $student = BatchStudent::where('user_id',$input['user_id'])->first();

        $grades = json_decode($student->grades, true);

        unset($grades[$input['period']]);

        $student->grades = json_encode($grades);
        $student->save();

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Grade removed.',
        ];
        return response()->json($response);
    }

    public function import_grades(Request $request){
        
        $input = $request->all();

        $batch = Batch::findOrFail($input['batch_id']);
        $batch_id = $batch->id;

        $all_students = BatchStudent::where('batch_id', $batch->id)->get();
        $all_student_numbers = [];

        $period = $input['period'];

        if($all_students == null){
            $response = [
                'notifStatus' => 'error',
                'notifTitle' => 'Please import students first.'
            ];
            return response()->json($response);
        }

        if($all_students != null){
            foreach ($all_students as $student) {
                $all_student_numbers[] = $student->user->student_number;
            }
        }


        if($request->hasFile('csv')){
            $ext = $request->file('csv')->getClientOriginalExtension();
            $file = $request->file('csv');
            $filename = str_random(50) . '.' . $ext;

            if($ext == 'csv'){
                // $file->move($this->upload_path, $filename);
                // $filepath = 'upload/imports/' . $filename;
                $filepath = $request->file('csv');

                $csv = array();
                if (($handle = fopen($filepath, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                        $csv[] = $data;
                    }
                    fclose($handle);
                }
                // unlink($filepath);

                foreach (array_chunk($csv, 500, true) as $chunk) {
                    GradesImport::dispatch($chunk, $batch_id, $period, $all_student_numbers);
                }

                $response = [
                    'notifStatus'=>'success',
                    'notifTitle'=>'Import may take a while.',
                    'notifMessage'=>'Please come back later.',
                    // 'redirect'=>route('adminBatchGrades', $input['batch_id'])
                ];
            }

        }
        else{
            $response = [
                'notifTitle'=>'File Upload Required.',
                'notifStatus'=>'error'
            ];
            return response()->json($response);
        }

        return response()->json($response);
    }

    public function export_students(Request $request)
    {
        $input = $request->all();

        $batch = Batch::findOrFail($input['batch_id']);
        $filename = Carbon::today()->format('Y-m-d').' '.$batch->name . '.csv';

        return Excel::download(new StudentsExport($batch->id), $filename);
    }

    public function students_sample(){
        $file_url = 'templates/batch-import-template.csv';

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
        readfile($file_url);
    }

    public function transfers_sample(){
        $file_url = 'templates/batch-transfer-template.csv';

        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
        readfile($file_url);
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        
        $batch = Batch::findOrFail($input['batch_id']);
        $batch_id = $batch->id;

        if($request->hasFile('csv')){
            $ext = $request->file('csv')->getClientOriginalExtension();
            $file = $request->file('csv');

            $filename = str_random(50) . '.' . $ext;

            if($ext == 'csv'){
                // $file->move($this->upload_path, $filename);
                // $filepath = 'upload/imports/' . $filename;
                $filepath = $request->file('csv');

                $csv = array();
                if (($handle = fopen($filepath, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                        $csv[] = $data;
                    }
                    fclose($handle);
                }
                // unlink($filepath);

                foreach (array_chunk($csv, 500, true) as $chunk) {
                    BatchImport::dispatch($chunk, $batch_id, true);
                }

                $response = [
                    'notifStatus'=>'success',
                    'notifTitle'=>'Importing may take a while.',
                    'notifMessage'=>'Please come back later.',
                    'resetForm'=>true,
                    // 'redirect'=>route('adminBatchStudents', $input['batch_id'])
                ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'notifTitle'=>'File Upload Required.',
                'notifStatus'=>'error'
            ];
            return response()->json($response);
        }
    }

    public function transfer(Request $request)
    {
        $input = $request->all();
        
        $batch = Batch::findOrFail($input['batch_id']);
        $batch_id = $batch->id;

        if($request->hasFile('csv')){
            $ext = $request->file('csv')->getClientOriginalExtension();
            $file = $request->file('csv');

            $filename = str_random(50) . '.' . $ext;

            if($ext == 'csv'){
                // $file->move($this->upload_path, $filename);
                // $filepath = 'upload/imports/' . $filename;
                $filepath = $request->file('csv');

                $csv = array();
                if (($handle = fopen($filepath, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                        $csv[] = $data;
                    }
                    fclose($handle);
                }
                // unlink($filepath);

                foreach (array_chunk($csv, 500, true) as $chunk) {
                    BatchTransfer::dispatch($chunk, $batch_id, true);
                }

                $response = [
                    'notifStatus'=>'success',
                    'notifTitle'=>'Transferring may take a while.',
                    'notifMessage'=>'Please come back later.',
                    'resetForm'=>true
                ];
                return response()->json($response);
            }
        }
        else{
            $response = [
                'notifTitle'=>'File Upload Required.',
                'notifStatus'=>'error'
            ];
            return response()->json($response);
        }
    }

    public function reset_password(Request $request){
        $input = $request->all();

        $user = User::findOrFail($input['id']);

        $request->merge(['email' => $user->email]);

        app('App\Http\Controllers\PasswordController')->send_request($request);
        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Password reset link sent.'
        ];
        return response()->json($response);
    }

    public function rerun_watermark(Request $request) {
        $input = $request->all();
        $user = User::findOrFail($input['id']);
        $assetData = $user->batch->batch;

        $assets = [];

        $mainImages = '';
        if ($assetData->assetGroups()->getResults()->where('name','BatchMainImages')->count() > 0) {
            $mainImages = $assetData->assetGroups()->getResults()->where('name', 'BatchMainImages')->first();
            $mainImages->assets = $mainImages->assets()->with('asset')->get()->toArray();
        }


        foreach ($mainImages->assets as $asset) {
            $wm = Watermark::where('user_id', $user->id)->where('asset_id', $asset['asset_id'])->first();
            if ($wm) {
                $wm->delete();
            }
            PreWatermark::dispatch($user->id, $asset['asset_id']);
        }
    }

    public function resend_welcome(Request $request){
        $input = $request->all();

        $user = User::findOrFail($input['id']);

        // copied from SendEmails
        $email = new SendEmails();
        $email->send_welcome_email($user);

        $response = [
            'notifStatus' => 'success',
            'notifTitle' => 'Welcome email will be sent shortly.'
        ];
        return response()->json($response);
    }
    
    public function edit($id)
    {
        $data = BatchStudent::findOrFail($id);
        $seo = $data->seo()->first();


        // $user = User::find($data->user_id);
        // $assetData = $user->batch->batch;

        // $assets = [];

        // $mainImages = '';
        // if ($assetData->assetGroups()->getResults()->where('name','BatchMainImages')->count() > 0) {
        //     $mainImages = $assetData->assetGroups()->getResults()->where('name', 'BatchMainImages')->first();
        //     $mainImages->assets = $mainImages->assets()->with('asset')->get()->toArray();
        // }

        // dd($mainImages->assets);

        return view('admin/batch_students/edit')
            ->with('title', 'Edit Student')
            ->with('menu', 'batch_students')
            ->with('data', $data)
            ->with('seo', $seo);
    }

    public function disable_all(Request $request){
        $input = $request->all();
        $batch = Batch::findOrFail($input['batch_id']);
        $batch_students = BatchStudent::where('batch_id', $batch->id)->get();

        foreach ($batch_students as $student) {
            $user = User::findOrFail($student->user_id);
            $user->status = 'blocked';
            $user->is_active = false;
            $user->save();
        }

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Students disabled successully.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminBatchStudents', $input['batch_id'])
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        DB::table('users')->whereIn('id', $input['ids'])->delete();

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminBatchStudents', $input['batch_id'])
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/batch_students', array('as'=>'adminBatchStudents','uses'=>'Admin\BatchStudentController@index'));
Route::get('admin/batch_students/create', array('as'=>'adminBatchStudentsCreate','uses'=>'Admin\BatchStudentController@create'));
Route::post('admin/batch_students/', array('as'=>'adminBatchStudentsStore','uses'=>'Admin\BatchStudentController@store'));
Route::get('admin/batch_students/{id}/show', array('as'=>'adminBatchStudentsShow','uses'=>'Admin\BatchStudentController@show'));
Route::get('admin/batch_students/{id}/view', array('as'=>'adminBatchStudentsView','uses'=>'Admin\BatchStudentController@view'));
Route::get('admin/batch_students/{id}/edit', array('as'=>'adminBatchStudentsEdit','uses'=>'Admin\BatchStudentController@edit'));
Route::patch('admin/batch_students/{id}', array('as'=>'adminBatchStudentsUpdate','uses'=>'Admin\BatchStudentController@update'));
Route::post('admin/batch_students/seo', array('as'=>'adminBatchStudentsSeo','uses'=>'Admin\BatchStudentController@seo'));
Route::delete('admin/batch_students/destroy', array('as'=>'adminBatchStudentsDestroy','uses'=>'Admin\BatchStudentController@destroy'));
*/
}

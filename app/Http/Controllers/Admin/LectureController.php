<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LectureRequest;

use Acme\Facades\Activity;

use App\BatchLecture;
use App\Lecture;
use App\Seo;
use App\Tag;

class LectureController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index(Request $request)
    {
        if ($name = $request->name) {
            $data = Lecture::where('name', 'LIKE', '%' . $name . '%')->paginate(25);
        } else {
            $data = Lecture::paginate(25);
        }
        $pagination = $data->appends($request->except('page'))->links();

        return view('admin/lectures/index')
            ->with('title', 'Lectures')
            ->with('menu', 'lectures')
            ->with('keyword', $request->name)
            ->with('data', $data)
            ->with('pagination', $pagination);
    }

    public function create()
    {
        return view('admin/lectures/create')
            ->with('title', 'Create lecture')
            ->with('tag_list', Tag::where('taggable_type','App\Lecture')->groupBy('name')->pluck('name','name'))
            ->with('menu', 'lectures');
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        $input['status'] = 'active';

        if(!isset($input['video_id'])){
            $response = [
                'notifStatus'=>'error',
                'notifTitle'=>'Please select a video.'
            ];
            return response()->json($response);
        }

        $lecture = Lecture::create($input);

        if(isset($input['subject'])){
            $lecture->tags()->delete();
            $value = $input['subject'];
            $tag = New Tag;
            if (!empty($value)) {
              $tag->name = $value;
              $lecture->tags()->save($tag);
            }
        }
        
        // $log = 'creates a new lecture "' . $lecture->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Save successful.',
            'notifMessage'=>'Redirecting to edit.',
            'resetForm'=>true,
            'redirect'=>route('adminLecturesEdit', [$lecture->id])
        ];

        return response()->json($response);
    }
    
    public function edit($id)
    {
        $data = Lecture::findOrFail($id);
        $seo = $data->seo()->first();

        return view('admin/lectures/edit')
            ->with('title', 'Edit lecture')
            ->with('menu', 'lectures')
            ->with('tag_list', Tag::where('taggable_type','App\Lecture')->groupBy('name')->pluck('name','name'))
            ->with('data', $data)
            ->with('seo', $seo);
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $lecture = Lecture::findOrFail($id);
        $lecture->update($input);

        if(isset($input['subject'])){
            $lecture->tags()->delete();
            $value = $input['subject'];
            $tag = New Tag;
            if (!empty($value)) {
              $tag->name = $value;
              $lecture->tags()->save($tag);
            }
        }

        // $log = 'edits a lecture "' . $lecture->name . '"';
        // Activity::create($log);

        $response = [
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();
        
        foreach ($input['ids'] as $id) {
            $lecture = Lecture::findOrFail($id);
            
            BatchLecture::where('lecture_id', $lecture->id)->delete();
        }
        Lecture::destroy($input['ids']);

        $response = [
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.',
            'redirect'=>route('adminLectures')
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/lectures', array('as'=>'adminLectures','uses'=>'Admin\LectureController@index'));
Route::get('admin/lectures/create', array('as'=>'adminLecturesCreate','uses'=>'Admin\LectureController@create'));
Route::post('admin/lectures/', array('as'=>'adminLecturesStore','uses'=>'Admin\LectureController@store'));
Route::get('admin/lectures/{id}/show', array('as'=>'adminLecturesShow','uses'=>'Admin\LectureController@show'));
Route::get('admin/lectures/{id}/view', array('as'=>'adminLecturesView','uses'=>'Admin\LectureController@view'));
Route::get('admin/lectures/{id}/edit', array('as'=>'adminLecturesEdit','uses'=>'Admin\LectureController@edit'));
Route::patch('admin/lectures/{id}', array('as'=>'adminLecturesUpdate','uses'=>'Admin\LectureController@update'));
Route::post('admin/lectures/seo', array('as'=>'adminLecturesSeo','uses'=>'Admin\LectureController@seo'));
Route::delete('admin/lectures/destroy', array('as'=>'adminLecturesDestroy','uses'=>'Admin\LectureController@destroy'));
*/
}

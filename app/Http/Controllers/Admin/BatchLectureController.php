<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\BatchLectureRequest;

use Acme\Facades\Activity;

use App\Batch;
use App\BatchLecture;
use App\Lecture;
use App\Seo;
use App\Tag;

use Carbon;

class BatchLectureController extends Controller
{

    public $pagination_count = 15;

    public function __construct()
    {
        $this->middleware('admin');
    }
    
    public function index($id)
    {

        $batch = Batch::findOrFail($id);

        $lectures = Lecture::where('status','active')->orderBy('created_at', 'DESC')->paginate($this->pagination_count);

        $all_lectures_count = count(Lecture::where('status', 'active')->get());

        $tag_list = Tag::where('taggable_type','App\Lecture')->groupBy('name')->pluck('name','name');
        $tag_list->prepend('All', '');

        return view('admin/batch_lectures/index')
            ->with('title', 'Lectures')
            ->with('menu', 'batches')
            ->with('batch', $batch)
            ->with('tag_list', $tag_list)
            ->with('all_count', $all_lectures_count)
            ->with('lectures', $lectures);
    }


    public function filter(Request $request)
    {
        $input = $request->all();

        $name = $input['filter_name'] != null ? $input['filter_name'] : '';
        $subject = $input['filter_subject'] != null ? $input['filter_subject'] : '';

        $lectures = Lecture::where('name', 'LIKE', '%'.$name.'%');

        if($subject != ''){
            $lectures = $lectures->whereHas('tags', function($q) use($subject){
                $q->where('name', $subject);
            });
        }
                                
        $lectures = $lectures->where('status', 'active')->get();

        $response = [
            'lectures' => $lectures
        ];

        return response()->json($response); 
    }

    public function get_lectures($id){
        
        $batch = Batch::findOrFail($id);

        $data = [];

        foreach ($batch->lectures as $l) {
            $data[] = array(
                'title' => $l->lecture->name,
                'start' => $l->start_date,
                'end' => date('Y-m-d', strtotime($l->end_date)).'T23:59:59',
                'id' => $l->id,
                'block' => 'true'
            );
        }

        return json_encode($data);
    }

    public function get(Request $request){
        $input = $request->all();

        $lecture = BatchLecture::findOrFail($input['id']);

        $response = [
            'lecture' => $lecture
        ];

        return response()->json($response);
    }
    
    public function store(Request $request)
    {
        $input = $request->all();

        $new_data = array(
            'batch_id' => $input['batch_id'],
            'lecture_id' => $input['lecture_id'],
            'start_date' => date('Y-m-d H:i', strtotime($input['date_clicked'].$input['start_time'])),
            'end_date' => date('Y-m-d H:i', strtotime($input['date_dragged'].$input['end_time'])),
        );

        BatchLecture::insert($new_data);

        // $log = 'creates a new batch_lecture "' . $batch_lecture->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifMessage'=>'Refreshing page.',
            'notifTitle'=>'Lecture added.',
        ];

        return response()->json($response);
    }
    
    
    public function update(Request $request)
    {
        $input = $request->all();
        $batch_lecture = BatchLecture::findOrFail($input['id']);
        $input['start_date'] = date('Y-m-d H:i', strtotime($input['date_clicked'].$input['start_time']));
        $input['end_date'] = date('Y-m-d H:i', strtotime($input['date_dragged'].$input['end_time']));

        $batch_lecture->update($input);

        // $log = 'edits a batch_lecture "' . $batch_lecture->name . '"';
        // Activity::create($log);

        $response = [
            'notifStatus'=>'success',
            'notifMessage'=>'Refreshing page.',
            'notifTitle'=>'Save successful.',
        ];

        return response()->json($response);
    }
    
    public function destroy(Request $request)
    {
        $input = $request->all();

        BatchLecture::destroy($input['id']);

        $response = [
            'notifStatus'=>'success',
            'notifTitle'=>'Delete successful.',
            'notifMessage'=>'Refreshing page.'
        ];

        return response()->json($response);
    }
    
/** Copy/paste these lines to app\Http\routes.base.php 
Route::get('admin/batch_lectures', array('as'=>'adminBatchLectures','uses'=>'Admin\BatchLectureController@index'));
Route::get('admin/batch_lectures/create', array('as'=>'adminBatchLecturesCreate','uses'=>'Admin\BatchLectureController@create'));
Route::post('admin/batch_lectures/', array('as'=>'adminBatchLecturesStore','uses'=>'Admin\BatchLectureController@store'));
Route::get('admin/batch_lectures/{id}/show', array('as'=>'adminBatchLecturesShow','uses'=>'Admin\BatchLectureController@show'));
Route::get('admin/batch_lectures/{id}/view', array('as'=>'adminBatchLecturesView','uses'=>'Admin\BatchLectureController@view'));
Route::get('admin/batch_lectures/{id}/edit', array('as'=>'adminBatchLecturesEdit','uses'=>'Admin\BatchLectureController@edit'));
Route::patch('admin/batch_lectures/{id}', array('as'=>'adminBatchLecturesUpdate','uses'=>'Admin\BatchLectureController@update'));
Route::post('admin/batch_lectures/seo', array('as'=>'adminBatchLecturesSeo','uses'=>'Admin\BatchLectureController@seo'));
Route::delete('admin/batch_lectures/destroy', array('as'=>'adminBatchLecturesDestroy','uses'=>'Admin\BatchLectureController@destroy'));
*/
}

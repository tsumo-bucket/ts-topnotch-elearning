<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Auth;

use App\Activity;
use App\Banner;
use App\User;
use Carbon\Carbon;

use View;

class AuthController extends Controller
{

    public function __construct()
    {
        View::share('banners', Banner::where('published', 'published')->get());
    }

    public function login(Request $request)
    {

        if (Auth::check()) {
            $user = User::findOrFail(Auth::user()->id);
            // save an activity based on user ID + IP
            $activity = new Activity;
            $activity->user_id = $user->id;
            $activity->value_from = $request->ip();
            $activity->value_to = json_encode($request->header());
            $activity->identifier_value = $user->id;
            $activity->log = 'Logged in from: '. $request->ip(). ' User:'. $user->name ;
            $user->activities()->save($activity);

            if($user->type == 'normal'){
                
                return redirect()->route('portalDashboard');
            }
            return redirect()->route('adminDashboard');
        }
        return view('admin/auth/login')
            ->with('title', 'Topnotch Online - Login');
    }

    public function authenticate(Request $request)
    {
        $input = $request->all();
        if (Auth::attempt(['email' => $input['email'], 'password' => $input['password'], 'status' => 'active'])) {
            $user = User::findOrFail(Auth::user()->id);
            
            if($user->is_active){
                Auth::logoutOtherDevices($input['password']);
            }

            $user->last_ip = $request->ip();
            $user->last_login = new Carbon();
            $user->is_active = true;

            $user->save();
            
            // save an activity based on user ID + asset ID
            $activity = new Activity;
            $activity->user_id = $user->id;
            $activity->value_from = $request->ip();
            $activity->value_to = json_encode($request->header());
            $activity->identifier_value = $user->id;
            $activity->log = 'Logged in from: '. $request->ip(). ' User:'. $user->name ;
            $user->activities()->save($activity);

            if($user->type == 'normal'){
                session()->flash('logged_in', true);
                return redirect()->intended(route('portalDashboard'));
            }
            return redirect()->intended(route('adminDashboard'));
        } else {
            $response = array(
                'notifStatus' => 'error',
                'notifMessage' => 'Email and/or password invalid.'
            );
            return redirect()->back()->with($response)->withInput();
        }
    }

    public function check_auth(){

        $logout = false;

        if (!Auth::check()) {
            $logout = true;
        }
        else{
            $user = User::find(Auth::user()->id);
    
            if($user->status != 'active'){
                $logout = true;
            }
        }

        $response = [
            'logout' => $logout
        ];

        return response()->json($response);
    }

    public function logout()
    {
        if(!Auth::check()){
            $errors = new MessageBag(['message' => ['Unauthorized Access.']]);
            return redirect()->route('login')->withErrors($errors)->withInput();
        }

        $user = User::findOrFail(Auth::user()->id);
        $user->is_active = false;
        $user->save();

        session()->flush();
        session()->regenerate();

        Auth::logout();
        
        // session()->flush();
        return redirect()->route('login');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Acme\Facades\Seo;

use App\Sample;
use App\Preview;
use App\Page;
use App\PageContent;

class HomeController extends Controller
{
    public function index()
    {
        return redirect()->route('login');  
    }

    public function preview($slug='') 
    {
        $preview = Preview::with('reference')->where('slug', $slug)->first();
        $preview_content = json_decode($preview->value, true);
        $content = PageContent::select('page_id')->findOrFail($preview->reference_id);
        $page = Page::findOrFail($content->page_id);
        
        if (view()->exists($page->slug)) {
            $view_name = 'app/' . $page->slug;
        } else {
            $view_name = 'app/home';
        }
        
        return view($view_name)
        ->with('data', $preview_content);
    }

    public function seo()
    {
        $sample = Sample::findOrFail(1);
        $seo = Seo::get($sample);

        return view('app/home');
    }
}

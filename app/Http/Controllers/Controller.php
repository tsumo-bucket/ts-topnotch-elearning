<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Announcement;

use Carbon;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function get_banner($batch_id)
    {   
        $now = Carbon::now();

		$banner = Announcement::where('category', 'banner')
								->where(function($q) use($batch_id){
									$q->whereHas('batches', function($query) use($batch_id) {
										$query->where('batch_id', $batch_id);
									})
									->orWhereDoesntHave('batches');
								})
								->where(function($q) use($now){
									$q->where('start_date', null)
										->orWhere('start_date', '<=', $now);
								})
								->where(function($q) use($now){
									$q->where('end_date', null)
										->orWhere('end_date', '>', $now);
								})
								->where('category', 'banner')
								->orderBy('created_at', 'DESC')
								->first();
                                    
        return $banner;
	}
}

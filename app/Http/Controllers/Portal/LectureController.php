<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Asset;
use App\Batch;
use App\BatchLecture;
use App\Lecture;
use App\LectureVideo;
use App\Tag;
use App\User;
use App\Video;
use App\Activity;
use Carbon;

class LectureController extends Controller
{

    public function __construct(){
        $this->middleware('portal');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $batch = $user->batch->batch;

        $lectures = BatchLecture::where('batch_id', $batch->id)
            ->where(function($q){
                $q->where('start_date', '<', Carbon::now());
                $q->where('end_date', '>', Carbon::now());
            })->get();

        $lecture_ids = $lectures->pluck('lecture_id');
        $subjects = Tag::join('lectures as l', 'taggable_id', '=', 'l.id')
            ->select('tags.*', 'l.name as lecture_name')
            ->where('taggable_type','App\Lecture')
            ->whereIn('taggable_id', $lecture_ids)
            ->orderBy('tags.name', 'ASC')
            ->orderBy('lecture_name')->get();

        $filtered_subjects = array();
        foreach ($subjects as $s) {
            if (empty($filtered_subjects[$s->name])) {
                $filtered_subjects[$s->name] = array();
            }
            array_push($filtered_subjects[$s->name], $s);
        }

        return view('portal.lectures.index')
            ->with('title', 'Lectures')
            ->with('menu', 'lectures')
            ->with('menu', 'lectures')
            ->with('filtered_subjects', $filtered_subjects)
            ->with('data', $lectures);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $batch = $user->batch->batch;

        $batch_lecture = BatchLecture::findOrFail($id);

        // save an activity based on user ID + lecture ID
        $activity = new Activity;
        $activity->user_id = $user->id;
        $activity->value_from = $request->ip();
        $activity->value_to = json_encode($request->header());
        $activity->identifier_value = $batch_lecture->lecture_id;
        $activity->log = 'Watched: '.$batch_lecture->lecture->name.' ID: '. $batch_lecture->lecture_id.' User: '. $user->name;
        $batch_lecture->lecture->activities()->save($activity);

        return view('portal.lectures.show')
            ->with('title', ucwords($batch_lecture->lecture->name))
            ->with('menu', 'lectures')
            ->with('user', $user)
            ->with('data', $batch_lecture);
    }

    /**
     * Watch the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function watch($id, $video_id)
    {
        $user = User::findOrFail(Auth::user()->id);
        $batch = $user->batch->batch;

        $batch_lecture = BatchLecture::where('id', $id)->first();
        $video = LectureVideo::where('lecture_id', $batch_lecture->lecture_id)
                                ->whereHas('video', function($q) use($video_id){
                                    $q->where('id', $video_id);
                                })->first();

        return view('portal.lectures.watch')
            ->with('title', 'Watch')
            ->with('menu', 'lectures')
            ->with('lecture', $batch_lecture)
            ->with('data', $video);
    }


    public function check_lecture($id)
    {
        $lecture = BatchLecture::findOrFail($id);

        $valid = (strtotime($lecture->start_date) <= time() && strtotime($lecture->end_date) >= time()) ? true : false;

        $response = [
            'status' => $valid
        ];

        return response()->json($response);
    }

}

<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Asset;
use App\Batch;
use App\Lecture;
use App\User;

use Carbon;

class ResourceController extends Controller
{
    
    public function __construct(){
        $this->middleware('portal');
    }

    public function index(Request $request){
        $user = User::find(Auth::user()->id);
        $data = $user->batch->batch;

        $assets = [];

        $now = Carbon::now();

        $mainImages = '';
        if ($data->assetGroups()->getResults()->where('name','BatchMainImages')->count() > 0) {
            $mainImages = $data->assetGroups()->getResults()->where('name', 'BatchMainImages')->first();
            // $mainImages->assets = $mainImages->assets()->with('asset')->get()->toArray();
            $mainImages->assets = $mainImages->assets()
                                            ->select(['asset_group_items.*', 'assets.name as name'])
                                            ->leftJoin('assets', 'asset_group_items.asset_id', '=', 'assets.id')
                                            ->where(function($q) use($now){
                                                $q->where('asset_group_items.start_date', null)
                                                    ->orWhere('asset_group_items.start_date', '<=', $now);
                                            })
                                            ->where(function($q) use($now){
                                                $q->where('asset_group_items.end_date', null)
                                                    ->orWhere('asset_group_items.end_date', '>=', $now);
                                            })
                                            ->orderBy('name', 'asc')
                                            ->with('asset')
                                            ->get();
        }

        foreach($mainImages->assets as $a){

            $asset = $a['asset'];
            $asset['start_date'] = $a['start_date'];
            $asset['end_date'] = $a['end_date'];

            $assets[strtolower($a['asset']['name'])] = $asset;
        }

        ksort($assets);

        return view('portal.resources.index')
                    ->with('user', $user)
                    ->with('title', 'Resources')
                    ->with('menu', 'resources')
                    ->with('data', $assets);
    }

    public function show($id){

        $asset = Asset::findOrFail($id);
        return view('portal.resources.show')
                    ->with('title', ucwords($asset->name))
                    ->with('menu', 'resources')
                    ->with('data', $asset);
    }

}

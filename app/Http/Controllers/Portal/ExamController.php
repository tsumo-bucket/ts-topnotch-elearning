<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Page;
use App\User;
use App\Activity;
use App\PageCategory;

class ExamController extends Controller
{
    public $category;

    public function __construct()
    {
        $this->category = PageCategory::where('slug', 'exams')->first();
    }

    public function index()
    {
        $user = User::findOrFail(auth()->user()->id);
        $batch = $user->batch->batch;
        $category = $this->category;
        $exams = Page::where('page_category_id', $category->id)
        ->where('published', 'published')
        ->where(function ($query) use($batch) {
            return $query->whereHas('batches', function($query) use($batch) {
                $query->where('batch_id', $batch->id);
            })
            ->orWhereDoesntHave('batches');
        })
        ->get();

        // ->where(function($q) use($category){
        //     $q->where('page_category_id', $category->id);
        //     $q->where('published', 'published');
        // })
        return view('portal.exams.index')
            ->with('exams', $exams)
            ->with('title', 'Exams')
            ->with('menu', 'exams');
    }

    public function show($id, Request $request)
    {
        $data = Page::findOrFail($id);

        // save an activity based on user ID + exam ID
        $user = User::findOrFail(Auth::user()->id);
        $activity = new Activity;
        $activity->user_id = $user->id;
        $activity->value_from = $request->ip();
        $activity->value_to = json_encode($request->header());
        $activity->identifier_value = $data->id;
        $activity->log = 'Opened exam: '.$data->name.' ID: '. $data->id. ' User: '. $user->name;
        $data->activities()->save($activity);

        return view('portal.exams.show')
            ->with('data', $data)
            ->with('title', $data->name)
            ->with('menu', 'exams');
    }
}

<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Announcement;
use App\Asset;
use App\Batch;
use App\BatchLecture;
use App\Lecture;
use App\LectureVideo;
use App\PageCategory;
use App\Page;
use App\User;
use App\Tag;
use App\Video;

use Carbon;
use View;

class DashboardController extends Controller
{

    public function __construct(){
		$this->middleware('portal');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::user()->id);
		$batch = $user->batch->batch;
		
		session()->put('banner', parent::get_banner($batch->id));

		$lectures = BatchLecture::where('batch_id', $batch->id)
								->where(function($q){
									$q->where('start_date', '<', Carbon::now());
									$q->where('end_date', '>', Carbon::now());
								})
								->get();

		$lecture_ids = $lectures->pluck('lecture_id');
		$subjects = Tag::join('lectures as l', 'taggable_id', '=', 'l.id')
						->select('tags.*', 'l.name as lecture_name')
						->where('taggable_type','App\Lecture')
						->whereIn('taggable_id', $lecture_ids)
						->orderBy('tags.name', 'ASC')
						->orderBy('lecture_name')
						->get();

		$filtered_subjects = array();
		foreach ($subjects as $s) {
			if (empty($filtered_subjects[$s->name])) {
				$filtered_subjects[$s->name] = array();
			}
			array_push($filtered_subjects[$s->name], $s);
		}

		$now = Carbon::now();

		$announcement = Announcement::where('category', 'announcement')
									->where(function($q) use($batch){
										$q->whereHas('batches', function($query) use($batch) {
											$query->where('batch_id', $batch->id);
										})
										->orWhereDoesntHave('batches');
									})
									->where(function($q) use($now){
										$q->where('start_date', null)
											->orWhere('start_date', '<=', $now);
									})
									->where(function($q) use($now){
										$q->where('end_date', null)
											->orWhere('end_date', '>', $now);
									})
									->orderBy('created_at', 'DESC')
									->first();
				
		$terms_and_conditions_category = PageCategory::where('slug', 'terms-and-conditions')->first();
		
		if($terms_and_conditions_category == null){
			app('App\Http\Controllers\Admin\LegalController')->seed();
			$terms_and_conditions_category = PageCategory::where('slug', 'terms-and-conditions')->first();
		}
        $terms_and_conditions = Page::where('page_category_id', $terms_and_conditions_category->id)->get();
		
        return view('portal/dashboard/index')
            ->with('user', $user)
            ->with('title', 'Lectures for the Day')
            ->with('menu', 'dashboard')
			->with('announcement', $announcement)
            ->with('filtered_subjects', $filtered_subjects)
            ->with('verified', $user->verified)
			->with('terms_and_conditions', $terms_and_conditions)
			->with('boards_date', $batch->boards_date)
            ->with('data', $lectures);
    }

    public function get_page_content($slug)
    {
        $page = Page::where('slug', $slug)
					->where('published', 'published')
					->with([
							'clientContents.controls.asset',
                            'clientContents.clientItems.controls.asset',
                            'seo'
                        ]);
        $page = $page->first();
        
        $data = [];
        // $data['seo']['title'] = @$page->seo->title;
		// $data['seo']['description'] = @$page->seo->description;
        // $data['published'] = @$page->published;
        
        if ($page) {
			foreach ($page->clientContents as $contentIndex => $content) {
				if ($page->allow_add_contents == 1) {
					$keyName = 'contents';			
					$data[$keyName][$contentIndex] = [
						"created_at" => $content->created_at
					];
				} else {
					$keyName = $content->slug;
					$data[$keyName] = [
						"created_at" => $content->created_at
					];
				}
                
				foreach ($content->controls as $key => $value) {
                    $data[$keyName]['slug'] = $keyName;
					if ($page->allow_add_contents == 1) {
						if ($value->type == 'asset' && @$value->asset) {
							$data[$keyName][$contentIndex][$value->name] = $value->asset;
						} 
						
						else if ($value->type == 'video' && @$value->video) {
							$data[$keyName][$contentIndex][$value->name] = $value->video;
						} 
						
						else {
							$data[$keyName][$contentIndex][$value->name] = $value->value;
						}
					} else {
						if ($value->type == 'asset' && @$value->asset) {
							$data[$keyName][$value->name] = $value->asset;
						} 
						
						else if ($value->type == 'video' && @$value->video) {
							$data[$keyName][$value->name] = $value->video;
						} 

						else {
							$data[$keyName][$value->name] = $value->value;

							if ($value->name == "min_denomination" || $value->name == "max_denomination" && isset($currency)) {
								$data[$keyName][$value->name] = ($value->value / $currency->peso_value);
								$data[$keyName]["currency"] = $currency;
							}
						}
					}
				}

				if (count($content->clientItems) > 0) {
					foreach ($content->clientItems as $key => $item) {
						$tmpItem = [];
						foreach ($item->controls as $key => $value) {
							if ($value->type == 'asset' && @$value->asset) {
								$tmpItem[$value->name] = $value->asset;
							} else {
								$tmpItem[$value->name] = $value->value;
							}
						}
						$tmpItem['slug'] = $item->slug;
						if ($page->allow_add_contents == 1) {
							if ($content->allow_add_items == 1) $data[$keyName][$contentIndex]['items'][] = $tmpItem;
							else $data[$keyName][$contentIndex][$item->slug] = $tmpItem;
						} else {
							if ($content->allow_add_items == 1) $data[$keyName]['items'][] = $tmpItem;
							else $data[$keyName][$item->slug] = $tmpItem;
						}
                    }
                    // return $data;
				}		
			}
        }

        return $data;
    }

}

<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Announcement;
use App\User;

use Carbon;

class AnnouncementController extends Controller
{
    public function __construct(){
        $this->middleware('portal');
    }

    public function index(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);
        $batch = $user->batch->batch;
        // $data = Announcement::orderBy('created_at', 'DESC')->get();

        $now = Carbon::now();

        $data = Announcement::where('category', 'announcement')
                                ->where(function($q) use($batch){
                                    $q->whereHas('batches', function($query) use($batch) {
                                        $query->where('batch_id', $batch->id);
                                    })
                                    ->orWhereDoesntHave('batches');
                                })
                                ->where(function($q) use($now){
                                    $q->where('start_date', null)
                                        ->orWhere('start_date', '<=', $now);
                                })
                                ->where(function($q) use($now){
                                    $q->where('end_date', null)
                                        ->orWhere('end_date', '>', $now);
                                })
                                ->orderBy('created_at', 'DESC')
                                ->get();

        return view('portal/announcements/index')
            ->with('title', 'Announcements')
            ->with('menu', 'announcements')
            ->with('data', $data);
    }

    public function show($id)
    {
        $data = Announcement::findOrFail($id);

        return view('portal/announcements/show')
            ->with('title', ucwords($data->title))
            ->with('menu', 'announcements')
            ->with('data', $data);
    }
}

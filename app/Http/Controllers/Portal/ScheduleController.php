<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\BatchLecture;
use App\LectureVideo;

use Carbon\CarbonPeriod;

class ScheduleController extends Controller
{
    //
    public function __construct(){
        $this->middleware('portal');
    }

    public function index(){
        
        $user = User::findOrFail(Auth::user()->id);
        $batch = $user->batch->batch;
        
        $dates = $this->get_days_of_week(date('Y-m-d'));

        $batch_lectures = BatchLecture::where('batch_id', $batch->id)->orderBy('start_date', 'ASC')->get();

        return view('portal.schedule.index')
                ->with('title', 'Schedule')
                ->with('menu', 'schedules')
                ->with('dates', $dates)
                ->with('data', $batch_lectures);
    }

    public function get_days_of_week($date){

        $start_time = strtotime($date);
        
        $start_date = (date('w',$start_time) == 0) ? $start_time : strtotime('last sunday', $start_time);

        $start = date('Y-m-d', $start_date);
        $end = date('Y-m-d', strtotime('next saturday', $start_date));

        $days_of_week = [];

        $flag = $start;

        while($flag <= $end){
            $days_of_week[] = $flag;
            $flag = date('Y-m-d', strtotime($flag.' +1 day'));
        }

        return $days_of_week;
    }

}

<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\BatchMetric;

class ProfileController extends Controller
{
    //
    public function __construct(){
        $this->middleware('portal');
    }

    public function index(){
        
        $user = User::findOrFail(Auth::user()->id);
        $grades = $user->batch->grades ? json_decode($user->batch->grades, true) : null;

        if ($grades != null) {
            foreach ($grades as $period => $values) {
                $grades[$period]['metric'] = BatchMetric::where('slug', $period)->where('batch_id', $user->batch->batch_id)->first();
            }
        }

        return view('portal.profile.index')
            ->with('title', 'Profile')
            ->with('grades', $grades)    
            ->with('data', $user);
    }

    public function accept_legal()
    {
        $user = User::findOrFail(Auth::user()->id);

        $user->verified = true;
        $user->save();
    }

    public function edit_password(){
        $user = User::findOrFail(Auth::user()->id);

        return view('portal.profile.password')
            ->with('title', 'Change Password')
            ->with('data', $user);
    }

    public function update_password(Request $request){
        
        $input = $request->all();

        $response = [];
        $response['notifMessage'] = '';
        $errors = null;

        $user = User::findOrFail($input['user_id']);

        if(!Hash::check($input['current_password'], $user->password)){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'Current Password is invalid.';
            $errors = true;
        }

        if($input['new_password'] != $input['confirm_password']){
            if($response['notifMessage'] != '') $response['notifMessage'] .= '<br/>';
            $response['notifMessage'] .= 'New and Confirm Passwords do not match.';
            $errors = true;
        }

        if($errors == true){
            $response['notifStatus'] = 'error';
        }
        else{

            $user->password = bcrypt($input['new_password']);
            $user->save();

            $response = [
                'notifStatus' => 'success',
                'notifTitle' => 'Password updated successfully',
                'notifMessage' => 'Logging out.',
                'redirect' => route('portalProfile')
            ];
        }

        return response()->json($response);
    }
}

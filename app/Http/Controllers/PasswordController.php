<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Acme\Facades\Option;

use App\Banner;
use App\User;
use App\PasswordReset;

use SumoMail;

use View;

class PasswordController extends Controller
{
    public function __construct()
    {
        View::share('banners', Banner::where('published', 'published')->get());
    }

    public function forgot_password(){

        return view('admin.auth.forgot-password')
            ->with('title', 'Topnotch - Forgot Password');

    }

    public function send_request(Request $request){
        
        $input = $request->all();

        $user = User::where('email', $input['email'])
                        ->where('status', 'active')
                        ->first();

        $response = [];

        if($user != null){

            $password_reset = array(
                'token' => md5(uniqid()),
                'email' => $input['email']
            );
            $reset = PasswordReset::create($password_reset);
            
            $options = Option::email();
            $params=[];

            $params['from_email'] = $options['sender-email'];
            $params['from_name'] =$options['sender-name'];
            $params['subject'] = $options['name'] . ': Password Reset Link';
            $params['to'] = $user->email;
            $params['replyTo'] =  $options['email'];
            $params['debug'] = false; 
    
            $data = [];
            $data['user'] = $user;
            $data['token'] = $reset->token;
    
            SumoMail::send('emails.password', $data, $params);

            $response = array(
                'notifStatus' => 'success',
                'notifMessage' => 'Password reset link sent was sent to your email.'
            );

        }
        else{
            $response = array(
                'notifStatus' => 'error',
                'notifMessage' => 'Invalid credentials.'
            );
        }

        return redirect()->route('forgotPassword')->with($response);
    }

    public function reset_password(Request $request, $token){

        $input = $request->all();
        $reset = PasswordReset::where('token', $token)->where('email', $input['email'])->first();
        
        if($reset == null){
            
            $response = array(
                'notifStatus' => 'error',
                'notifMessage' => 'Password Reset Link is already expired.'
            );
            return redirect()->route('forgotPassword')->with($response);
        }
        
        $user = User::where('email', $reset->email)
                    ->where('status', 'active')
                    ->first();
        
        return view('admin.auth.password-reset')
                ->with('title', 'Reset Password')
                ->with('reset', $reset)
                ->with('user', $user);
    }

    public function update_password(Request $request){
        $input = $request->all();
        
        if($input['new_password'] != $input['confirm_password']){

            $response = array(
                'notifStatus' => 'error',
                'notifMessage' => 'Passwords do not match.'
            );

            return redirect()->back()->with($response);
        }
        
        $user = User::findOrFail($input['user_id']);
        $user->password = bcrypt($input['new_password']);
        $user->save();
        
        PasswordReset::where('token', $input['token'])->delete();

        $response = array(
            'notifStatus' => 'success',
            'notifMessage' => 'Password updated successfully.'
        );

        return redirect()->route('login')->with($response);

    }

}

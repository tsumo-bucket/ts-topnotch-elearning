<?php 

namespace App\Acme\Asset;

use Illuminate\Support\Collection;
use App\Asset as AssetModel;

use App\AssetGroup;
use App\AssetGroupItem;

use Carbon;

class Asset
{
    /*
        Save Group of Assets 
        @param db = Instance of the related model
        @param data = json encoded data of the group to be created/updated
                      including it's assets
    */
    public function sync($db, $data = "")
    { 
        try {
            $groupData = json_decode($data);
        } catch(Exception $e) {}

        if (isset($groupData) && @$groupData) {
            try {
                $group = [];
                $group = $db->assetGroups()->updateOrCreate(
                                    ['id'=>$groupData->id], 
                                    ['name'=>$groupData->name]);

                $group->assets()->delete();
            } catch(Exception $e) {}

            if (@$groupData->assets) {
                foreach ($groupData->assets as $key => $val) {
                    $asset = New AssetGroupItem;
                    $asset->asset_id = $val->asset_id;
                    $asset->order = $val->order;
                    $asset->start_date = @$val->start_date ? Carbon::parse(@$val->start_date) : null;
                    $asset->end_date = @$val->end_date ? Carbon::parse(@$val->end_date) : null;
                    $group->assets()->save($asset);
                }

                return [
                    'id' => @$group->id,
                    'header' => @$groupData->id
                ];
            }
        }

        return ['id'=>'0','header'=>'0'];
    }
    
    public function filter_by_tags($assets, $tags, $not_in = false)
    {
        $filteredAssets = $assets->filter(function ($item) use($tags, $not_in) {
            if ($item->asset->tags->count() == 0 && $not_in) return true;
            $isHasTag = false;
            foreach ($item->asset->tags as $key => $tag) {
                $isHasTag = $not_in ? !in_array($tag->name, $tags) : in_array($tag->name, $tags);
            }
            return $isHasTag;
        });

        return $filteredAssets;
    }
}
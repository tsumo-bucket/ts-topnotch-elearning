<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Inquiry extends Mailable
{
    use Queueable, SerializesModels;

    protected $input;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(isset($this->input['from'])){
            return $this->from($this->input['from'])
                        ->subject($this->input['subject'])
                        ->view('emails.inquiry')
                        ->with('content', $this->input['message']);
        }
        else{
            return $this->view('emails.inquiry')
                        ->subject($this->input['subject'])
                        ->with('content', $this->input['message']);
        }
    }
}

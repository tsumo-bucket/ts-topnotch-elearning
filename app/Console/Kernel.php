<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Jobs\SendEmails;
use App\Jobs\SetVideoSettings;
use App\Jobs\TriggerVideoSync;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Start queue jobs
        $schedule->command('queue:listen --timeout=1800 --tries=2')->name('queue_listen')->withoutOverlapping()->hourly()->runInBackground();
        
        // Send emails
        $schedule->job(new SendEmails)
            ->name('send_emails')
            ->withoutOverlapping(5)
            ->everyFiveMinutes()
            ->onOneServer();
        
        // Send emails
        $schedule->job(new TriggerVideoSync)
            ->name('trigger_video_sync')
            ->hourly()
            ->onOneServer();
        
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

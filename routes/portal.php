<?php

/*
|--------------------------------------------------------------------------
| PORTAL Routes
|--------------------------------------------------------------------------
|
| (do not modify here. if you have updates to base, update laravel-base repo instead)
|
*/

/* DASHBOARD */
Route::get('/', array('as'=>'portalDashboard','uses'=>'DashboardController@index')); 
Route::get('/accept-legal', array('as'=>'portalAcceptLegal', 'uses'=>'ProfileController@accept_legal'));

/* ANNOUNCEMENTS */
Route::get('/announcements', array('as'=>'portalAnnouncements', 'uses'=>'AnnouncementController@index'));
Route::get('/announcements/{id}', array('as'=>'portalAnnouncementsShow', 'uses'=>'AnnouncementController@show'));

/* RESOURCES */
Route::get('/resources', array('as'=>'portalResources', 'uses'=>'ResourceController@index'));
Route::get('/resources/{id}/view', array('as'=>'portalResourcesShow', 'uses'=>'ResourceController@show'));

/* LECTURES */
Route::get('/lectures', array('as'=>'portalLectures', 'uses'=>'LectureController@index'));
Route::get('/lectures/{id}/show', array('as'=>'portalLecturesShow', 'uses'=>'LectureController@show'));
Route::get('/lectures/{id}/watch/{video_id}', array('as'=>'portalLecturesWatch', 'uses'=>'LectureController@watch'));
Route::get('/lectures/check/{id}', array('as'=>'portalCheckVideo', 'uses'=>'LectureController@check_lecture'));

/* PROFILE */
Route::get('/profile', array('as'=>'portalProfile', 'uses'=>'ProfileController@index'));
Route::get('/profile/password/edit', array('as'=>'portalPasswordEdit', 'uses'=>'ProfileController@edit_password'));
Route::post('/profile/password/update', array('as'=>'portalPasswordUpdate', 'uses'=>'ProfileController@update_password'));

/* SCHEDULE */
Route::get('/schedule', array('as'=>'portalSchedule', 'uses'=>'ScheduleController@index'));

/* EXAMS */
Route::get('/exams', array('as'=>'portalExams', 'uses'=>'ExamController@index'));
Route::get('/exams/{id}', array('as'=>'portalExamsShow', 'uses'=>'ExamController@show'));
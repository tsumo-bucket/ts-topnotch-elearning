<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', array('as'=>'test','uses'=>'HomeController@amp'));
Route::get('/', array('as'=>'homePage','uses'=>'HomeController@index'));
Route::get('page/{slug}/preview', array('as'=>'pagePreview','uses'=>'HomeController@preview'));
Route::get('/seo', 'HomeController@seo');

/* AUTH */
Route::get('/auth-check', array('as' => 'checkAuth', 'uses' => 'AuthController@check_auth'));
Route::get('/login', array('as'=>'login','uses'=>'AuthController@login'));
Route::post('login', array('as'=>'authenticate','uses'=>'AuthController@authenticate'));
Route::get('/logout', array('as'=>'logout','uses'=>'AuthController@logout'));

/* PASSWORD */
Route::get('/forgot-password', array('as'=>'forgotPassword', 'uses'=>'PasswordController@forgot_password'));
Route::post('/forgot-password/submit', array('as'=>'forgotPasswordSubmit', 'uses'=>'PasswordController@send_request'));
Route::get('/password/reset/{token}', array('as'=>'passwordReset', 'uses'=>'PasswordController@reset_password'));
Route::post('/password/reset/submit', array('as'=>'passwordResetSubmit', 'uses'=>'PasswordController@update_password'));

/* DOWNLOAD PDF WITH MATERMARK */
Route::get('/download-pdf/{id}/{hash}', array('as'=>'downloadPDF', 'middleware'=>['portal'], 'uses'=>'Admin\AssetController@downloadPDF'));

/* TEST URL */
Route::get('/test/video', array('as'=>'testURL', 'uses'=>'Admin\DashboardController@test'));
Route::get('/test/video/view', array('as'=>'testVideo', 'uses'=>'Admin\DashboardController@test_video'));
Route::get('/test/pdf/download', array('as'=>'testDownloadPDF', 'uses'=>'Admin\AssetController@test_downloadPDF'));
Route::get('/test/email-template', function(){

    $data = [
        'message' => 'Lorem Ipsum Dolor Sit Amet'
    ];

    return view('emails.inquiry')->with($data);

});
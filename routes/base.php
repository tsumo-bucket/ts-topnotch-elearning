<?php

/*
|--------------------------------------------------------------------------
| BASE Routes
|--------------------------------------------------------------------------
|
| (do not modify here. if you have updates to base, update laravel-base repo instead)
|
*/

// Route::auth(); // cause error during upgrading to laravel 7
Route::get('autogen', array('as'=>'autogen','uses'=>'AutogenController@index'));

Route::get('admin', array('as'=>'adminDashboard','uses'=>'Admin\DashboardController@index'));

/* SAMPLES */
Route::get('admin/samples', array('as'=>'adminSamples','uses'=>'Admin\SampleController@index'));
Route::get('admin/samples/datatable', array('as'=>'adminSamplesDatatable','uses'=>'Admin\SampleController@datatable'));
Route::get('admin/samples/create', array('as'=>'adminSamplesCreate','uses'=>'Admin\SampleController@create'));
Route::post('admin/samples/', array('as'=>'adminSamplesStore','uses'=>'Admin\SampleController@store'));
Route::get('admin/samples/{id}/show', array('as'=>'adminSamplesShow','uses'=>'Admin\SampleController@show'));
Route::get('admin/samples/{id}/view', array('as'=>'adminSamplesView','uses'=>'Admin\SampleController@view'));
Route::get('admin/samples/{id}/edit', array('as'=>'adminSamplesEdit','uses'=>'Admin\SampleController@edit'));
Route::patch('admin/samples/{id}', array('as'=>'adminSamplesUpdate','uses'=>'Admin\SampleController@update'));
Route::post('admin/samples/seo', array('as'=>'adminSamplesSeo','uses'=>'Admin\SampleController@seo'));
Route::delete('admin/samples/destroy', array('as'=>'adminSamplesDestroy','uses'=>'Admin\SampleController@destroy'));
Route::get('admin/samples/crop/url', array('as'=>'adminSamplesCropUrl','uses'=>'Admin\SampleController@crop_url'));
Route::get('admin/samples/{id}/crop/{column}/{asset_id}', array('as'=>'adminSamplesCropForm','uses'=>'Admin\SampleController@crop_form'));
Route::patch('admin/samples/{id}/crop', array('as'=>'adminSamplesCrop','uses'=>'Admin\SampleController@crop'));

/* USERS */
Route::get('admin/users', array('as'=>'adminUsers','uses'=>'Admin\UserController@index'));
Route::get('admin/users/create', array('as'=>'adminUsersCreate','uses'=>'Admin\UserController@create'));
Route::post('admin/users/', array('as'=>'adminUsersStore','uses'=>'Admin\UserController@store'));
Route::get('admin/users/{id}/change_password', array('as'=>'adminUsersChangePassword','uses'=>'Admin\UserController@change_password'));
Route::patch('admin/users/{id}/change_password', array('as'=>'adminUsersUpdatePassword','uses'=>'Admin\UserController@user_password_update'));
Route::get('admin/users/{id}/show', array('as'=>'adminUsersShow','uses'=>'Admin\UserController@show'));
Route::get('admin/users/{id}/edit', array('as'=>'adminUsersEdit','uses'=>'Admin\UserController@edit'));
Route::patch('admin/users/{id}', array('as'=>'adminUsersUpdate','uses'=>'Admin\UserController@update'));
Route::delete('admin/users/destroy', array('as'=>'adminUsersDestroy','uses'=>'Admin\UserController@destroy'));
Route::patch('admin/users/update/status', array('as'=>'adminUserStatusUpdate','uses'=>'Admin\UserController@update_status'));

/* PROFILE */
Route::get('admin/profile', array('as'=>'adminProfile','uses'=>'Admin\ProfileController@index'));
Route::get('admin/profile/edit', array('as'=>'adminProfileEdit','uses'=>'Admin\ProfileController@edit'));
Route::patch('admin/profile/edit', array('as'=>'adminProfileUpdate','uses'=>'Admin\ProfileController@update'));
Route::get('admin/profile/change_password', array('as'=>'adminProfilePasswordEdit','uses'=>'Admin\ProfileController@password_edit'));
Route::patch('admin/profile/change_password', array('as'=>'adminProfilePasswordUpdate','uses'=>'Admin\ProfileController@password_update'));

/* OPTIONS */
Route::get('admin/options', array('as'=>'adminOptions','uses'=>'Admin\OptionController@index'));
Route::get('admin/options/create', array('as'=>'adminOptionsCreate','uses'=>'Admin\OptionController@create'));
Route::post('admin/options/', array('as'=>'adminOptionsStore','uses'=>'Admin\OptionController@store'));
Route::get('admin/options/{id}/show', array('as'=>'adminOptionsShow','uses'=>'Admin\OptionController@show'));
Route::get('admin/options/{id}/view', array('as'=>'adminOptionsView','uses'=>'Admin\OptionController@view'));
Route::get('admin/options/{id}/edit', array('as'=>'adminOptionsEdit','uses'=>'Admin\OptionController@edit'));
Route::patch('admin/options/{id}', array('as'=>'adminOptionsUpdate','uses'=>'Admin\OptionController@update'));
Route::delete('admin/options/destroy', array('as'=>'adminOptionsDestroy','uses'=>'Admin\OptionController@destroy'));

/* ASSETS */
Route::post('admin/assets/upload', array('as'=>'adminAssetsUpload','uses'=>'Admin\AssetController@upload'));
Route::post('admin/assets/redactor', array('as'=>'adminAssetsRedactor','uses'=>'Admin\AssetController@redactor'));
Route::get('admin/assets/all', array('as'=>'adminAssetsAll','uses'=>'Admin\AssetController@all'));
Route::get('admin/assets/get', array('as'=>'adminAssetsGet','uses'=>'Admin\AssetController@get'));
Route::get('admin/assets/download', array('as'=>'adminAssetsDownload','uses'=>'Admin\AssetController@download'));
Route::post('admin/assets/update', array('as'=>'adminAssetsUpdate','uses'=>'Admin\AssetController@update'));
Route::post('admin/assets/destroy', array('as'=>'adminAssetsDestroy','uses'=>'Admin\AssetController@destroy'));
Route::get('admin/assets/get_asset_tags', array('as'=>'adminGetAssetTags','uses'=>'Admin\AssetController@getAssetTags'));
Route::post('admin/assets/get-pdf-version', array('as'=>'adminAssetPdfVersion', 'uses'=>'Admin\AssetController@pdf_version_api'));

/* LONG NAMES */
Route::get('admin/long_names', array('as'=>'adminLongNames','uses'=>'Admin\LongNameController@index'));
Route::get('admin/long_names/create', array('as'=>'adminLongNamesCreate','uses'=>'Admin\LongNameController@create'));
Route::post('admin/long_names/', array('as'=>'adminLongNamesStore','uses'=>'Admin\LongNameController@store'));
Route::get('admin/long_names/{id}/show', array('as'=>'adminLongNamesShow','uses'=>'Admin\LongNameController@show'));
Route::get('admin/long_names/{id}/edit', array('as'=>'adminLongNamesEdit','uses'=>'Admin\LongNameController@edit'));
Route::patch('admin/long_names/{id}', array('as'=>'adminLongNamesUpdate','uses'=>'Admin\LongNameController@update'));
Route::delete('admin/long_names/destroy', array('as'=>'adminLongNamesDestroy','uses'=>'Admin\LongNameController@destroy'));

/* PAGE CATEGORIES */
Route::get('admin/page_categories', array('as'=>'adminPageCategories','uses'=>'Admin\PageCategoryController@index'));
Route::get('admin/page_categories/create', array('as'=>'adminPageCategoriesCreate','uses'=>'Admin\PageCategoryController@create'));
Route::post('admin/page_categories/', array('as'=>'adminPageCategoriesStore','uses'=>'Admin\PageCategoryController@store'));
Route::get('admin/page_categories/{id}/show', array('as'=>'adminPageCategoriesShow','uses'=>'Admin\PageCategoryController@show'));
Route::get('admin/page_categories/{id}/view', array('as'=>'adminPageCategoriesView','uses'=>'Admin\PageCategoryController@view'));
Route::get('admin/page_categories/{id}/edit', array('as'=>'adminPageCategoriesEdit','uses'=>'Admin\PageCategoryController@edit'));
Route::patch('admin/page_categories/{id}', array('as'=>'adminPageCategoriesUpdate','uses'=>'Admin\PageCategoryController@update'));
Route::post('admin/page_categories/seo', array('as'=>'adminPageCategoriesSeo','uses'=>'Admin\PageCategoryController@seo'));
Route::delete('admin/page_categories/destroy', array('as'=>'adminPageCategoriesDestroy','uses'=>'Admin\PageCategoryController@destroy'));
Route::get('admin/page_categories/pages/', array('as' => 'adminPageByCategory', 'uses' => 'Admin\PageCategoryController@getPageByCategory'));

/* PAGES */
Route::get('admin/pages', array('as'=>'adminPages','uses'=>'Admin\PageController@index'));
Route::get('admin/pages/order', array('as'=>'adminPagesOrder','uses'=>'Admin\PageController@order'));
Route::get('admin/pages/create', array('as'=>'adminPagesCreate','uses'=>'Admin\PageController@create'));
Route::post('admin/pages/', array('as'=>'adminPagesStore','uses'=>'Admin\PageController@store'));
Route::get('admin/pages/{id}/show', array('as'=>'adminPagesShow','uses'=>'Admin\PageController@show'));
Route::get('admin/pages/{id}/view', array('as'=>'adminPagesView','uses'=>'Admin\PageController@view'));
Route::get('admin/dev/pages/{id}/edit', array('as'=>'adminPagesEdit','uses'=>'Admin\PageController@edit'));
Route::patch('admin/pages/{id}', array('as'=>'adminPagesUpdate','uses'=>'Admin\PageController@update'));
Route::post('admin/pages/seo', array('as'=>'adminPagesSeo','uses'=>'Admin\PageController@seo'));
Route::delete('admin/pages/destroy', array('as'=>'adminPagesDestroy','uses'=>'Admin\PageController@destroy'));

/* EXAMS */
Route::get('admin/exams', array('as'=>'adminExams', 'uses'=>'Admin\ExamController@index'));
Route::get('admin/exams/create', array('as'=>'adminExamsCreate', 'uses'=>'Admin\ExamController@create'));
Route::post('admin/exams', array('as'=>'adminExamsStore', 'uses'=>'Admin\ExamController@store'));
Route::get('admin/exams/{id}/edit', array('as'=>'adminExamsEdit', 'uses'=>'Admin\ExamController@edit'));
Route::patch('admin/exams/{id}/update', array('as'=>'adminExamsUpdate', 'uses'=>'Admin\ExamController@update'));
Route::delete('admin/exams/destroy', array('as'=>'adminExamsDestroy', 'uses'=>'Admin\ExamController@destroy'));

/* BANNER PICTURES */
Route::get('admin/banner-pictures', array('as'=>'adminBanners','uses'=>'Admin\BannerController@index'));
Route::get('admin/banner-pictures/create', array('as'=>'adminBannersCreate','uses'=>'Admin\BannerController@create'));
Route::post('admin/banner-pictures/', array('as'=>'adminBannersStore','uses'=>'Admin\BannerController@store'));
Route::get('admin/banner-pictures/{id}/show', array('as'=>'adminBannersShow','uses'=>'Admin\BannerController@show'));
Route::get('admin/banner-pictures/{id}/view', array('as'=>'adminBannersView','uses'=>'Admin\BannerController@view'));
Route::get('admin/banner-pictures/{id}/edit', array('as'=>'adminBannersEdit','uses'=>'Admin\BannerController@edit'));
Route::patch('admin/banner-pictures/{id}', array('as'=>'adminBannersUpdate','uses'=>'Admin\BannerController@update'));
Route::get('admin/banner-pictures/order', array('as'=>'adminBannersOrder','uses'=>'Admin\BannerController@order'));
Route::post('admin/banner-pictures/seo', array('as'=>'adminBannersSeo','uses'=>'Admin\BannerController@seo'));
Route::delete('admin/banner-pictures/destroy', array('as'=>'adminBannersDestroy','uses'=>'Admin\BannerController@destroy'));
Route::get('admin/banner-pictures/crop/url', array('as'=>'adminBannersCropUrl','uses'=>'Admin\BannerController@crop_url'));
Route::get('admin/banner-pictures/{id}/crop/{column}/{asset_id}', array('as'=>'adminBannersCropForm','uses'=>'Admin\BannerController@crop_form'));
Route::patch('admin/banner-pictures/{id}/crop', array('as'=>'adminBannersCrop','uses'=>'Admin\BannerController@crop'));

/* USER PERMISSIONS */
Route::post('admin/user_permissions/', array('as'=>'adminUserPermissionsStore','uses'=>'Admin\UserPermissionController@store'));
Route::get('admin/user_permissions/{id}/show', array('as'=>'adminUserPermissionsShow','uses'=>'Admin\UserPermissionController@show'));
Route::get('admin/user_permissions/{id}/view', array('as'=>'adminUserPermissionsView','uses'=>'Admin\UserPermissionController@view'));
Route::get('admin/user_permissions/{id}/edit', array('as'=>'adminUserPermissionsEdit','uses'=>'Admin\UserPermissionController@edit'));
Route::patch('admin/user_permissions/{id}', array('as'=>'adminUserPermissionsUpdate','uses'=>'Admin\UserPermissionController@update'));
Route::post('admin/user_permissions/seo', array('as'=>'adminUserPermissionsSeo','uses'=>'Admin\UserPermissionController@seo'));
Route::delete('admin/user_permissions/destroy', array('as'=>'adminUserPermissionsDestroy','uses'=>'Admin\UserPermissionController@destroy'));
Route::get('admin/user_permissions/{id}/permissions', array('as'=>'adminUserPermissions','uses'=>'Admin\UserPermissionController@index'));
Route::get('admin/user_permissions/{id}/create', array('as'=>'adminUserPermissionsCreate','uses'=>'Admin\UserPermissionController@create'));
Route::get('admin/user_permissions/order', array('as'=>'adminUserPermissionsOrder','uses'=>'Admin\UserPermissionController@order'));

/* USER ROLES */
Route::get('admin/user_roles', array('as'=>'adminUserRoles','uses'=>'Admin\UserRoleController@index'));
Route::get('admin/user_roles/create', array('as'=>'adminUserRolesCreate','uses'=>'Admin\UserRoleController@create'));
Route::post('admin/user_roles/', array('as'=>'adminUserRolesStore','uses'=>'Admin\UserRoleController@store'));
Route::get('admin/user_roles/{id}/show', array('as'=>'adminUserRolesShow','uses'=>'Admin\UserRoleController@show'));
Route::get('admin/user_roles/{id}/view', array('as'=>'adminUserRolesView','uses'=>'Admin\UserRoleController@view'));
Route::get('admin/user_roles/{id}/edit', array('as'=>'adminUserRolesEdit','uses'=>'Admin\UserRoleController@edit'));
Route::patch('admin/user_roles/{id}', array('as'=>'adminUserRolesUpdate','uses'=>'Admin\UserRoleController@update'));
Route::post('admin/user_roles/seo', array('as'=>'adminUserRolesSeo','uses'=>'Admin\UserRoleController@seo'));
Route::delete('admin/user_roles/destroy', array('as'=>'adminUserRolesDestroy','uses'=>'Admin\UserRoleController@destroy'));

/* ARTICLES */
Route::get('admin/articles', array('as'=>'adminArticles','uses'=>'Admin\ArticleController@index'));
Route::get('admin/articles/create', array('as'=>'adminArticlesCreate','uses'=>'Admin\ArticleController@create'));
Route::post('admin/articles/', array('as'=>'adminArticlesStore','uses'=>'Admin\ArticleController@store'));
Route::get('admin/articles/{id}/show', array('as'=>'adminArticlesShow','uses'=>'Admin\ArticleController@show'));
Route::get('admin/articles/{id}/view', array('as'=>'adminArticlesView','uses'=>'Admin\ArticleController@view'));
Route::get('admin/articles/{id}/edit', array('as'=>'adminArticlesEdit','uses'=>'Admin\ArticleController@edit'));
Route::patch('admin/articles/{id}', array('as'=>'adminArticlesUpdate','uses'=>'Admin\ArticleController@update'));
Route::post('admin/articles/seo', array('as'=>'adminArticlesSeo','uses'=>'Admin\ArticleController@seo'));
Route::delete('admin/articles/destroy', array('as'=>'adminArticlesDestroy','uses'=>'Admin\ArticleController@destroy'));
Route::get('admin/articles/crop/url', array('as'=>'adminArticlesCropUrl','uses'=>'Admin\ArticleController@crop_url'));
Route::get('admin/articles/{id}/crop/{column}/{asset_id}', array('as'=>'adminArticlesCropForm','uses'=>'Admin\ArticleController@crop_form'));
Route::patch('admin/articles/{id}/crop', array('as'=>'adminArticlesCrop','uses'=>'Admin\ArticleController@crop'));

/* EMAILS */
Route::get('admin/emails', array('as'=>'adminEmails','uses'=>'Admin\EmailController@index'));
Route::get('admin/emails/create', array('as'=>'adminEmailsCreate','uses'=>'Admin\EmailController@create'));
Route::post('admin/emails/', array('as'=>'adminEmailsStore','uses'=>'Admin\EmailController@store'));
Route::get('admin/emails/{id}/show', array('as'=>'adminEmailsShow','uses'=>'Admin\EmailController@show'));
Route::get('admin/emails/{id}/view', array('as'=>'adminEmailsView','uses'=>'Admin\EmailController@view'));
Route::get('admin/emails/{id}/edit', array('as'=>'adminEmailsEdit','uses'=>'Admin\EmailController@edit'));
Route::patch('admin/emails/{id}', array('as'=>'adminEmailsUpdate','uses'=>'Admin\EmailController@update'));
Route::post('admin/emails/seo', array('as'=>'adminEmailsSeo','uses'=>'Admin\EmailController@seo'));
Route::delete('admin/emails/destroy', array('as'=>'adminEmailsDestroy','uses'=>'Admin\EmailController@destroy'));

/* ACTIVITIES */
Route::get('admin/activities', array('as'=>'adminActivities','uses'=>'Admin\ActivityController@index'));
Route::get('admin/activities/{id}/show', array('as'=>'adminActivitiesShow','uses'=>'Admin\ActivityController@show'));
Route::get('admin/activities/{id}/view', array('as'=>'adminActivitiesView','uses'=>'Admin\ActivityController@view'));

/* VIDEO OPTIONS */
Route::get('admin/video_options', array('as'=>'adminVideoOptions','uses'=>'Admin\VideoOptionController@index'));
Route::post('admin/video_options/create', array('as'=>'adminVideoOptionsCreate','uses'=>'Admin\VideoOptionController@create'));
Route::post('admin/video_options/delete', array('as'=>'adminVideoOptionsDeleteVideo','uses'=>'Admin\VideoOptionController@deleteVideoBanner'));
Route::post('admin/video_options/', array('as'=>'adminVideoOptionsStore','uses'=>'Admin\VideoOptionController@store'));
Route::get('admin/getUploadedVideos/', array('as'=>'getUploadedVideos','uses'=>'Admin\VideoOptionController@getUploadedVideos'));
Route::get('admin/video_options/{id}/show', array('as'=>'adminVideoOptionsShow','uses'=>'Admin\VideoOptionController@show'));
Route::get('admin/video_options/{id}/view', array('as'=>'adminVideoOptionsView','uses'=>'Admin\VideoOptionController@view'));
Route::get('admin/video_options/{id}/edit', array('as'=>'adminVideoOptionsEdit','uses'=>'Admin\VideoOptionController@edit'));
Route::patch('admin/video_options/{id}', array('as'=>'adminVideoOptionsUpdate','uses'=>'Admin\VideoOptionController@update'));
Route::post('admin/video_options/seo', array('as'=>'adminVideoOptionsSeo','uses'=>'Admin\VideoOptionController@seo'));
Route::delete('admin/video_options/destroy', array('as'=>'adminVideoOptionsDestroy','uses'=>'Admin\VideoOptionController@destroy'));
Route::get('admin/video-options/youtube', array('as'=>'adminVideosYoutube','uses'=>'Admin\VideoOptionController@youtube'));
Route::get('admin/video-options/vimeo', array('as'=>'adminVideosVimeo','uses'=>'Admin\VideoOptionController@vimeo'));
Route::get('admin/video-options/library', array('as'=>'adminVideosLibrary','uses'=>'Admin\VideoOptionController@library'));

/* NAVBAR LINKS */
Route::get('admin/navbar_links', array('as'=>'adminNavbarLinks','uses'=>'Admin\NavbarLinkController@index'));
Route::post('admin/navbar_links/', array('as'=>'adminNavbarLinksStore','uses'=>'Admin\NavbarLinkController@store'));
Route::get('admin/navbar_links/{id}/show', array('as'=>'adminNavbarLinksShow','uses'=>'Admin\NavbarLinkController@show'));
Route::get('admin/navbar_links/{id}/view', array('as'=>'adminNavbarLinksView','uses'=>'Admin\NavbarLinkController@view'));
Route::patch('admin/navbar_links/{id}', array('as'=>'adminNavbarLinksUpdate','uses'=>'Admin\NavbarLinkController@update'));
Route::post('admin/navbar_links/seo', array('as'=>'adminNavbarLinksSeo','uses'=>'Admin\NavbarLinkController@seo'));
Route::delete('admin/navbar_links/destroy', array('as'=>'adminNavbarLinksDestroy','uses'=>'Admin\NavbarLinkController@destroy'));
Route::get('admin/navbar_links/order', array('as'=>'adminNavbarLinksOrder','uses'=>'Admin\NavbarLinkController@order'));

/* PAGE CONTENTS */
Route::get('admin/page_contents', array('as'=>'adminPageContents','uses'=>'Admin\PageContentController@index'));
Route::get('admin/page_contents/{id}/create', array('as'=>'adminPageContentsCreate','uses'=>'Admin\PageContentController@create'));
Route::post('admin/page_contents/', array('as'=>'adminPageContentsStore','uses'=>'Admin\PageContentController@store'));
Route::get('admin/page_contents/{id}/show', array('as'=>'adminPageContentsShow','uses'=>'Admin\PageContentController@show'));
Route::get('admin/page_contents/{id}/view', array('as'=>'adminPageContentsView','uses'=>'Admin\PageContentController@view'));
Route::get('admin/page_contents/{id}/edit', array('as'=>'adminPageContentsEdit','uses'=>'Admin\PageContentController@edit'));
Route::patch('admin/page_contents/{id}', array('as'=>'adminPageContentsUpdate','uses'=>'Admin\PageContentController@update'));
Route::post('admin/page_contents/seo', array('as'=>'adminPageContentsSeo','uses'=>'Admin\PageContentController@seo'));
Route::delete('admin/page_contents/destroy', array('as'=>'adminPageContentsDestroy','uses'=>'Admin\PageContentController@destroy'));
Route::get('admin/page_contents/order', array('as'=>'adminPageContentsOrder','uses'=>'Admin\PageContentController@order'));

/* PAGE CONTENT ITEMS */
Route::get('admin/page_content_items', array('as'=>'adminPageContentItems','uses'=>'Admin\PageContentItemController@index'));
Route::get('admin/page_content_items/{id}/create', array('as'=>'adminPageContentItemsCreate','uses'=>'Admin\PageContentItemController@create'));
Route::post('admin/page_content_items/', array('as'=>'adminPageContentItemsStore','uses'=>'Admin\PageContentItemController@store'));
Route::get('admin/page_content_items/{id}/show', array('as'=>'adminPageContentItemsShow','uses'=>'Admin\PageContentItemController@show'));
Route::get('admin/page_content_items/{id}/view', array('as'=>'adminPageContentItemsView','uses'=>'Admin\PageContentItemController@view'));
Route::get('admin/page_content_items/{id}/edit', array('as'=>'adminPageContentItemsEdit','uses'=>'Admin\PageContentItemController@edit'));
Route::patch('admin/page_content_items/{id}', array('as'=>'adminPageContentItemsUpdate','uses'=>'Admin\PageContentItemController@update'));
Route::post('admin/page_content_items/seo', array('as'=>'adminPageContentItemsSeo','uses'=>'Admin\PageContentItemController@seo'));
Route::delete('admin/page_content_items/destroy', array('as'=>'adminPageContentItemsDestroy','uses'=>'Admin\PageContentItemController@destroy'));
Route::get('admin/page_content_items/order', array('as'=>'adminPageContentItemsOrder','uses'=>'Admin\PageContentItemController@order'));

/* PAGES */
Route::get('admin/page/{slug?}', array('as'=>'adminClientPage','uses'=>'Admin\PageController@view'));
Route::post('admin/page/toggle', array('as'=>'adminClientPageToggle','uses'=>'Admin\PageController@pageToggle'));
Route::get('admin/page/{slug}/preview', array('as'=>'adminPagesPreview','uses'=>'Admin\PageController@preview'));

// Route::get('admin/page/{slug}/content/{content}/item/create', array('as'=>'adminClientCaboodlePageContentItemCreate','uses'=>'Caboodle\PageContentItemController@clientCreate'));
Route::get('admin/page/{slug}/content/{content}/item/edit/{id}', array('as'=>'adminClientPageContentItemEdit','uses'=>'Admin\PageContentItemController@clientEdit'));
// Route::delete('admin/page/content/item/{id}/delete', array('as'=>'adminClientCaboodlePageContentItemDestroy','uses'=>'Caboodle\PageContentItemController@clientDestroy'));
Route::post('admin/page/{slug}/content/{content}/item/sort', array('as'=>'adminClientCaboodlePageContentItemSort','uses'=>'Caboodle\PageContentItemController@clientSort'));

/* PAGE CONTENT */
Route::get('admin/page/{slug}/content/edit/{id}',array('as'=>'adminClientPageContentEdit','uses'=>'Admin\PageContentController@clientEdit'));
Route::get('admin/page/{slug}/content/edit/{id}/preview/{preview_id}',array('as'=>'adminClientPageContentPreviewEdit','uses'=>'Admin\PageContentController@clientEdit'));
Route::get('admin/page/{slug}/content/create/{content_id?}', array('as'=>'adminClientPageContentCreate','uses'=>'Admin\PageContentController@clientCreate'));
Route::post('admin/page/content/store/{content_id?}', array('as'=>'adminPageClientStore','uses'=>'Admin\PageContentController@clientStore'));
Route::patch('admin/page/{slug}/content/{id}/{preview_id?}', array('as'=>'adminClientPageContentUpdate','uses'=>'Admin\PageContentController@clientUpdate'));
Route::delete('admin/page/content/{id}/delete', array('as'=>'adminClientPageContentDestroy', 'uses'=>'Admin\PageContentController@clientDestroy'));
Route::delete('admin/page/{slug}/content/{id}/preview/delete', array('as'=>'adminPreviewDestroy', 'uses'=>'Admin\PageContentController@preview_destroy'));

/* PAGE CONTENT ITEMS */
Route::get('admin/page/page_content_item/{slug}/{id}/create', array('as'=>'adminClientPageContentItemCreate','uses'=>'Admin\PageContentItemController@clientCreate'));
Route::patch('admin/page/{slug}/content/{content}/item/{id}', array('as'=>'adminClientPageContentItemUpdate','uses'=>'Admin\PageContentItemController@clientUpdate'));
Route::post('admin/page/{slug}/content/{content}/item', array('as'=>'adminClientPageContentItemStore','uses'=>'Admin\PageContentItemController@clientStore'));
Route::delete('admin/page/content/item/delete/{id}',array('as'=>'adminClientPageContentItemDestroy', 'uses'=>'Admin\PageContentItemController@clientDestroy' ));

/* TERMS AND CONDITIONS */
Route::get('admin/terms', array('as'=>'adminTerms', 'uses'=>'Admin\LegalController@index'));
Route::get('admin/terms/{id}/edit', array('as'=>'adminTermsEdit','uses'=>'Admin\LegalController@edit'));
Route::patch('admin/terms/{id}/update', array('as'=>'adminTermsUpdate', 'uses'=>'Admin\LegalController@update'));

/* Route::get('admin/legal/items/{slug}/{id}/add', array('as'=>'adminLegalPageItemsCreate','uses'=>'Admin\LegalController@itemAdd'));
Route::get('admin/legal/items/{slug}/{content}/item/{id}/edit', array('as'=>'adminLegalPageItemsEdit','uses'=>'Admin\LegalController@itemEdit'));
Route::post('admin/legal/items/{slug}/{content}/store', array('as'=>'adminLegalPageItemsStore','uses'=>'Admin\LegalController@itemStore')); */

// Route::get('admin/test',array('as'=>'pagetemplate', 'uses'=>'Admin\PageTemplateController@index'));
Route::post('admin/test',array('as'=>'pageTemplateEdit', 'uses'=>'Admin\PageTemplateController@edit'));

/* CONTACTS */
Route::get('admin/contacts', array('as'=>'adminContacts','uses'=>'Admin\ContactController@index'));
Route::get('admin/contacts/create', array('as'=>'adminContactsCreate','uses'=>'Admin\ContactController@create'));
Route::post('admin/contacts/', array('as'=>'adminContactsStore','uses'=>'Admin\ContactController@store'));
Route::get('admin/contacts/{id}/show', array('as'=>'adminContactsShow','uses'=>'Admin\ContactController@show'));
Route::get('admin/contacts/{id}/view', array('as'=>'adminContactsView','uses'=>'Admin\ContactController@view'));
Route::get('admin/contacts/{id}/edit', array('as'=>'adminContactsEdit','uses'=>'Admin\ContactController@edit'));
Route::patch('admin/contacts/{id}', array('as'=>'adminContactsUpdate','uses'=>'Admin\ContactController@update'));
Route::post('admin/contacts/seo', array('as'=>'adminContactsSeo','uses'=>'Admin\ContactController@seo'));
Route::delete('admin/contacts/destroy', array('as'=>'adminContactsDestroy','uses'=>'Admin\ContactController@destroy'));
Route::get('admin/contacts/order', array('as'=>'adminContactsOrder','uses'=>'Admin\ContactController@order'));

Route::post('admin/contacts', array('as'=>'adminFilterResults','uses'=>'Admin\ContactController@filterResults'));
Route::get('admin/contacts/filterDate', array('as'=>'adminFilterDate','uses'=>'Admin\ContactController@filterByDate'));
Route::get('admin/contacts/export', array('as'=>'adminExport','uses'=>'Admin\ContactController@export'));

Route::get('admin/pages/get_pages', array('as'=>'adminPagesGet','uses'=>'Admin\PageController@get_pages'));
Route::get('admin/articles/get_articles', array('as'=>'adminArticlesGet','uses'=>'Admin\ArticleController@get_articles'));

/* BATCHES */
Route::get('admin/batches', array('as'=>'adminBatches','uses'=>'Admin\BatchController@index'));
Route::get('admin/batches/create', array('as'=>'adminBatchesCreate','uses'=>'Admin\BatchController@create'));
Route::post('admin/batches/', array('as'=>'adminBatchesStore','uses'=>'Admin\BatchController@store'));
Route::get('admin/batches/{id}/show', array('as'=>'adminBatchesShow','uses'=>'Admin\BatchController@show'));
Route::get('admin/batches/{id}/view', array('as'=>'adminBatchesView','uses'=>'Admin\BatchController@view'));
Route::get('admin/batches/{id}/edit', array('as'=>'adminBatchesEdit','uses'=>'Admin\BatchController@edit'));
Route::patch('admin/batches/{id}', array('as'=>'adminBatchesUpdate','uses'=>'Admin\BatchController@update'));
Route::post('admin/batches/seo', array('as'=>'adminBatchesSeo','uses'=>'Admin\BatchController@seo'));
Route::delete('admin/batches/destroy', array('as'=>'adminBatchesDestroy','uses'=>'Admin\BatchController@destroy'));
Route::post('admin/batches/save-pdf', array('as'=>'adminBatchesSavePDF', 'uses'=>'Admin\BatchController@save_pdf'));
Route::get('admin/batches/{id}/email', array('as'=>'adminBatchEmail', 'uses'=>'Admin\BatchController@batch_email'));
Route::post('admin/batches/email/send', array('as'=>'adminBatchEmailSend', 'uses'=>'Admin\BatchController@batch_email_send'));
Route::get('admin/batches/email/filter', array('as'=>'adminBatchEmailFilter', 'uses'=>'Admin\BatchController@batch_email_filter'));

/* BATCH STUDENTS */
Route::get('admin/batch/{id}/students', array('as'=>'adminBatchStudents', 'uses'=>'Admin\BatchStudentController@index'));
Route::get('admin/batch/students/{id}/edit', array('as'=>'adminBatchStudentsEdit', 'uses'=>'Admin\BatchStudentController@edit'));
Route::get('admin/batch/students/{id}/create', array('as'=>'adminBatchStudentsCreate', 'uses'=>'Admin\BatchStudentController@create'));
Route::post('admin/batch/student/add', array('as'=>'adminBatchStudentsAdd', 'uses'=>'Admin\BatchStudentController@add_student'));
Route::post('admin/batch/students/store', array('as'=>'adminBatchStudentsStore', 'uses'=>'Admin\BatchStudentController@store'));
Route::post('admin/batch/students/transfer', array('as'=>'adminBatchStudentsTransfer', 'uses'=>'Admin\BatchStudentController@transfer'));
Route::patch('admin/batch/students/{id}', array('as'=>'adminBatchStudentsUpdate','uses'=>'Admin\BatchStudentController@update'));
Route::delete('admin/batch/students/destroy', array('as'=>'adminBatchStudentsDestroy', 'uses'=>'Admin\BatchStudentController@destroy'));
Route::get('admin/batch/download-template', array('as'=>'adminBatchStudentsDownloadCsv', 'uses'=>'Admin\BatchStudentController@students_sample'));
Route::get('admin/batch/download-transfer-template', array('as'=>'adminBatchTransferDownloadCsv', 'uses'=>'Admin\BatchStudentController@transfers_sample'));
Route::post('admin/batch/students/disable-all', array('as'=>'adminBatchStudentDisable', 'uses'=>'Admin\BatchStudentController@disable_all'));
Route::post('admin/batch/students/reset-password', array('as'=>'adminBatchStudentsResetPassword', 'uses'=>'Admin\BatchStudentController@reset_password'));
Route::post('admin/batch/students/rerun-watermark', array('as'=>'adminBatchStudentsRerunWatermark', 'uses'=>'Admin\BatchStudentController@rerun_watermark'));
Route::post('admin/batch/students/resend-welcome', array('as'=>'adminBatchStudentsResendWelcome', 'uses'=>'Admin\BatchStudentController@resend_welcome'));
Route::get('admin/batch/students/export', array('as'=>'adminBatchStudentsExport', 'uses'=>'Admin\BatchStudentController@export_students'));

/* BATCH GRADES */
Route::get('admin/batch/{id}/grades', array('as'=>'adminBatchGrades', 'uses'=>'Admin\BatchStudentController@grades'));
Route::get('admin/batch/{id}/grades/download-template', array('as'=>'adminBatchGradesDownload', 'uses'=>'Admin\BatchStudentController@grades_sample'));
Route::get('admin/batch/{id}/grades/{student_number}/edit', array('as'=>'adminBatchGradesEdit', 'uses'=>'Admin\BatchStudentController@edit_grades'));
Route::get('admin/batch/{id}/grades/{student_number}/add', array('as'=>'AdminBatchGradesCreate', 'uses'=>'Admin\BatchStudentController@create_grade'));
Route::post('admin/batch/grades/store', array('as'=>'adminBatchGradesStore', 'uses'=>'Admin\BatchStudentController@store_grade'));
Route::post('admin/batch/grades/update-grades', array('as'=>'adminBatchGradesUpdate', 'uses'=>'Admin\BatchStudentController@update_grades'));
Route::post('admin/batch/grades/remove', array('as'=>'adminBatchGradesDestroy', 'uses'=>'Admin\BatchStudentController@remove_grade'));
Route::post('admin/batch/grades/import', array('as'=>'adminBatchGradesImport', 'uses'=>'Admin\BatchStudentController@import_grades'));

/* BATCH METRICS */
Route::get('admin/batch/{id}/metrics', array('as'=>'adminBatchMetrics', 'uses'=>'Admin\BatchMetricController@index'));
Route::get('admin/batch/metrics/{id}/edit', array('as'=>'adminBatchMetricsEdit', 'uses'=>'Admin\BatchMetricController@edit'));
Route::post('admin/batch/metrics/store', array('as'=>'adminBatchMetricsStore', 'uses'=>'Admin\BatchMetricController@store'));
Route::post('admin/batch/metrics/update', array('as'=>'adminBatchMetricsUpdate', 'uses'=>'Admin\BatchMetricController@update'));
Route::delete('admin/batch/metrics/destroy', array('as'=>'adminBatchMetricsDestroy', 'uses'=>'Admin\BatchMetricController@destroy'));

/* BATCH LECTURES */
Route::get('admin/batch/{id}/lectures', array('as'=>'adminBatchLectures','uses'=>'Admin\BatchLectureController@index'));
Route::post('admin/batch/lectures/', array('as'=>'adminBatchLecturesStore','uses'=>'Admin\BatchLectureController@store'));
Route::delete('admin/batch/lectures/destroy', array('as'=>'adminBatchLecturesDestroy','uses'=>'Admin\BatchLectureController@destroy'));
Route::get('admin/batch/{id}/lectures/all', array('as'=>'adminBatchLecturesAll', 'uses'=>'Admin\BatchLectureController@get_lectures'));
Route::post('admin/batch/lecture/update', array('as'=>'adminBatchLecturesUpdate', 'uses'=>'Admin\BatchLectureController@update'));
Route::post('admin/batch/lecture/get', array('as'=>'adminBatchLecturesGet', 'uses'=>'Admin\BatchLectureController@get'));
Route::post('admin/batch/lecture/filter', array('as'=>'adminBatchLecturesFilter', 'uses'=>'Admin\BatchLectureController@filter'));

/* LECTURES */
Route::get('admin/lectures', array('as'=>'adminLectures','uses'=>'Admin\LectureController@index'));
Route::get('admin/lectures/create', array('as'=>'adminLecturesCreate','uses'=>'Admin\LectureController@create'));
Route::post('admin/lectures/', array('as'=>'adminLecturesStore','uses'=>'Admin\LectureController@store'));
Route::get('admin/lectures/{id}/show', array('as'=>'adminLecturesShow','uses'=>'Admin\LectureController@show'));
Route::get('admin/lectures/{id}/view', array('as'=>'adminLecturesView','uses'=>'Admin\LectureController@view'));
Route::get('admin/lectures/{id}/edit', array('as'=>'adminLecturesEdit','uses'=>'Admin\LectureController@edit'));
Route::patch('admin/lectures/{id}', array('as'=>'adminLecturesUpdate','uses'=>'Admin\LectureController@update'));
Route::post('admin/lectures/seo', array('as'=>'adminLecturesSeo','uses'=>'Admin\LectureController@seo'));
Route::delete('admin/lectures/destroy', array('as'=>'adminLecturesDestroy','uses'=>'Admin\LectureController@destroy'));

/* LECTURE VIDEOS */
Route::post('admin/lecture/videos/', array('as'=>'adminLectureVideosStore','uses'=>'Admin\LectureVideoController@store'));
Route::post('admin/lecture/videos/seo', array('as'=>'adminLectureVideosSeo','uses'=>'Admin\LectureVideoController@seo'));
Route::delete('admin/lecture/videos/destroy', array('as'=>'adminLectureVideosDestroy','uses'=>'Admin\LectureVideoController@destroy'));

/* VIDEOS */
Route::get('admin/videos', array('as'=>'adminVideos','uses'=>'Admin\VideoController@index'));
Route::get('admin/videos/all', array('as'=>'adminVideosAll', 'uses'=>'Admin\VideoController@all'));
Route::post('admin/videos/upload', array('as'=>'adminVideosUpload', 'uses'=>'Admin\VideoController@upload'));
Route::get('admin/videos/get', array('as'=>'adminVideosGet', 'uses'=>'Admin\VideoController@get'));
Route::get('admin/videos/create', array('as'=>'adminVideosCreate','uses'=>'Admin\VideoController@create'));
Route::post('admin/videos/destroy', array('as'=>'adminVideosDestroy','uses'=>'Admin\VideoController@destroy'));
Route::post('admin/videos/update', array('as'=>'adminVideosUpdate','uses'=>'Admin\VideoController@update'));
Route::post('admin/videos/store', array('as'=>'adminVideosStore', 'uses'=>'Admin\VideoController@store'));
Route::post('admin/videos/get-html', array('as'=>'adminVideosGetHTML', 'uses'=>'Admin\VideoController@get_html'));
Route::post('admin/videos/trigger-video-sync', array('as'=>'adminTriggerVideoSync', 'uses'=>'Admin\VideoController@trigger_video_sync'));

/* ANNOUNCEMENTS AND BANNER TEXTS */
Route::get('admin/posts', array('as'=>'adminAnnouncements','uses'=>'Admin\AnnouncementController@index'));
Route::get('admin/posts/create', array('as'=>'adminAnnouncementsCreate','uses'=>'Admin\AnnouncementController@create'));
Route::post('admin/posts/', array('as'=>'adminAnnouncementsStore','uses'=>'Admin\AnnouncementController@store'));
Route::get('admin/posts/{id}/edit', array('as'=>'adminAnnouncementsEdit','uses'=>'Admin\AnnouncementController@edit'));
Route::patch('admin/posts/{id}', array('as'=>'adminAnnouncementsUpdate','uses'=>'Admin\AnnouncementController@update'));
Route::delete('admin/posts/destroy', array('as'=>'adminAnnouncementsDestroy','uses'=>'Admin\AnnouncementController@destroy'));
Route::get('admin/posts/get-batches', array('as'=>'adminAnnouncementsGetBatches', 'uses'=>'Admin\AnnouncementController@get_batches'));
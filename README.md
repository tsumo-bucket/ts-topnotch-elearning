# Topnotch EB Deployment

AWS Key secret can be found in basecamp
-- how to install EB link here
[https://3.basecamp.com/3368673/buckets/1109201/documents/1239649359/edit]

### run eb deploy once configured
```sh
eb deploy
```

# Think Sumo Laravel Base
### FIRST TIME INSTALLERS
- download Python 2.7.11: [https://www.python.org/downloads/release/python-2711/]
- add python to PATH
- npm config set python python2.7
- npm config set msvs_version 2015 --global

### DO THIS AT THE START
```sh
composer update
npm install
npm run development
php artisan migrate --seed 
```
### Remember these!

##### Sumo Autogen
- type in console to generate from database
```sh
php artisan sumo:generate
```
- autogen will NOT override files that already exists
- autogen controller (Http/Controllers/AutogenController)
- autogen templates (resources/views/autogen/*)


##### Query Debugging
- Debug and Die
```sh
$product = Product::findOrFail($id);
dd($product)
```

- echo db queries
```sh
// import DB object
use Illuminate\Support\Facades\DB;
```

```sh
// enable and display query log
DB::enableQueryLog(); 
$product = Product::findOrFail($id);
dd(DB::getQueryLog());
```

### FAQs
- API is auto-generated in /Http/Controllers/Api (need to copy paste routes)
- Pag may error sa seed at migrations na gawa ng ibang dev, try "composer dump-autoload"
- Use Factory to make fake values; can also be used in a Seeder
- If you are getting errors during node-sass installation install https://github.com/felixrieseberg/npm-windows-upgrade

### Release notes

#### 2.1.0
1. Update to Laravel 6.4
https://laravel.com/docs/6.x/upgrade
2. Release Note Laravel 6.x
https://laravel.com/docs/6.x/releases

#### 2.0.0 
1. Update to Laravel 5.8

#### 1.7.0
1. Fixed controller updates for cropping
https://bitbucket.org/tsumo-bucket/laravel-base/commits/0eb45f4b124f3d951da694bb4ef32bccf187984d

#### 1.6.0
1. Updated Fontawesome to 5.3.1
https://bitbucket.org/tsumo-bucket/laravel-base/commits/92b8cf3439c3587ab39d310c8a7950f80f85c813
2. Fix bug in SumoMail
https://bitbucket.org/tsumo-bucket/laravel-base/commits/fecab9944a2d0604be3a401b1b32a1cba6bdc262

#### 1.5.1
1. Updated SumoMail to accept multiple attachments
https://bitbucket.org/tsumo-bucket/laravel-base/commits/4ba1d06f29f9c188e0fa4268e6cec5968a1935bd

#### 1.5.0
1. Updated redactor to 3. Yes, we PAID!
https://bitbucket.org/tsumo-bucket/laravel-base/commits/f29a0de8dfd16e75a20c5e9634dbe8891c8b3540
2. removed doneCallback since jenkins is no longer doing gulp copy
https://bitbucket.org/tsumo-bucket/laravel-base/commits/0cf25fd178ae0a1301d8cfc549916518d126fa75

#### 1.4.0
1. Fixed parsely for multiple forms
https://bitbucket.org/tsumo-bucket/laravel-base/commits/e721d90f6cabdfe1eadaeb0c49f71fdebe2ee828
2. Added HELP field for Site Options
https://bitbucket.org/tsumo-bucket/laravel-base/commits/921f3fb3bd09abfabf85c88519a1ea3202a3a541
3. Add fix so that gifs will not be optimized
https://bitbucket.org/tsumo-bucket/laravel-base/commits/4bd4a84720376f7caaf3e858d82c06e99e885c6f
4. Change status codes on Auth middleware
https://bitbucket.org/tsumo-bucket/laravel-base/commits/a35aab5a281afb683eb0f3d35cb7e0d6405e4cea
5. Added IG Library (c/o LAU)
https://bitbucket.org/tsumo-bucket/laravel-base/commits/8b7c86f3680ce43a46804771b5e018fdce812aaa

#### 1.3.0
1. Updated API status codes to conform to proper REST behavior
https://bitbucket.org/tsumo-bucket/laravel-base/commits/8bba4063435eb732349f45e61a88cd41f61111b3
2. Updated Bootstrap to ^4.1.1 and FontAwesome to ^5.2.0
https://bitbucket.org/tsumo-bucket/laravel-base/commits/3fb134d87ca263215b5af718f7c67802594adce7

#### 1.2.0
1. S3 access key and id in default .env no longer has global s3 permission.  To create a project specific s3 key/secret, refer to this documentation
https://3.basecamp.com/3368673/buckets/1109201/documents/1196827008

#### 1.1.1
1. GA code retrieval added in Acme::General. Will use site-options "analytics-script" -- For BA's Just PUT the GA ID here configurable in site options. https://bitbucket.org/tsumo-bucket/laravel-base/commits/491faf3e848017064b05f8357c80c1020990e60d
2. Added new site option in seeder -- For Devs, use this email address for Contact Us forms
https://bitbucket.org/tsumo-bucket/laravel-base/commits/3baccee61debf017757cb3d9477158e53b2cc156
3. Fixed parsely for multiple forms. -- For Devs, you can now have multiple parsely forms in admin backend.
https://bitbucket.org/tsumo-bucket/laravel-base/commits/5ad39d45bdbad5b517d5da9d6dc708968907f069

#### 1.0.0 
Initial Version

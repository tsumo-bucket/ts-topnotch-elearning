<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_number')->unique();
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->string('contact_no')->nullable();
            $table->string('address')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('school')->nullable();
            $table->boolean('cms');
            $table->boolean('verified');
            $table->enum('status', ['active', 'blocked', 'freeze', 'completed']);
            $table->enum('type', ['super', 'admin', 'normal']);
            $table->boolean('is_active')->default(false);
            $table->string('last_login')->nullable();
            $table->string('last_ip')->nullable();
            $table->rememberToken();
            $table->string('token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

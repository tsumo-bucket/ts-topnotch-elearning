<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMetricOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::table('batch_metrics', function (Blueprint $table) {
            $table->boolean('hide_color_grade')->nullable();
            $table->boolean('hide_percentile')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batch_metrics', function (Blueprint $table) {
            $table->dropColumn('hide_color_grade');
            $table->dropColumn('hide_percentile');
        });
    }
}

<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        	[
	            'name' => 'ThinkSumo Developer',
	            'email' => 'dev@sumofy.me',
	            'password' => bcrypt('qweasd'),
	            'cms' => '1',
	            'verified' => '1',
	            'status' => 'active',
	            'type' => 'super'
            ]
        );
        DB::table('users')->insert(
            [
                'student_number' => 0,
                'name' => 'Admin',
                'email' => 'admin@sumofy.me',
                'password' => bcrypt('admin'),
                'cms' => '1',
                'verified' => '1',
                'status' => 'active',
                'type' => 'admin'
            ]
        );
    }
}

@extends('layouts.email-template')
@section('content')
<p>Dear Dr. {{$user['name']}},</p>

<p>Peace!</p>

<p>Welcome to Topnotch Online. We are excited to study with you as you prepare for the greatest exam of your life, the Philippine Physician Licensure Examinations.</p>

<p>As you commence your online study, please log-in using your credentials provided below. Remember to change your password immediately, and please do not share it with anyone for our mutual protection.</p>

<p style="padding: 20px 10px;
    border: 1px solid #000;
    background: rgba(126, 214, 223,0.3)" >
	<span>Username: <b>{{$user['email']}}</b></span>
	<br /><br/>
	<span>Password: <b>{{$password}}</b></span>
	<br /><br/>
	<span>Link: <a href="{{URL::to('/login')}}"> {{URL::to('/login')}} </a></span>
</p>

<p>Your student number is <b>{{ $user['student_number'] }}</b>. Kindly write this down as  it would be used for your diagnostic, pre-midterms, midterms, pre-finals and final exams.</p>

<p>We will see you soon, Sons and Daughters of Hippocrates.</p>

<p>
	<span>Your Teacher,</span>
	<br/>
	<span>Enrico Paolo C. Banzuela, MD</span>
	<br/>
	<span>And the Entire Topnotch Family</span>
</p>
 @stop
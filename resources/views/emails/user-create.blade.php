@extends('layouts.email-template')
@section('content')
<p>Dear {{$user['name']}},</p>

<p>Peace!</p>

<p>Welcome to Topnotch Online. You have been given access to the admin panel. Please use the credentials below to access the site: </p>

<p style="margin: 20px 0;" >
	<span>Username: <b>{{$user['email']}}</b></span>
	<br /><br/>
	<span>Password: <b>{{$password}}</b></span>
	{{-- <br /><br/>
	<span>Link: <a href="{{URL::to('/login')}}"> {{URL::to('/login')}} </a></span> --}}
</p>

<p>
	<span>Sincerely,</span>
	<br/>
	<span>Enrico Paolo C. Banzuela, MD</span>
	<br/>
	<span>And the Entire Topnotch Family</span>
</p>
 @stop
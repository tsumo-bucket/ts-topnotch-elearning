<script id="asset-image-template" type="text/x-handlebars-template">
	<div class="sumo-asset-img-container @{{ visibility }}" data-id="@{{ id }}">
		<img src="@{{ src }}">
		<div class="sumo-asset-img-hover-overlay"><ul>
			<li><a href="#" class="preview" data-toggle="tooltip" title="Preview" data-original-title="Preview"><i class="fa fa-eye"></i></a></li>
			<li><a href="#" class="remove" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fa fa-trash"></i></a></li>
		</ul></div>
    </div>
    <span class="name sub-text-1" style="font-size: 10px;" >@{{ this.name }}</span>
</script>
<script id="asset-multiple-image-template" type="text/x-handlebars-template">
    <div class="images-container row">
        <div class="empty-bg @{{ visibility }}">
            <i class="fa fa-image"></i>
        </div>
        @{{#if images}}
        	@{{#each images}}
            <div class="col col-sm-4 col-sm-4 box-container sumo-asset-select" id="@{{ this.asset_id }}">
                <div class="sumo-asset-img-container" data-id="@{{ this.asset_id }}">
                    <img src="@{{ this.src }}">
                    <div class="sumo-asset-img-hover-overlay"><ul>
                        <li><a href="#" class="preview" data-toggle="tooltip" title="Preview" data-original-title="Preview"><i class="fa fa-eye"></i></a></li>
                        <li><a href="#" class="remove" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fa fa-trash"></i></a></li>
                    </ul></div>
                </div>
                <span class="name sub-text-1" style="font-size: 10px;" >@{{ this.name }}</span>
            </div>
            @{{/each}}
        @{{/if}}
    </div>
</script>
<style>
    .date-container {
        position: relative;
    }
    .date-container .clear-btn {
        position: absolute;
        right: 0;
        top: 5px;
        cursor: pointer;
    }
    .datetimepicker {
        border: 0; 
        border-bottom: 1px solid #bdc2c6; 
        background: transparent;
        padding-bottom: 3px;
        padding-right: 10px;
        position: relative;
    }
</style>
    
<script id="asset-file-template" type="text/x-handlebars-template">
	<tr class="sumo-asset-select sumo-asset-file-container" data-save-url="{{ route('adminBatchesSavePDF') }}" data-id="@{{ id }}" id="@{{ id }}" >
        <td class="text-muted" > <a class="name preview" style="font-size: 10px;" href="#">@{{ name }}</a> </td>
        <td>
            <input type="text" name="start_date" data-asset-id="@{{ id }}" value="" class="datetimepicker start-date" placeholder="Start Date" >
        </td>
        <td>
            <input type="text" name="end_date" data-asset-id="@{{ id }}" value="" class="datetimepicker end-date" placeholder="End Date" >
        </td>
        <td class="created_at" > @{{ created_at }} </td>
        <td class="text-center">
            <a href="#" class="clear-btn" data-toggle="tooltip" title="Clear Dates" ><i class="fas fa-calendar-times"></i></a>
        </td>
        <td class="text-center" >
            <a href="#" class="remove" data-original-title="Delete"><i class="fa fa-trash"></i></a>
        </td>
    </tr>
</script>
<script id="asset-multiple-files-template" type="text/x-handlebars-template">
    <div class="table-responsive">
        <table class="caboodle-table caboodle-table-small mb-3">
            <thead>
                <th>File</th>
                <th colspan="2" >Publish Dates</th>
                <th>Uploaded on</th>
                <th colspan="2"></th>
            </thead>
            <tbody class="files-container" >
                <tr class="empty-bg @{{ visibility }}" >
                    <td colspan="6" class="text-center py-5" >No resources found</td>
                </tr>
                @{{#if images}}
                    @{{#each images}}
                        <tr class="sumo-asset-select sumo-asset-file-container" data-save-url="{{ route('adminBatchesSavePDF') }}" data-id="@{{ this.asset_id }}" id="@{{ this.asset_id }}" >
                            <td class="text-muted" > <a class="name preview" style="font-size: 10px;" href="#">@{{ this.name }}</a> </td>
                            <td>
                                <input type="text" name="start_date" data-asset-id="@{{ this.asset_id }}" value="@{{ this.start_date }}"  class="datetimepicker start-date" placeholder="Start Date" >
                            </td>
                            <td>
                                <input type="text" name="end_date" data-asset-id="@{{ this.asset_id }}" value="@{{ this.end_date }}" class="datetimepicker end-date" placeholder="End Date" >
                            </td>
                            <td class="created_at" > @{{ this.created_at }} </td>
                            <td class="text-center">
                                <a href="#" class="clear-btn" data-toggle="tooltip" title="Clear Dates" ><i class="fas fa-calendar-times"></i></a>
                            </td>
                            <td class="text-center" >
                                <a href="#" class="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @{{/each}}
                @{{/if}}
            </tbody>
        </table>
    </div>
</script>
<script id="asset-image-for-gallery-template" type="text/x-handlebars-template">
    @{{#if images}}
        @{{#each images}}
            <div class="col-sm-4 col-sm-3">
                <div class="sumo-asset-image-container thumbnail">
                    <img class="img img-responsive" src="@{{ this }}">
                </div>
            </div>
        @{{else}}
            <div class="col-sm-4 col-sm-3">
                <div class="sumo-asset-image-container thumbnail">
                    <img class="img img-responsive" src="@{{ images }}">
                </div>
            </div>
        @{{/each}}
    @{{/if}}
</script>
<input disabled readonly type="hidden" value="{!! route('adminAssetsGet') !!}" id="adminAssetsGetUrl">
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminUsers') }}">Users</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminUsers') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminUsers') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
    {!! Form::open(['route'=>'adminUsersStore', 'files' => true, 'method' => 'post', 'class'=>'form form-parsley form-create']) !!}
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    @include('admin.users.form')
                </div>
            </div>
            @if (count($batches) > 0)
                <div class="caboodle-card" id="student-forms" >
                    <div class="caboodle-card-body">
                        <div class="caboodle-form-group">
                            <label for="name">Student Number</label>
                            {!! Form::number('student_number', null, ['class'=>'form-control', 'id'=>'student_number', 'placeholder'=>'Student Number', 'required']) !!}
                        </div>
                        <div class="caboodle-form-group">
                            <label for="name">Batch</label>
                            <select name="batch_id" id="batch" class="form-control">
                                @foreach ($batches as $b)
                                    <option value="{{ $b->id }}">{{ ucwords($b->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('added-scripts')
    <script>

        $(document).ready(function(){
            var type = $('#type').val();

            if(type != 'normal'){
                $('#student-forms').css('display', 'none');
                $('#student_number').removeAttr('required');
            }
        });

        $('#type').on('change', function(e){
            var value = $(this).val();

            if(value == 'normal'){
                $('#student-forms').css('display', 'block');
                $('#student_number').attr('required');
            }
            else{
                $('#student-forms').css('display', 'none');
                $('#student_number').removeAttr('required');
            }
        });
    </script>
@endsection

@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Users</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminUsersCreate') }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card px-3">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminUsers', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Search Name']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body p-0">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminUsersDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table caboodle-table-small">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminUsersDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting {{$title}}">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide">Details</th>
                  <th class="caboodle-table-col-header hide">Type</th>
                  <th class="caboodle-table-col-header hide">Status</th>

                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td class="">
                      <div>
                          <div>{{$d->name}}</div>
                          <div class="sub-text-1">{{$d->email}}</div>
                      </div>
                    </td>
                    <td class="uppercase sub-text-1">{{$d->type}}</td>
                    <td class="uppercase sub-text-1">{{ $d->status == 'active' ? 'Active' : 'Inactive' }}</td>
 
                    <td width="110px" class="text-center">
                      @if (Auth::user()->type == 'super')
                        <a href="{{route('adminUsersChangePassword', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Change Password"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-lock" aria-hidden="true"></i>
                        </a>
                        <a href="{{route('adminUsersEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatches')}}">Batches</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchGrades', $batch->id)}}">Manage Grades</a></li>
        <li class="breadcrumb-item far active"><span>Metrics</span></li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchGrades', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
        {{-- <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button> --}}
    </div>
</header>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-4 col-12">
            {{-- DEFAULTS --}}
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <h4><i class="fas fa-sliders-h"></i> DEFAULTS</h4>
                    <table class="caboodle-table caboodle-table-thin">
                        <tbody>
                            @foreach (@$data as $item)
                                @if (@$item->type == 'defaults')
                                    <tr>
                                        <td>&bull; {{ @$item->name }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            {{-- FORM --}}
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <form class="form form-parsley form-create" action="{{ route('adminBatchMetricsStore') }}" method="post" id="metricForm" >
                        @csrf
                        <input type="hidden" id="batchIDField" name="batch_id" value="{{ @$batch->id }}" >
                        <input type="hidden" id="metricIDField" name="metric_id" value="">
                        <div class="caboodle-form-group">
                            <label for="metricField">Metric</label>
                            {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'e.g. Quiz', 'id'=>'metricField', 'required']) !!}
                            <div>
                                <label class="x-small" for="published">
                                    Hide Color Grade
                                </label>
                            </div>
                            <div class="mdc-switch no-margin">
                            {!! Form::hidden('hide_color_grade', false) !!}
                            {!! Form::checkbox('hide_color_grade', '1', null, ['class'=>'mdc-switch__native-control', 'id'=> 'metricHideColorGrade']) !!}
                                <div class="mdc-switch__background">
                                    <div class="mdc-switch__knob"></div>
                                </div>
                            </div>

                            <div>
                                <label class="x-small" for="published">
                                    Hide Percentile
                                </label>
                            </div>
                            <div class="mdc-switch no-margin">
                            {!! Form::hidden('hide_percentile', false) !!}
                            {!! Form::checkbox('hide_percentile', '1', null, ['class'=>'mdc-switch__native-control', 'id'=> 'metricHidePercentile']) !!}
                                <div class="mdc-switch__background">
                                    <div class="mdc-switch__knob"></div>
                                </div>
                            </div>

                            <!-- {!! Form::checkbox('hide_color_grade', '1', true) !!} 
                            {!! Form::checkbox('hide_percentile', '1', true) !!} -->
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>
                                <button id="metricFormBtnCancel" class="btn d-inline hide" data-toggle="tooltip" title="Cancel" data-mdc-auto-init="MDCRipple"><i class="fas fa-times"></i></button>
                                <button id="metricFormBtnDelete" class="btn d-inline btn-danger hide" data-toggle="tooltip" title="Delete" data-mdc-auto-init="MDCRipple"><i class="fas fa-trash"></i></button>
                            </div>
                            <div class="text-right">
                                <button id="metricFormBtnSubmit" class="btn d-inline btn-primary form-parsley-submit" data-mdc-auto-init="MDCRipple"><i class="fas fa-check"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-12">
            {{-- CUSTOM --}}
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <h4><i class="fas fa-plus-circle"></i> ADDITIONAL METRICS</h4>
                    @php
                        $customs = [];

                        foreach($data as $item) {
                            if ($item->type == 'custom'){
                                $customs[] = $item;
                            }
                        }
                    @endphp
                    @if (count($customs) < 1)
                        <div class="mx-auto my-5 text-center" >No additional metrics found</div>
                    @else
                        <table class="caboodle-table caboodle-table-small">
                            @foreach (@$customs as $item)
                                <tr>
                                    <td>
                                        <a href="#" class="d-block edit-metric" data-toggle="tooltip" title="Click to edit" data-id="{{ $item->id }}" data-value="{{ $item->name }}" data-hide_percentile="{{ $item->hide_percentile }}" data-hide_color_grade="{{ $item->hide_color_grade }}">&bull; {{ @$item->name }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('added-scripts')
    <script>
        
        var storeUrl = "{{ route('adminBatchMetricsStore') }}";
        var updateUrl = "{{ route('adminBatchMetricsUpdate') }}";
        var deleteUrl = "{{ route('adminBatchMetricsDestroy') }}";

        var isUpdate = false;

        var metricForm = $('#metricForm');
        var metricField = $('#metricField');
        var metricHideColorGrade = $('#metricHideColorGrade');
        var metricHidePercentile = $('#metricHidePercentile');
        var batchIDField = $('#batchIDField');
        var metricIDField = $('#metricIDField');

        var btnCancel = $('#metricFormBtnCancel') 
        var btnDelete = $('#metricFormBtnDelete') 
        var btnSubmit = $('#metricFormBtnSubmit') 

        $('.edit-metric').on('click', function(e){
            e.preventDefault();
            var value = $(this).attr('data-value');
            var id = $(this).attr('data-id');
            var hidePercentile = $(this).attr('data-hide_percentile');
            var hideColorGrade = $(this).attr('data-hide_color_grade');

            metricIDField.val(id);
            metricField.val(value);
            metricHideColorGrade.attr('checked', hideColorGrade? 'checked': null);
            metricHidePercentile.attr('checked', hidePercentile? 'checked': null);

            btnCancel.removeClass('hide');
            btnDelete.removeClass('hide');

            metricForm.attr('action', updateUrl);
        });

        $(document).on('click', '#metricFormBtnCancel', function(e){
            e.preventDefault();

            metricIDField.val('');
            metricField.val('');
            metricHideColorGrade.attr('checked', null);
            metricHidePercentile.attr('checked', null);

            btnCancel.addClass('hide');
            btnDelete.addClass('hide');

            metricForm.attr('action', storeUrl);
        });

        $(document).on('click', '#metricFormBtnDelete', function(e){
            e.preventDefault();

            swal({
                title: "Are you sure?",
                text: "This will delete the selected metric",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "Cancel",
                confirmButtonText: "Yes",
                confirmButtonClass: "btn btn-primary"
            },
            function(){
                
                var id = metricIDField.val();
                var batch_id = batchIDField.val();

                $.ajax({
                    url: deleteUrl,
                    type: 'delete',
                    data: {
                        id: id,
                        batch_id: batch_id
                    },
                    success: function(response){
                        showNotifyToaster(response.notifStatus, response.notifTitle, response.notifMessage);

                        setTimeout(() => {
                            location.href = response.redirect
                        }, 1500);
                    }
                })
            });

        });

    </script>
@endsection
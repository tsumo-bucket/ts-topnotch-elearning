<translatable object-class="App\PageControl">
	<input type="hidden" name="page_id" value="{{ @$page->id }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="row">
		<div class="col-sm-8">
			@if (@$page->allow_add_contents == 1 || count(@$content->controls) > 0)
			<div class="caboodle-card no-padding">
				<div class="flex align-center pad-top pad-left-30 pad-right-30">
					<div class="flex-1 text-right no-margin margin-right">
						Publish
					</div>
					<div>
						<div class="mdc-switch no-margin">
							{!! Form::checkbox('published', '1', @$page->published == 'published', ['class'=>'mdc-switch__native-control', 'data-id' => @$page->id]) !!}
							<div class="mdc-switch__background">
								<div class="mdc-switch__knob"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="caboodle-card-body pad-left-30 pad-right-30">
					@include('admin.pages.partials.controls')
				</div>
			</div>
			@else
				<div class="flex align-center pad-top">
					<div class="flex-1 text-right no-margin margin-right">
						Publish
					</div>
					<div>
						<div class="mdc-switch no-margin">
							{!! Form::checkbox('published', '1', @$page->published == 'published', ['class'=>'mdc-switch__native-control', 'data-id' => $page->id]) !!}
							<div class="mdc-switch__background">
								<div class="mdc-switch__knob"></div>
							</div>
						</div>
					</div>
				</div>
				@include('admin.page_contents.partials.items', ['view' => @$content->name])
			@endif
		</div>
		<div class="col-sm-4">
            @if (@$content && ($page->allow_add_contents == 1 || count(@$content->controls) > 0))
                @include('admin.pages.partials.version')
			    <div class="row">
                    <div class="col-sm-12">
                        @include('admin.page_contents.partials.items')
                    </div>
			    </div>
            @endif
		</div>
	</div>
</translatable>

@section('added-scripts')

@include('admin.page_contents.partials.scripts.added-script')

@include('admin.pages.partials.added-script-ordering')


<script>
   $('.item-delete').on('click', function(e) {
		e.preventDefault();
		var link = $(this).attr('href');
		var id = $(this).data('id');
		swal({
			title: "Delete item?",
			text: 'This action cannot be reversed after saving.',
			type: "warning",
			html: true,
			showCancelButton: true,
			confirmButtonColor: "#dc4f34",
			confirmButtonText: "Delete",
			cancelButtonText: "Cancel",
		}, function (isConfirm) {
			if (isConfirm) {
				showLoader(true);
				showNotifyToaster('info', '', '<i class="far fa-circle-notch fa-spin" style="margin-right: 5px"></i> Deleting item');
				$.ajax({
					type: "DELETE",
					url: link,
					data: { 
						_token: "{{ csrf_token() }}",
						id: id
					},
					success: function (data) {
						var title = data.notifTitle;
						var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
						var status = (_.isUndefined(data.notifStatus) ? '' : data.notifStatus);

						if (status != '' && (title != '' || message != '')) {
							toastr.clear();
							showNotifyToaster(status, title, message);
						}
						setTimeout(function () {
							window.location.reload();
						}, 1500);
					},
					error: function (data, text, error) {
						showLoader(false);
						var message = '';
						_.each(data.responseJSON, function (val) {
							message += val + ' ';
						});
						toastr.clear();
						showNotifyToaster('error', 'Error deleting.', message);
					}
				});
			}
		});
	});

	$('body').on('input','input[name=published]',function(){
		var input = $(this);
		var id = input.data('id');
		var status = (input.prop('checked')) ? 'published' : 'draft' ;

		$.ajax({
			type: "POST",
			headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"}, 
			url: "{!! route('adminClientPageToggle') !!}",
			data: {id: id, status: status},
			dataType: "JSON",
			success: function(res){
				if(res.success){
					console.log(res.message);
				}else{
					console.log(res.message);
				};
			}, error: function(xhr){
				console.log(xhr);
			}
		});
	});
</script>
@endsection
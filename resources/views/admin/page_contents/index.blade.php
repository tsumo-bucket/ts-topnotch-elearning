@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Page Contents</span></li>
  </ol>
</nav>
@stop

@section('header')

<header class="flex-center">
	<h1>{{ $title }}</h1>
	@if($page->allow_add_contents == 1)
	<div class="header-actions">
		<a
			class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
			data-mdc-auto-init="MDCRipple"
			href="{{route('adminClientPageContentCreate', $page->slug)}}"
			permission-action="create"
		>
			Create
		</a>
	</div>
	@endif
</header>

@stop

@section('content')
<div class="row">
@if(isset($data->clientContents))
	@if ($page->allow_add_contents == 1 || count($data->clientContents) > 0)
	<div class="col-sm-7">
		<div class="caboodle-card">
			<!-- <div class="caboodle-card-header">
			</div> -->
			<div class="flex align-center pad-top">
				<div class="flex-1 text-right no-margin margin-right">
					Publish
				</div>
				<div>
					<div class="mdc-switch no-margin">
						{!! Form::checkbox('published', '1', @$page->published == 'published', ['class'=>'mdc-switch__native-control publish-page', 'data-id' => $page->id]) !!}
						<div class="mdc-switch__background">
							<div class="mdc-switch__knob"></div>
						</div>
					</div>
				</div>
			</div>
			{!! Form::open(['route'=>['adminClientPageContentDestroy', $page->slug], 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
				<div class="caboodle-card-body">
					@if (count($data->clientContents) > 0) 
					{!! Form::open(['route'=>'adminPages', 'method' => 'get']) !!}
					<table class="caboodle-table">
						<thead>
							<tr>
								<th width="30px"></th>
								@if($page->allow_add_contents == 1)
								<th width="50px">
	                                <div class="mdc-form-field" data-toggle="tooltip" title="Select all products">
	                                    <div class="mdc-checkbox caboodle-table-select-all">
	                                        <input type="checkbox" class="mdc-checkbox__native-control" name="select_all"/>
	                                        <div class="mdc-checkbox__background">
	                                            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
	                                            <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
	                                            </svg>
	                                            <div class="mdc-checkbox__mixedmark"></div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </th>
	                            <th colspan="1" class="caboodle-table-col-action hide">
	                                <div class="dropdown actions-dropdown">
	                                    <button class="caboodle-btn caboodle-btn-medium caboodle-btn-cancel mdc-button mdc-ripple-upgraded btn-custom-width" type="button" id="tableActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-mdc-auto-init="MDCRipple">
	                                        Actions <i class="fas fa-caret-down"></i>
	                                    </button>
	                                    <div class="dropdown-menu" aria-labelledby="tableActions">
	                                        <a class="dropdown-item caboodle-table-action" 
	                                            href="#"
	                                            data-toggle-alert="warning"
	                                            data-alert-form-to-submit=".form-delete"
	                                            permission-action="delete"
	                                            >Delete Content</a>
	                                    </div>
	                                </div>
	                            </th>
								@endif
								<th class="caboodle-table-col-header">Name</th>
								<th width="48px"></th>
							</tr>
						</thead>
						<tbody id="sortable" sortable-data-url="{{route('adminPageContentsOrder')}}">
							@foreach ($data->clientContents as $d)
								<tr sortable-id="page_contents-{{$d->id}}">
									<td>
										<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i>
									</td>
									@if($page->allow_add_contents == 1)
										<td>
											<div class="mdc-form-field">
												<div class="mdc-checkbox">
													<input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}"/>
													<div class="mdc-checkbox__background">
														<svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
															<path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
														</svg>
														<div class="mdc-checkbox__mixedmark"></div>
													</div>
												</div>
											</div>
										</td>
									@endif
									<td>
										@if($page->allow_add_contents == 1)
											@if (@$d->controls()->first()->type == "asset")
												<img src="{{asset(@$d->controls()->first()->asset->path)}}" style="width: 100px; height: 80px; object-fit: contain;"/>
											@else
												{{@$d->controls()->first()->value }}
											@endif
										@else
											{{$d->name}}
										@endif
									</td>
									<td class="text-center">
										<a
											href="{{route('adminClientPageContentEdit', [$page->slug,$d->id])}}"
											class="mdc-icon-toggle animated-icon"
											data-toggle="tooltip"
											@if($d->editable == 1)
											title="Edit"
											@else
											title="View"
											@endif
											role="button"
											aria-pressed="false"
											permission-action="edit"
										>
											@if($d->editable == 1)
											<i class="far fa-edit" aria-hidden="true"></i>
											@else
											<i class="far fa-external-link" aria-hidden="true"></i>
											@endif
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					
					{!! Form::close() !!} 
					@else
						@if($page->allow_add_contents == 0)
						<div class="empty text-center">
							No results found
						</div>
						@else
						<a href="{{route('adminClientPageContentCreate', $page->slug)}}">
							<div class="empty-message {{@$data->clientContents->count() > 0 ? 'hide' : ''}}" role="button">
								Add a page content here...
							</div>
						</a>
						@endif
					@endif
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	@endif
@endif
	<div class="col-sm-5">
		<div class="row">
			<div class="col-sm-12">
				<div class="caboodle-card">
					<div class="caboodle-card-header">
						<h4>Search Engine Optimization</h4>
					</div>
					<div class="caboodle-card-body">
						<div class="seo-url" data-url="{{route('adminPagesSeo')}}">
							@include('admin.seo.form')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section ('added-scripts')
@include('admin.pages.partials.added-script-ordering')
	<script>

		$('body').on('input','input.publish-page',function(){
            var input = $(this);
            var id = input.data('id');
            var status = (input.prop('checked')) ? 'published' : 'draft' ;

            $.ajax({
                type: "POST",
                headers: {"X-CSRF-TOKEN": "{{ csrf_token() }}"}, 
                url: "{!! route('adminClientPageToggle') !!}",
                data: {id: id, status: status},
                dataType: "JSON",
                success: function(res){
                    if(res.success){
                        console.log(res.message);
                    }else{
                        console.log(res.message);
                    };
                }, error: function(xhr){
                    console.log(xhr);
                }
            });
		});
	</script>
@stop

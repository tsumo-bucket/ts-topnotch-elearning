@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatches') }}">Batches</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchesEdit', $data->batch->id) }}">{{ $data->batch->name }}</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchStudents', $data->batch->id) }}">Students</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchStudents', $data->batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminBatchStudents', $data->batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <a href="#" data-email="{{ $data->user->email }}" data-id="{{ $data->user->id }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button rerun-watermark" data-toggle="tooltip" title="Regenerate handouts" >
            <i class="far fa-file-pdf"></i>
        </a>
        <a href="#" data-email="{{ $data->user->email }}" data-id="{{ $data->user->id }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button password-reset" data-toggle="tooltip" title="Reset Password" >
            <i class="far fa-lock"></i>
        </a>
        <a href="#" data-email="{{ $data->user->email }}" data-id="{{ $data->user->id }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button resend-welcome" data-toggle="tooltip" title="Resend initial email" >
            <i class="far fa-handshake"></i>
        </a>

        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>

    </div>
</footer>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                {!! Form::model($data, ['route'=>['adminBatchStudentsUpdate', $data->user->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
                @include('admin.batch_students.form')
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('added-scripts')
    <script>
    $('.password-reset').on('click', function(e){
        e.preventDefault();
        var email = $(this).attr('data-email');
        var id = $(this).attr('data-id');
        var url = "{{ route('adminBatchStudentsResetPassword') }}";
        swal({
          type: 'warning',
          title: 'Reset Password',
          text: 'Reset password for ' + email + '?',
          showCancelButton: true
        }, function(confirm){
          if(confirm == true){
            showNotifyToaster('warning', '', 'Sending password reset link...');
            $.ajax({
              method: 'post',
              url: url,
              data: {
                id: id,
                email: email
              },
              success: function(response){
                showNotifyToaster(response.notifStatus, '', response.notifTitle);
              }
            });
          }
        });
    });

    $('.rerun-watermark').on('click', function(e){
        e.preventDefault();
        var email = $(this).attr('data-email');
        var id = $(this).attr('data-id');
        var url = "{{ route('adminBatchStudentsRerunWatermark') }}";
        swal({
          type: 'warning',
          title: 'Rerun handout watermarking',
          text: 'Rerun handout watermarking for ' + email + '?',
          showCancelButton: true
        }, function(confirm){
          if(confirm == true){
            showNotifyToaster('warning', '', 'Resources are being watermarked...');
            $.ajax({
              method: 'post',
              url: url,
              data: {
                id: id,
                email: email
              },
              success: function(response){
                showNotifyToaster(response.notifStatus, '', response.notifTitle);
              }
            });
          }
        });
    });

    $('.resend-welcome').on('click', function(e){
        e.preventDefault();
        var email = $(this).attr('data-email');
        var id = $(this).attr('data-id');
        var url = "{{ route('adminBatchStudentsResendWelcome') }}";
        swal({
          type: 'warning',
          title: 'Resend email',
          text: 'Resend welcome email to ' + email + '?',
          showCancelButton: true
        }, function(confirm){
          if(confirm == true){
            showNotifyToaster('warning', '', 'Updating user...');
            $.ajax({
              method: 'post',
              url: url,
              data: {
                id: id,
                email: email
              },
              success: function(response){
                showNotifyToaster(response.notifStatus, '', response.notifTitle);
              }
            });
          }
        });
      });
    </script>
@endsection
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatches')}}">Batches</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchesEdit', $batch->id)}}">{{ $batch->name }}</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchGrades', $batch->id)}}">Grades</a></li>
        <li class="breadcrumb-item far active"><span>Edit</span></li>
    </ol>
</nav>
@stop 


@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        @if (@$grades)
            <a href="{{ route('adminBatchGrades', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Back</a>
            <a href="{{ route('AdminBatchGradesCreate', [$batch->id, $user->student_number]) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-light mdc-button mx-2" data-mdc-auto-init="MDCRipple">Input</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
            @else
            <a href="{{ route('adminBatchGrades', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button  " data-mdc-auto-init="MDCRipple">Back</a>
            <a href="{{ route('AdminBatchGradesCreate', [$batch->id, $user->student_number]) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Input</a>
        @endif
    </div>
</header>
@stop

@section('content')
    <form action="{{ route('adminBatchGradesUpdate') }}" class="form form-parsley form-edit" method="post">
        @csrf
        <input type="hidden" name="batch_id" class="form form-parsley" value="{{ $batch->id }}">
        <input type="hidden" name="user_id" value="{{ $user->id }}">
        <div class="row">
            @if (@$grades)
                    @foreach ($grades as $period => $grade)
                        <div class="col-sm-12" id="{{ $period }}">
                            <h4>{{ $grade['metric'] }} <small><a class="remove-grade" data-url="{{ route('adminBatchGradesDestroy') }}" data-user-id="{{ $user->id }}" data-period="{{ $period }}" href="#" data-toggle="tooltip" title="Remove" ><i class="far fa-times"></i></a></small></h4>
                            <div class="caboodle-card">
                                <div class="caboodle-card-body">
                                    <div class="caboodle-form-group">
                                        <label for="raw_score">Raw Score</label>
                                        <input type="number" name="grades[{{$period}}][raw_score]" id="raw_score" value="{{$grade['raw_score']}}" min="0" required class="form-control">
                                    </div>
                                    <div class="caboodle-form-group">
                                        <label for="percentile_rank">Percentile Rank</label>
                                        <input type="number" name="grades[{{$period}}][percentile_rank]" id="percentile_rank" value="{{$grade['percentile_rank']}}" min="0" required class="form-control">
                                    </div>
                                    <div class="caboodle-form-group">
                                        <label for="color_grade">Color Grade</label>
                                        {!! Form::select('grades['.$period.'][color_grade]', ['RED'=>'RED', 'CRIMSON RED'=>'CRIMSON RED', 'ORANGE'=>'ORANGE','YELLOW'=>'YELLOW', 'GREEN'=>'GREEN'] ,@$grade['color_grade'], ['class'=>'form-control', 'id'=>'color_grade']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            @else
                <div class="col-sm-12">
                    <div class="caboodle-card">
                        <div class="caboodle-card-body">
                            <p class="text-center my-3">No grades submitted yet.</p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </form>
@endsection

@section('added-scripts')
    <script>
        $(document).on('click', '.remove-grade', function(e){
            e.preventDefault();

            var url = $(this).attr('data-url');
            var userId = $(this).attr('data-user-id');
            var period = $(this).attr('data-period');

            $.ajax({
                method: 'post',
                url: url,
                data: {
                    user_id: userId,
                    period: period,
                    _token: $('meta[name=csrf-token]').attr('content')
                },
                success: function(response){

                    $('#'+period).remove();
                    showNotifyToaster(response.notifStatus, '', response.notifTitle);
                    $('.form-edit').load(location.href + ' .form-edit');

                }
            })

        });
    </script>
@endsection
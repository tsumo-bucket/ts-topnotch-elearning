@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatches')}}">Batches</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchesEdit', $batch->id)}}">{{ $batch->name }}</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchGrades', $batch->id)}}">Grades</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchGradesEdit', [$batch->id,$user->student_number])}}">Manage Grades</a></li>
        <li class="breadcrumb-item far active"><span>Input Grade</span></li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchGradesEdit', [$batch->id,$user->student_number]) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Back</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <form class="form form-parsley form-create" action="{{ route('adminBatchGradesStore') }}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <div class="col-sm-6 col-12 p-0">
                            <div class="form-group" style="margin-bottom: 30px;" >
                                <label for="period" class="d-block mb-2" >Metric</label>
                                {!! Form::select('period', @$batch_metrics, null, ['class'=>'form-control input-sm']) !!}
                                {{-- <div class="custom-control custom-radio custom-control-inline">
                                    <input required value="diagnostic" checked type="radio" id="diagnostic" name="period" class="custom-control-input">
                                    <label class="custom-control-label" for="diagnostic">Diagnostic</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input required value="premidterm" type="radio" id="premidterm" name="period" class="custom-control-input">
                                    <label class="custom-control-label" for="premidterm">Pre-Midterm</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input required value="midterm" type="radio" id="midterm" name="period" class="custom-control-input">
                                    <label class="custom-control-label" for="midterm">Midterm</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input required value="prefinals" type="radio" id="prefinals" name="period" class="custom-control-input">
                                    <label class="custom-control-label" for="prefinals">Pre-Finals</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input required value="finals" type="radio" id="finals" name="period" class="custom-control-input">
                                    <label class="custom-control-label" for="finals">Finals</label>
                                </div> --}}
                            </div>
                        </div>
                        <div class="caboodle-form-group">
                            <label for="raw_score">Raw Score</label>
                            <input type="number" name="raw_score" id="raw_score" value="" min="0" required class="form-control">
                        </div>
                        <div class="caboodle-form-group">
                            <label for="percentile_rank">Percentile Rank</label>
                            <input type="number" name="percentile_rank" id="percentile_rank" value="" min="0" required class="form-control">
                        </div>
                        <div class="caboodle-form-group">
                            <label for="color_grade">Color Grade</label>
                            {!! Form::select('color_grade', ['RED'=>'RED', 'CRIMSON RED'=>'CRIMSON RED', 'ORANGE'=>'ORANGE','YELLOW'=>'YELLOW', 'GREEN'=>'GREEN'] ,null, ['class'=>'form-control', 'id'=>'color_grade']) !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
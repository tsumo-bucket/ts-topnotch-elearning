
<div class="caboodle-form-group">
  <label for="name">Student Number</label>
  {!! Form::number('student_number', @$data->user->student_number, ['class'=>'form-control', 'id'=>'student_number', 'placeholder'=>'Student Number', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="first_name">Given Name</label>
  {!! Form::text('first_name', @$data->user->first_name, ['class'=>'form-control', 'id'=>'first_name', 'placeholder'=>'Given Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="last_name">Family Name</label>
  {!! Form::text('last_name', @$data->user->last_name, ['class'=>'form-control', 'id'=>'last_name', 'placeholder'=>'Family Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="email">Email Address</label>
  {!! Form::email('email', @$data->user->email, ['class'=>'form-control', 'id'=>'email', 'placeholder'=>'Email Address', 'required']) !!}
</div>

@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatches') }}">Batches</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchesEdit', $batch->id) }}">{{ $batch->name }}</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchStudents', $batch->id) }}">Students</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchStudents', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminBatchStudents', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <form action="{{ route('adminBatchStudentsAdd') }}" class="form form-parsley form-create" method="post">
                        @csrf
                        <input type="hidden" name="batch_id" value="{{ $batch->id }}">
                        @include('admin.batch_students.form')
                        <div class="caboodle-form-group">
                            <label for="address">Contact No.</label>
                            {!! Form::number('contact_no', null, ['class'=>'form-control', 'id'=>'contact_no', 'placeholder'=>'Contact No.']) !!}
                        </div>
                        <div class="caboodle-form-group">
                            <label for="address">Address</label>
                            {!! Form::text('address', null, ['class'=>'form-control', 'id'=>'address', 'placeholder'=>'Address']) !!}
                        </div>
                        <div class="caboodle-form-group">
                            <label for="birthdate">Birthdate</label>
                            {!! Form::text('birthdate', null, ['class'=>'form-control date', 'id'=>'birthdate', 'placeholder'=>'Birthdate']) !!}
                        </div>
                        <div class="caboodle-form-group">
                            <label for="school">School</label>
                            {!! Form::text('school', null, ['class'=>'form-control', 'id'=>'school', 'placeholder'=>'School']) !!}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('added-scripts')
    <script>
        $('.date').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    </script>
@endsection
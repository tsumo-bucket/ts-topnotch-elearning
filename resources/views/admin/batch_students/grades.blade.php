@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatches')}}">Batches</a></li>
        <li class="breadcrumb-item far" ><a href="{{route('adminBatchesEdit', $batch->id)}}">{{ $batch->name }}</a></li>
        <li class="breadcrumb-item far active"><span>Grades</span></li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchesEdit', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Back</a>
        <a href="{{ route('adminBatchMetrics', $batch->id) }}" class="mx-1 caboodle-btn caboodle-btn-large caboodle-btn-light mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Metrics</a>
        <a href="#" data-toggle="modal" data-target="#importGradeModal" class="mx-1 caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Import</a>
        {{-- <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button> --}}
    </div>
</header>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                    <div class="filters no-padding">
                        {!! Form::open(['method' => 'get', 'class'=>'no-margin']) !!}
                        <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                            <label class="no-margin single-search no-padding">
                                {!! Form::text('keyword', @$keyword, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Search name']) !!}
                                <button>
                                    <i class="fa fa-search"></i>
                                </button>
                            </label>
                            <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="caboodle-card-body">
                    @if ($data->count() > 0)
                        <table class="caboodle-table caboodle-table-thin">
                            <thead>
                                <th style="width: 150px;" class="caboodle-table-col-header" >Student #</th>
                                <th class="caboodle-table-col-header" >Name</th>
                                <th class="caboodle-table-col-header" >Email</th>
                                <th class="caboodle-table-col-header text-right" ></th>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                    <tr>
                                        <td>{{ $d->student_number }}</td>
                                        <td>{{ $d->name }}</td>
                                        <td>{{ $d->email }}</td>
                                        <td class="text-right" >
                                            <a 
                                                href="{{route('adminBatchGradesEdit', [$batch->id, $d->student_number])}}"
                                                aria-expanded="false"
                                                class="btn btn-default" >
                                                Manage Grades
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-center my-2">No students found.</p>
                    @endif
                    @if ($pagination ?? '')
                        <div class="caboodle-pagination">
                            {{$data->links('layouts.pagination')}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="importGradeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form id="importGradeModalForm" accept-charset="UTF-8" method="POST" action="{{ route('adminBatchGradesImport') }}" enctype="multipart/form-data" class="form">
                    <div class="modal-header">
                        <h4 class="modal-title">Import Grades</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body" >
                        @csrf
                        <input accept=".csv" name="csv" type="file" class="mb-2" >
                        <p>Download the <a href="{{ route('adminBatchGradesDownload', $batch->id) }}">sample template</a> to see an example of the format required.</p>
                        <input type="hidden" name="batch_id" value="{{ $batch->id }}">
                        <label for="period"><strong>Metric</strong></label>
                        {!! Form::select('period', @$batch_metrics, null, ['class'=>'form-control input-sm']) !!}
                        {{-- <div class="custom-control custom-radio">
                            <input value="diagnostic" checked type="radio" id="diagnostic" name="period" class="custom-control-input">
                            <label class="custom-control-label" for="diagnostic">Diagnostic</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input value="premidterm" type="radio" id="premidterm" name="period" class="custom-control-input">
                            <label class="custom-control-label" for="premidterm">Pre-Midterm</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input value="midterm" type="radio" id="midterm" name="period" class="custom-control-input">
                            <label class="custom-control-label" for="midterm">Midterm</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input value="prefinals" type="radio" id="prefinals" name="period" class="custom-control-input">
                            <label class="custom-control-label" for="prefinals">Pre-Finals</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input value="finals" type="radio" id="finals" name="period" class="custom-control-input">
                            <label class="custom-control-label" for="finals">Finals</label>
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Cancel</button>
                        <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('added-scripts')

    <script>
        $('#importGradeModal form').on('submit',function(e){
            e.preventDefault();

            var form = document.getElementById('importGradeModalForm');
            var formData = new FormData(form);
            var request = new XMLHttpRequest();
            var url = $(this).attr('action');

            showLoader(true);

            request.open("post", url, true);
            request.onload = function(e) {

                showLoader(false);
                $("#importGradeModal").modal('hide');

                var result = JSON.parse(request.response);

                showNotifyToaster(
                    result.notifStatus,
                    (result.notifTitle != '' || typeof result.notifTitle != 'undefined' ) ? result.notifTitle : '',
                    (result.notifMessage != '' || typeof result.notifMessage != 'undefined' ) ? result.notifMessage : '',
                );
                if(typeof result.redirect != 'undefined'){
                    setTimeout(function() {
                    window.location = result.redirect;
                    }, 1500);
                }
            };

            request.send(formData);

        });
    </script>
@endsection


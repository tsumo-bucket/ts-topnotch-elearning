@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('adminBatches')}}">Batches</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('adminBatchesEdit', $batch->id)}}">{{ $batch->name }}</a></li>
    <li class="breadcrumb-item far active"><span>Students</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchesEdit', $batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Back</a>
        @if (count($batch->students) > 0 && $batch->status != 'completed' )
          <a
              role="button"
              class="caboodle-btn caboodle-btn-large caboodle-btn-danger mdc-button mdc-button--unelevated disable-all-btn"
              data-mdc-auto-init="MDCRipple"
              data-id="{{ $batch->id }}"
              data-url="{{ route('adminBatchStudentDisable') }}"
              href="#"
          >
              Disable All
          </a>
        @endif
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mx-1"
            data-mdc-auto-init="MDCRipple"
            href="#"
            data-toggle="modal"
            data-target="#importModal"
        >
            Import
        </a>
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mx-1"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminBatchStudentsExport', ['batch_id' => $batch->id]) }}"
        >
            Download
        </a>
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mx-1"
            data-mdc-auto-init="MDCRipple"
            href="#"
            data-toggle="modal"
            data-target="#transferModal"
        >
            Transfer
        </a>
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminBatchStudentsCreate', $batch->id) }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div id="importModal" class="modal fade import-modal" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      <form method="POST" action="{{ route('adminBatchStudentsStore') }}" accept-charset="UTF-8" class="form form-upload" id="importModalForm" enctype="multipart/form-data">
          <div class="modal-header">
              @csrf
              <input type="hidden" name="batch_id" value="{{ $batch->id }}">
              <h4 class="modal-title">Import Students</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
          </div>
          <div class="modal-body">
              <input accept=".csv" name="csv" type="file" class="mb-2" >
              <p>This enrolls all students in the CSV file into the currently selected batch.</p>
              <p>Students with existing student IDs or emails are ignored during file processing.</p>
              <p>Download the <a href="{{ route('adminBatchStudentsDownloadCsv') }}">sample template</a> to see an example of the format required.</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Cancel</button>
              <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Upload</button>
          </div>
      </form>
      </div>
  </div>
</div>
<div id="transferModal" class="modal fade transfer-modal" role="dialog">
  <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
      <form method="POST" action="{{ route('adminBatchStudentsTransfer') }}" accept-charset="UTF-8" class="form form-upload" id="transferModalForm" enctype="multipart/form-data">
          <div class="modal-header">
              @csrf
              <input type="hidden" name="batch_id" value="{{ $batch->id }}">
              <h4 class="modal-title">Transfer Students</h4>
              <button type="button" class="close" data-dismiss="modal">×</button>
          </div>
          <div class="modal-body">
              <input accept=".csv" name="csv" type="file" class="mb-2" >
              <p>This transfers all students identified in the CSV file into the currently selected batch</p>
              <p>Entries with no matching email or student # are automatically ignored</p>
              <p>Download the <a href="{{ route('adminBatchTransferDownloadCsv') }}">sample template</a> to see an example of the format required.</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Cancel</button>
              <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Upload</button>
          </div>
      </form>
      </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('keyword', @$keyword, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Student name']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminBatchStudentsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <input type="hidden" name="batch_id" value="{{ $batch->id }}">
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action" colspan="5">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                      data-mdc-auto-init="MDCRipple"
                      href="{{ route('adminBatchStudentsDestroy') }}"
                      method="DELETE"
                      data-toggle-alert="warning"
                      data-alert-form-to-submit=".form-delete"
                      permission-action="delete"
                      data-notif-message="Deleting...">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Student #</th>
                  <th class="caboodle-table-col-header hide" >Name</th>
                  <th class="caboodle-table-col-header hide" >Email</th>
                  <th class="caboodle-table-col-header hide" >Received Credentials</th>
                  <th class="caboodle-table-col-header hide" >Status</th>
                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td>{{$d->student_number}}</td>
                    <td>{{$d->name}}</td>
                    <td class="sub-text-1" >{{$d->email}}</td>
                    <td  >{{ $d->email_verified_at != null ? 'YES' : 'NO'  }}</td>
                    <td>
                      @if ($batch->status != 'completed')
                        <div class="mdc-switch">
                          {!! Form::checkbox('status', @$d->status, @$d->status == 'active', ['class'=>'mdc-switch__native-control row-data-control enabled-switch status-switch', 'data-id'=>$d->id]) !!}

                          <div class="mdc-switch__background"  >
                              <div class="mdc-switch__knob"></div>
                          </div>
                        </div>
                      @else
                        Completed
                      @endif
                    </td>
                    <td width="110px" class="text-center">
                        <a href="{{route('adminBatchStudentsEdit', $d->batch->id)}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                        @if ($d->email_verified_at != null)
                          <a href="#" data-email="{{ $d->email }}" data-id="{{ $d->id }}" class="mdc-icon-toggle animated-icon password-reset" data-toggle="tooltip" title="Reset Password" >
                            <i class="far fa-lock"></i>
                          </a>
                        @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination ?? '')
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('added-scripts')
    <script>
      $('.status-switch').on('change', function(e){
        var _this = $(this);
        var id = _this.attr('data-id');
        var checked = _this.prop('checked');
        var status = '';
        var token = $('meta[name=csrf-token]').attr('content');
        if(checked){
          status = 'active';
        }
        else{
          status = 'blocked';
        }
        $.ajax({
          type: 'POST',
          url: "{{route('adminUserStatusUpdate')}}",
          data: {_method: 'patch', status: status, _token: token, id: id},
          beforeSend: function(){
            _this.css('pointer-events', 'none');
          },
          success: function(res){
            if(res) {
              if(res.notifStatus == 'success') {
                  showNotifyToaster('success','', `Status Updated`);

              } else {
                  showNotifyToaster('error','', res.notifTitle);
                  setTimeout(() => {
                      location.reload();
                  }, 1000);
              }
              _this.css('pointer-events', 'auto');
            }
          }
        });

      });

      $('.password-reset').on('click', function(e){
        e.preventDefault();
        var email = $(this).attr('data-email');
        var id = $(this).attr('data-id');
        var url = "{{ route('adminBatchStudentsResetPassword') }}";
        swal({
          type: 'warning',
          title: 'Reset Password',
          text: 'Reset password for ' + email + '?',
          showCancelButton: true
        }, function(confirm){
          if(confirm == true){
            showNotifyToaster('warning', '', 'Sending password reset link...');
            $.ajax({
              method: 'post',
              url: url,
              data: {
                id: id,
                email: email
              },
              success: function(response){
                showNotifyToaster(response.notifStatus, '', response.notifTitle);
              }
            });
          }
        });
      });

      $('.disable-all-btn').on('click', function(e){
        e.preventDefault();

        var url = $(this).attr('data-url');
        var data = {
          '_token': $('meta[name=csrf-token]').attr('content'),
          'batch_id': $(this).attr('data-id')
        };

        $.ajax({
          method: 'POST',
          url: url,
          data: data,
          success: function(response){
            showNotifyToaster(response.notifStatus, response.notifTitle, response.notifMessage);
            setTimeout(() => {
              location.reload();
            }, 1500);
          }
        });

      });

      $('#importModal form').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('importModalForm');
        var formData = new FormData(form);
        var request = new XMLHttpRequest();
        var url = $(this).attr('action');
        showLoader(true);
        request.open("post", url, true);
        request.onload = function(e) {
          showLoader(false);
          $("#importModal").modal('hide');
          var result = JSON.parse(request.response);
          console.log(result); 

          showNotifyToaster(
            result.notifStatus,
            (result.notifTitle != '' || typeof result.notifTitle != 'undefined' ) ? result.notifTitle : '',
            (result.notifMessage != '' || typeof result.notifMessage != 'undefined' ) ? result.notifMessage : '',
          );
          if(typeof result.redirect != 'undefined'){
            setTimeout(function() {
              window.location = result.redirect;
            }, 1500);
          }
        };

        request.send(formData);
      });

      $('#transferModal form').on('submit', function(e){
        e.preventDefault();
        var form = document.getElementById('transferModalForm');
        var formData = new FormData(form);
        var request = new XMLHttpRequest();
        var url = $(this).attr('action');
        showLoader(true);
        request.open("post", url, true);
        request.onload = function(e) {
          showLoader(false);
          $("#transferModal").modal('hide');
          var result = JSON.parse(request.response);
          console.log(result); 

          showNotifyToaster(
            result.notifStatus,
            (result.notifTitle != '' || typeof result.notifTitle != 'undefined' ) ? result.notifTitle : '',
            (result.notifMessage != '' || typeof result.notifMessage != 'undefined' ) ? result.notifMessage : '',
          );
          if(typeof result.redirect != 'undefined'){
            setTimeout(function() {
              window.location = result.redirect;
            }, 1500);
          }
        };

          request.send(formData);
      });
    </script>
@endsection
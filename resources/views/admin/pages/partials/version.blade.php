<div class="caboodle-card">
	<div class="caboodle-card-header pad-top-15 pad-bottom-15">
		<div class="no-padding">
			<div class="flex align-center caboodle-form-control-connected">
				<h4 class="no-margin flex-1">
					Versions
				</h4>
                <a href="{{route('adminClientPageContentCreate', [$page->slug, @$content->id])}}" class="add-form-control-toggle btn-transparent btn-sm text-right">
					Create new version
				</a>
            </div>
		</div>
	</div>
	<div class="caboodle-card-body">
        {{-- @if (count(@$versions) > 0) 
            {!! Form::open(['route'=>['adminPreviewDestroy', $page->slug, $content->id], 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
                <thead>
                    <tr>
                        <th width="50px">
                            <div class="mdc-form-field" data-toggle="tooltip" title="Select all">
                                <div class="mdc-checkbox caboodle-table-select-all">
                                    <input type="checkbox" class="mdc-checkbox__native-control" name="select_all"/>
                                    <div class="mdc-checkbox__background">
                                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                        <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                        </svg>
                                        <div class="mdc-checkbox__mixedmark"></div>
                                    </div>
                                </div>
                            </div>
                        </th>
                        <th colspan="1" class="caboodle-table-col-action hide">
                            <div class="dropdown actions-dropdown">
                                <button class="caboodle-btn caboodle-btn-medium caboodle-btn-cancel mdc-button mdc-ripple-upgraded btn-custom-width" type="button" id="tableActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-mdc-auto-init="MDCRipple">
                                    Actions <i class="fas fa-caret-down"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="tableActions">
                                    <a class="dropdown-item caboodle-table-action" 
                                        href="#"
                                        data-toggle-alert="warning"
                                        data-alert-form-to-submit=".form-delete"
                                        permission-action="delete"
                                        >Delete</a>
                                </div>
                            </div>
                        </th>
                        <th class="caboodle-table-col-header">Name</th>
                        <th width="48px"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($versions as $version)
                        <tr>
                            <td>
                                <div class="mdc-form-field">
                                    <div class="mdc-checkbox">
                                        <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $version->id }}"/>
                                        <div class="mdc-checkbox__background">
                                            <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                                <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                            </svg>
                                            <div class="mdc-checkbox__mixedmark"></div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>
                                {{$version->created_at}}
                            </td>
                            <td width="110px" class="text-right">
                                <a href="{{route('adminClientPageContentPreviewEdit',[$page->slug,$content->id,$version->id])}}" 
                                    data-toggle="tooltip"
                                    title="Edit"
                                    role="button"
                                    aria-pressed="false"
                                    permission-action="edit">
                                    <i class="far fa-edit" aria-hidden="true"></i>
                                </a>
                                <a href="{{route('pagePreview',$version->slug)}}" 
                                    data-toggle="tooltip"
                                    title="Preview"
                                    role="button"
                                    aria-pressed="false"
                                    permission-action="edit" 
                                    target="_blank">
                                    <i class="far fa-external-link" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {!! Form::close() !!} 
        @else
            <div class="empty text-center">
                No version found
            </div>
        @endif --}}
	</div>
</div>
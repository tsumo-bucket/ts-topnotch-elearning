<input type="hidden" name="page_category_id" value="{{ @$category->id }}">
<div class="caboodle-form-group">
    <label for="name">Name</label>
    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
@if (isset($data))
<div class="caboodle-form-group">
    <label for="slug">Slug</label>
    {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'Slug','readonly']) !!}
</div>
@endif
<div class="caboodle-form-group">
<label for="published">Published</label>
{!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
</div>
<div class="caboodle-form-group">
<label for="content">Content</label>
{!! Form::textarea('content', null, ['class'=>'form-control redactor', 'id'=>'content', 'placeholder'=>'Content', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
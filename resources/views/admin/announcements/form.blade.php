<div class="caboodle-form-group">
  <label for="title">Title</label>
  {!! Form::text('title', null, ['class'=>'form-control', 'id'=>'title', 'placeholder'=>'Title', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="batches">Batch Restrictions (Optional)</label>
  {!! Form::select('batches[]', @$all_batches, @$data->branches, ['id'=>'batches','class'=>'form-control','multiple']) !!}
</div>
@if (@$category != 'banner')
  <div class="caboodle-form-group">
    <label for="description">Description</label>
    {!! Form::textarea('description', null, ['class'=>'form-control redactor', 'id'=>'evaluation', 'placeholder'=>'Description', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
  </div>
@endif
<div class="caboodle-form-group">
  <div class="form-row">
    <div class="col">
      <label for="start_date">Start Date</label>
      {!! Form::text('start_date', null, ['class'=>'form-control datetimepicker', 'id'=>'start_date']) !!}
    </div>
    <div class="col">
      <label for="end_date">End Date</label>
      {!! Form::text('end_date', null, ['class'=>'form-control datetimepicker', 'id'=>'end_date']) !!}
    </div>
  </div>
</div>

@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>{{ @$breadcrumb }}</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminAnnouncementsCreate') }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminAnnouncements', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left align-items-end">
              <div>
                <label class="p-0" for="">Title</label>
                {!! Form::text('title', @$filters['title'], ['class'=>'form-control input-sm', 'placeholder'=>'Title']) !!}
              </div>
              <div class="mx-2" >
                <label class="p-0" for="">Batch</label>
                {!! Form::text('batch', @$filters['batch'], ['class'=>'form-control input-sm', 'placeholder'=>'Batch']) !!}
              </div>
              <button class="caboodle-btn caboodle-btn-small caboodle-btn-primary mdc-button mdc-button--unelevated" >Filter</button>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
            <input type="hidden" name="category" value="{{ @$category }}">
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminAnnouncementsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminAnnouncementsDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting...">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Title</th>
                  @if (@$category != 'banner')  
                    <th class="caboodle-table-col-header hide" width="200px" >Description</th>
                  @endif
                  <th class="caboodle-table-col-header hide" >Start Date</th>
                  <th class="caboodle-table-col-header hide" >End Date</th>
                  <th class="caboodle-table-col-header hide" >Batch</th>
                    
                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td >{{$d->title}}</td>
                    @if (@$category != 'banner')  
                      <td class="uppercase sub-text-1" ><span class="line-clamp">{{$d->description}}</span></td>
                    @endif
                    <td class="uppercase sub-text-1" >{{@$d->start_date ? Carbon::parse(@$d->start_date)->format('M j, Y h:i A') : '---'}}</td>
                    <td class="uppercase sub-text-1" >{{@$d->end_date ? Carbon::parse(@$d->end_date)->format('M j, Y h:i A') : '---'}}</td>
                    <td >
                      @if (count(@$d->batches) == 0)
                        <span class="uppercase sub-text-1" >ALL</span>
                      @else
                        @foreach (@$d->batches as $item)
                            <span class="badge badge-primary">{{ $item->batch->name }}</span>
                        @endforeach
                      @endif
                    </td>
                    <td width="110px" class="text-center">
                      @if (Auth::user()->type == 'super' || Auth::user()->type == 'admin')
                        <a href="{{route('adminAnnouncementsEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop
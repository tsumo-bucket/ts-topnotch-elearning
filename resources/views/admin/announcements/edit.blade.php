@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminAnnouncements') }}">{{ @$breadcrumb }}</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminAnnouncements') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                {!! Form::model($data, ['route'=>['adminAnnouncementsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
                @include('admin.announcements.form')
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

@section('added-scripts')
    <script>
        $(document).ready(function(){
            $('#batches').select2({
                multiple: true
            });
        });

        $('.datetimepicker').each(function(i, el){

            var settings = {
                sideBySide: true,
                useCurrent: false,
                format: 'lll'
            };

            var value = $(this).val();

            if (value != '') {
                settings.date = moment(value);
            }

            $(this).datetimepicker(settings);
        });
    </script>
@endsection
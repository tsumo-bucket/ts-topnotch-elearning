<table class='table table-striped table-bordered'>
	<tr>
		<th>Title</th>
		<td>{{$data->title}}</td>
	</tr>
	<tr>
		<th>Description</th>
		<td>{{$data->description}}</td>
	</tr>
</table>
<a href='{{route("adminAnnouncementsView", [$data->id])}}' class='btn btn-primary'>More Details</a>
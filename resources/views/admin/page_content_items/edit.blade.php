@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
    
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
			<span>Pages</span>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{ route('adminClientPage', $page->slug)}}">{{$page->name}}</a>
		</li>
		{{--<li class="breadcrumb-item far">
			<a href="{{route('adminClientPageContentEdit', [$page->slug, $content->id]) }}">
				@if ($page->allow_add_contents != 1)
					Content
				@else
					{{ $content->controls()->first()->value }}
				@endif
			</a>
		</li>--}}
		<li class="breadcrumb-item far active">
			@if($data->editable == 1)
			<span>Edit Item</span>
			@else
			<span>View Item</span>
			@endif
		</li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
	<div class="header-actions">
		@if ($page->allow_add_contents !== 1 && count($page->pageContent) == 1)
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminClientPage', $page->slug) }}">Cancel</a>
		@else
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{route('adminPageContentsEdit', [$page->slug, $content->id]) }}">Cancel</a>
		@endif
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
	</div>
</header>
@stop


@section('footer')
<footer>
	<div class="text-right">
		@if ($page->allow_add_contents !== 1 && count($page->pageContent) == 1)
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminClientPage', $page->slug) }}">Cancel</a>
		@else
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{route('adminPageContentsEdit', [$page->slug, $content->id]) }}">Cancel</a>
		@endif
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
	</div>
</footer>
@stop

@section('content')
  	{!! Form::model($data, ['route'=>['adminClientPageContentItemUpdate', $page->slug, $content->id, $item->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit form-clear']) !!}
		@include('admin.page_content_items.form')
	{!! Form::close() !!}
@stop
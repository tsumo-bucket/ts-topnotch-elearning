@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatches') }}">Batches</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    @if(!@$data->seo)
        <div class="header-actions">
            <a href="{{ route('adminBatches') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    @endif
</header>
@stop

@if(!@$data->seo)
    @section('footer')
    <footer>
        <div class="text-right">
            <a href="{{ route('adminBatches') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    </footer>
    @stop
@endif

@section('content')
    {!! Form::model($data, ['route'=>['adminBatchesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
    <div class="row">
        <div class="col-sm-8">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    @include('admin.batches.form')
                </div>
            </div>
            {{-- <div class="caboodle-card">
                <div class="caboodle-card-body" id="lecturesTable">
                    <div class="caboodle-flex caboodle-flex-space-between flex-align-center" >
                        <h4 class="no-margin"><i class="fas fa-window-restore"></i> LECTURES</h4>
                        <a href="{{ route('adminBatches') }}" class="txt-primary" style="font-size: 18px;" data-mdc-auto-init="MDCRipple" data-toggle="modal" data-target="#lecturesModal" ><i data-toggle="tooltip" title="Add Lectures" class="far fa-plus-square"></i></a>
                    </div>
                    @if (@$data->lectures->count() > 0)
                        <table class="caboodle-table"  >
                            <thead>
                                <th width="150px" >Name</th>
                                <th>Description</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach ($data->lectures as $l)
                                    <tr>
                                        <td>
                                            <a href="{{route('adminLecturesEdit', $l->lecture->id)}}">{{ $l->lecture->name }}</a></td>
                                        <td class="sub-text-1" >{{ substr($l->lecture->description, 0, 40) }}...</td>
                                        <td width="110px" class="text-right">
                                            @if (Auth::user()->type == 'super' || Auth::user()->type == 'admin')
                                                <a 
                                                    role="button"
                                                    data-id="{{ $l->id }}" 
                                                    class="animated-icon remove-lecture-btn" 
                                                    data-toggle="tooltip"
                                                    title="Remove"
                                                    aria-pressed="false">
                                                    <i class="far fa-times" aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-center my-3" >No lectures assigned.</p>
                    @endif
                </div>
            </div> --}}
            <div class="caboodle-card" id="resources-container">
                <input type="hidden" name="batch_id" id="batch_id" value="{{ @$data->id }}">
                <div class="caboodle-card-body">
                    <div class="form-box sumo-asset-select-multi" data-id="{{ @$mainImages->id }}" data-name="BatchMainImages">
                        <div class="caboodle-flex caboodle-flex-space-between caboodle-flex-align-center sumo-asset-multiple-header">
                            <h4><i class="fas fa-copy"></i> RESOURCES</h4>
                            {!! Form::hidden('main_images', json_encode(@$mainImages), ['class'=>'sumo-asset-multiple']) !!}
                        </div>
                    </div>
                    <p class="card-text sub-text-1">
                        <strong>Note:</strong> For pdf uploads, please convert the pdf file to version <u>1.4</u>,
                        follow this link to convert: <a target="__blank" href="https://docupub.com/pdfconvert/">https://docupub.com/pdfconvert/</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <a style="display: flex; align-items: center;" href="{{ route('adminBatchStudents', [$data->id] ) }}" class="text-light caboodle-btn caboodle-btn-large caboodle-btn-light btn-block mdc-button mdc-button--unelevated mb-2" data-mdc-auto-init="MDCRipple"><i style="width: 35%; text-align: right; padding-right: 10px" class="fas fa-users"></i> Manage Students</a>
            <a style="display: flex; align-items: center;" href="{{ route('adminBatchLectures', $data->id ) }}" class="text-light caboodle-btn caboodle-btn-large caboodle-btn-light btn-block mdc-button mdc-button--unelevated mb-2" data-mdc-auto-init="MDCRipple"><i style="width: 35%; text-align: right; padding-right: 10px" class="fas fa-window-restore"></i> Manage Lectures</a>
            <a style="display: flex; align-items: center;" href="{{ route('adminBatchGrades', [$data->id] ) }}" class="text-light caboodle-btn caboodle-btn-large caboodle-btn-light btn-block mdc-button mdc-button--unelevated mb-2" data-mdc-auto-init="MDCRipple"><i style="width: 35%; text-align: right; padding-right: 10px" class="fas fa-address-book"></i> Manage Grades</a>
            <a style="display: flex; align-items: center;" href="{{ route('adminBatchEmail', [$data->id] ) }}" class="text-light caboodle-btn caboodle-btn-large caboodle-btn-light btn-block mdc-button mdc-button--unelevated mb-4" data-mdc-auto-init="MDCRipple"><i style="width: 35%; text-align: right; padding-right: 10px" class="fas fa-envelope"></i> Batch Email</a>
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <div class="caboodle-form-group">
                        <h4 for="status">STATUS</h4>
                        <select name="status" class="form-control">
                            @if (@$data->status == 'draft')
                                <option @if(@$data->status == 'draft') selected @endif value="draft">Draft</option>
                            @endif
                            <option @if(@$data->status == 'ongoing') selected @endif value="ongoing">Ongoing</option>
                            <option @if(@$data->status == 'completed') selected @endif value="completed">Completed</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('added-scripts')
    <script>

        var inputMainImages = $('input[name=main_images]');

        var ajaxData = {
            _token: $('meta[name=csrf-token]').attr('content'),
            main_images: inputMainImages.val(),
            batch_id: $('#batch_id').val()
        }

        $('#assets-modal').find('.btn-select').on('click', function(){

            ajaxData.operation = 'add';
            ajaxData.main_images = inputMainImages.val();

            if(inputMainImages.val() != '""'){
                setTimeout(() => {
                    sendRequest(ajaxData, true)
                }, 1000);
            }
        });

        $(document).on('click', '.sumo-asset-file-container .remove', function(e){

            var asset_id = $(this).parents('.sumo-asset-file-container').data('id');

            $('.tooltip').css('display', 'none');

            ajaxData.operation = 'remove';
            ajaxData.main_images = inputMainImages.val();

            setTimeout(() => {
                sendRequest(ajaxData);
            }, 1000);
        });

        $(document).on('click', '.clear-btn', function(e){

            e.preventDefault();

            var asset_id = $(this).parents('.sumo-asset-file-container').data('id');
            
            var main_images_val = JSON.parse(inputMainImages.val());

            main_images_val.assets.map(item => {
                if (item.asset_id == asset_id) {
                    item.start_date = null;
                    item.end_date = null;
                }
            });

            $(this).parents('.sumo-asset-file-container').find('input').val('');

            inputMainImages.val(JSON.stringify(main_images_val));

            ajaxData.main_images = inputMainImages.val(),
            ajaxData.operation = 'update';
            sendRequest(ajaxData);
        });

        $(document).find('.datetimepicker').each(function(){

            var value = $(this).val();

            var settings = {
                sideBySide: true,
                useCurrent: false,
                format: 'lll'
            };

            if (value != '') {
                settings.date = moment(new Date(value)).format('lll')
            }

            $('.datetimepicker').datetimepicker(settings);

            var asset_id = $(this).data('asset-id');
            var name = $(this).attr('name');

            $(this).on('dp.hide', function(e){

                console.log(e.date);

                var main_images_val = JSON.parse(inputMainImages.val());

                var errors = 0;

                main_images_val.assets.map(item => {
                    if (item.asset_id == asset_id) {
                        item[name] = moment(e.date).format()

                        if (item.start_date != null && item.end_date != null) {
                            if (moment(item.start_date) >= moment(item.end_date)) {

                                showNotifyToaster('error', 'Invalid dates', 'End date must be set after the start date')
                                errors = 1;
                            }
                        }
                    }
                });
                
                if (errors > 0) {
                    $(this).val('')
                    return;
                }

                inputMainImages.val(JSON.stringify(main_images_val));

                ajaxData.main_images = inputMainImages.val(),
                ajaxData.operation = 'update';
                sendRequest(ajaxData, true);
            })
        });

        function sendRequest(data, blur = false) {
            $.ajax({
                method: 'post',
                url: "{{route('adminBatchesSavePDF')}}",
                data: data,
                beforeSend: function(){
                    if (blur) {
                        $('#resources-container').css({
                            'opacity' : '0.5',
                            'pointer-events' : 'none'
                        });
                    }
                },
                success: function() {
                    if (blur) {
                        $('#resources-container').css({
                            'opacity' : '1',
                            'pointer-events' : 'auto'
                        });
                    }
                }
            })
        }

    </script>
@endsection
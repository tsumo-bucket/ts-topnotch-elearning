<script id="batch-email-table-row-template" type="text/x-handlebars-template" >
    <tr>
        <td>
            <div class="custom-control custom-checkbox">
                <input type="checkbox" {{checked}} data-name="{{ student.name }}" data-email="{{ student.email }}" class="custom-control-input student-checkbox" value="{{ student.id }}" id="checkbox-{{ student.id }}">
                <label class="custom-control-label" for="checkbox-{{ student.id }}">
                    {{ student.student_number }}<br>
                    <small><strong>{{ student.name }}</strong> <span class="sub-text-1" style="font-size: 10px;" >&lt;{{ student.email }}&gt;</span></small>
                </label>
            </div>
        </td>
    </tr>
</script>
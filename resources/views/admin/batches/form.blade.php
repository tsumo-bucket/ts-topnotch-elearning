<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="boards_date">Board Exams Date</label>
  {!! Form::text('boards_date', null, ['class'=>'form-control datepicker', 'id'=>'boards_date', 'placeholder'=>'Board exams date']) !!}
</div>
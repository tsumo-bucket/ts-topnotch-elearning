@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatches') }}">Batches</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchesEdit', @$batch->id) }}">{{ ucwords(@$batch->name) }}</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminBatchesEdit', @$batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Send</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminBatchesEdit', @$batch->id) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Send</button>
    </div>
</footer>
@stop

@section('content')
    {!! Form::open(['route'=>'adminBatchEmailSend', 'files' => true, 'method' => 'post', 'class'=>'form form-parsley form-send']) !!}
    <input type="hidden" name="batch_id" id="batch_id" value="{{ @$batch->id }}">
    <div class="row">
        <div class="col-sm-5">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <h6>Filters</h6>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <select name="query_type" id="query_type" class="custom-select rounded-0">
                                    <option value="name">Student Name</option>
                                    <option value="student_number">Student Number</option>
                                </select>
                            </div>
                            <input type="text" name="keyword" id="keyword" class="form-control" placeholder="Keyword">
                        </div>
                    </div>
                    <div class="text-right mt-2">
                        <a href="" class="btn btn-primary btn-sm" id="btn-apply-filter" >Apply Filter</a> 
                    </div>
                    <h6>Student List</h6>
                    <div style="height: 200px; position: relative;" id="loader" >
                        <div class="loader" style="height: 200px;" >
                            <svg class="circular" viewBox="25 25 50 50">
                                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </div>
                    </div>
                    <div style="max-height: 300px; overflow: auto;" >
                        <table class="table table-hover table-sm" id="table-students">
                            <thead>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkbox-all">
                                        <label class="custom-control-label sub-text-1" for="checkbox-all">Select All</label>
                                    </div>
                                </th>
                            </thead>
                            <tbody id="table-students-body" >
                                {{-- @foreach ($student_list as $s)
                                    <tr>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" data-name="{{ $s->name }}" data-email="{{ $s->email }}" name="checked_students[]" class="custom-control-input student-checkbox" value="{{$s->id}}" id="checkbox-{{$s->id}}">
                                                <label class="custom-control-label" for="checkbox-{{$s->id}}">
                                                    {{ $s->student_number }}<br>
                                                    <small><strong>{{ ucwords($s->name) }}</strong> <span class="sub-text-1" style="font-size: 10px;" >&lt;{{ $s->email }}&gt;</span></small>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                    <div id="see-more-container">
                    </div>
                </div>
            </div>
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <h6>Selected Students</h6>
                    <div id="selected-students" style="max-height: 200px; overflow: auto;" ></div>
                    <div id="clear-all-container"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <input type="hidden" name="emails" id="emails-field">
                    <div class="caboodle-form-group">
                        <label for="from_name">From Name</label>
                        {!! Form::text('from_name', null, ['class'=>'form-control', 'id'=>'from_name']) !!}
                    </div>
                    <div class="caboodle-form-group">
                        <label for="subject">Subject</label>
                        {!! Form::text('subject', null, ['class'=>'form-control', 'id'=>'subject', 'required']) !!}
                    </div>
                    <div class="caboodle-form-group">
                        <label for="message">Message</label>
                        {!! Form::textarea('message', null, ['class'=>'form-control redactor', 'id'=>'message', 'required', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

@section('added-scripts')
@include('admin.batches.partials.table-row-template')
<script>

    var checkedArr = [];

    var studentsTableBody = $('#table-students-body');

    var emailsField = $('#emails-field');

    $(document).ready(function(){
        $('.student-select').select2();
        fetchStudents();
    });


    // Filter
    $('#btn-apply-filter').on('click', function(e){
        e.preventDefault();

        fetchStudents(true);
    });

    // Fetch Students
    function fetchStudents(filter = false){
        $.ajax({
            method: 'get',
            url: "{{ route('adminBatchEmailFilter') }}",
            data: {
                query_type: $('#query_type').val(),
                keyword: $('#keyword').val(),
                batch_id: $('#batch_id').val(),
                _token: $('meta[name=csrf-token]').attr('content')
            },
            beforeSend: function(){
                if(filter == true){
                    $('#loader').removeClass('hide');
                }
                $('#table-students').addClass('hide');
            },
            success: function(response){

                $('#loader').addClass('hide');

                studentsTableBody.html('');

                var template = Handlebars.compile($('#batch-email-table-row-template').html());

                response.items.data.forEach(function(object, index){

                    var checked = $.grep(checkedArr, function(value, i){
                        return value.id == object.id
                    });

                    studentsTableBody.append(template({
                        student: object,
                        checked: checked.length < 1 ? '' : 'checked'
                    }));
                });

                $('#table-students').removeClass('hide');

                seeMore(response.items.next_page_url);

                if($('.student-checkbox:not(:checked)').length > 0){
                    $('#checkbox-all').prop('checked', false);
                }
            }
        });
    }

    // See more button
    function seeMore(url){
        $('#see-more-container').html('');
        if(url != null){
            $('#see-more-container').append("<a href='#' data-url='"+url+"' class='btn btn-primary btn-block btn-sm mt-2' id='see-more' >See More</a>");
        }
    }

    // See more button click
    $(document).on('click', '#see-more', function(e){
        e.preventDefault();

        $.ajax({
            method: 'get',
            url: $(this).attr('data-url'),
            data: {
                query_type: $('#query_type').val(),
                keyword: $('#keyword').val(),
                batch_id: $('#batch_id').val(),
                _token: $('meta[name=csrf-token]').attr('content')
            },
            success: function(response){

                var template = Handlebars.compile($('#batch-email-table-row-template').html());

                response.items.data.forEach(function(object, index){

                    var checked = $.grep(checkedArr, function(value, i){
                        return value.id == object.id
                    });

                    studentsTableBody.append(template({
                        student: object,
                        checked: checked.length < 1 ? '' : 'checked'
                    }));
                });

                seeMore(response.items.next_page_url);

                if($('.student-checkbox:not(:checked)').length > 0){
                    $('#checkbox-all').prop('checked', false);
                }
            }
        })
    });

    // Select all
    $('#checkbox-all').change(function(){
        if($(this).prop('checked') == true){
            $('.student-checkbox').prop('checked', true);
            
            $('.student-checkbox:checked').each(function(index, element){
                addItem({
                    id: this.value,
                    email: $(this).attr('data-email')
                });
            }); 
        }
        else{
            $('.student-checkbox').prop('checked', false);

            $('.student-checkbox:not(:checked)').each(function(index, element){
                removeItem({
                    id: this.value,
                    email: $(this).attr('data-email')
                });
            });
        }
        syncStudents();
    });

    // Student checkbox click
    $(document).on('change', '.student-checkbox', function(){

        var item = {
            id: this.value,
            email: $(this).attr('data-email')
        };

        if($(this).prop('checked') == true){
            addItem(item);
        }
        else{
            removeItem(item);
        }
        syncStudents();
    });

    // Student remove
    $(document).on('click', '.remove-student', function(e){
        e.preventDefault();

        var id = $(this).attr('data-id');

        $('#checkbox-'+id).prop('checked', false);

        removeItem({
            id: id,
        });

        syncStudents();
    });

    // Add item
    function addItem(item){

        var exist = $.grep(checkedArr, function(obj){
            return obj.id == item.id;
        });

        if(exist.length < 1){
            checkedArr.push(item);
        }
    }

    // Remove Item
    function removeItem(item){
        
        checkedArr = $.grep(checkedArr, function(obj){
            return obj.id != item.id;
        });
    }

    // Sync student list
    function syncStudents(){
        if($('.student-checkbox:not(:checked)').length > 0){
            $('#checkbox-all').prop('checked', false);
        }

        $('#selected-students').html('');

        checkedArr.forEach(function(item){
            $('#selected-students').append('<span data-id="'+item.id+'" class="badge badge-pill badge-secondary mr-1">'+item.email+'<a href="#" class="text-light remove-student" data-id="'+item.id+'" ><i class="fas fa-times ml-1"></i></a></span>');
        })

        emailsField.val(JSON.stringify(checkedArr));

        $('#clear-all-container').html('');

        if(checkedArr.length > 0){
            $('#clear-all-container').append("<a href='#' id='clear-all' class='btn btn-sm btn-block btn-primary mt-2' >Clear</a>");
        }
    }

    // Clear all
    $(document).on('click', '#clear-all', function(e){
        e.preventDefault();

        checkedArr = [];
        emailsField.val(JSON.stringify(checkedArr));

        $('.student-checkbox').prop('checked', false);
        $('#selected-students').html('');
        $('#clear-all-container').html('');
        $('#checkbox-all').prop('checked', false);
    });

</script>
@endsection
<table class='table table-striped table-bordered'>
	<tr>
		<th>Name</th>
		<td>{{$data->name}}</td>
	</tr>
	<tr>
		<th>Status</th>
		<td>{{$data->status}}</td>
	</tr>
</table>
<a href='{{route("adminBatchesView", [$data->id])}}' class='btn btn-primary'>More Details</a>
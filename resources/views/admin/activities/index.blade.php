@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Activities</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminActivities', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Keyword']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          <table class="caboodle-table">
            <thead>
              <tr>
                <th class="caboodle-table-col-header hide" >User</th>
                <th class="caboodle-table-col-header hide" >Message</th>
                <th class="caboodle-table-col-header hide" >Timestamps</th>
                <th colspan="6" ></th>
              </tr>
            </thead>
            <tbody>
              @foreach($data as $d)
                <tr>
                  <td >{{$d->user->name}}</td>
                  <td>{{$d->log}}</td>
                  <td>{{ date('F j, Y \a\t\ h:i A', strtotime($d->created_at)) }}</td>
                    
                  <td width="110px" class="text-center">
                    @if (Auth::user()->type == 'super' || Auth::user()->type == 'admin')
                      <a href="{{route('adminActivitiesView', [$d->id])}}" 
                          class="mdc-icon-toggle animated-icon" 
                          data-toggle="tooltip"
                          title="View"
                          role="button"
                          aria-pressed="false"
                          permission-action="edit">
                          <i class="far fa-search" aria-hidden="true"></i>
                      </a>
                    @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('content')
<div class="col-sm-12">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-table"></i> Table
      </div>
      <div class="right">
        <a class="btn-transparent btn-sm" href="{{route('adminActivities')}}"><i class="fa fa-eye"></i> Show All</a>
      </div>
    </div>
    <div class="filters">
      {!! Form::open(['route'=>'adminActivities', 'method' => 'get']) !!}
      <label>
        Search: {!! Form::text('log', $keyword, ['class'=>'form-control input-sm', 'placeholder'=>'']) !!}
        <button><i class="fa fa-search"></i></button>
      </label>
      {!! Form::close() !!}
    </div>
    @if (count($data) > 0)
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th>Reference</th>
            <th>Message</th>
            <th>Date Created</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $d)
          <tr>
            <td><a href='{{route("adminActivitiesView", [$d->id])}}' class="text-info">{{$d->identifier_value}}</a></td>
            <td>{{$d->log}}</td>
            <td>{{$d->created_at}}</td>
            <td width="140px" class="text-center">

              <a href='{{route("adminActivitiesView", [$d->id])}}' class='btn btn-primary btn-sm'>View</a>
             <!--  <button type="button" class="btn btn-primary btn-sm" role="button" data-toggle="popover"
                data-trigger="click" title="{{$d->product_id}}" data-placement="left" data-html="true"
                data-content="@include('admin.activities.show', ['data' => $d])">
                VIEW
              </button> -->
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @if ($pagination)
      <div class="pagination-links text-right">
        {!! $pagination !!}
      </div>
      @endif
    </div>
    @else
    <div class="empty text-center">
      No results found
    </div>
    @endif
  </div>
</div>
@stop

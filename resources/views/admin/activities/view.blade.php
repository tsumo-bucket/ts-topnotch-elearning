@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far"><a href="{{route('adminActivities')}}" >Activities</a></li>
    <li class="breadcrumb-item far active"><span>View</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminActivities') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminActivities') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</footer>
@stop

@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="caboodle-card">
            <div class="caboodle-card-header">
                <h6 class="no-margin">ACTIVITY INFO</h6>
                <span class="sub-text-1">{!! $data->log !!}</span>
            </div>
            <div class="caboodle-card-body">
                <div class="row">
                    <div class="col-sm-1 my-1">
                        <b>ID: </b> <span class="sub-text-1">{!! $data->id !!}</span>
                    </div>
                    <div class="col-sm-2 my-1">
                        <b>Reference: </b> <span class="sub-text-1">{!! $data->identifier_value !!}</span>
                    </div>
                    <div class="col-sm-3 my-1">
                        <b>Name: </b> <span class="sub-text-1">{!!$data->user->name!!}</span>
                    </div>
                    <div class="col-sm-3 my-1">
                        <b>Created at: </b> 
                        <span class="sub-text-1">
                            {{ date('F j, Y \a\t\ h:i A', strtotime($data->created_at)) }}
                        </span>
                    </div>
                    <div class="col-sm-3 my-1">
                        <b>Updated at: </b>
                        <span class="sub-text-1">
                            {{ date('F j, Y \a\t\ h:i A', strtotime($data->updated_at)) }}
                        </span>
                    </div>
                </div>
            </div>
        </div>

        
        @if ($data->value_from)
            <?php $from = json_decode($data->value_from,true); ?>
            <?php $keys = array_keys($from); ?>
        @endif
        @if($data->value_to)
            <?php $to = json_decode($data->value_to,true); ?>
            <?php $tkeys = array_keys($to); ?>
            @if(isset($keys))
                <?php $keys = array_unique(array_merge($keys,$tkeys)); ?>
            @else
                <?php $keys = $tkeys ?>
            @endif
        @endif
        <div class="row">
            <div class="col-sm-6">
                <div class="caboodle-card">
                    <div class="caboodle-card-header">
                        <h6 class="no-margin">BEFORE</h6>
                    </div>
                    <div class="caboodle-card-body">
                        <div class="row">
                            @foreach($keys as $key)
                                @if($data->value_from && array_key_exists($key,$to))
                                    @if($key !== 'content')
                                    <div class="col-sm-6 my-3">
                                        <b>{{ strtoupper($key) }}: </b> <span class="sub-text-1" style="width: 100%;" >{!!@$from[$key] !!}</span>
                                    </div>
                                    @else
                                    <div class="col-sm-12 my-3">
                                        <b>{{ strtoupper($key) }}: </b> <br><br>
                                        {!!@$from[$key] !!}
                                    </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="caboodle-card">
                    <div class="caboodle-card-header">
                        <h6 class="no-margin">AFTER</h6>
                    </div>
                    <div class="caboodle-card-body">
                        <div class="row">
                            @foreach($tkeys as $key)
                                @if($data->value_from && array_key_exists($key,$to))
                                    @if($key !== 'content')
                                    <div class="col-sm-6 my-3">
                                        <b>{{ strtoupper($key) }}: </b> <span class="sub-text-1">{!! is_array($to[$key])? $to[$key][0] : $to[$key] !!}</span>
                                    </div>
                                    @else
                                    <div class="col-sm-12 my-3">
                                        <b>{{ strtoupper($key) }}: </b> <br><br>
                                        {!! is_array($to[$key])? $to[$key][0] : $to[$key] !!}
                                    </div>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop


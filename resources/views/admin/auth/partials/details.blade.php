@if (count(@$banners) > 0)
    <div class="banner-details-container">
        @foreach (@$banners as $banner)
            <div class="details">
                <h1 class="heading">{{ @$banner->name }}</h1>
                <h3 class="sub-heading">{{ @$banner->caption }}</h3>
            </div>
        @endforeach
    </div>
@endif
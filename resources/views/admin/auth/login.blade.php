@extends('layouts.admin-auth')

@section('content')
  @if (session('notifStatus'))
    <div class="text-center alert alert-{{ session('notifStatus') == 'success' ? 'success':'danger' }}">
        <i class="fas fa-{{ session('notifStatus') == 'success' ? 'check-circle-0':'exclamation-circle' }}"></i> {{ session('notifMessage') }}
    </div>
  @endif
  <div class="card">
    <div class="card-body">
      <div class="text-center">
        <img src="{{ asset('img/admin/topnotch-logo.png') }}" class="img-fluid" alt="">
        <hr>
      </div>
      {!! Form::open(['route'=>'authenticate']) !!}
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="far fa-envelope"></i>
            </span>
          </div>
          <input type="email" name="email" class="form-control form-control-sm" id="email" placeholder="Email" value="{{ old('email') }}">
        </div>
      </div>
      <div class="form-group">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="far fa-lock"></i>
            </span>
          </div>
          <input type="password" name="password" class="form-control form-control-sm" id="password" placeholder="Password">
        </div>
      </div>
      <div class="form-group text-center">
        <button type="submit" class="btn btn-primary btn-block">Login</button>
      </div>
      {!! Form::close() !!}
    </div>
    <div class="card-footer text-center">
      <a class="text-center" href="{{ route('forgotPassword') }}">Forgot password?</a>
    </div>
  </div>
@stop

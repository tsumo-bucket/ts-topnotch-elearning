@extends('layouts.admin-auth')

@section('content')
@if (session('notifStatus'))
    <div class="text-center alert alert-{{ session('notifStatus') == 'success' ? 'success':'danger' }}">
        <i class="fas fa-{{ session('notifStatus') == 'success' ? 'check-circle-0':'exclamation-circle' }}"></i> {{ session('notifMessage') }}
    </div>
@endif
<div class="card">
    <div class="card-body">
        <div class="text-center">
        <img src="{{ asset('img/admin/topnotch-logo.png') }}" class="img-fluid" alt="">
        <hr>
        </div>
        {!! Form::open(['route'=>'passwordResetSubmit']) !!}
        <input type="hidden" name="user_id" value="{{ $user->id }}">
        <input type="hidden" name="token" value="{{ $reset->token }}">
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="far fa-lock"></i>
                    </span>
                </div>
                <input placeholder="New Password" required type="password" name="new_password" class="form-control form-control-sm" id="new_password" value="">
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <i class="far fa-lock"></i>
                    </span>
                </div>
                <input placeholder="Confirm Password" required type="password" name="confirm_password" class="form-control form-control-sm" id="confirm_password" value="">
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-block btn-primary btn-sm">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
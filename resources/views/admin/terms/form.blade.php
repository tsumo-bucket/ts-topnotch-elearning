<input type="hidden" name="page_category_id" value="{{ @$category->id }}">
<div class="caboodle-form-group">
    <label for="name">Title</label>
    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
<label for="content">Content</label>
{!! Form::textarea('content', null, ['class'=>'form-control redactor', 'id'=>'content', 'placeholder'=>'Content', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>
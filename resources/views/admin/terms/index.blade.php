@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Terms</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Cancel</a>
    </div>
</header>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    @if (count(@$data) > 0)
                        <table class="caboodle-table">
                            <thead>
                                <th class="caboodle-table-col-header hide" >Name</th>
                                <th class="caboodle-table-col-header hide" >Status</th>
                                <th class="caboodle-table-col-header hide" >Created at</th>
                                <th colspan="6" ></th>
                            </thead>
                            <tbody>
                                @foreach (@$data as $d)
                                    <tr>
                                        <td >{{$d->name}}</td>
                                        <td class="uppercase sub-text-1" >{{$d->published}}</td>
                                        <td class="uppercase sub-text-1" >{{date('F j, Y \a\t\ h:i A', strtotime($d->created_at))}}</td>
                                        
                                        <td width="110px" class="text-center">
                                        <a href="{{route('adminTermsEdit', [$d->id])}}" 
                                            class="mdc-icon-toggle animated-icon" 
                                            data-toggle="tooltip"
                                            title="Manage"
                                            role="button"
                                            aria-pressed="false"
                                            permission-action="edit">
                                            <i class="far fa-edit" aria-hidden="true"></i>
                                        </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="empty text-center">
                            No results found
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- @section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    {!! Form::model($data, ['route'=>['adminLegalUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
                        @include('admin.legal.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection --}}
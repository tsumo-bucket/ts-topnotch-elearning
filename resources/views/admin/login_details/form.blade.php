<div class="caboodle-form-group">
  <label for="user_id">User Id</label>
  {!! Form::text('user_id', null, ['class'=>'form-control', 'id'=>'user_id', 'placeholder'=>'User Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="ip">Ip</label>
  {!! Form::text('ip', null, ['class'=>'form-control', 'id'=>'ip', 'placeholder'=>'Ip', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="active">Active</label>
  <br />
   <?php $boolCheck = (@$data->active) ? 'disabled="disabled"': ''; ?>
     {!! Form::checkbox('active', '0', null , ['hidden',$boolCheck]) !!}
    {!! Form::checkbox('active', '1', null , ['data-toggle'=>'toggle','data-size'=>'small','class'=>'form-control ', 'id'=>'active','data-onstyle'=>'warning','data-on'=>"<i class='fa fa-star'></i> On",'data-off'=>"<i class='fa fa-star-o'></i> Off "]) !!}
</div>
<script>
    (function() {
    var assetModal = $('#videos-modal');
    var assetForm = $('#video-file-upload-form');
    var fileDropzone = document.getElementById('videos-modal');
    var fileDropzoneJquery = $('#video-file-dropzone');
    var fileLibrary = $('#video-file-library .files');
    var assetInput = assetModal.find('.input-file');
    var assetDetailForm = $('#video-detail-form');
    var assetDetail = assetModal.find('.asset-details');
    var assetSelect = $('.sumo-asset-select-video');
    var videosCard = $('#videos-card');
    var assetSelected = {
        container: '',
        id: '',
        path: ''
    };
    var assetDisplay = $('.sumo-asset-display');
    var assetInitLoad = false;
    var assetLoadMoreBtn = assetModal.find('.btn-more');
    var loaderModal = $('.loader-asset');
    var loaderDetails = assetModal.find('.loader-detail');
    var photos, notify;

    var openModalBySB = true;
    var ajaxActiveCounter = 0;

    var numberOfSelected = 0;
    var numberOfCreatedMultiple = 0;

    var lectureId = $('#lecture-id').attr('data-value');

    // handlebars Templates
    var assetImageHTML = Handlebars.compile($("#asset-image-template").html());
    var assetMultipleImageHTML = Handlebars.compile($("#asset-multiple-image-template").html());

    assetModal.on('shown.bs.modal', function() {
        if (!assetInitLoad) {
            getAssetList(0);
        }
    });

    function getAssetList(count) {
        var searchInput = assetModal.find('input[name="searchInput"]').val() ? assetModal.find('input[name="searchInput"]').val() : '';
        var sortBy = getSortBy();
        var url = fileLibrary.data('url');
        assetLoadMoreBtn.attr('disabled', 'disabled');
        $.ajax({
            type: 'GET',
            url: url,
            header: 'assetManager',
            data: {
                'count': count,
                'searchInput': searchInput,
                sortBy
            },
            dataType: 'json',
            success: function(data) {
                assetInitLoad = true;
                fileLibrary.append(data.view);
                assetModal.find('.loader-asset').addClass('hide');
                assetModal.find('.row-asset').removeClass('hide');
                assetLoadMoreBtn.removeAttr('disabled');
                bindFiles();
                findActiveAsset();
                if (!data.next) {
                    assetLoadMoreBtn.addClass('hide');
                } else {
                    assetLoadMoreBtn.removeClass('hide');
                }
            },
            error: function(data, text, error) {
                var count = fileLibrary.find('.file').length - 1;
                getAssetList(count);
                console.log(data);
                console.log(text);
                console.log(error);
            }
        });
    };

    function getSortBy() {
        var sortType = assetModal.find('select[name="sortBy"]').val();
        var sortBy;
        var sortOrderBy;

        if (sortType == 'oldest' || sortType == 'latest') {
            sortBy = 'created_at';
        } else if (sortType == 'az' || sortType == 'za') {
            sortBy = 'name';
        }

        if (sortType == 'oldest' || sortType == 'az') {
            sortOrderBy = 'ASC';
        } else if (sortType == 'latest' || sortType == 'za') {
            sortOrderBy = 'DESC';
        }

        return {
            order: sortBy,
            by: sortOrderBy
        };
    }

    function findActiveAsset() {
        fileLibrary.find('.file').removeClass('active');
        // fileLibrary.find('.file[data-id="' + assetSelected.id + '"]').addClass('active');
    }

    function getAssetDetails(id) {
        var url = assetDetail.data('url');
        $.ajax({
            type: 'GET',
            url: url,
            header: 'assetManager',
            data: {
                id: id
            },
            dataType: 'json',
            beforeSend: function() {
                ajaxActiveCounter += 1;
            },
            complete: function() {
                ajaxActiveCounter -= 1;
            },
            success: function(asset) {
                displayDetailsToForm(asset);
                loaderDetails.addClass('hide');
                assetDetail.removeClass('hide');
            },
            error: function(data, text, error) {}
        });
    }
    assetLoadMoreBtn.on('click', function() {
        // -1 because of clone file
        var count = fileLibrary.find('.file').length - 1;
        getAssetList(count);
    });

    function checkSelectedAsset(asset) {
        var container = asset;
        var id = container.find('.sumo-asset').val();
        if (!id) {
            container.find('.loader').remove();
            return false;
        };
        var url = assetDetail.data('url');

        $.ajax({
            type: 'GET',
            url: url,
            header: 'assetManager',
            data: {
                id: id
            },
            dataType: 'json',
            success: function(asset) {
                assetSelected.container = container;
                assetSelected.id = asset.id;
                assetSelected.path = asset.absolute_path;
                displaySelectedAsset();
            },
            error: function(data, text, error) {

            },
            complete: function(event, xhr, options) {
                container.find('.loader').remove();
            }
        });
    }

    function displaySelectedAsset() {
        fileLibrary.find('.file.active').each(function() {
            if ($(this).attr('data-id')) {
                var id = $(this).attr('data-id');
                var image = $(this).css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");

                assetSelected.path = image;
                assetSelected.id = id;
            };
        });

        /* var addVideoRequest = new XMLHttpRequest();
        var addVideoFormData = new FormData();

        addVideoFormData.append('_token', $('meta[name=csrf-token]').attr('content'));
        addVideoFormData.append('lecture_id', lectureId);
        addVideoFormData.append('video_id', assetSelected.id);

        addVideoRequest.open('POST', "{{route('adminLectureVideosStore')}}", true);
        addVideoRequest.onload = function(e){
            var notif = JSON.parse(this.responseText);
            showNotifyToaster(notif.notifStatus, '', notif.notifTitle);
            videosCard.load(location.href + " #videos-card");
        }

        addVideoRequest.send(addVideoFormData); */

        assetSelected.container.find('.sumo-asset-img-container').removeClass('hide').attr('data-id', assetSelected.id);
        assetSelected.container.find('img').attr('src', assetSelected.path);
        assetSelected.container.find('.sumo-asset').val(assetSelected.id);
        if (assetInitLoad) {
            assetSelected.container.find('.sumo-asset').addClass('unloadvalidation');
        }
    };

    assetModal.find('.btn-select').on('click', function() {
        displaySelectedAsset();
    });

    function displayDetailsToForm(asset) {

        assetSelected.id = asset.id;
        assetSelected.path = asset.absolute_path;
        assetDetailForm.find('.photo iframe').attr('src', asset.src_path);
        assetDetailForm.find('[name="id"]').val(asset.id);
        assetDetailForm.find('[name="name"]').val(asset.name);
        assetDetailForm.find('[name="description"]').val(asset.description);
        $('.asset-details').find('small').html('Uploaded at ' + moment(asset.created_at).format('LLL'));

    }

    assetDetailForm.submit(function(e) {
        e.preventDefault();
        var url = assetDetailForm.attr('action');

        // add fix for tag upload
        var data = {};
        data._token = assetForm.find('input[name="_token"]').val();
        data.id = assetDetailForm.find('[name="id"]').val();
        data.name = assetDetailForm.find('[name="name"]').val();
        data.description = assetDetailForm.find('[name="description"]').val();
        // upload da tags

        $.ajax({
            type: 'POST',
            url: url,
            header: 'assetManager',
            data: data,
            dataType: 'json',
            success: function(data) {
                showNotifyToaster('success', '', data.notifTitle);
                assetDetailForm.find('input, textarea').removeClass('check-on-unload');

            },
            error: function(data, text, error) {
                showNotifyToaster('error', '', 'An error occurred.');
            }
        });
    });

    //====== DOWNLOAD FILE
    assetModal.find('.download-btn').on('click', function(e) {
        e.preventDefault();
        var id = assetDetailForm.find('[name="id"]').val();
        var url = $(this).find('a').attr('href');
        window.open(url + '?id=' + id, '_blank');
    });

    //===== DELETE FUNCTIONS
    assetDetail.find('.delete-btn').on('click', function() {
        assetDetail.find('.delete-btn').addClass('hide');
        assetDetail.find('.confirm-btn').removeClass('hide');
    });
    assetDetail.find('.confirm-btn a').on('click', function(e) {
        e.preventDefault();
        resetDeleteBtns();
    });
    assetDetail.find('.confirm-btn .confirm-delete-btn').on('click', function(e) {
        var id = assetDetailForm.find('[name="id"]').val();
        var url = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: url,
            header: 'assetManager',
            data: assetDetailForm.serialize(),
            dataType: 'json',
            success: function(asset) {
                if(asset.notifStatus == 'success'){
                    fileLibrary.find('.file').removeClass('active');
                    fileLibrary.find('.file[data-id="' + id + '"]').remove();
                    assetDetail.addClass('hide');

                    showNotifyToaster(asset.notifStatus,'',asset.notifTitle);

                    //find the original form then deletes the old value if the id of deleted asset equals to the value..
                    $(document).find('.form-parsley .sumo-asset').each(function() {
                        var value = $(this).val();
                        if (value == id) {
                            $(this).val('');
                            $(this).next().attr('src', '');
                        }
                    });
                }
                else{
                    showNotifyToaster(asset.notifStatus,'',asset.notifTitle);
                }
            },
            error: function(data, text, error) {

            }
        });
    });

    function resetDeleteBtns() {
        assetDetail.find('.delete-btn').removeClass('hide');
        assetDetail.find('.confirm-btn').addClass('hide');
    }

    assetDetailForm.find('input:text, select').on('change', function() {
        if (!ajaxActiveCounter) {
            assetDetailForm.submit();
        }
    });

    //===== INITIALIZE IMAGE SELECTOR FROM FORM
    assetSelectInit();

    function assetSelectInit() {
        assetSelect.each(function() {
            var asset = $(this);
            var assetInput = asset.find('.sumo-asset');
            asset.find('label').append('<span style="border: 1px solid #ddd; margin-left:10px;" class="sumo-asset-select-button mdc-button caboodle-btn caboodle-btn-small caboodle-btn-cancel btn-icon-left select" data-mdc-auto-init="MDCRipple" data-toggle="modal" data-target="#videos-modal" href="#"><i class="far fa-image"></i> Select Video</span>');
            // if (! _.isUndefined(assetInput.data('thumbnail')) && ! _.isUndefined(assetInput.data('id'))) {
            // 	asset.find('label').append('<a class="caboodle-btn caboodle-btn-small mdc-button mdc-button--stroked caboodle-btn-secondary btn-icon-left crop" href="#"><i class="far fa-crop"></i>Crop</a>');
            // }
            // asset.find('label').append('<a class="remove" href="#">Remove</a>');
            var context = {
                visibility: 'hide',
                src: ''
            };
            var html = assetImageHTML(context);

            asset.append(html);
            asset.append('<div class="loader loader-asset"><div class="spinner"></div></div>');

            checkSelectedAsset(asset);
        });
    }

    $('select[name="sortBy"], select[name="filterBy"], select[name="filterByFileType"], input[name="searchInput"]').on('change', function() {
        $(".file[data-id!='']").remove();
        getAssetList(0);
    });

    fileDropzoneJquery.on('click', function() {
        assetInput.click();
    });

    function addEventHandler(obj, evt, handler) {
        if (obj.addEventListener) {
            obj.addEventListener(evt, handler, false);
        } else if (obj.attachEvent) {
            obj.attachEvent('on' + evt, handler);
        } else {
            obj['on' + evt] = handler;
        }
    }
    addEventHandler(fileDropzone, 'drop', function(e) {
        e = e || window.event;
        if (e.preventDefault) {
            e.preventDefault();
        }
        files = e.target.files;
        uploadFiles(e.dataTransfer.files);
        return false;
    });
    addEventHandler(fileDropzone, 'dragenter', function(e) {
        e.preventDefault();
        // console.log('dragenter');
    });
    addEventHandler(fileDropzone, 'dragover', function(e) {
        e.preventDefault();
        // console.log('dragover');
    });
    bindSelectBtns();

    function bindSelectBtns() {
        assetSelect.find('.select').on('click', function(e) {
            e.preventDefault();
            var btn = $(this);
            assetModal.find('.modal-footer .btn-select').removeClass('multi-select');
            openModalBySB = false;
            assetSelected.container = btn.closest('.sumo-asset-select-video');
            assetSelected.id = assetSelected.container.find('.sumo-asset').val();
            if (assetSelected.id) {
                getAssetDetails(assetSelected.id);
                findActiveAsset();
            };

            assetDetail.closest('.col-xs-3').removeClass('hide');
        });
        assetSelect.find('.crop').on('click', function(e) {
            e.preventDefault();
            hideLoader(false);
            showNotify('Retrieving URL.');

            var btn = $(this);
            var input = btn.closest('.sumo-asset-select-video').find('input');
            assetSelected.container = btn.closest('.sumo-asset-select-video');
            var url = btn.closest('.sumo-asset-select-video').data('crop-url');
            var data = {
                'id': input.data('id'),
                'column': input.data('thumbnail'),
                'asset_id': assetSelected.container.find('.sumo-asset').val()
            }
            $.ajax({
                type: 'GET',
                url: url,
                header: 'assetManager',
                data: data,
                dataType: 'json',
                success: function(data) {
                    showNotify('Redirecting.');
                    setTimeout(function() {
                        window.location = data.redirect;
                    }, 1000);
                },
                error: function(data, text, error) {

                }
            });
        });
        assetSelect.find('.remove').on('click', function(e) {
            e.preventDefault();
            var btn = $(this);
            var asset = btn.closest('.sumo-asset-select-video');
            asset.find('.sumo-asset-img-container').addClass('hide');
            asset.find('.sumo-asset').val('');
            asset.find('img').attr('src', '');
        });
        assetSelect.find('.preview').on('click', function(e) {
            e.preventDefault();
            if (!$(this).closest('.sumo-asset-select-multi').length) {
                var btn = $(this);

                var id = btn.parents('.sumo-asset-select-video').find('input.sumo-asset').val();
                var url = "{{route('adminVideosGetHTML')}}";

                var getVideoRequest = new XMLHttpRequest();
                var getVideoForm = new FormData();

                getVideoForm.append('_token', $('meta[name=csrf-token]').attr('content'));
                getVideoForm.append('id', id);

                getVideoRequest.open('POST', url, true);
                getVideoRequest.onload = function(e){
                    var response = JSON.parse(this.responseText);

                    // var selectedImage = btn.closest('.sumo-asset-img-container').find('img').attr('src');
                    // console.log(selectedImage);
                    $.fancybox.open([{
                        src: response.src,
                        type: 'iframe',
                    }], {
                        loop: false
                    });
                };

                getVideoRequest.send(getVideoForm);
            }
        })
    }

    function bindFiles() {
        fileLibrary.find('.file').each(function() {
            var file = $(this);
            file.unbind();
            file.on('click', function() {
                var clicked = $(this);
                if (!assetModal.find('.modal-footer .multi-select').hasClass('multi-select')) {
                    fileLibrary.find('.file').removeClass('active');
                }
                if (!clicked.hasClass('active')) {
                    loaderDetails.removeClass('hide');
                    assetDetail.addClass('hide');
                    clicked.addClass('active');
                    var id = clicked.data('id');
                    getAssetDetails(id);

                    numberOfSelected++;
                    clicked.attr('order', numberOfSelected);
                } else {
                    $('.asset-details').addClass('hide');
                    clicked.removeClass('active');

                    numberOfSelected--;
                    clicked.siblings('.file.active').each(function() {
                        if ($(this).attr('order') > clicked.attr('order')) {
                            $(this).attr('order', $(this).attr('order') - 1);
                        };
                    });
                    clicked.removeAttr('order');
                }
            })
        })
    };

    //===== UPLOAD ASSETS
    assetInput.change(function(e) {
        files = e.target.files;
        uploadFiles(files);
    });

    function uploadFiles(files) {
        assetModal.find('.loader-asset').removeClass('hide');
        assetModal.find('.row-asset').addClass('hide');
        if (files && files[0]) {
            loopFiles(files, 0);
        }
    }

    function loopFiles(files, index){

        var route = assetForm.attr('action');
        var request = [];
        var formdata = [];
        var types = [];
        var randomID;

        var localUploadResponse = [];

        var vimeoRequestForm = [];
        var vimeoSubmitForm = [];
        var vimeoRequestFormBody = [];
        var responses = [];

        var current_file;
        var current_type;

        var i = index;

        if(index < files.length){

            var filename = files[i].name;
            var filenameExtension = filename.substr(filename.lastIndexOf('.') + 1);

            if(filenameExtension != 'mp4'){
                showNotifyToaster('error','','Please upload an mp4 file.');
                assetModal.find('.loader-asset').addClass('hide');
                assetModal.find('.row-asset').removeClass('hide');
                return false;
            }

            if (files[i].type.match(/image.*/)) {
                types[i] = 'image';
            } else if (files[i].type.match(/video.*/)) {
                types[i] = 'video';
            } else if (files[i].type.match(/audio.*/)) {
                types[i] = 'audio';
            } else {
                types[i] = 'file';
            }

            current_file = files[i];
            current_type = types[i];

            //VIMEO REQUEST
            vimeoRequestForm[i] = new XMLHttpRequest();
            vimeoRequestFormBody[i] = {
                upload: {
                    approach: 'tus',
                    size: files[i].size,
                },
                name: filename.replace('.mp4', ''),
                embed: {
                    buttons: {
                        embed: false,
                        hd: true,
                        like: false,
                        share: false,
                        watchlater: false,
                    },
                    title: {
                        name: 'hide',
                        owner: 'hide',
                        portrait: 'hide'
                    },
                    logos: {
                        vimeo: false
                    }
                }
            };
            vimeoRequestForm[i].open('POST', 'https://api.vimeo.com/me/videos', true);

            vimeoRequestForm[i].setRequestHeader('Content-Type', 'application/json');
            vimeoRequestForm[i].setRequestHeader('Authorization', 'bearer '+"{{env('VIMEO_ACCESS_TOKEN')}}");

            vimeoRequestForm[i].onload = function(e){
                if (this.status === 200) {
                    assetModal.find('.loader-asset').addClass('hide');
                    assetModal.find('.row-asset').removeClass('hide');
                    assetModal.find('a[href="#video-file-library"]').tab('show');

                    responses[i] = JSON.parse(this.response);

                    // LARAVEL REQUEST
                    request[i] = new XMLHttpRequest();
                    formdata[i] = new FormData();

                    randomID = 'asst_' + Math.floor(Math.random() * 1000000000000);
                    formdata[i].append('type', current_type);
                    formdata[i].append('_token', assetForm.find('input[name="_token"]').val());
                    formdata[i].append('url', responses[i].uri);
                    formdata[i].append('name', filename.replace('.mp4', ''));
                    formdata[i].append('upload_link', responses[i].upload.upload_link)
                    
                    request[i].open('POST', route, true);

                    var test = request[i].onload = function(){
                        if(this.status == 200){
                            localUploadResponse[i] = JSON.parse(this.responseText);

                            // VIMEO UPLOAD VIDEO
                            vimeoSubmitForm[i] = new XMLHttpRequest();
                            vimeoSubmitForm[i].open('PATCH', localUploadResponse[i].upload_link, true);
                            vimeoSubmitForm[i].setRequestHeader('Tus-Resumable', '1.0.0');
                            vimeoSubmitForm[i].setRequestHeader('Upload-Offset', '0');
                            vimeoSubmitForm[i].setRequestHeader('Content-Type','application/offset+octet-stream');
                            
                            vimeoSubmitForm[i].onload = function(){
                                var data = localUploadResponse[i];
                                // console.log(data);
                                $('.' + randomID).attr('data-id', data.id);
                                $('.' + randomID).find('.progress').hide();
                                $('.' + randomID).css({
                                    'background-image': 'url(' + "{{ url('img/admin/file-video.png') }}" + ')'
                                });
                                $('.' + randomID).append('<span class="name">' + data.name + '</span>');

                            }
                            processPhoto(vimeoSubmitForm[i], current_type, randomID);

                            vimeoSubmitForm[i].send(current_file);
                        }
                    }

                    request[i].send(formdata[i]);

                    loopFiles(files, i+=1);
                }
            };
            
            vimeoRequestForm[i].send(JSON.stringify(vimeoRequestFormBody[i]));
        }
    }

    function processPhoto(vimeoRequest, type, id) {
        var asset = $('.' + id);
        fileLibrary.find('.clone').clone().prependTo(fileLibrary).removeClass('clone hide').addClass(id);
        vimeoRequest.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                var ratio = Math.floor((e.loaded / e.total) * 100);
                $('.' + id).find('.progress').attr('aria-valuenow', ratio);
                $('.' + id).find('.progress-bar').width(ratio + '%');
            }
        };
        /* request.addEventListener('load', function(e) {
            $('.' + id).attr('data-id', id);
            $('.' + id).find('.progress').hide();
            $('.' + id).css({
                'background-image': 'url(' + "{{ url('img/admin/file-video.png') }}" + ')'
            });
            $('.' + id).append('<span class="name">' + name + '</span>');
        }, false); */
        bindFiles(); 
    }
})();
</script>
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminLectures') }}">Lectures</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminLectures') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('content')
    {!! Form::model($data, ['route'=>['adminLecturesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
    <div class="row">
        <div class="col-sm-8">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    @include('admin.lectures.form')        
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <label for="status" style="margin-bottom: 5px;" >Status</label>
                    {!! Form::select('status', ['active' => 'Active', 'archive' => 'Archive'], null, ['class'=>'form-control']) !!}
                </div>
            </div>
            {{-- <a href="#" data-toggle="modal" data-target="#videos-modal" class="sumo-asset-select-button text-light caboodle-btn caboodle-btn-large caboodle-btn-light btn-block mdc-button mdc-button--unelevated mb-2" data-mdc-auto-init="MDCRipple"><i class="fas fa-video"></i> Add Video</a>
            <span class="hide" id="lecture-id" data-value="{{ $data->id }}" ></span>
            <div class="caboodle-card p-0" id="videos-card">
                <div class="caboodle-card-body p-0">
                    @if (count(@$data->videos) > 0)
                        <table class="caboodle-table videos-table">
                            <tbody >
                                @foreach (@$data->videos as $d)
                                    <tr>
                                        <td>
                                            <img src="{{ $d->video->vimeo['pictures']['sizes'][2]['link_with_play_button'] }}" alt="">
                                        </td>
                                        <td class="sub-text-1" >{{ $d->video->name }}</td>
                                        <td class="text-right" >
                                            <a href="#" class="video-delete" data-id="{{ $d->id }}" data-method="delete" >
                                                <i class="fas fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-center my-3">No videos uploaded</p>
                    @endif
                </div>
            </div> --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('added-scripts')
    <script>

        $(document).ready(function(){

            $.ajax({
                method: 'get',
                url: "{{ route('adminVideosGet') }}",
                data: {
                    id: "{{ $data->video_id }}",
                    _token: $('meta[name=csrf-token]').attr('content')
                },
                success: function(response){
                    $('#video-container').find('.sumo-asset-img-container').find('img').attr('src', response.vimeo.pictures.sizes[4].link_with_play_button);
                    $('#video-container').find('.sumo-asset-img-container').removeClass('hide');
                }
            });

            $('.subject-select').select2({
                tags: true
            })

        });

        /* var videosTable = $('#videos-table');
        var videosLoader = $('#videos-card').find('.loader');
        $('.video-delete').on('click', function(e){
            e.preventDefault();

            var id = $(this).attr('data-id');
            var method = $(this).attr('data-method');
            var token = $('meta[name=csrf-token]').attr('content');
            var url = "{{route('adminLectureVideosDestroy')}}";

            $.ajax({
                type: method,
                url: url,
                data: {
                    id: id,
                    _method: method,
                    _token: token
                },
                success: function(response){
                    showNotifyToaster('success','',response.notifTitle);
                    $('#videos-card').load(location.href + " #videos-card");
                }
            });
        }); */

    </script>
@endsection
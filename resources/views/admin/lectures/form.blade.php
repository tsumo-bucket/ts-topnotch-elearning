<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="subject">Subject</label>
  {!! Form::select('subject', @$tag_list, @$data->tags[0]->name, ['class'=>'form-control subject-select', 'id'=>'subject', 'required']) !!}
</div>
<div class="caboodle-form-group sumo-asset-select-video" id="video-container" data-crop-url="{{route('adminSamplesCropUrl')}}">
  <label for="video_id">Video</label>
  {!! Form::hidden('video_id', null, ['class'=>'sumo-asset', 'data-id'=>@$data->video_id, 'data-thumbnail'=>'image_thumbnail', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="description">Description</label>
  {!! Form::textarea('description', null, ['class'=>'form-control redactor', 'id'=>'evaluation', 'placeholder'=>'Description', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>

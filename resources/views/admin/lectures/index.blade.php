@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Lectures</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated sync-videos"
            data-mdc-auto-init="MDCRipple"
            href="#"
        >
          <i class="far fa-sync"></i>&nbsp;Sync Videos
        </a>
        &nbsp;
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminLecturesCreate') }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminLectures', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Lecture name']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminLecturesDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminLecturesDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting...">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Name</th>
                  <th class="caboodle-table-col-header hide" >Subject</th>
                  <th class="caboodle-table-col-header hide" >Description</th>
                  <th class="caboodle-table-col-header hide" >Status</th>
                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td >{{$d->name}}</td>
                    <td>{{ $d->tags->first()->name }}</td>
                    <td class="uppercase sub-text-1" >{{substr($d->description,0,50)}}...</td>
                    <td>{{ ucwords($d->status) }}</td>
                    <td width="110px" class="text-center">
                      @if (Auth::user()->type == 'super' || Auth::user()->type == 'admin')
                        <a href="{{route('adminLecturesEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('added-scripts')
    <script>
        $(document).ready(function(){
          $('.sync-videos').click((e) => {
            // $('a.sync-videos').prop('disabled', true);
            e.preventDefault();
            // getVideos('https://api.vimeo.com/users/'+"{{env('VIMEO_USER_ID')}}"+'/videos?per_page=100');
            var url = "{{ route('adminTriggerVideoSync') }}";
            $.ajax({
              method: 'post',
              url: url,
              data: {},
              success: function(response){
                console.log(response);
                // showNotifyToaster(response.notifStatus, '', response.notifTitle);
                showNotifyToaster('info','Vimeo Sync Triggered. Please wait a few minutes for all videos to sync');
              }
            });
            $('.sync-videos').hide();
          })
        });

        /* function getVideos(url){
					var csrf_token = $('meta[name=csrf-token]').attr('content');

					var getVideosRequest = new XMLHttpRequest();

					getVideosRequest.open('GET', url, true);
					
					getVideosRequest.setRequestHeader('Content-Type', 'application/json');
					getVideosRequest.setRequestHeader('Authorization', 'bearer ' + "{{env('VIMEO_ACCESS_TOKEN')}}");
					getVideosRequest.onload = function(e){

          var response = JSON.parse(this.responseText);

          var storeVideosRequest = new XMLHttpRequest();
          var storeVideosFormData = new FormData;

          // clean data to only send minimal information
          response.data = response.data.map(d => {
            var path = ''; // best use lodash in the future for defensive coding
            if (d && d['pictures'] && d['pictures']['sizes'] && d['pictures']['sizes'][4] && d['pictures']['sizes'][4]['link_with_play_button']) {
              path =  d['pictures']['sizes'][4]['link_with_play_button'];
            }
            return {
              uri: d.uri,
              name: d.name,
              description: d.description,
              path: path
            }
          });

          storeVideosFormData.append('data', JSON.stringify(response.data));
          storeVideosFormData.append('_token', csrf_token);

          storeVideosRequest.open('POST', "{{route('adminVideosStore')}}", true);
          storeVideosRequest.send(storeVideosFormData);

          if(response.paging.next != null){
            getVideos('https://api.vimeo.com'+response.paging.next);
          }

					}
					getVideosRequest.send();
				} */
    </script>
@endsection
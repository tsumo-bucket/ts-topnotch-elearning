@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {!! $video->vimeo['embed']['html'] !!}
        </div>
    </div>
@endsection

@section('added-scripts')
    <script src="https://player.vimeo.com/api/player.js"></script>
    <script>
        $(document).ready(function(){
            $('.mdc-list-item').addClass('hide');
            $('.navbar-right').find('.navbar-button-web').addClass('hide');

            var player = new Vimeo.Player(document.querySelector('iframe'));
            player.play();
        });
    </script>
@endsection
<div class="caboodle-form-group">
  <label for="caption">Caption</label>
  {!! Form::text('caption', null, ['class'=>'form-control', 'id'=>'caption', 'placeholder'=>'Caption']) !!}
</div>
<div class="caboodle-form-group">
  <label for="start">Start</label>
  {!! Form::text('start', null, ['class'=>'form-control', 'id'=>'start', 'placeholder'=>'Start', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="end">End</label>
  {!! Form::text('end', null, ['class'=>'form-control', 'id'=>'end', 'placeholder'=>'End', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="path">Path</label>
  {!! Form::text('path', null, ['class'=>'form-control', 'id'=>'path', 'placeholder'=>'Path', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="url">Url</label>
  {!! Form::text('url', null, ['class'=>'form-control', 'id'=>'url', 'placeholder'=>'Url', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="status">Status</label>
  {!! Form::text('status', null, ['class'=>'form-control', 'id'=>'status', 'placeholder'=>'Status']) !!}
</div>

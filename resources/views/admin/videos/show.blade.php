<table class='table table-striped table-bordered'>
	<tr>
		<th>Caption</th>
		<td>{{$data->caption}}</td>
	</tr>
	<tr>
		<th>Start</th>
		<td>{{$data->start}}</td>
	</tr>
	<tr>
		<th>End</th>
		<td>{{$data->end}}</td>
	</tr>
	<tr>
		<th>Path</th>
		<td>{{$data->path}}</td>
	</tr>
	<tr>
		<th>Url</th>
		<td>{{$data->url}}</td>
	</tr>
</table>
<a href='{{route("adminVideosView", [$data->id])}}' class='btn btn-primary'>More Details</a>
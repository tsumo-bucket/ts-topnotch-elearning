@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{route('adminDashboard')}}">Dashboard</a></li>
  <li><a href="{{route('adminVideos')}}">Videos</a></li>
  <li class="active">View</li>
</ol>
@stop

@section('content')
<div class="col-md-8">
	<table class='table table-striped table-bordered table-view'>
		<tr>
			<th>Id</th>
			<td>{!!$data->id!!}</td>
		</tr>
		<tr>
			<th>Caption</th>
			<td>{!!$data->caption!!}</td>
		</tr>
		<tr>
			<th>Start</th>
			<td>{!!$data->start!!}</td>
		</tr>
		<tr>
			<th>End</th>
			<td>{!!$data->end!!}</td>
		</tr>
		<tr>
			<th>Path</th>
			<td>{!!$data->path!!}</td>
		</tr>
		<tr>
			<th>Url</th>
			<td>{!!$data->url!!}</td>
		</tr>
		<tr>
			<th>Status</th>
			<td>{!!$data->status!!}</td>
		</tr>
		<tr>
			<th>Created at</th>
			<td>
				@if ($data->created_at)
				<?php $created_at = new Carbon($data->created_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Updated at</th>
			<td>
				@if ($data->updated_at)
				<?php $created_at = new Carbon($data->updated_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
		<tr>
			<th>Deleted at</th>
			<td>
				@if ($data->deleted_at)
				<?php $created_at = new Carbon($data->deleted_at); ?>
				{{$created_at->toFormattedDateString() . ' ' . $created_at->toTimeString()}}
				@endif
			</td>
		</tr>
	</table>
</div>
@stop
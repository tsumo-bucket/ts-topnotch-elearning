<input type="hidden" name="page_category_id" value="{{ @$category->id }}">
<div class="caboodle-form-group">
    <label for="name">Name</label>
    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name', 'required']) !!}
</div>
<div class="caboodle-form-group">
<label for="published">Status</label>
{!! Form::select('published', ['draft' => 'Draft', 'published' => 'Published'], null, ['class'=>'form-control']) !!}
</div>
<div class="caboodle-form-group">
  <label for="batches">Batch Restrictions (Optional)</label>
  {!! Form::select('batches[]', @$all_batches, @$data->batches, ['id'=>'batches','class'=>'form-control','multiple']) !!}
</div>
<div class="caboodle-form-group">
<label for="content">Embed Form</label>
{!! Form::textarea('content', null, ['class'=>'form-control redactor', 'id'=>'content', 'placeholder'=>'Content', 'data-redactor-upload'=>route('adminAssetsRedactor')]) !!}
</div>


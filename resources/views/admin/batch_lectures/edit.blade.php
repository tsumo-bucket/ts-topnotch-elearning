@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminBatchLectures') }}">Batch Lectures</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>{{ $title }}</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    @if(!@$data->seo)
        <div class="header-actions">
            <a href="{{ route('adminBatchLectures') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    @endif
</header>
@stop

@if(!@$data->seo)
    @section('footer')
    <footer>
        <div class="text-right">
            <a href="{{ route('adminBatchLectures') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
        </div>
    </footer>
    @stop
@endif

@section('content')
    <div class="row">
        @if(@$data->seo)
        <div class="col-sm-8">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    {!! Form::model($data, ['route'=>['adminBatchLecturesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
                    @include('admin.batch_lectures.form')
                    {!! Form::close() !!}
                    <footer>
                        <div class="text-right">
                            <a href="{{ route('adminBatchLectures') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
                            <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
			<div class="caboodle-card">
                <div class="caboodle-card-header">
                <h4 class="no-margin"><i class="far fa-globe"></i> SEO</h4>
                </div>
				<div class="caboodle-card-body">
					<div class="seo-url" data-url="{{route('adminBatchLecturesSeo')}}">
						@include('admin.seo.form')
					</div>
				</div>
			</div>
		</div>
        @else
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-header">
                <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
                </div>
                <div class="caboodle-card-body">
                {!! Form::model($data, ['route'=>['adminBatchLecturesUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley form-edit']) !!}
                @include('admin.batch_lectures.form')
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        @endif
    </div>
@stop
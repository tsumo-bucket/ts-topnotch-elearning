<script id="tableRowTemplate" type="text/x-handlebars-template" >
<tr>
    <td>
        <div class="custom-control custom-radio">
            <input type="radio" {{checked}} required id="radio{{ lecture.id }}" value="{{{lecture.id}}}" name="lecture_id" class="custom-control-input">
            <label class="custom-control-label" for="radio{{ lecture.id }}"></label>
        </div>
    </td>
    <td><label for="radio{{ lecture.id }}">{{lecture.name}}</label></td>
    <td>{{subject}}</td>
</tr>
</script>
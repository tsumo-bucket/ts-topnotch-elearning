<div id="lecturesModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
            <form method="POST" action="" accept-charset="UTF-8" class="form form-lectures" id="lecturesModalForm">
                <div class="modal-header">
                    <h4 class="modal-title">Add Lectures</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body" id="lecturesModalBody" >
                    @csrf
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="">Filter Lectures</label>
                                <div class="form-row mb-2">
                                    <div class="col-6">
                                        <input name="filter_name" id="filter_name" type="text" placeholder="Name" class="form-control form-control-sm">
                                    </div>
                                    <div class="col-4">
                                        {!! Form::select('filter_subject', $tag_list, null, ['class'=>'form-control form-control-sm', 'id'=>'filter_subject']) !!}
                                    </div>
                                    <div class="col-2">
                                        <a href="#" class="btn btn-secondary btn-block btn-sm" id="btn-filter" >Apply</a>
                                    </div>
                                </div>
                            </div>
                            <div style="max-height: 350px; overflow-y: auto;" id="lectures-list" >
                                <div class="loader hide">
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                                    </svg>
                                </div>
                                <p class="text-center my-3 hide result-empty">No lectures found.</p>
                                @if ($lectures->count() > 0)
                                    <table class="caboodle-table caboodle-table-thin"  >
                                        <thead>
                                            <th width="50px" >
                                            </th>
                                            <th>Name</th>
                                            <th>Subject</th>
                                        </thead>
                                        <tbody id="lectures-body" >
                                            @foreach ($lectures as $index => $l)
                                                <tr>
                                                    <td>
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" required id="radio{{$l->id}}" value="{{ $l->id }}" name="lecture_id" class="custom-control-input">
                                                            <label class="custom-control-label" for="radio{{$l->id}}"></label>
                                                        </div>
                                                    </td>
                                                    <td><label for="radio{{$l->id}}">{{ $l->name }}</label></td>
                                                    <td>{{ $l->tags->first()->name }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="text-center my-3 no-lectures">No lectures available.</p>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div>
                                <div class="form-group">
                                    <label for="date_clicked">Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" data-toggle="tooltip" title="Start Date"  ><i class="far fa-calendar"></i></span>
                                        </div>
                                        <input type="text" required name="date_clicked" id="date_clicked" class="form-control datepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="date_dragged">End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" data-toggle="tooltip" title="Start Date"  ><i class="far fa-calendar"></i></span>
                                        </div>
                                        <input type="text" required name="date_dragged" id="date_dragged" class="form-control datepicker">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="start_time">Start Time</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" data-toggle="tooltip" title="Start Time"  ><i class="far fa-clock"></i></span>
                                        </div>
                                        <input type="text" required name="start_time" id="start_time" class="form-control datetimepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="end_time">End Time</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" data-toggle="tooltip" title="End Time"  ><i class="far fa-clock"></i></span>
                                        </div>
                                        <input type="text" required name="end_time" id="end_time" class="form-control datetimepicker">
                                    </div>
                                </div>
                                <input type="hidden" name="batch_id" value="{{ $batch->id }}">
                                <input type="hidden" name="id">
                            </div>
                        </div>
                    </div>
                </div>
                @if ($lectures->count() > 0)
                    <div class="modal-footer">
                        @if ($lectures->links())
                            <span class="float-left w-100 d-flex alig-items-center" id="lectures-pagination">
                                {!! $lectures->links() !!}
                            </span>
                        @endif
                        <button type="button" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-ripple-upgraded" data-dismiss="modal" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Cancel</button>
                        <button type="button" id="deleteFromCalendar" data-id="" class="caboodle-btn caboodle-btn-large caboodle-btn-danger mdc-button mdc-button--unelevated mdc-ripple-upgraded hide" data-mdc-auto-init="MDCRipple" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Delete</button>
                        <button type="submit" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple" style="--mdc-ripple-fg-size:0px; --mdc-ripple-fg-scale:Infinity;">Add</button>
                    </div>
                @endif
            </form>
            </div>
        </div>
    </div>
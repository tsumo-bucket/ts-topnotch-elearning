<div class="caboodle-form-group">
  <label for="batch_id">Batch Id</label>
  {!! Form::text('batch_id', null, ['class'=>'form-control', 'id'=>'batch_id', 'placeholder'=>'Batch Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="lecture_id">Lecture Id</label>
  {!! Form::text('lecture_id', null, ['class'=>'form-control', 'id'=>'lecture_id', 'placeholder'=>'Lecture Id', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="status">Status</label>
  {!! Form::text('status', null, ['class'=>'form-control', 'id'=>'status', 'placeholder'=>'Status']) !!}
</div>

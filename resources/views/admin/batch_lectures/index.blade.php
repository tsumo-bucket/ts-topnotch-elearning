@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('adminBatchesEdit', $batch->id)}}">{{ ucwords($batch->name) }}</a></li>
    <li class="breadcrumb-item far active"><span>Lectures</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
      <a href="{{route('adminBatchesEdit', $batch->id)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
  <div class="row">
    <div class="col-sm-12">
      <div class="caboodle-card">
        <div class="caboodle-card-body">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>
  @include('admin.batch_lectures.partials.add-lecture-modal')
@stop

@section('added-scripts')
  @include('admin.batch_lectures.partials.table-row-template')
  <script>

    var lecturesModal = $('#lecturesModal');
    var lecturesModalForm = $('#lecturesModalForm');
    var formStartTime = lecturesModalForm.find('#start_time');
    var formEndTime = lecturesModalForm.find('#end_time');
    var btnDeleteFromCalendar = lecturesModalForm.find('#deleteFromCalendar');
    var clickedLectureID;

    $(lecturesModalForm).on('keypress keyup', function(e){
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        filter_lectures(e);
      }
    });

    $(document).on('click', '#lecturesModal .pagination .page-link', function(e){
      e.preventDefault();
      $(this).parents('.pagination').find('.active').removeClass('active');
      $(this).parent().addClass('active');

      $('#lectures-body').load($(this).attr('href') + ' #lectures-body>');
      $('#lectures-pagination').load($(this).attr('href') + ' #lectures-pagination>');
    });

    lecturesModal.on('hidden.bs.modal', function(){
        $('#lectures-body').load(location.href + ' #lectures-body>');
        $('#lectures-pagination').load(location.href + ' #lectures-pagination>');
    });

    $('.datetimepicker').datetimepicker({
        format: 'LT'
    });
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD'
    });

    $('#btn-filter').on('click', function(e){
      filter_lectures(e);
    });

    function filter_lectures(e){
      e.preventDefault();

      var filter_name = $('#filter_name').val();
      var filter_subject = $('#filter_subject').val();
      var token = $('meta[name=csrf-token]').attr('content');
      var url = "{{route('adminBatchLecturesFilter')}}";
      var lecturesBody = $('#lectures-body');

      $.ajax({
        method: 'post',
        url: url,
        data: {
          filter_name: filter_name,
          filter_subject: filter_subject,
          _token: token
        },
        beforeSend: function(){
          lecturesModal.find('.loader').removeClass('hide');
          lecturesModal.find('.caboodle-table').addClass('hide');
          lecturesModal.find('.result-empty').addClass('hide');
          lecturesModal.find('.no-lectures').addClass('hide');
          lecturesModal.find('#lectures-pagination').addClass('hide');
        },
        success: function(response){
          lecturesModal.find('.loader').addClass('hide');

          if(response.lectures.length > 0){

            lecturesBody.empty();

            $.each(response.lectures, function(index, lecture){

              var current_id = clickedLectureID;
              var lecture_id = lecture.id;
              var checked = current_id == lecture_id ? 'checked' : 'unchecked';

              var template = Handlebars.compile($('#tableRowTemplate').html());

              lecturesBody.append(template({
                lecture: lecture,
                subject: lecture.tags[0].name,
                checked: checked
              }));

            });

            lecturesModal.find('.caboodle-table').removeClass('hide');
          }
          else{
            lecturesModal.find('.result-empty').removeClass('hide');
          }

        }
      })
    }

    $('#calendar').fullCalendar({
      timeFormat: 'h:mm A',
      selectable: true,
      eventTextColor: 'white',
      eventBackgroundColor: '#2a8cf2',
      eventBorderColor: 'transparent',
      events: "{{route('adminBatchLecturesAll',$batch->id)}}",
      select: function(start, end, allDay){

        var start_day = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
        var end_day = moment(end, 'DD.MM.YYYY').subtract(1, 'days').format('YYYY-MM-DD');

        lecturesModalForm.attr('action', "{{ route('adminBatchLecturesStore') }}");

        formStartTime.val('');
        formEndTime.val('');
        lecturesModalForm.find('input[name=lecture_id]').prop('checked', false);
        lecturesModal.find('#date_clicked').val(start_day);
        lecturesModal.find('#date_dragged').val(end_day);
        lecturesModalForm.find('input[name=id]').val('');
        
        btnDeleteFromCalendar.addClass('hide');
        btnDeleteFromCalendar.attr('data-id', '');

        lecturesModal.modal('show');

      },
      eventClick: function(event){

        var id = event.id;

        $.ajax({
          method: 'POST',
          url: "{{route('adminBatchLecturesGet')}}",
          data: {
            id: id
          },
          success: function(response){
            
            lecturesModalForm.attr('action', "{{ route('adminBatchLecturesUpdate') }}");

            clickedLectureID = response.lecture.lecture_id;

            lecturesModalForm.find('#radio'+response.lecture.lecture_id).prop('checked', true);
            lecturesModalForm.find('#start_time').val(moment(response.lecture.start_date).format('hh:mm A'));
            lecturesModalForm.find('#end_time').val(moment(response.lecture.end_date).format('hh:mm A'));
            lecturesModalForm.find('#date_clicked').val(moment(response.lecture.start_date).format('YYYY-MM-DD'));
            lecturesModalForm.find('#date_dragged').val(moment(response.lecture.end_date).format('YYYY-MM-DD'));
            lecturesModalForm.find('input[name=id]').val(id);
            btnDeleteFromCalendar.removeClass('hide');
            btnDeleteFromCalendar.attr('data-id', id);
            lecturesModalForm.find('button[type=submit]').html('Save');


            lecturesModal.modal('show');

          }
        });
      }
    });

    btnDeleteFromCalendar.on('click', function(e){
      e.preventDefault();

      var id = $(this).attr('data-id');
          
      $.ajax({
        type: 'DELETE',
        url: "{{route('adminBatchLecturesDestroy')}}",
        data: {
          id: id,
          _token: $('meta[name=csrf-token]').attr('content'),
          _method: 'DELETE'
        },
        beforeSend: function(){
          lecturesModal.modal('hide');
        },
        success: function(response){
          showNotifyToaster(response.notifStatus, response.notifTitle, response.notifMessage);
          location.reload();
        }
      });
    });

    $('#calendar td').css('cursor', 'pointer');

    lecturesModal.find('form').on('submit', function(e){
      e.preventDefault();

      var start_time = new Date($('#date_clicked').val()+' '+$(this).find('#start_time').val()).getTime();
      var end_time = new Date($('#date_dragged').val()+' '+$(this).find('#end_time').val()).getTime();

      var url = $(this).attr('action');

      if(start_time >= end_time){
        showNotifyToaster('error','','End date/time should be set after the start date/time');
        return false;
      }

      $.ajax({
        method: 'post',
        url: url,
        data: $(this).serialize(),
        success: function(response){
          lecturesModal.modal('hide');
          showNotifyToaster(response.notifStatus, response.notifTitle, response.notifMessage);
          setTimeout(() => {
            location.reload();
          }, 1500);
        }
      });



    });

  </script>
@endsection
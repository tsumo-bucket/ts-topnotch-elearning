@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Profile</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
	<h1>{{ $title }}</h1>
	<div class="header-actions">
		<a role="button" href="{{ route('adminDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
	</div>
</header>
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="caboodle-card">
				<div class="caboodle-card-header">
					<h4 class="no-margin">PERSONAL INFORMATION</h4>
				</div>
				<div class="caboodle-card-body">
                    <div class="form-group row">
                        <label for="full-name" class="col-sm-2 col-form-label">Student Number</label>
                        <div class="col-sm-10">
                            <strong><input type="text" readonly class="form-control-plaintext" id="full-name" value="{{ $user->student_number }}"></strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="full-name" class="col-sm-2 col-form-label">Full Name</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="full-name" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="email" value="{{ $user->email }}">
                        </div>
                    </div>
                </div>
			</div>
			<div class="caboodle-card">
				<div class="caboodle-card-header">
					<h4 class="no-margin">PASSWORD</h4>
				</div>
				<div class="caboodle-card-body">
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="password" value="&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;">
                        </div>
                    </div>
                    <a href="{{ route('adminProfilePasswordEdit') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Change Password</a>
                </div>
			</div>
		</div>
	</div>
@endsection

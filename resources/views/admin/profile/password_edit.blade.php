@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item far">
            <a href="{{ route('adminDashboard') }}">Dashboard</a>
        </li>
        <li class="breadcrumb-item far">
            <a href="{{ route('adminProfile') }}">Profile</a>
        </li>
        <li class="breadcrumb-item far active" aria-current="page">
            <span>Change Password</span>
        </li>
    </ol>
</nav>
@stop 

@section('header')
<header class="flex-center">
    <h1>Change Password</h1>
    <div class="header-actions">
        <a href="{{ route('adminProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('content')
    <div class="row">
      <div class="col-sm-12">
        <div class="caboodle-card">
          <div class="caboodle-card-body">
            {!! Form::model($user, ['route'=>['adminProfilePasswordUpdate'], 'method' => 'patch', 'class'=>'form form-parsley']) !!}
            <div class="form-group row">
              <label for="current" class="col-sm-3 col-form-label">Current Password</label>
              <div class="col-sm-6">
                  <input required type="password" name="current" class="form-control" id="current">
              </div>
            </div>
            <div class="form-group row">
              <label for="new" class="col-sm-3 col-form-label">New Password</label>
              <div class="col-sm-6">
                  <input required type="password" name="new" class="form-control" id="new" data-parsley-minlength="6" >
              </div>
            </div>
            <div class="form-group row">
              <label for="new_confirmation" class="col-sm-3 col-form-label">Confirm New Password</label>
              <div class="col-sm-6">
                  <input required type="password" name="new_confirmation" class="form-control" id="new_confirmation" data-parsley-equalto="#new" data-parsley-minlength="6" >
              </div>
            </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
@endsection
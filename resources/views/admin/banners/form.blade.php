<div class="caboodle-form-group">
  <label for="name">Name</label>
  {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'name', 'placeholder'=>'Name','required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="caption">Caption</label>
  {!! Form::text('caption', null, ['class'=>'form-control', 'id'=>'caption', 'placeholder'=>'Caption']) !!}
</div>
<!-- <div class="caboodle-form-group">
  <label for="published">Type</label>
  {!! Form::select('type', ['image' => 'image', 'video-file' => 'video'], null, ['class'=>'form-control select2']) !!}
</div> -->
<div class="caboodle-form-group sumo-asset-select image-banner" data-crop-url="{{route('adminBannersCropUrl')}}">
  <label for="image">Image</label>
  {!! Form::hidden('image', null, ['class'=>'sumo-asset', 'data-id'=>@$data->id, 'data-thumbnail'=>'image_thumbnail']) !!}
  <span class="sub-text-1">The required image size is 100x100 pixels minimum</span>
</div>
{{-- <div class="caboodle-form-group video-banner">
  <label for="video">
  {!! Form::hidden('video', null, ['class'=>'sumo-video', 'id'=>'sumo-video-select'.@$data->id, 'data-id'=>@$data->id]) !!}
    Video
    <span data-selectvideo="#sumo-asset-select-button-{{@$data->id}}" data-input="#sumo-video-select{{@$data->id}}"  class="sumo-asset-select-button mdc-button caboodle-btn caboodle-btn-small caboodle-btn-cancel btn-icon-left select mdc-ripple-upgraded" data-mdc-auto-init="MDCRipple"  data-toggle="modal" data-target="#video-banner-modal" href="#" style="--mdc-ripple-fg-size:99.5812px; --mdc-ripple-fg-scale:1.81248; --mdc-ripple-fg-translate-start:31.3031px, -36.7906px; --mdc-ripple-fg-translate-end:33.1938px, -30.2906px;">
      <i class="far fa-film"></i> Select / Upload
    </span>
  </label>
  
  <div class="sumo-asset-video-container delete_{{@$data->video}} @if(@$data->video) test  @else hide @endif" id="sumo-asset-select-button-{{@$data->id}}">
		<img src="{{asset('img/admin/file-video.png')}}" id="sumo-video-asset-select">
		<div class="sumo-asset-video-hover-overlay"><ul>
			<li id="delete-selected-video" data-deletedatainput='#sumo-video-select{{@$data->id}}' data-deletevideobanner=".delete_{{@$data->video}}"><a href="#" class="remove"  data-toggle="tooltip" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a></li>
		</ul></div>
  </div>
  <!-- <div class="video-select-container" style="background-image: url({{asset('img/admin/file-video.png')}}); height: 120px; width: 120px; background-position: center; background-size: cover; background-repeat: no-repeat;"></div>
  <span class="name">test</span> -->
</div> --}}
{{-- <div class="caboodle-form-group">
  <label for="link">Link</label>
  {!! Form::text('link', null, ['class'=>'form-control', 'id'=>'link', 'placeholder'=>'Link']) !!}
</div> --}}
<div class="caboodle-form-group">
  <label for="published">Published</label>
  {!! Form::select('published', ['draft' => 'draft', 'published' => 'published'], null, ['class'=>'form-control select2']) !!}
</div>

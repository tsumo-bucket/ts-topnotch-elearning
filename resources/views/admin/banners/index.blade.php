@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Banners Pictures</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('adminBannersCreate') }}"
        >
            Create
        </a>
    </div>
</header>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding">
          {!! Form::open(['route'=>'adminBanners', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Keyword']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminBannersDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminBannersDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting {{$title}}">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Name</th>
                  <th class="caboodle-table-col-header hide" >Image</th>
                  <th class="caboodle-table-col-header hide" >Caption</th>
                  <th class="caboodle-table-col-header hide" >Published</th>
                  <th colspan="6" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td >{{$d->name}}</td>
                    <td><div class="sumo-asset-display" data-id="{{$d->image}}" data-url="{{route('adminAssetsGet')}}"></div></td>
                    <td>{{$d->caption}}</td>
                    <td class="uppercase sub-text-1" >{{$d->published}}</td>
                     
                    <td width="110px" class="text-center">
                      @if (Auth::user()->type == 'super' || Auth::user()->type == 'admin')
                        <a href="{{route('adminBannersEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop

@section('content')
{{-- <div class="col-sm-12">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-table"></i> Table
      </div>
      <div class="right">
        <a class="btn-transparent btn-sm" href="{{route('adminBanners')}}"><i class="fa fa-eye"></i> Show All</a>
        <a class="btn-transparent btn-sm" href="{{route('adminBannersCreate')}}"><i class="fa fa-plus-circle"></i> Create</a>
        <a class="btn-transparent btn-sm" href="#" data-toggle="modal" data-target="#delete-modal"><i class="fa fa-minus-circle"></i> Delete</a>
      </div>
    </div>
    <div class="filters">
      {!! Form::open(['route'=>'adminBanners', 'method' => 'get']) !!}
      <label>
        Search: {!! Form::text('name', $keyword, ['class'=>'form-control input-sm', 'placeholder'=>'']) !!}
        <button><i class="fa fa-search"></i></button>
      </label>
      {!! Form::close() !!}
    </div>
    @if (count($data) > 0)
    <div class="table-responsive">
      {!! Form::open(['route'=>'adminBannersDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
      <table class="table table-bordered table-hover table-striped" >
        <thead>
          <tr>
            <th width="30px"><i class="fa fa-arrows" aria-hidden="true"></th>
            <th width="30px">
              <label>
                <input type="checkbox" name="delete-all" class="toggle-delete-all">
                <i class="fa fa-square input-unchecked"></i>
                <i class="fa fa-check-square input-checked"></i>
              </label>
            </th>
            <th>Name</th>
            <th>Image</th>
            <th>Caption</th>
            <th width="80px">Published</th>
            <th></th>
          </tr>
        </thead>
        <tbody class="sortable" sortable-data-url="{{route('adminBannersOrder')}}">
          @foreach ($data as $d)
          <tr sortable-id="banner-{{$d->id}}" >
            <td><i class="fa fa-th-large sortable-icon" aria-hidden="true"></i></td>
            <td>
              <label>
                <input type="checkbox" name="ids[]" value="{{$d->id}}">
                <i class="fa fa-square input-unchecked"></i>
                <i class="fa fa-check-square input-checked"></i>
              </label>
            </td>
            <td>{{$d->name}}</td>
            <td><div class="sumo-asset-display" data-id="{{$d->image}}" data-url="{{route('adminAssetsGet')}}"></div></td>
            <!-- <td>{{$d->image_thumbnail}}</div></td> -->
            <td>{{$d->caption}}</td>
            <td>{{$d->published}}</td>
            <td width="140px" class="text-center">
              <button type="button" class="btn btn-primary btn-sm" role="button" data-toggle="popover" 
                data-trigger="focus" title="{{$d->name}}" data-placement="left" data-html="true"
                data-content="@include('admin.banners.show', ['data' => $d])">
                VIEW
              </button>
              <a href="{{route('adminBannersEdit', [$d->id])}}" class="btn btn-primary btn-sm">EDIT</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {!! Form::close() !!}
    </div>
    @else
    <div class="empty text-center">
      No results found
    </div>
    @endif
  </div>
</div> --}}
@stop
<div class="modal" id="legal-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            {{-- <div class="modal-header">
                <strong>TOPNOTCH MEDICAL BOARD PREP</strong>
            </div> --}}
            <div class="modal-body p-0">

                {{-- <h6 class="mb-3" ><u>Please read carefully: </u></h6> --}}

                <div class="accordion" id="legal-accordion" >
                    @if (@$terms_and_conditions)    
                        <div class="card">
                            @foreach (@$terms_and_conditions as $t)
                                <div class="card-header">
                                    <a href="#" data-toggle="collapse" data-target="#{{@$t->slug}}" style="color: #000;" ><h4 class="mb-0" >{{ @$t->name }} <span class="float-right"><i class="far fa-chevron-up collapse-icon"></i></span></h4></a>
                                </div>
                                <div class="collapse show" id="{{ @$t->slug }}">
                                    <div class="card-body rounded-0 border-bottom">
                                        {!! @$t->content !!}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer p-0">
                <button type="button" class="btn btn-primary btn-block rounded-0" data-dismiss="modal">Accept</button>
            </div>
        </div>
    </div>
</div>
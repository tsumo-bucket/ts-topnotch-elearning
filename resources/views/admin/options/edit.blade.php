@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far"><a href="{{route('adminOptions', ['category'=>$data->category])}}">Settings</a></li>
    <li class="breadcrumb-item far active"><span>Create</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('adminOptions', ['category'=>$data->category]) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop

@section('footer')
<footer>
    <div class="text-right">
        <a href="{{ route('adminOptions', ['category'=>$data->category]) }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
<div class="row">
  @if(!empty($data->help))
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <h4 class="no-margin"><i class="far fa-file-alt"></i> HELP</h4>
      </div class="caboodle-card-header">
      <div class="caboodle-card-body">
        <form class="form" >
          {{$data->help}}
        </form>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
      </div>
      <div class="caboodle-card-body">
        {!! Form::model($data, ['route'=>['adminOptionsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley']) !!}
        @include('admin.options.form')
        {!! Form::close() !!}
        </div>
    </div>
  </div>
  @else
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <h4 class="no-margin"><i class="far fa-file-alt"></i> FORM</h4>
      </div>
      <div class="caboodle-card-body">
        {!! Form::model($data, ['route'=>['adminOptionsUpdate', $data->id], 'files' => true, 'method' => 'patch', 'class'=>'form form-parsley']) !!}
        @include('admin.options.form')
        {!! Form::close() !!}
        </div>
    </div>
  </div>
  @endif
</div>
@stop
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('adminDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Contacts</span></li>
  </ol>
</nav>
@stop
<?php
    if($subject){
      $options = [];
      try {
        $options = json_decode($subject);
      } catch(Exception $e) {}
    }
	?>
@section('header')
<header class="flex-center contact-subject">
    <h1>{{ $title }} @if($selectedSubject) - {{$selectedSubject}} @endif </h1>
    @if($subject)
    {!! Form::open(['route'=>'adminFilterResults', 'method' => 'post', 'class'=>'filter-rs']) !!}
    <div class="row">
      <div class="caboodle-form-group margin-bottom">
          <label for="type[]" class="less-margin x-small mr-3">Subject</label>
          
          <select name="subject" class="sumo-select" id="">
            <option selected disabled hidden>Select Subject</option>
            @foreach($options as $option)
              <option value="{{ $option->value }}" >{{ $option->label }}</option>
            @endforeach
          </select>
          
      </div>
      <div class="header-actions">
          <button
              type="submit"
              class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated submitResults"
              data-mdc-auto-init="MDCRipple"
          >
          Submit
          </button>
      </div>
    </div>
    {!! Form::close() !!}
    @endif
    
</header>
@endsection

@section('content')
<div class="row mb-3">
  <div class="col-sm-3">
    <div class="caboodle-card full-height caboodle-flex caboodle-flex-center caboodle-flex-align-center bg-white">
        <div class="caboodle-card-body small-padding text-center">
            <div class="heading-3">Today's Inquiries</div>
            <div class="count">
            @if($inquiry)
            {{ $inquiry }}
            @else
            0
            @endif
            </div>
        </div>
    </div>
  </div>
  <div class="col-sm-9">
    <div class="chart-container">
         <canvas id="buyers" width="730" height="200"></canvas>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="caboodle-card">
      <div class="caboodle-card-header">
        <div class="filters no-padding" style="display: grid;grid-template-columns: 1fr 1fr;">
          {!! Form::open(['route'=>'adminContacts', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
                <label class="no-margin single-search no-padding">
                    {!! Form::text('name', null, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Keyword']) !!}
                    <button>
                        <i class="fa fa-search"></i>
                    </button>
                </label>
                <!-- {!! Form::hidden('sort', @$sort) !!} {!! Form::hidden('sortBy', @$sortBy) !!} -->
            </div>
          {!! Form::close() !!}
          {!! Form::open(['route'=>'adminFilterDate', 'method' => 'get', 'class'=>'no-margin']) !!}
            <div class="caboodle-form-group caboodle-flex caboodle-flex-row caboodle-flex-left caboodle-form-control-connected">
              <label class="no-margin single-search no-padding">
                {!! Form::date('date', @$filterDate, ['class'=>'form-control input-sm no-margin', 'placeholder'=>'Keyword']) !!}
               
              </label>
              <button class="ml-2 filter-date-contacts " type="submit">Filter</button>
              <a class="ml-2 caboodle-export-data-btn" href="{{ route('adminExport') }}">Export Data</a>
            </div>
            
          {!! Form::close() !!}
          
        </div>
      </div>
      <div class="caboodle-card-body">
        @if(count($data) > 0)
          {!! Form::open(['route'=>'adminContactsDestroy', 'method' => 'delete', 'class'=>'form form-parsley form-delete']) !!}
            <table class="caboodle-table">
              <thead>
                <tr>
                  <th width="50px">
                    <div class="mdc-form-field" data-toggle="tooltip" title="Select All">
                        <div class="mdc-checkbox caboodle-table-select-all">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="select_all" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                    </div>
                  </th>
                  <th class="caboodle-table-col-action">
                    <a class="caboodle-btn caboodle-btn-icon caboodle-btn-danger mdc-button mdc-ripple-upgraded mdc-button--unelevated x-small uppercase" 
                            data-mdc-auto-init="MDCRipple"
                            href="{{ route('adminContactsDestroy') }}"
                            method="DELETE"
                            data-toggle-alert="warning"
                            data-alert-form-to-submit=".form-delete"
                            permission-action="delete"
                            data-notif-message="Deleting...">
                        <i class="fas fa-trash"></i>
                    </a>
                  </th>
                  <th class="caboodle-table-col-header hide" >Name</th>
                                            <th class="caboodle-table-col-header hide" >Email</th>
                                            <th class="caboodle-table-col-header hide" >Contact number</th>
                    
                  <th colspan="100%" ></th>
                </tr>
              </thead>
              <tbody>
                @foreach($data as $d)
                  <tr>
                    <td>
                      <div class="mdc-form-field">
                        <div class="mdc-checkbox">
                            <input type="checkbox" class="mdc-checkbox__native-control" name="ids[]" value="{{ $d->id }}" />
                            <div class="mdc-checkbox__background">
                                <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                                    <path class="mdc-checkbox__checkmark-path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59" />
                                </svg>
                                <div class="mdc-checkbox__mixedmark"></div>
                            </div>
                        </div>
                      </div>
                    </td>
                    <td >
                    {{$d->name}}            </td>
                                    <td class="uppercase sub-text-1" >{{$d->email}}</td>
                                    <td class="uppercase sub-text-1" >{{$d->contact_number}}</td>
                     
                    <td width="110px" class="text-center">
                      {{--@if (Auth::user()->type == 'super')
                        <a href="{{route('adminContactsEdit', [$d->id])}}" 
                            class="mdc-icon-toggle animated-icon" 
                            data-toggle="tooltip"
                            title="Manage"
                            role="button"
                            aria-pressed="false"
                            permission-action="edit">
                            <i class="far fa-edit" aria-hidden="true"></i>
                        </a>
                      @endif--}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          {!! Form::close() !!}
        @else
          <div class="empty text-center">
              No results found
          </div>
        @endif
        @if ($pagination)
          <div class="caboodle-pagination">
              {{$data->links('layouts.pagination')}}
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
@stop
@section('added-scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
      
      var now = new Date();
      if (now.getMonth() == 11) {
          var current = new Date(now.getFullYear() + 1, 0, 1);
      } else {
          var current = new Date(now.getFullYear(), now.getMonth() + 1, 1);
      }
       var buyerData = {
                labels : {!! json_encode($dates) !!},
                datasets : [
                {
                    fillColor : "rgba(172,194,132,0.4)",
                    strokeColor : "#ACC26D",
                    pointColor : "#fff",
                    pointStrokeColor : "#9DB86D",
                    data : {!! json_encode($inquiries) !!}//[203,156,99,251,305,247] //{!! json_encode($inquiries) !!} //
                },
                // {
                //     fillColor : "rgba(172,194,132,0.4)",
                //     strokeColor : "#ACC26D",
                //     pointColor : "#fff",
                //     pointStrokeColor : "#9DB86D",
                //     data : [100,156,99,251,305,247]
                // },
            ]
            }
            // get line chart canvas
            var buyers = document.getElementById('buyers').getContext('2d');
            // draw line chart
            new Chart(buyers).Line(buyerData);
            // pie chart data
                     
    });
</script>
@stop
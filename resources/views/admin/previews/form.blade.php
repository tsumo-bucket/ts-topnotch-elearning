@if (isset($data))
<div class="caboodle-form-group">
  <label for="slug">Slug</label>
  {!! Form::text('slug', null, ['class'=>'form-control', 'id'=>'slug', 'placeholder'=>'Slug','readonly']) !!}
</div>
@endif
<div class="caboodle-form-group">
  <label for="value">Value</label>
  {!! Form::text('value', null, ['class'=>'form-control', 'id'=>'value', 'placeholder'=>'Value', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="status">Status</label>
  {!! Form::text('status', null, ['class'=>'form-control', 'id'=>'status', 'placeholder'=>'Status', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="action">Action</label>
  {!! Form::text('action', null, ['class'=>'form-control', 'id'=>'action', 'placeholder'=>'Action', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="reference_type">Reference Type</label>
  {!! Form::text('reference_type', null, ['class'=>'form-control', 'id'=>'reference_type', 'placeholder'=>'Reference Type', 'required']) !!}
</div>
<div class="caboodle-form-group">
  <label for="reference_id">Reference Id</label>
  {!! Form::text('reference_id', null, ['class'=>'form-control', 'id'=>'reference_id', 'placeholder'=>'Reference Id', 'required']) !!}
</div>

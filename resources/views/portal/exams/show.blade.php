@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalExams')}}">Exams</a></li>
    <li class="breadcrumb-item far active"><span>{{ ucwords(@$data->name) }}</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ @$data->name }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalExams') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            {!! @$data->content !!}
        </div>
    </div>
@endsection
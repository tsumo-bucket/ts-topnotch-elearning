@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Exams</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        @if (count(@$exams) > 0)
            <div class="col-sm-12">
                <div class="caboodle-card p-0">
                    <div class="caboodle-card-body p-0">
                        <table class="caboodle-table caboodle-table-small">
                            <thead>
                                <th class="caboodle-table-col-header" >Exam</th>
                                <th class="caboodle-table-col-header" >Date Published</th>
                                <th></th>
                            </thead>
                            <tbody>
                                @foreach (@$exams as $exam)
                                    <tr>
                                        <td><a href="{{ route('portalExamsShow', $exam->id) }}">{{ $exam->name }}</a></td>
                                        <td class="sub-text-1" >{{ date('F j, Y \a\t\ h:i A', strtotime($exam->created_at)) }}</td>
                                        <td class="sub-text-1 text-right pr-3" ><a href="{{ route('portalExamsShow', $exam->id) }}">TAKE EXAM</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-sm-12">
                <div class="caboodle-card">
                    <div class="caboodle-card-body">
                        <div class="empty text-center">
                            No exams available
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
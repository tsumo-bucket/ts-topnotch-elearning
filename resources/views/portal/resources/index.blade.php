@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Resources</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')

    {{-- Desktop View --}}
    <div class="row d-none d-sm-block">
        <div class="col-sm-12">
            <div class="caboodle-card p-0">
                <div class="caboodle-card-body p-0">
                    @if (!empty(@$data) && count(@$data) > 0)
                        <table class="caboodle-table caboodle-table-small">
                            <thead>
                                <tr>
                                    <th class="caboodle-table-col-header" >File</th>
                                    <th class="caboodle-table-col-header" >Description</th>
                                    <th class="caboodle-table-col-header" >Uploaded on</th>
                                    <th class="caboodle-table-col-header" >Available Until</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                    @if ($d['type'] == 'pdf')    
                                        <tr>
                                            <td width="20%" >
                                                <a href="{{ route('downloadPDF', [$d['id'], md5($user->id.$d['id'])]) }}"
                                                    target="_blank" 
                                                    class="btn" 
                                                    aria-pressed="false">
                                                {{ $d['name'] }}
                                                </a>
                                            </td>
                                            <td>{{ $d['caption'] ?? '---' }}</td>
                                            <td >{{ date('F j, Y h:i A', strtotime($d['created_at'])) }}</td>
                                            <td >{{ @$d['end_date'] ? date('F j, Y h:i A', strtotime($d['end_date'])) : 'N/A' }}</td>
                                            <td class="text-right" >
                                                <a href="{{ route('downloadPDF', [$d['id'], md5($user->id.$d['id'])]) }}"
                                                    target="_blank" 
                                                    class="btn" 
                                                    aria-pressed="false">
                                                    Download
                                                </a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="empty text-center py-3">
                            No resources available
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{-- Mobile View --}}
    <div class="row d-block d-sm-none">
        @if (!empty(@$data) && count(@$data) > 0)
            @foreach ($data as $d)
                <div class="col-sm-12">
                    <div class="caboodle-card">
                        <div class="caboodle-card-body">
                            <h1>{{ $d['name'] }} <br><small class="sub-text-1" style="font-size: 12px;" >{{ date('F j, Y h:i A', strtotime($d['created_at'])) }}</small></h1>
                            <div class="w-100">
                                {!! $d['caption'] !!}
                            </div>
                        </div>
                        <div class="caboodle-card-footer">
                            <a href="{{ route('downloadPDF', [$d['id'], md5($user->id.$d['id'])]) }}" target="_blank" >Download</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-sm-12">
                <div class="caboodle-card">
                    <div class="caboodle-card-body">
                        <div class="empty text-center">
                            No resources available
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
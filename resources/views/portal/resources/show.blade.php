@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalResources')}}">Resources</a></li>
    <li class="breadcrumb-item far active"><span>View</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>View</h1>
    <div class="header-actions">
        <a href="{{ route('portalResources') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
        @if (@$data->file_version == '1.4')
            <a href="{{ route(['downloadPDF', [$data->id,  md5($user->id.$data->id)]}}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Download</a>
        @endif
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <iframe id="iframe" src="{{ url($data->path) }}#toolbar=0" allowfullscreen webkitallowfullscreen frameborder="0" style="width: 100%; min-height: 500px;" ></iframe>
            <div id="iframeCover" ></div>
        </div>
    </div>
@endsection

@section('added-scripts')
    <script>
        $(document).contextmenu(function(){
            return false;
        });
    </script>
@endsection
@extends('layouts.admin')

@section('header')
@if (@$announcement)
  <div class="alert alert-warning custom-alert fade show" role="alert">
    <div class="alert-container">
      <div class="icon-container">
        <span class="fa-stack fa-lg border-warning">
          <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
          <i class="fas fa-info fa-stack-1x" ></i>
        </span>
      </div>
      <div class="content-container">
        <div class="date">{{ date('F j, Y', strtotime($announcement->created_at)) }}</div>
        <div class="title">{{ ucwords($announcement->title) }}</div>
        <div class="content w-100">
          {!! $announcement->description !!}
        </div>
      </div>
    </div>
  </div>
@endif
@if (@$boards_date)
  @php
      $boards = Carbon::parse($boards_date);
      $now = Carbon::now();
  @endphp
  @if ($now->lt($boards))    
    <div class="alert alert-primary custom-alert fade show" role="alert">
      <div class="alert-container">
        <div class="icon-container">
          <span class="fa-stack fa-lg border-warning">
            <i class="fas fa-circle fa-stack-2x fa-inverse"></i>
            <i class="fas fa-calendar-alt fa-stack-1x" ></i>
          </span>
        </div>
        <div class="content-container">
          <div class="title">{{ $boards->diffInDays($now) }} days before the boards</div>
        </div>
      </div>
    </div>
  @endif
@endif
<header class="flex-center">
  <h1>{{ $title }}</h1>
</header>
@endsection

@section('content')
<div class="row">
  @if (count(@$data) > 0)
    @foreach ($filtered_subjects as $subject_name => $lectures)

        <div class="col-sm-12">
          <h4>{{ $subject_name }}</h4>
          <div class="caboodle-card p-0">
            <div class="caboodle-card-body p-0">
              <table class="caboodle-table caboodle-table-small">
                <thead>
                    <th class="caboodle-table-col-header hide" >Lecture</th>
                    <th class="caboodle-table-col-header hide d-none d-md-table-cell" >Opens On</th>
                    <th class="caboodle-table-col-header hide d-none d-md-table-cell" >Closes On</th>
                    <th class="caboodle-table-col-header hide d-table-cell d-md-none" >Date Available</th>
                    <th class="caboodle-table-col-header hide text-center" >Status</th>
                </thead>
                <tbody>
                    @foreach ($lectures as $subject)
                    @foreach ($data as $d)
                        @if ($d->lecture_id == $subject->taggable_id)    
                            <tr class="clickable-row" style="cursor: pointer;" data-url="{{ route('portalLecturesShow', $d->id) }}" >
                                <td ><strong>{{ ucwords($d->lecture->name) }}</strong></td>
                                
                                {{-- Desktop View --}}
                                <td class="d-none d-md-table-cell sub-text-1" >{{ date('F j, Y h:i A', strtotime($d->start_date)) }}</td>
                                <td class="d-none d-md-table-cell sub-text-1" >{{ date('F j, Y h:i A', strtotime($d->end_date)) }}</td>
                                
                                {{-- Mobile View --}}
                                <td class="d-table-cell d-md-none" >{{ date(' F j, Y') }} <br> {{ date('h:i A', strtotime($d->start_date)) }} - {{ date('h:i A', strtotime($d->start_date)) }}</td>

                                <td class="text-center" >
                                    @if (Carbon::now() >= $d->start_date && Carbon::now() <= $d->end_date)
                                        <span class="badge badge-success">Open</span>
                                    @elseif(Carbon::now() <= $d->start_date)
                                        <span class="badge badge-secondary">Soon</span>
                                    @else
                                        <span class="badge badge-danger">Closed</span>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    @endforeach
                </tbody>
            </table>
            </div>
          </div>
        </div>
    @endforeach
  @else
      <div class="col-sm-12">
        <div class="caboodle-card">
          <div class="caboodle-card-body">
            <div class="empty text-center">
                No lectures available
            </div>
          </div>
        </div>
      </div>
  @endif
</div>
@stop

@section('added-scripts')
  @if (session()->has('logged_in'))
    <script>
      $(document).ready(function(){
        $('#legal-modal').modal('show');
      });
      $('#legal-modal').find('[data-toggle=collapse]').on('click', function(e){
        if($(this).find('.collapse-icon').hasClass('fa-chevron-down')){
          $(this).find('.collapse-icon').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }
        else{
          $(this).find('.collapse-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        }
      });
    </script>
  @endif
@endsection

@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalLectures')}}">Lectures</a></li>
    <li class="breadcrumb-item far active"><span>{{ $title }}</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ url()->previous() }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        @if ((strtotime(@$data->start_date) <= time()) && (strtotime(@$data->end_date) >= time()))
            <div class="col-sm-12 mb-2 video-container">
                <div class="name-overlay"><span>{{ $user->name }}</span></div>
                {!! $data->lecture->video->vimeo['embed']['html'] !!}
            </div>
            <div class="col-sm-12">
                <div class="caboodle-card">
                    <div class="caboodle-card-body">
                        <h4 class="card-title">Description</h4>
                        <div class="w-100">
                            {!! $data->lecture->description !!}
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="col-sm-12">
                <?php if(Carbon::now() < $data->start_date): ?>
                    <h4>This lecture will open soon.</h4>
                <?php else : ?>
                    <h4>This lecture is closed.</h4>
                <?php endif ?>
            </div>
        @endif
    </div>
@endsection

@section('added-scripts')
    <script>

        /* function changeHandler(e) {
            $('.video-container').toggleClass('full-screen');
        }
        document.addEventListener("fullscreenchange", changeHandler, false);
        document.addEventListener("webkitfullscreenchange", changeHandler, false);
        document.addEventListener("mozfullscreenchange", changeHandler, false);

        $('.full-screen-btn').on('click', function(e){
            e.preventDefault();
            document.querySelector('.video-container').webkitRequestFullScreen();
        }); */

        $(document).ready(function(){

            setInterval(() => {
                
                $.ajax({
                    method: 'GET',
                    url: "{{route('portalCheckVideo', $data->id)}}",
                    success: function(response){
                        if (response.status == false) {
                            location.reload();
                        }
                    }
                })

            }, 300000);

        });
    </script>
@endsection
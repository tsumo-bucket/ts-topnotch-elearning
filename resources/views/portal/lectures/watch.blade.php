@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalLectures')}}">Lectures</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalLecturesShow', $lecture->id)}}">{{ ucwords($lecture->lecture->name) }}</a></li>
    <li class="breadcrumb-item far active"><span>{{ $title }}</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{route('portalLecturesShow', $lecture->id)}}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (strtotime(@$lecture->start_date) <= time() && strtotime(@$lecture->end_date) >= time())
                {!! $data->video->vimeo['embed']['html'] !!}
            @else
                <h4>This video is no longer available for viewing</h4>
            @endif
        </div>
    </div>
@endsection

@section('added-scripts')
    <script>
        $(document).ready(function(){

            setInterval(() => {
                
                $.ajax({
                    method: 'GET',
                    url: "{{route('portalCheckVideo', $data->id)}}",
                    success: function(response){
                        if (response.status == false) {
                            location.reload();
                        }
                    }
                })

            }, 60000);

        });
    </script>
@endsection
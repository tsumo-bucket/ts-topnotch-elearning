@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Lectures</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        @if (count(@$data) > 0)
            @foreach ($filtered_subjects as $subject_name => $lectures)
                <div class="col-sm-12">
                    <h4>{{ $subject_name }}</h4>
                    <div class="caboodle-card p-0">
                        <div class="caboodle-card-body p-0">
                            <table class="caboodle-table caboodle-table-small">
                                <thead>
                                    <th class="caboodle-table-col-header" >Lecture<!--  {{ $a = 5 }} --></th>
                                    <th class="caboodle-table-col-header d-none d-md-table-cell" >Opens On</th>
                                    <th class="caboodle-table-col-header d-none d-md-table-cell" >Closes On</th>
                                    <th class="caboodle-table-col-header d-table-cell d-md-none" >Date Available</th>
                                    <th class="caboodle-table-col-header text-center" >Status</th>
                                </thead>
                                <tbody>
                                    @foreach ($lectures as $subject)
                                    @foreach ($data as $d)
                                        @if ($d->lecture_id == $subject->taggable_id)    
                                            <tr class="clickable-row" style="cursor: pointer;" data-url="{{ route('portalLecturesShow', $d->id) }}" >
                                                <td ><strong>{{ ucwords($d->lecture->name) }}</strong></td>
                                                
                                                {{-- Desktop View --}}
                                                <td class="d-none d-md-table-cell sub-text-1" >{{ date('F j, Y h:i A', strtotime($d->start_date)) }}</td>
                                                <td class="d-none d-md-table-cell sub-text-1" >{{ date('F j, Y h:i A', strtotime($d->end_date)) }}</td>
                                                
                                                {{-- Mobile View --}}
                                                <td class="d-table-cell d-md-none" >{{ date(' F j, Y') }} <br> {{ date('h:i A', strtotime($d->start_date)) }} - {{ date('h:i A', strtotime($d->start_date)) }}</td>

                                                <td class="text-center" >
                                                    @if (Carbon::now() >= $d->start_date && Carbon::now() <= $d->end_date)
                                                        <span class="badge badge-success">Open</span>
                                                    @elseif(Carbon::now() <= $d->start_date)
                                                        <span class="badge badge-secondary">Soon</span>
                                                    @else
                                                        <span class="badge badge-danger">Closed</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-sm-12">
                <div class="caboodle-card">
                    <div class="caboodle-card-body">
                        <div class="empty text-center">
                            No lectures available
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
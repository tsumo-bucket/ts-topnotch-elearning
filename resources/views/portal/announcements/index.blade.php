@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Announcements</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('portalDashboard') }}"
        >
            Back
        </a>
    </div>
</header>
@endsection

@section('content')
    {{-- Desktop View --}}
    <div class="row d-none d-sm-flex">
        <div class="col-sm-12">
            <div class="caboodle-card p-0">
                <div class="caboodle-card-body p-0">
                    @if (count(@$data) > 0)
                        <table class="caboodle-table caboodle-table-small">
                            <thead>
                                <th class="caboodle-table-col-header hide" >Title</th>
                                <th class="caboodle-table-col-header hide" >Posted On</th>
                            </thead>
                            <tbody>
                                @foreach (@$data as $d)
                                    <tr>
                                        <td width="40%" ><a href="{{ route('portalAnnouncementsShow', $d->id) }}">{{ ucwords($d->title) }}</a></td>
                                        <td>{{ date('F j, Y \a\t\ h:i A', strtotime($d->created_at)) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="empty text-center py-4">
                            No announcements available
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    {{-- Mobile View --}}
    <div class="row d-flex d-sm-none">
        @if (count(@$data) > 0)
            @foreach (@$data as $d)
                <div class="col-sm-12">
                    <div class="caboodle-card">
                        <div class="caboodle-card-body">
                            <h5 class="card-title">{{ ucwords($d->title) }} <br><small>{{ date('F j, Y \a\t\ h:i A', strtotime($d->created_at)) }}</small></h5>
                        </div>
                        <div class="caboodle-card-footer">
                            <a href="{{ route('portalAnnouncementsShow', $d->id) }}">VIEW</a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="col-sm-12">
                <div class="caboodle-card">
                    <div class="caboodle-card-body">
                        <div class="empty text-center">
                            No announcements available
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalAnnouncements')}}">Announcements</a></li>
    <li class="breadcrumb-item far active"><span>View</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>View</h1>
    <div class="header-actions">
        <a
            class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated"
            data-mdc-auto-init="MDCRipple"
            href="{{ route('portalAnnouncements') }}"
        >
            Back
        </a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4>{{ ucwords($data->title) }}</h4>
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    <div class="w-100">
                        {!! $data->description !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
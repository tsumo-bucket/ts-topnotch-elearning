@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far" ><a href="{{route('portalProfile')}}">Profile</a></li>
    <li class="breadcrumb-item far active"><span>Change Password</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalProfile') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="caboodle-card">
                <div class="caboodle-card-body">
                    {!! Form::open(['route'=>'portalPasswordUpdate','method'=>'POST', 'class'=>'form form-parsley form-edit']) !!}
                    <input type="hidden" name="user_id" value="{{ $data->id }}">
                    <div class="form-group row">
                        <label for="current_password" class="col-sm-3 col-form-label">Current Password</label>
                        <div class="col-sm-6">
                            <input required type="password" name="current_password" class="form-control" id="current_password" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="new_password" class="col-sm-3 col-form-label">New Password</label>
                        <div class="col-sm-6">
                            <input required type="password" name="new_password" class="form-control" id="new_password" value="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="confirm_password" class="col-sm-3 col-form-label">Confirm New Password</label>
                        <div class="col-sm-6">
                            <input required type="password" name="confirm_password" class="form-control" id="confirm_password" value="">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
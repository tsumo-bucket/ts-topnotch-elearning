@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Profile</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="d-block d-sm-none">Personal Information</h4>
            <div class="caboodle-card">
                <div class="caboodle-card-header d-none d-sm-block">
                    <h4 class="no-margin uppercase">Personal Information</h4>
                </div>
                <div class="caboodle-card-body">
                    <div class="form-group row">
                        <label for="full-name" class="col-sm-2 col-form-label">Student #</label>
                        <div class="col-sm-10">
                            <strong><input type="text" readonly class="form-control-plaintext" id="full-name" value="{{ $data->student_number }}"></strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="full-name" class="col-sm-2 col-form-label">Full Name</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="full-name" value="{{ $data->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="email" value="{{ $data->email }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="contact-no" class="col-sm-2 col-form-label">Contact No.</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="contact-no" value="{{ $data->contact_no ? $data->contact_no : '---' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="address" class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="address" value="{{ $data->address ? $data->address : '---' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="birthdate" class="col-sm-2 col-form-label">Birthdate</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="birthdate" value="{{ $data->birthdate != '1970-01-01' ? date('F j, Y', strtotime($data->birthdate)) : '---' }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="school" class="col-sm-2 col-form-label">School</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="school" value="{{ $data->school ? ucwords($data->school) : '---' }}">
                        </div>
                    </div>
                </div>
            </div>
            {{-- DESKTOP VIEW --}}
            <div class="caboodle-card d-none d-sm-block">
                <div class="caboodle-card-header">
                    <h4 class="no-margin">GRADES</h4>
                </div>
                <div class="caboodle-card-body">
                    @if (@$grades != null) 
                        <table class="caboodle-table">
                            <thead>
                                <th>Metric</th>
                                <th>Raw Score</th>
                                <th>Percentile Rank</th>
                                <th class="text-center" >Status</th>
                            </thead>
                            <tbody>
                                @foreach ($grades as $period => $values)
                                    <?php
                                        $color_grade = $values['color_grade'];
                                        $color = null;
                                        switch ($color_grade) {
                                            case 'RED':
                                                $color = 'red';
                                                break;

                                            case 'CRIMSON RED':
                                                $color = '#DC143C';
                                                break;
                                                
                                            case 'ORANGE':
                                                $color = '#FFA500';
                                                break;

                                            case 'YELLOW':
                                                $color = '#FFFF00';
                                                break;
                                            
                                            case 'GREEN':
                                                $color = '#008000';
                                                break;
                                            
                                            default:
                                            # code...
                                            break;
                                        }
                                    ?>
                                    <tr>
                                        <td>{{ $values['metric']->name }}</td>
                                        <td>{{ $values['raw_score'] }}</td>

                                        <!-- PERCENTILES -->
                                        @if ($values['metric']->hide_percentile)
                                            <td>N/A</td>
                                        @else
                                        <td>
                                            {{ $values['percentile_rank'] }}
                                        </td>
                                        @endif

                                        <!-- COLOR GRADES -->
                                        @if ($values['metric']->hide_color_grade)
                                            <td class="text-center">N/A</td>
                                        @else
                                        <td style="font-size: 10px;" class="text-center" >
                                            <span style="vertical-align: middle;" >{{ ucwords($color_grade) }}</span> <span class="badge badge-pill" style="background-color: {{$color}};" >&nbsp;</span>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <p class="text-center my-3">No grades available for viewing</p>
                    @endif
                </div>
            </div>
            {{-- MOBILE VIEW --}}
            <h4 class="d-block d-sm-none">Grades</h4>
            @if (@$grades != null)
                @foreach ($grades as $period => $values)
                    <?php
                        $color_grade = $values['color_grade'];
                        $color = null;
                        switch ($color_grade) {
                            case 'RED':
                                $color = 'red';
                                break;

                            case 'CRIMSON RED':
                                $color = '#DC143C';
                                break;
                                
                            case 'ORANGE':
                                $color = '#FFA500';
                                break;

                            case 'YELLOW':
                                $color = '#FFFF00';
                                break;
                            
                            case 'GREEN':
                                $color = '#008000';
                                break;
                            
                            default:
                            # code...
                            break;
                        }
                    ?>
                    <div class="caboodle-card d-block d-sm-none">
                        <div class="caboodle-card-header">
                            <h4 class="no-margin">{{ ucwords($period) }}</h4>
                        </div>
                        <div class="caboodle-card-body">
                            <div class="row mb-2">
                                <div class="col-6"><b>Raw Score:</b></div>
                                <div class="col-6 text-right">{{ $values['raw_score'] }}</div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-6"><b>Percentile:</b></div>
                                <div class="col-6 text-right">{{ $values['percentile_rank'] }}</div>
                            </div>
                            <div class="row">
                                <div class="col-6"><b>Status:</b></div>
                                <div class="col-6 text-right" style="font-size: 10px;" ><span style="vertical-align: middle;" >{{ ucwords($color_grade) }}</span> <span class="badge badge-pill" style="background-color: {{$color}};" >&nbsp;</span></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="caboodle-card d-block d-sm-none">
                    <div class="caboodle-card-body">
                        <p class="text-center my-3">No grades available for viewing</p>
                    </div>
                </div> 
            @endif
            <h4 class="d-block d-sm-none">Password</h4>
            <div class="caboodle-card">
                <div class="caboodle-card-header d-none d-sm-block">
                    <h4 class="no-margin">PASSWORD</h4>
                </div>
                <div class="caboodle-card-body">
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="password" value="&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;">
                        </div>
                    </div>
                    <a href="{{ route('portalPasswordEdit') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Change Password</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb" >
  <ol class="breadcrumb">
    <li class="breadcrumb-item far" ><a href="{{route('portalDashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item far active"><span>Schedule</span></li>
  </ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
        <a href="{{ route('portalDashboard') }}" class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated" data-mdc-auto-init="MDCRipple">Back</a>
    </div>
</header>
@endsection

@section('content')
    <div class="row">

        {{-- Desktop View --}}
        <div class="col-sm-12  d-none d-md-block">
            <div class="caboodle-card p-0">
                <table class="table table-bordered table-schedule">
                    <thead class="" >
                        @foreach ($dates as $d)
                            <th class="text-center @if($d == date('Y-m-d')) current-day @endif " ><span>{{ date('l', strtotime($d)) }} <br><small class="sub-text-1" style="font-size: 10px;" >{{ date('M j', strtotime($d)) }}</span></th>
                        @endforeach
                    </thead>
                    <tbody>
                        @foreach ($data as $index => $d)
                            @php
                                $start = (new Carbon)->parse(date('Y-m-d', strtotime($d->start_date)));
                                $end = (new Carbon)->parse(date('Y-m-d', strtotime($d->end_date)));

                                $period = (new Carbon\CarbonPeriod)->create($start, $end)->toArray();
                                
                                $period_dates = [];

                                foreach ($period as $p) {
                                    $period_dates[] = $p->format('Y-m-d');
                                }

                                $intersects = array_intersect($period_dates, $dates);
                            @endphp
                            @if (count($intersects) > 0)
                                <tr>
                                    @foreach ($dates as $date)
                                        @php
                                            $colspan = count($intersects);
                                        @endphp
                                        @if (in_array($date, $intersects))
                                            @if (reset($intersects) == $date)
                                                <td colspan="{{ $colspan }}" style="border: 0px;" >
                                                    <a href="{{ route('portalLecturesShow', $d->id) }}">
                                                        <span class="badge badge-primary"><div>&bull; {{ $d->lecture->name }}</div><strong>{{ date('h:i A', strtotime($d->start_date)).' - '.date('h:i A', strtotime($d->end_date)) }}</strong></span>
                                                    </a>
                                                </td>
                                            @endif
                                        @else
                                            <td style="border: 0px;"></td>
                                        @endif
                                        
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        {{-- Mobile View   --}}
        @foreach ($dates as $date)
            <div class="col-sm-12 d-block d-md-none">
                <div class="caboodle-card schedule-card @if($date == date('Y-m-d')) current-day @endif">
                    <div class="caboodle-card-header">
                        <h4 class="no-margin">{{ date('l', strtotime($date)) }} <br> <small style="font-size: 10px;" class="sub-text-1">{{ date('M j', strtotime($date)) }}</small></h4>
                    </div>
                    <div class="caboodle-card-body">
                        @foreach ($data as $d)
                            @php
                                $start = (new Carbon)->parse(date('Y-m-d', strtotime($d->start_date)));
                                $end = (new Carbon)->parse(date('Y-m-d', strtotime($d->end_date)));
                                $date = (new Carbon)->parse(date('Y-m-d', strtotime($date)));
                            @endphp
                            @if ($date->between($start, $end))
                                <a href="{{ route('portalLecturesShow', $d->id) }}">
                                    <span class="badge badge-primary mb-1">&bull; {{$d->lecture->name}} <br><span class="mt-1">{{ date('M j \a\t\ g:i A', strtotime($d->start_date))}} - {{ date('M j \a\t\ g:i A', strtotime($d->end_date))}}</span></span>
                                </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
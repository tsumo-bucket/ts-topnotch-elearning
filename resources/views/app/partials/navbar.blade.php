<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="{{ route('homePage') }}">Think Big</a>
    <button class="navbar-toggler" type="button" onclick="openNav()">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            @foreach(General::get_menu() as $menu)
                @if(!$menu->hide)
                    @if(count($menu->children) > 0)
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" target="{{$menu->new_tab ? '_blank':''}}" href="{{$menu->disable_link ? '#':$menu->link}}" id="dropdown-{{$menu->id}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$menu->name}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdown-{{$menu->id}}">
                                @foreach($menu->children as $child)
                                    <a class="nav-link" href="{{$child->type == 'external' ? $child->link:$child->page->slug}}">{{$child->name}}</a>
                                @endforeach
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" target="{{$menu->new_tab ? '_blank':''}}" href="{{$menu->type == 'external' ? $menu->link:$menu->page->slug}}">{{$menu->name}}</a>
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>
</nav>

<!-- changeable to left/right -->
<aside id="sidenav" class="bg-dark left">
    <button class="btn btn-transparent" onclick="closeNav()"><i class="far fa-times"></i></button>
    <ul>
        @foreach(General::get_menu() as $menu)
            @if(!$menu->hide)
                @if(count($menu->children) > 0)
                    <li class="nav-item">
						<a class="nav-link" data-toggle="collapse" href="#collapse-{{$menu->id}}">
                            {{$menu->name}}
                            <i class="far fa-chevron-down"></i>
						</a>
						<ul class="collapse-list collapse no-icon" id="collapse-{{$menu->id}}">
                            @foreach($menu->children as $child)
								<li class="nav-item">
                                    <a class="nav-link" target="{{$menu->new_tab ? '_blank':''}}" href="{{$child->type == 'external' ? $child->link:$child->page->slug}}">{{$child->name}}</a>
								</li>
							@endforeach
						</ul>
					</li>
                @else
                    <li class="nav-item active">
                        <a class="nav-link" target="{{$menu->new_tab ? '_blank':''}}" href="{{$menu->type == 'external' ? $menu->link:$menu->page->slug}}">{{$menu->name}}</a>
                    </li>
                @endif
            @endif
        @endforeach
    </ul>
</aside>
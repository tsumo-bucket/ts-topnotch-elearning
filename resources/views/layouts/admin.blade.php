<!DOCTYPE html>
<html>
	<head>
		<!-- CHANGE TITLE WITH FROM FACADE -->
		<meta name="viewport" content="width=device-width">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Topnotch @if(Auth::user()->type == 'normal') - Portal @else - Admin @endif </title>
		<meta name="description" content="Topnotch Medical Board Prep - The Best Institution to Prepare You for the Philippine Medical Board Exam">
		{!! Html::style('third_party/redactor/redactor.min.css') !!}
  		{!! Html::style('css/banner-video-modal.css') !!}
  		{!! Html::style('css/page_template.css') !!}
		{!! Html::style('css/caboodle.css') !!}

		<link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/favicons/apple-icon-57x57.png')}}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/favicons/apple-icon-60x60.png')}}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicons/apple-icon-72x72.png')}}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicons/apple-icon-76x76.png')}}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicons/apple-icon-114x114.png')}}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/favicons/apple-icon-120x120.png')}}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/favicons/apple-icon-144x144.png')}}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/favicons/apple-icon-152x152.png')}}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicons/apple-icon-180x180.png')}}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{{asset('img/favicons/android-icon-192x192.png')}}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicons/favicon-32x32.png')}}">
		<link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicons/favicon-96x96.png')}}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicons/favicon-16x16.png')}}">
		<link rel="manifest" href="{{asset('img/favicons/manifest.json')}}">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="{{asset('img/favicons/ms-icon-144x144.png')}}">
		<meta name="theme-color" content="#ffffff">
		
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<style>
			.line-clamp{
				display: -webkit-box;
				-webkit-line-clamp: 3;
				-webkit-box-orient: vertical;  
				overflow: hidden;
				text-overflow: ellipsis;
			}
		</style>
	</head>

	<body id="body">
		<nav class="navbar sticky-top flex-center bg-primary">
			<div class="navbar-left flex-center">
				<a class="navbar-brand flex-center" href="#">
					<img src=" {{asset('img/admin/topnotch-logo.png')}}" class="rounded-circle"  width="30" height="30" alt="">
          			<span class="hide-on-mobile">TOPNOTCH ONLINE</span>
				</a>
				@if (session()->has('banner'))	
					<div class="navbar-banner-text mobile">
						{!! session()->get('banner') != null ? session()->get('banner')->title : '' !!}
					</div>
				@endif
				<div class="navbar-toggle">
					<div class="navbar-toggle"> 
						<span class="navbar-button navbar-menu-toggle mdc-icon-toggle menu-btn btn-icon" role="button" aria-pressed="false"> 
							<i class="far fa-bars" aria-hidden="true"></i> 
						</span>
					</div> 
				</div>
			</div>
			@if (session()->has('banner'))	
				<div class="navbar-banner-text desktop">
					{!! session()->get('banner') != null ? session()->get('banner')->title : '' !!}
				</div>
			@endif
			<div class="navbar-right flex-center hide-on-mobile">
				<a href="{{ Auth::user()->type == 'normal' ? route('portalProfile') : route('adminProfile') }}" class="btn-icon-left navbar-button navbar-button-web -btn -btn-small -btn-transparent mdc-button" data-mdc-auto-init="MDCRipple">
					<i class="fal fa-user-alt"></i>
					Profile
				</a>
				&nbsp;
				<a href="{{ route('logout') }}" class="navbar-button mdc-icon-toggle btn-icon hide-on-mobile logout-btn" role="button" aria-pressed="false" 
					  data-toggle="tooltip" 
					  title="Sign Out"> 
					<i class="fal fa-sign-out" aria-hidden="true"></i> 
				</a>
			</div>
		</nav>

		<aside class="">
			<div class="scrollbar-macosx">
				<ul class="mdc-list menu-list">
					@if (Auth::user()->type == 'normal')
						<li class="mdc-list-item {{Menu::active('dashboard', @$menu)}}">
							<a href="{{route('portalDashboard')}}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-chart-bar"></i>
									<span>Dashboard</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('announcements', @$menu)}}">
							<a href="{{ route('portalAnnouncements') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-bell"></i>
									<span>Announcements</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('lectures', @$menu)}}">
							<a href="{{ route('portalLectures') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-window-restore"></i>
									<span>Lectures</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('exams', @$menu)}}">
							<a href="{{ route('portalExams') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-file-alt"></i>
									<span>Exams</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('schedules', @$menu)}}">
							<a href="{{ route('portalSchedule') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-calendar-alt"></i>
									<span>Schedule</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('resources', @$menu)}}">
							<a href="{{ route('portalResources') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-images"></i>
									<span>Resources</span>
								</div>
							</a>
						</li>
					@elseif(Auth::user()->type == 'super')
						<li class="mdc-list-item {{Menu::active('dashboard', @$menu)}}">
							<a href="{{route('adminDashboard')}}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-chart-bar"></i>
									<span>Dashboard</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('terms', @$menu)}}">
							<a href="{{route('adminTerms')}}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-file-alt"></i>
									<span>Terms</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('users', @$menu)}}">
							<a href="{{ route('adminUsers') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-user-circle"></i>
									<span>Users</span>
								</div>
							</a>
						</li>
						<hr class="mobile-el">
						<li permission-module="setting mdc-list-item {{Menu::active('options', @$menu)}} ">
							<span class="mdc-list-item {{Menu::active('settings', @$menu)}}" 
								data-toggle="collapse" 
								href="#settings" 
								aria-expanded="{{Menu::active('settings', @$menu, 'true')}}" 
								aria-controls="settings">
								<span class="list-item {{Menu::active('settings', @$menu)}}">
									<div class="flex-center text-icon">
										<i class="far fa-cogs"></i><span>Settings</span>
										<i class="far fa-chevron-down collapse-icon"></i>
									</div>
								</span>
							</span>
							<ul class="collapse-list collapse {{Menu::active('settings', @$menu)}} no-icon" id="settings">
								<li class="mdc-list-item {{Menu::active('options-general', @$menu)}}">
									<a href="{{route('adminOptions',['category=general'])}}" class="list-item"><i class="far fa-globe"></i> General</a>
								</li>
								<li class="mdc-list-item {{Menu::active('options-email', @$menu)}}">
									<a href="{{route('adminOptions',['category=email'])}}" class="list-item"><i class="far fa-envelope"></i> Email Settings</a>
								</li>
							</ul>
						</li>
					@else	
						<li class="mdc-list-item {{Menu::active('dashboard', @$menu)}}">
							<a href="{{route('adminDashboard')}}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-chart-bar"></i>
									<span>Dashboard</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('announcements', @$menu)}}">
							<a href="{{ route('adminAnnouncements', ['category' => 'announcements']) }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-bell"></i>
									<span>Announcements</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('banners', @$menu)}}">
							<a href="{{ route('adminBanners') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-map"></i>
									<span>Banner Pictures</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('banner-texts', @$menu)}}">
							<a href="{{ route('adminAnnouncements', ['category' => 'banners']) }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-comment-alt"></i>
									<span>Banner Texts</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('batches', @$menu)}}">
							<a href="{{ route('adminBatches') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-calendar-alt"></i>
									<span>Batches</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('exams', @$menu)}}">
							<a href="{{ route('adminExams') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-file-alt"></i>
									<span>Exams</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item {{Menu::active('lectures', @$menu)}}">
							<a href="{{ route('adminLectures') }}" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-window-restore"></i>
									<span>Lectures</span>
								</div>
							</a>
						</li>
						<li class="mdc-list-item">
							<a href="#" id="videos-modal-no-buttons" class="list-item">
								<div class="flex-center text-icon">
									<i class="far fa-images"></i>
									<span>Videos</span>
								</div>
							</a>
						</li>
					@endif
					<li class="mdc-list-item mobile-el">
            			<a href="{{ Auth::user()->type == 'normal' ? route('portalProfile') : route('adminProfile') }}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-user" ></i>
									Profile
							</div>	
						</a>
          			</li>
					<li class="mdc-list-item mobile-el">
						<a href="{{route('logout')}}" class="list-item logout-btn">
							<div class="flex-center text-icon">
								<i class="far fa-sign-out-alt"></i>
									Log-out
							</div>
						</a>
          			</li>
					<li class="mdc-list-item mobile-el hide-on-mobile" data-toggle="tooltip" data-placement="bottom" title="Menu">
						<a href="#" class="menu-btn"><i class="fa fa-bars"></i></a>
					</li>
				</ul>
			</div>
		</aside>

		<main>
			<div>
				@yield('breadcrumbs')
				@yield('header')
				@yield('content')
				@yield('footer')
			</div>
		</main>

		<div class="showbox hide" id="page-loader">
			<div class="loader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
				</svg>
			</div>
		</div>
		@include('admin.modals.assets')
		@include('admin.modals.delete')
		@include('admin.templates.asset_image')
		@include('admin.modals.video-banner')
		@include('admin.modals.videos')
		@include('admin.modals.legal')
		
		
		<!-- SUMO BROWSE TEMPLATES -->
		
		{!! Html::script('js/admin.js') !!}
		{!! Html::script('js/caboodle.js') !!}
		{!! Html::script('third_party/redactor/redactor.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/table/table.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/fontcolor/fontcolor.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/fontsize/fontsize.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/alignment/alignment.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/counter/counter.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/widget/widget.min.js') !!}

				
		@yield('added-scripts')
		@if (session()->has('errorToken'))
			<script>
				showNotifyToaster('error','','{{session()->get("errorToken")}}');
			</script>
		@endif
		@include('admin.lectures.partials.videos-modal-scripts')

		<script>

			$('.navbar-menu-toggle').on('click', function(){
				$('#body').toggleClass('hide-menu');
			});

			$(document).ready(function(e){

				$('iframe').addClass('bg-dark');
				$('.clickable-row').on('click', function(e){
					window.location = $(this).attr('data-url');
				});

				$('.datepicker').datetimepicker({
					format: 'LL',
					minDate: moment.now()
				})

				setInterval(() => {

					$.ajax({
						method: 'get',
						url: "{{route('checkAuth')}}",
						success: function(response){
							if(response.logout == true){
								window.location = "{{route('logout')}}";
							}
						}
					});

				}, 300000);

			});
		</script>

		

		@if (Auth::user()->type == 'admin')	
			<script>

				var videosModal = $('#videos-modal');

				$('#videos-modal-no-buttons').on('click', function(e){
					e.preventDefault();
					videosModal.find('.modal-footer').addClass('hide');
					videosModal.modal('show');
				});

				videosModal.on('hidden.bs.modal', function(){
					videosModal.find('.modal-footer').removeClass('hide');
				});

				$(document).on('click', '.form-delete .mdc-checkbox', function(e){
					if($('.form-delete input[data-parsley-multiple=ids]:checked').length > 0){
						$('.form-delete').find('.caboodle-table-col-header').addClass('hide');
						$('.form-delete').find('.caboodle-table-col-action').removeClass('hide');
					}
					else{
						$('.form-delete').find('.caboodle-table-col-header').removeClass('hide');
						$('.form-delete').find('.caboodle-table-col-action').addClass('hide');
					}
				});
			</script>
		@endif
	</body>
</html>

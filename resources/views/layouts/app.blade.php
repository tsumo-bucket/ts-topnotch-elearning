<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no"> <!-- Disable Safari phone number detection that transform text to link  -->

  <title>{!! Seo::title(@$seo['title']) !!}</title>
  <meta name="description" content="{!! Seo::description(@$seo['description']) !!}">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  {!! Html::style('css/app-import.css') !!}
  {!! Html::style('css/app.css') !!}

  <!-- HEADER SCRIPT -->
  {!! General::get_site_option('header-script') !!}

</head>
<!-- GA code is configured with CONFIG options -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '{!! General::get_ga_code() !!}', 'auto');
  ga('send', 'pageview');
</script> 
<body>
  @include('app.partials.navbar')
  <!-- <example-component></example-component> -->
  
  
  <div class="overlay" onclick="closeNav()"></div>
  <main id="content">
    @yield('content')
  </main>

  @include('admin.templates.asset_image')
  {!! Html::script('js/all.js') !!}
  {!! Html::script('js/app.js') !!}
  <script type="text/javascript">
    $.ajaxSetup({
        beforeSend: function(xhr,data) {
          xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        }
    });  
  </script>
  <script type="text/javascript">
    function openNav() {
        $("#sidenav").animate({
            left: '0',
        });
        $('.overlay').show();
    }

    function closeNav() {
        $("#sidenav").animate({
            left: '-300px'
        });
        $('.overlay').hide();
    }
</script>
  @if(Session::has('message'))
    <script type="text/javascript">
      swal({title: "",
            text:"Oops... {{ Session::get('message') }}",
            type: "error",
            confirmButtonText: "OK",
            closeOnConfirm: false,
            showCancelButton: false
          },
          function(){
            location.reload();
      });
    </script>
  @endif
  @yield('added-scripts')
   <!-- FOOTER SCRIPT -->
   {!! General::get_site_option('footer-script') !!}
</body>
</html>

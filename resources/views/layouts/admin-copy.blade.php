<!DOCTYPE html>
<html>
	<head>
		<!-- CHANGE TITLE WITH FROM FACADE -->
		<meta name="viewport" content="width=device-width">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Admin</title>
		{!! Html::style('third_party/redactor/redactor.min.css') !!}
  		{!! Html::style('css/banner-video-modal.css') !!}
  		{!! Html::style('css/page_template.css') !!}
		{!! Html::style('css/caboodle.css') !!}
		<!-- {!! Html::style('css/admin.css') !!} -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>

	<body id="body">
		<nav class="navbar sticky-top flex-center bg-primary">
			<div class="navbar-left flex-center">
				<a class="navbar-brand flex-center" href="#">
					<img src=" {{asset('img/admin/default.png')}} " width="30" height="30" alt="">
          			<span>THINKSUMO</span>
				</a>
				<div class="navbar-toggle">
					<div class="navbar-toggle"> 
						<span class="navbar-button navbar-menu-toggle mdc-icon-toggle menu-btn btn-icon" role="button" aria-pressed="false"> 
							<i class="far fa-bars" aria-hidden="true"></i> 
						</span>
					</div> 
				</div>
			</div>
			<div class="navbar-right flex-center hide-on-mobile">
				<a href="{{ route('adminProfile') }}" class="btn-icon-left navbar-button navbar-button-web -btn -btn-small -btn-transparent mdc-button" data-mdc-auto-init="MDCRipple">
					<i class="fal fa-user-alt"></i>
					Profile
				</a>
				&nbsp;
				<a href="{{ route('adminLogout') }}" class="navbar-button mdc-icon-toggle btn-icon hide-on-mobile" role="button" aria-pressed="false" 
					  data-toggle="tooltip" 
					  title="Sign Out"> 
					<i class="fal fa-sign-out" aria-hidden="true"></i> 
				</a>
			</div>
		</nav>

		<aside class="">
			<div class="scrollbar-macosx">
				<ul class="mdc-list menu-list">
					<li class="mdc-list-item {{Menu::active('dashboard', @$menu)}}">
						<a href="{{route('adminDashboard')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-chart-bar"></i>
								<span>Dashboard</span>
							</div>
            			</a>
					</li>
					<li class="mdc-list-item {{Menu::active('batches', @$menu)}}">
						<a href="{{ route('adminBatches') }}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-address-book"></i>
								<span>Batches</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('lectures', @$menu)}}">
						<a href="{{ route('adminLectures') }}" class="list-item">
							<div class="flex-center text-icon">
								<i class="fas fa-book"></i>
								<span>Lectures</span>
							</div>
						</a>
					</li>
					{{-- <li class="mdc-list-item {{Menu::active('activities', @$menu)}}">
						<a href="{{route('adminActivities')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-suitcase"></i>
								<span>Activities</span>
							</div>
						</a>
					</li>
                    <li class="mdc-list-item {{Menu::active('navbar_links', @$menu)}}">
						<a href="{{route('adminNavbarLinks')}}" class="list-item">
                            <div class="flex-center text-icon">
                                <i class="far fa-list"></i>
                                <span>Menu</span>
                            </div>
                        </a>
					</li>
					<li class="mdc-list-item {{Menu::active('banners', @$menu)}}">
						<a href="{{route('adminBanners')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-image"></i>
								<span>Banners</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('articles', @$menu)}}">
						<a href="{{route('adminArticles')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-book"></i>
								<span>Articles</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('page_categories', @$menu)}}">
						<a href="{{route('adminPageCategories')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-sitemap"></i>
								<span>Page Categories</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('pages', @$menu)}}">
						<a href="{{route('adminPages')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-sticky-note"></i>
								<span>Pages</span>
							</div>
						</a>
					</li>
					<li permission-module="page">
						<span class="mdc-list-item {{Menu::active('page', @$menu)}}" data-toggle="collapse" href="#page" aria-expanded="{{Menu::active('page', @$menu, 'true')}}"
						aria-controls="page">
							<span class="list-item {{Menu::active('page', @$menu)}}">
								<div class="flex-center text-icon">
									<i class="far fa-sticky-note"></i>
									<span>Pages</span>
									<i class="far fa-chevron-down collapse-icon"></i>
								</div>
							</span>
						</span>
						<ul class="collapse-list collapse {{Menu::active('page', @$menu)}} no-icon" id="page">
							@foreach(General::get_store_pages() as $key => $page)
								<li class="mdc-list-item {{Menu::active('page-' . $page->slug, @$menu)}}">
									<a href="{{ route('adminClientPage', $page->slug) }}" class="list-item">
										<div class="flex-center text-icon">{{ $page->name }}</div>
									</a>
								</li>
							@endforeach
						</ul>
					</li>
					<li class="mdc-list-item {{Menu::active('users', @$menu)}}">
						<a href="{{ route('adminUsers') }}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-user-circle"></i>
								<span>Users</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('user_roles', @$menu)}}">
						<a href="{{ route('adminUserRoles') }}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-id-card"></i>
								<span>User Roles</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('user_permissions', @$menu)}}">
						<a href="{{route('adminUserPermissions',[0])}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-cog"></i>
								<span>Functions</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item {{Menu::active('contact_analytics', @$menu)}}">
						<a href="{{route('adminContacts')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="far fa-address-book"></i>
								<span>Contact Analytics</span>
							</div>
						</a>
					</li>
					<li class="mdc-list-item">
						<a href="#" data-toggle="modal" data-target="#assets-modal" class="asset-sidebar-link list-item">
							<div class="flex-center text-icon">
								<i class="far fa-folder"></i>
								<span>Assets</span>
							</div>
						</a>
					</li> --}}
					<hr class="mobile-el">
					<li class="mdc-list-item mobile-el" data-toggle="tooltip" data-placement="bottom" title="Profile">
            			<a href="{{route('adminProfile')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="fa fa-user" ></i>
									Profile
							</div>	
						</a>
          			</li>
					<li class="mdc-list-item mobile-el" data-toggle="tooltip" data-placement="bottom" title="Logout">
						<a href="{{route('adminLogout')}}" class="list-item">
							<div class="flex-center text-icon">
								<i class="fa fa-sign-out-alt"></i>
									Log-out
							</div>
						</a>
          			</li>
					<li class="mdc-list-item mobile-el hide-on-mobile" data-toggle="tooltip" data-placement="bottom" title="Menu">
						<a href="#" class="menu-btn"><i class="fa fa-bars"></i></a>
					</li>
					<li permission-module="setting mdc-list-item {{Menu::active('options', @$menu)}} ">
						<span class="mdc-list-item {{Menu::active('settings', @$menu)}}" 
							data-toggle="collapse" 
							href="#settings" 
							aria-expanded="{{Menu::active('settings', @$menu, 'true')}}" 
							aria-controls="settings">
							<span class="list-item {{Menu::active('settings', @$menu)}}">
								<div class="flex-center text-icon">
									<i class="far fa-cogs"></i><span>Settings</span>
									<i class="far fa-chevron-down collapse-icon"></i>
								</div>
							</span>
						</span>
						<ul class="collapse-list collapse {{Menu::active('settings', @$menu)}} no-icon" id="settings">
							<li class="mdc-list-item {{Menu::active('options-general', @$menu)}}">
								<a href="{{route('adminOptions',['category=general'])}}" class="list-item"><i class="far fa-globe"></i> General</a>
							</li>
							<li class="mdc-list-item {{Menu::active('options-email', @$menu)}}">
								<a href="{{route('adminOptions',['category=email'])}}" class="list-item"><i class="far fa-envelope"></i> Email Settings</a>
							</li>
    						</ul>
					</li>
					
					{{-- <li permission-module="setting mdc-list-item ">
						<span class="mdc-list-item {{Menu::active('dev', @$menu)}}" 
							data-toggle="collapse" 
							href="#dev" 
							aria-expanded="{{Menu::active('dev', @$menu, 'true')}}" 
							aria-controls="dev">
							<span class="list-item {{Menu::active('dev', @$menu)}}">
								<div class="flex-center text-icon">
									<i class="far fa-code"></i><span>Dev Modules</span>
									<i class="far fa-chevron-down collapse-icon"></i>
								</div>
							</span>
						</span>
						<ul class="collapse-list collapse {{Menu::active('dev', @$menu)}}  no-icon" id="dev">
							<li class="mdc-list-item {{Menu::active('dev-pages', @$menu)}}">
								<a href="{{route('adminPages')}}" class="list-item"> Pages</a>
							</li>
						</ul>
					</li> --}}
				</ul>
			</div>
			<div class="caboodle-brand">
				Powered by <a href="https://sumofy.me/"><img src="{{ asset('img/caboodle/thinksumo.png') }}" alt="Think Sumo"></a>
			</div>
		</aside>

		<main>
			<div>
				@yield('breadcrumbs')
				@yield('header')
				@yield('content')
				@yield('footer')
			</div>
		</main>

		<div class="showbox hide" id="page-loader">
			<div class="loader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
				</svg>
			</div>
		</div>
		@include('admin.modals.assets')
		@include('admin.modals.delete')
		@include('admin.templates.asset_image')
  		@include('admin.modals.video-banner')
		
		
		<!-- SUMO BROWSE TEMPLATES -->
		
		{!! Html::script('js/admin.js') !!}
		{!! Html::script('js/caboodle.js') !!}
		{!! Html::script('third_party/redactor/redactor.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/table/table.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/fontcolor/fontcolor.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/fontsize/fontsize.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/alignment/alignment.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/counter/counter.min.js') !!}
		{!! Html::script('third_party/redactor/_plugins/widget/widget.min.js') !!}
				
		@yield('added-scripts')
		<script>
			/* $(document).ready(function(){
				setInterval(() => {
					$.ajax({
						method: 'GET',
						url: "{{route('checkAuth')}}",
						success: function(authenticated){
							if(!authenticated){
								location.reload();
							}
						}
					});
				}, 3000);
			}); */
		</script>
	</body>
</html>

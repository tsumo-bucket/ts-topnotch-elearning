<!DOCTYPE html>
<html>
<head>
  <title>{{ $title }}</title>
  <meta name="viewport" content="width=device-width">
  <meta name="description" content="Topnotch Medical Board Prep - The Best Institution to Prepare You for the Philippine Medical Board Exam">
  {!! Html::style('css/admin-import.css') !!}
  {!! Html::style('css/admin.css') !!}

  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/favicons/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/favicons/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicons/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicons/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicons/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/favicons/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/favicons/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/favicons/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicons/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('img/favicons/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicons/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicons/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicons/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('img/favicons/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="{{asset('img/favicons/ms-icon-144x144.png')}}">
  <meta name="theme-color" content="#ffffff">
</head>
<body>
  <div id="login">
    @if (count(@$banners) > 0)
      <div class="banner-container">
        @foreach (@$banners as $banner)
            <div class="banner" style="background-image: url({{env('S3_ACTIVE') ? @$banner->asset->path : asset(@$banner->asset->path) }})" ></div>
        @endforeach
      </div>
    @endif
    <div class="container">
      <div class="row login-container">
        <div class="col-lg-4 offset-lg-0 col-sm-6 offset-sm-3 col-12">
          @yield('content')
        </div>
        <div class="d-none d-lg-block col-md-8">
          @include('admin.auth.partials.details')
        </div>
      </div>
    </div>
  </div>
  {!! Html::script('js/admin-auth.js') !!}
  @yield('added-scripts')
  @if (count(@$banners) > 0)
    @if (count(@$banners) > 1)
      <script>
        $(document).ready(function(){
          $('.banner-details-container').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            autoplay: false,
            fade: true,
            draggable: false
          });
          $('.banner-container').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            autoplay: true,
            fade: true,
            autoplaySpeed: 5000,
            draggable: false
          })
          .on('afterChange', function(slick, currentSlide){
            console.log(currentSlide);
            $('.banner-details-container').slick('slickGoTo', currentSlide.currentSlide)
          });
        });
      </script>
    @endif
  @endif
</body>
</html>
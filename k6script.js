import http from 'k6/http';
import { check, fail } from "k6";
import { sleep } from 'k6';

export default function() {
  let res = http.get('http://homestudy.topnotchboardprep.com.ph/test/pdf/download');
  // let res = http.get('https://topnotchonline-prod.eba-dre3q2wt.ap-southeast-1.elasticbeanstalk.com/test/pdf/download');
  if (!check(res, {
    "status code MUST be 200": (res) => res.status == 200,
  })) {
      console.log(JSON.stringify(res));
      fail("status code was *not* 200");
  }
  // sleep(1);
}